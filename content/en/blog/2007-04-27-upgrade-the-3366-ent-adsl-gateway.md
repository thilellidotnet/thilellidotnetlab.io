---
date: "2007-04-27T20:39:27Z"
tags:
- Network
title: Upgrade the 3366-ENT ADSL Gateway
---

Decided to upgrade the Cayman 3366-ENT ADSL Gateway from
[Netopia](http://www.netopia.com/equipment/products/3000/3300_bus.html "3300 Business Series Routers"),
currently hosting the public link for the Thilelli.NET Project. All went
smooth and well. The gateway is now at the latest firmware level
available (8.5.0 at the time of this writing).

At the same time, try to avoid some recurrent problem on the link (seems
ok but doesn\'t let real traffic to pass through it) helped by the
24-Hour Scheduled Connection
[procedure](http://www.netopia.com/support/technotes/hardware/NQG_020.html "24-Hour Scheduled Connection").
Waiting to see if it really works for our site\... thanks to our
[ISP](http://www.nerim.net/ "Nerim") for providing a nice administration
and status [page](https://admin.nerim.net/ "Administration Center").
