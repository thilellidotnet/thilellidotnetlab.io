---
date: "2008-12-01T20:20:36Z"
tags:
- GRUB
- SVM
- Boot
- Failsafe
title: GRUB Boot Archive With SVM, A Better Approach
---

In a [previous
discussion](/post/2008/03/13/Update-A-Corrupted-GRUB-Boot-Archive-With-SVM)
about the GRUB boot archive and how it can be regenerated in Failsafe
mode, I mentioned that it will not be as easy as it can be when the root
file system use the `md` driver. I previously show a method to do this
which necessitate to unmirror one or more file systems when the root
file system is build upon a SVM mirror. This was not very optimal since
a lot of of manipulations are involved, which may lead to human
error(s), and may seems to be a little complicated.

This method was build on [Performing System
Recovery](http://docs.sun.com/app/docs/doc/816-4520/6manpieqm?l=en&a=view)
from the Solaris Volume Manager official documentation, which show up
last month on the
[Sun-Managers](http://www.sunmanagers.org/mailman/listinfo/sunmanagers)
mailing list.

*Note: Although this test case was done using [Solaris 10
10/08](http://www.sun.com/software/solaris/get.jsp) under a virtual
machine build upon
[VirtualBox](http://www.sun.com/software/products/virtualbox/get.jsp) on
latest [OpenSolaris release](http://www.opensolaris.com/get/index.jsp),
the instructions must be valid for Solaris 10 1/06 and later.*

### Initial setup

As we saw before, the system use only a root file system, and a swap
device. Both are encapsulated with SVM:

    # df -k -F ufs
    Filesystem     kbytes    used   avail capacity  Mounted on
    /dev/md/dsk/d0 6147798 3455578 2630743      57%  /
    # swap -l
    swapfile             dev  swaplo blocks   free
    /dev/md/dsk/d1      85,1       8 4194288 4194288
    # metastat -c d0 d1
    d0               m  6.0GB d10 d20
        d10          s  6.0GB c0d0s0
        d20          s  6.0GB c1d1s0
    d1               m  2.0GB d11 d21
        d11          s  2.0GB c0d0s1
        d21          s  2.0GB c1d1s1

### Regenerate the GRUB boot archive

The idea is to boot on the GRUB Failsafe mode, get the `md`
configuration from local root file system, and load manually the `md`
module, hence properly configured. The main advantage is to be fully
self hosted from the Failsafe mode, and not have to manipulate SVM more
than necessary, especially when breaking the mirror, loosing redundancy
for a time.

    [...]
    Booting to milestone "milestone/single-user:default".
    Configuring devices.
    Searching for installed OS instances...
    /dev/dsk/c0d0s0 is under md control, skipping.
    /dev/dsk/c1d1s0 is under md control, skipping.
    No installed OS instance found.

    Starting shell.
    # mount -F ufs -o ro /dev/dsk/c0d0s0 /a
    # cp -p /a/kernel/drv/md.conf /kernel/drv
    # umount /a
    # update_drv -f md
    devfsadm: mkdir failed for /dev 0x1ed: Read-only file system
    # metainit -r
    # metasync d0
    # fsck /dev/md/rdsk/d0
    # mount -F ufs /dev/md/dsk/d0 /a
    # bootadm update-archive -R /a
    # umount /a
    # reboot

Really interesting!
