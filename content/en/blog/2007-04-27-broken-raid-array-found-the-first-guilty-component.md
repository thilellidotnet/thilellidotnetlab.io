---
date: "2007-04-27T21:06:46Z"
tags:
- RAID
title: Broken RAID Array... Found the First Guilty Component
---

Last week, the mirror array for the system broke one more time. After
some investigation, It turns out to be a faulty
[Serillel](http://www.abit-usa.com/technology/serillel.php "Serillel ATA 150")
adapter. Luckily i had one more of this in the stock\... just in case
(what a good idea, isn\'t it?).

On the other hand, the mirror array for the data (home directories)
broke itself during the overall restore procedure. Still in this
degraded mode for now, need more time to work on this\... quickly :(

    # grep DEGRADED /var/log/messages | tail -1
    Jul 17 17:54:24 bento kernel: ar1: 117246MB <ATA RAID1 array> [14946/255/63] status: DEGRADED subdisks:

    # atacontrol status ar1
    ar1: ATA RAID1 subdisks: ad8 status: DEGRADED

**Don\'t forget to make some good backups! There is *no good* reason not
to do so.** It is just a matter of time to use them.
