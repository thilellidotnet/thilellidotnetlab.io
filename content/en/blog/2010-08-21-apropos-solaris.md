---
date: "2010-08-21T12:38:24Z"
tags:
- ZFS
- JumpStart
- Oracle
- Zone
title: Apropos Solaris
---

John Fowler (Oracle Executive Vice President for Server and Storage
Systems) held an on-line webcast on August 10 on the strategy for
hardware servers based on SPARC and x86, and the formalization of the
upcoming release of Solaris 11 in 2011.

This post is only aimed at summarize the main points, the complete
slides of the presentation are available at the [Oracle web
site](http://www.oracle.com/dm/offers/fy11/oracle_systems_strategy_update_fowler.pdf).

1.  *Message \#1:* SPARC is alive and will continue. Solaris is alive
    and will continue. Both actively.
2.  *Message \#2:* What is interesting here is that this is not only
    intentions, it is a real roadmap up to five years, on the ex-Sun
    well-known products. Oracle clearly has some strong plans about
    Solaris, SPARC ad x86 platforms, and just began to speak publicly
    about them. We will see probably more about them all at the Oracle
    OpenWorld in few weeks now.

The points are:

-   A roadmap for SPARC and Solaris up to 2015.
-   SPARC will double performance improvement every two years:
    -   Cores: 128 (32 in 2010).
    -   Threads: 16384 (512 in 2010).
    -   Memory capacity: 64TB (4TB in 2010).
    -   Logical Domains: 256 (128 in 2010).
    -   Java Ops per second: 50000 (5000 in 2010).
-   Very SPARC oriented: it seems that there will only be one SPARC
    brand at the end of 2015.
-   Two big families of SPARC servers: lots of threads known as the
    T-Series, lots of sockets known as M-Series.
-   A least one Update to Solaris 10 around 2010Q3, a beta program of
    Solaris 11 known as Solaris 11 Express due to last 2010, then
    Solaris 11 due in 2011 and up to 2015.

Solaris 11 will be based on the now close OpenSolaris distribution,
which will include:

-   Image Packaging System (IPS): totally new packaging system fully
    integrated with ZFS and Boot Environment Administration (aimed at
    replacing Live Upgrade).
-   Crossbow network virtualization stack.
-   ZFS de-duplication, and lots of recent optimizations and
    functionalities.
-   CIFS file services : in-kernel implementation of CIFS.
-   Enhanced Gnome user environment.
-   Updated installer and auto network installer (\"AI\", aimed at
    replacing JumpStart)
-   Network Automagic configuration.
-   And many more (I heard Solaris 10 BrandZ\...).
