---
date: "2007-04-27T17:54:41Z"
tags:
- LVM
- RAID
- Crash
title: Stability Problem and New RAID-1 System
---

Just after the big update and new infrastructure installation during the
[past
month](/post/2006/03/25/New-Infrastructure-Server-for-the-Hosting-Project),
the server encountered stability problem on a regular basis (sometimes
crashing more than one time a day). At first though, we noted it may be
caused by the new processor architecture, a less mature FreeBSD
distribution than the well known i386.

But the problem appears to be specifically related to I/O, especially
when the input/output are very intensive, i.e. file system `dump(8)`,
file system snapshot `mksnap_ffs(8)` or big `tar(1)` or `cpio(1L)`
archive transfers.

After days, we didn\'t succeed to clearly isolate the source of the
problem, but decided to quickly put the system under RAID-1 system
management (disks mirroring) and configure the excellent `gmirror(8)` to
do the job. So, i switched from an old hardware RAID mechanism, to a
pure LVM one. Hope we will be able to find something new without
disturbing the ThNET services anymore.

More on the involved manipulations to put the system under the
`gmirror(8)`control on a later post. Stay tuned.
