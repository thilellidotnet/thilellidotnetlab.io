---
date: "2007-04-22T17:36:09Z"
tags:
- memory
title: 'Memory Behaviour: Tuning Linux''s Kernel Overcommit'
---

After encounter some problem at work running large Oracle databases on a
RHEL4 system, we need to prevent kernel overcommit from exceeding a
certain threshold. In fact, the problem was that the system begin to
kill processes under *heavy* load: in our case `ssh` connections\...
Ouch!

The solution was to alter the default behavior of the Linux kernel in
this area, as mentioned in `Documentation/vm/overcommit-accounting` in
the corresponding source code tarball.

Interestingly, an overall and great explanation of this mechanism is
available today on the O\'Reilly LinuxDevCenter.com. It is worth
[reading
it](http://www.linuxdevcenter.com/pub/a/linux/2006/11/30/linux-out-of-memory.html "When Linux Runs Out of Memory"),
i think.
