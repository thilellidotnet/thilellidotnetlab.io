---
date: "2007-04-28T10:04:00Z"
tags:
- Bug
- Patch
title: How to Patch an AIX 5L System
---

On a fresh AIX 5.3ML1 (the corresponding output of `oslevel -r` is
5300-01), a bug was encountered when trying to change the NIS password
of a remote user with `yppasswd(1)`: the command systematically dumped
core.

Opening a soft call at the IBM support center leads to patch the system
with the APAR IY66747 (memory fault in chpass\_netgroup). The fix
package contains 19 filesets for a total package size of 54,464,512
bytes. After downloading it from IBM Fix Central (which can be found at
`http://www-912.ibm.com/eserver/support/fixes/`), all the `.bff` were
stored in a temporary location `/files/tmpcdinst/IY66747` which was NFS
mounted in order to reduce needed space on each concerned host.

*The System Management Interface Tool (SMIT) recognizes
`/usr/sys/inst.images` as the default directory for fixes downloaded
onto hard disk. This was not the case here!*

Here are the steps to install the fix package:

    # cd /files/tmpcdinst/IY66747
    # inutoc
    # TERM=vt220 smitty update_all
    /*
     * INPUT device / directory for software               [.]
     *  SOFTWARE to update                                  _update_all
     *  PREVIEW only? (update operation will NOT occur)     no
     *  COMMIT software updates?                            no
     *  SAVE replaced files?                                yes
     *  AUTOMATICALLY install requisite software?           yes
     *  EXTEND file systems if space needed?                yes
     *  VERIFY install and check file sizes?                no
     *  DETAILED output?                                    no
     *  Process multiple volumes?                           yes
     *  ACCEPT new license agreements?                      yes
     *  PREVIEW new LICENSE agreements?                     no
     */

Verify that the fix package applied correctly:

    # instfix -ik IY66747
        All filesets for IY66747 were found.

Then, here is the correct behavior of the freshly patched system:

    # yppasswd jpeg
    Old NIS password:
    jpeg's New password:
    Re-enter jpeg's new password:
    NIS passwd changed on neptune

All seems ok!

**Side note:** Since `smitty update_all` need to issue the `bosboot`
command at the end of the update, a reboot is mostly recommended!
