---
date: "2007-04-27T21:03:01Z"
tags:
- Drupal
title: '[headsup] Thilelli.NET Website Updated and Fully Switched to Drupal'
---

After weeks of testings, finally switched the website of the
Thilelli.NET project to the
[Drupal](http://www.drupal.org/ "Open source content management platform")
content management system yesterday since it gave very good satisfaction
during that time.
[SPIP](http://www.spip.net/ "Système de Publication pour l'Internet")
was notably good, but the overall filling of Drupal smelt better to me,
especially a little more *polished*.
