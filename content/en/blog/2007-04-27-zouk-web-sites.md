---
date: "2007-04-27T17:31:30Z"
tags:
- music
title: Zouk Web Sites
---

Since a friend of mine told me recently to give him my very favorite
links about zouk music on the Net, here it is. In fact, i currently post
this one listening a web radio station using
[RealPlayer](http://www.real.com/ "Introducing RealPlayer") plugin
running on top of
[OpenSolaris](http://www.opensolaris.org/ "OpenSolaris Project") on my
personal [Sun Ultra 20
Workstation](http://sunsolve.sun.com/handbook_pub/Systems/Ultra20/Ultra20.html "Sun Ultra[tm] 20 Workstation")
;)

1.  [ZoukStation.com](http://www.zoukstation.com/ "ZoukStation.com")
    \[fr\]
2.  [Zouker.com](http://www.zouker.com/ "Zouker.com") \[fr\]
3.  [ZoukOnWeb](http://www.zoukonweb.com/ "ZOW") \[fr\]
4.  [Sol2Zouk](http://www.sol2zouk.com/ "Sol2Zouk") \[fr\]

Don\'t hesitate to email me if you have some good web sites too; i
promise you to update this entry, if any!
