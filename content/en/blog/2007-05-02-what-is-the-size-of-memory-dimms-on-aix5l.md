---
date: "2007-05-02T18:49:30Z"
tags:
- memory
title: What is the Size of Memory DIMMs on AIX5L
---

Well, here is a quick one-liner to get the available memory on an AIX
system, with an detailed listing of each size of each memory DIMM on
currently installed banks (provided in MB):

    # lscfg -vp | egrep 'Memory DIMM|Size'
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
         Memory DIMM:
           Size........................2048
