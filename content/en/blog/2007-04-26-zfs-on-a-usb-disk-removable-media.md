---
date: "2007-04-26T20:46:42Z"
tags:
- ZFS
- USB
title: ZFS on a USB Disk (removable Media)
---

With a relatively recent versions of Solaris or OpenSolaris (say Nevada
build 36, Solaris Express 4/06 and Solaris 10 6/06 release), here is how
it is possible to use
[ZFS](http://opensolaris.org/os/community/zfs/ "Zettabyte File System")
as the backing file system for such a removable device, easily.

The first step is certainly to disable the `vold(1M)` SMF service, for
the operating system not trying to mount it automatically each time this
device will be plugged in:

    # svcadm disable volfs

Knowing that Solaris USB driver presents any USB storage device as
removable media, it can be seen using both the `format` command in
*expert mode*, and the `rmformat` program:

    # format -e < /dev/null
    Searching for disks...done

    AVAILABLE DISK SELECTIONS:
           0. c1d0 <DEFAULT cyl 9720 alt 2 hd 255 sec 63>
              /pci@0,0/pci-ide@7/ide@0/cmdk@0,0
           1. c2d0 <ST325082-         4ND0XKT-0001-232.89GB>
              /pci@0,0/pci-ide@7/ide@1/cmdk@0,0
           2. c3t0d0 <ST940211-5A-0000-37.26GB>
              /pci@0,0/pci108e,5347@2,1/storage@3/disk@0,0
    Specify disk (enter its number):
    #
    # rmformat
    Looking for devices...
         1. Logical Node: /dev/rdsk/c0t0d0p0
            Physical Node: /pci@0,0/pci-ide@6/ide@0/sd@0,0
            Connected Device: LITE-ON  DVD SOHD-16P9S   F3S2
            Device Type: DVD Reader
            Bus: IDE
            Size: <Unknown>
            Label: <Unknown>
            Access permissions: <Unknown>
         2. Logical Node: /dev/rdsk/c3t0d0p0
            Physical Node: /pci@0,0/pci108e,5347@2,1/storage@3/disk@0,0
            Connected Device: ST940211 5A               0000
            Device Type: Removable
            Bus: USB
            Size: 38.2 GB
            Label: <Unknown>
            Access permissions: Medium is not write protected.

Now that the device name is clearly identified, it possible to create a
specialized `pool`:

    # zpool create rmzp c3t0d0
    # zpool list rmzp
    NAME                    SIZE    USED   AVAIL    CAP  HEALTH     ALTROOT
    rmzp                     37G   28.6M   37.0G     0%  ONLINE     -

Since the purpose of this disk is, among other things, to be a backup of
my home directory, here is how to do so.

First, create the correct `zfs` hierarchy on the USB disk:

    # zfs create rmzp/home
    # zfs set compression=on rmzp/home

Take a snapshot of current home directory, send and restore it on the
fly to the new `pool`:

    # zfs snapshot datazp/home/jgabel@rmzp.0
    # zfs send datazp/home/jgabel@rmzp.0 | zfs receive rmzp/home/jgabel
    # zfs destroy rmzp/home/jgabel@rmzp.0
    # zfs list -r rmzp
    NAME                   USED  AVAIL  REFER  MOUNTPOINT
    rmzp                  28.6M  36.4G  25.5K  /rmzp
    rmzp/home             28.5M  36.4G  26.5K  /rmzp/home
    rmzp/home/jgabel      28.5M  36.4G  28.5M  /rmzp/home/jgabel

Then, when you are ready to take it away, just export the `pool` as with
a classical disk:

    # zpool export rmzp
    # zpool list rmzp
    cannot open 'rmzp': no such pool
    # zpool import
      pool: rmzp
        id: 1670601809438763813
     state: ONLINE
    action: The pool can be imported using its name or numeric identifier.
    config:

            rmzp        ONLINE
              c3t0d0    ONLINE

Wow! What an easy and powerful way to do backups on removable media,
isn\'t it?

Since there is new ZFS porting work on the way (e.g. [ZFS on
FUSE/Linux](http://www.wizy.org/wiki/ZFS_on_FUSE "ZFS File system for FUSE/Linux")
and [ZFS on
FreeBSD](http://lists.freebsd.org/pipermail/freebsd-current/2006-August/065306.html "Porting ZFS file system to FreeBSD")),
we can expect to share this kind of devices between Unix-like OSes very
quickly.
