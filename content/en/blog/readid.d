#!/usr/sbin/dtrace -s
#pragma D option quiet

dtrace:::BEGIN
{
  printf("%s %s %s", "Sampling for", $$2, "... Please wait.");
}

syscall::*read:entry
/execname == $$1/
{
  self->thr = 1;
}

syscall::*read:return
/self->thr/
{
  @ridnum[execname,pid] = count();
  self->thr = 0;
}

profile:::tick-$2
{
  printf("%-15s %10s %10s", "PROGRAM", "PID", "COUNT");
  printa("%-15s %10d %@10d", @ridnum);
  exit(0);
}
