---
date: "2007-04-27T21:04:55Z"
tags:
- Drupal
- Password
title: Alter the password of a user in Drupal, directly via the SGBD
---

Using
[Drupal](http://www.drupal.org/ "Open source content management platform")
as the CMS of choice for time now, there is a little thing assumed by
the system at the first time the web site is launched which isn\'t
welcomed in all situation: an already working mail environment.

In fact, when creating the first Drupal user (and so with administrative
rights), the password is by default sent to the email address
provided\... even if there is no working email subsystem in order to
take delivery of the random generated password. This can render the
freshly installed CMS very\... inefficient, don\'t you think?

So, in order to be able to log in, the password for this account may be
directly reset to the SGBD (a
[MySQL](http://dev.mysql.com/ "The world's most popular open source database")
data server in the following case). Here is how this can easily be
achieved.

Obtain the checksum of the given string corresponding to the wanted
password:

    server# md5 -s padawan
    MD5 ("padawan") = 9a4b83013501fde3e8864f4f4b5fb7c5

Update the password entry for the administrative account:

    server# mysql -u dba_user -p drupal_database
    Enter password: ********
    mysql> UPDATE users SET pass = '9a4b83013501fde3e8864f4f4b5fb7c5'
        -> WHERE name = 'obiwan'\g\q
    Query OK, 0 rows affected (0.00 sec)
    Rows matched: 1  Changed: 0  Warnings: 0
    Bye

That\'s all: just log in as usual via the Drupal web portal, using
**obiwan**/*padawan* as **login**/*password*. And be prepared to build a
working mail subsystem very soon for next time\...
