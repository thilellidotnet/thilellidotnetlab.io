---
date: "2012-05-11T14:58:39Z"
tags:
- Press
title: 'Press Review #11'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a lot more:

### T4 Crypto Cheat Sheet

-   <https://blogs.oracle.com/cmt/entry/t4_crypto_cheat_sheet>

### Processors for: AIX, HP-UX, and Solaris

-   <http://netmgt.blogspot.fr/2012/05/processors-for-aix-hp-ux-and-solaris.html>

### How to Set Up a Cluster of x86 Servers with Oracle VM 3

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/o12-033-x86clusger-oraclevm-1619861.html>

Step-by-step instructions for using Oracle VM 3 to set up a cluster of
highly available Sun x86 servers.

### How to Set Up Automated Installation Services for Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/howto-autoinstall-s11-1624122.html>

How to get started creating, customizing, and configuring systems using
Automated Installer in Oracle Solaris 11.

### Independent Analyst Documents Significant ROI for Oracle Support Option

-   <http://www.oracle.com/us/dm/60155-wwmk11055824mpp015c002-se-1558971.html>

Organizations can see a 43 percent positive ROI over three years with
Oracle Premier Support for Systems, Oracle's comprehensive support
program for server and storage hardware, according to a new study
conducted by Forrester Consulting and commissioned by Oracle.

### Oracle Launches Oracle Enterprise Manager Ops Center 12c

-   <http://www.oracle.com/us/corporate/events/cloudcontrolforsystems/>

Oracle Enterprise Manager Ops Center 12c, which is now included with
Oracle Premier Support on SPARC servers, allows you to accelerate
mission-critical cloud deployment; unleash the power of Oracle Solaris
11, the first cloud operating system; and simplify Oracle engineered
systems management.

### Optimize Your Data Center with Oracle's SPARC Servers and Oracle Solaris 11 Operating System

-   <http://www.oracle.com/goto/newsletters/qtr/sparc/0512/vshow_2485.html?msgid=3-6385309861>

See how Oracle's SPARC servers and Oracle Solaris 11 operating system
can provide the highest reliability, scalability, virtualization, and
security, along with the greatest choice of applications.

### White paper: A Technical Overview of Oracle's SPARC SuperCluster T4-4

-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/supercluster-t4-4-arch-wp-1537679.pdf>

### Linux Cost Calculator

-   <http://www.bit.ly/mX94rf>
-   <http://www.oracle.com/us/media/calculator/linuxtco/linux-tco-calculator-detailed-422889.html>

Oracle Linux vs. Red Hat Enterprise Linux.

### Free 30-Day Trial of Ksplice Zero Downtime Uptime for Red Hat Enterprise Linux Customers

-   <http://www.ksplice.com/rhel-signup>

Take back your weekend and say goodbye to lengthy maintenance windows
for kernel updates. With Ksplice, you can install kernel updates while
the system is running.

### Capacity Planning and Performance Management on IBM PowerVM Virtualized Environment

-   <http://neerajbhatia.wordpress.com/2011/10/07/capacity-planning-and-performance-management-on-ibm-powervm-virtualized-environment/>

As a capacity planner, I have to be proficient in virtualization
techniques and latest happening in this space. Last year I got an
opportunity to work on IBM's power systems. Being from Solaris/Linux
background it was quite difficult in the beginning to grasp the
terminologies as they are different from Oracle VM, Solaris logical
domains. So I decided to learn it and get comfortable with it. My paper
is a result of my efforts in that direction.

### So, what makes ZFS so cool? (Part I: high level overview)

-   <https://blogs.oracle.com/orasysat/entry/so_what_makes_zfs_so>

Don\'t get me wrong, ZFS is really cool. But it isn\'t exactly new
technology, it\'s been around for a while now, the first implementations
in 2003, included in Solaris 10 since S10 Update 2 in 2006. Everyone has
heard about it being awesome, but every now and then I get the question
for details: So tell me, what really makes ZFS so cool?

### Tips for Maintaining Oracle Solaris 11 Systems with Support Repository Updates

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/tips-maintain-s11-sru-1627108.html>

The Oracle Support Repository contains a number of bug fixes and
critical security fixes that can be applied to existing Oracle Solaris
11 installations helping to ensure that systems run without a hitch in
the data center. The Support Repository is updated on a monthly basis,
and these updates are called Support Repository Updates (SRU). Unlike
Oracle Solaris Update releases, which include a wide range of new
operating system features, the Oracle Support Repository is available
only to systems under a support contract and includes a smaller set of
critical changes.

### building an appliance? physical ? virtual? production quality? use Oracle Linux

-   <https://blogs.oracle.com/wim/entry/building_an_appliance_physical_virtual>

One Enterprise Linux distribution, for all of the above. We make it easy
for you. Grab the code, binaries and source, use it, distribute it,
build your environments with it, freely, no contracts needed. Need our
help, get a support subscription. Choice, open. Virtual, physical,
cloud. Not just obfuscated tar balls. No license or activation key, good
consistent SLAs for releasing security updates, well tested,\... Run
Oracle Linux in-house in test and development environments, run it in
production environments, use it for customer systems, distribute it, any
or all of the above. One distribution that you can manage across all the
use cases. No need to manage different versions even if they\'re
similar, no need to make different distribution choices based on your
use case and pay/not pay.

### How to Deploy Oracle RAC On Zone Clusters

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/rac-zone-cluster-1631291.html>

How to create an Oracle Solaris Zone cluster, install and configure
Oracle Grid Infrastructure 11.2.0.2 and Oracle Real Application Clusters
11.2.0.2 in the zone cluster, and create an Oracle Solaris Cluster
resource for Oracle RAC.

### Translations

-   <http://www.c0t0d0s0.org/archives/7466-Translations.html>

Whenever you use some virtual memory, there has to be some mapping from
the virtual addresses to the real addresses. However to prevent the CPU
to look again and again in - from CPU cycles perspective - distant
memory areas, there are little caches in modern processors called
translation lookaside buffer or short TLB. This TLBs are rather small, a
T4 core has 128 entries. While sounding small, the TLB with such a
number of entries has an astonishing high hit rate.

### How to Upgrade to Oracle Solaris Cluster 4.0

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/upgrade-cluster4-0-1635866.html>

This multi-part article provides a step-by-step example of how a
single-instance cold-failover Oracle database can be upgraded from an
Oracle Solaris 10 and Oracle Solaris Cluster 3.3 5/11 cluster to a new
Oracle Solaris 11 and Oracle Solaris Cluster 4.0 cluster. Note: There is
no \"in place\" upgrade path (that is, no direct path using the same
hardware) to Oracle Solaris 11 and Oracle Solaris Cluster 4.0.

### Summary: What\'s new with Solaris 11 since the launch?

-   <https://blogs.oracle.com/orasysat/entry/what_s_new_with_solaris>

There was a great online forum titled: Solaris 11: What\'s new since the
launch? last week, and it has brought quite an amount of update and
information about what indeed is going on on the Solaris 11 roadmap and
how Solaris interacts and extends other products. I recommend you to
watch it (just register, or if you have registered, after providing the
registered mailaddress you can re-watch the recorded session.). For the
ones lacking the time to watch the videos, allow me to attempt a
summary.

### Announcing Oracle VM Server for SPARC 2.2 Release

-   <https://blogs.oracle.com/virtualization/entry/announcing_oracle_vm_server_for1>

The new release delivers significant enhancements that improve workload
agility and performance, maximize the availability of business-critical
applications, and increase flexibility in provisioning and deployment.
Oracle VM Server for SPARC takes advantage of the massive thread scale
offered by SPARC T-Series servers and the Oracle Solaris operating
system.

### How to Get Started Configuring Your Network in Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/s11-network-config-1632927.html>

The Oracle Solaris 11 network architecture is significantly different
from previous releases of Oracle Solaris. Not only has the
implementation changed, but so have the names of network interfaces and
the commands and methods for administering and configuring them.

These changes were introduced to bring a more consistent and integrated
experience to network administration, particularly as administrators add
more-complex configurations including link aggregation, bridging, load
balancing, or virtual networks. In addition to the traditional fixed
networking configuration, Oracle Solaris 11 introduced automatic network
configuration through network profiles.

### How to Create Multiple Internal Repositories for Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/int-s11-repositories-1632678.html>

Some customers connect directly to hosted Oracle Solaris package
repositories to get the latest fixes, but most customers set up a local
repository due to network restrictions or the desire to control which
updates their systems have access to. This article provides best
practices for managing local repositories through the complete software
lifecycle from development and testing to production deployment.

### Benefits of using Ops Center to deploy and manage Solaris 11

-   <https://blogs.oracle.com/oem/entry/benefits_of_using_ops_center>

One of the more significant new features in Oracle Enterprise Manager
Ops Center 12c is the ability to install Ops Center on Oracle Solaris
11, and to deploy and manage systems running Solaris 11. The Solaris 11
capabilities are in addition to the analogous features for Solaris 10
and Linux, which can all be handled from the same Ops Center
infrastructure.
