---
date: "2007-04-26T20:41:42Z"
tags:
- Kernel
title: Non-Debug OpenSolaris Binaries on the x64 Platform
---

Just a little news to promote the
[work](http://blogs.sun.com/tpenta/entry/non_debug_encumbered_binaries_available "non-debug encumbered binaries...")
done by Alan Hargreaves for providing non-debug encumbered binaries for
the latest ON sources at this time, for x64 platform only (yet).

As a side note, you can find a very nice how to on the (updated)
required
[steps](http://www.blastwave.org/articles/BLS-0056/index.html "How To Build NON-debug OpenSolaris")
to build a (non-debug) OpenSolaris from the Solaris Express Community
Release distribution, written by well known Dennis Clarke.

Not yet tested myself, but i hope to do so in the next days, or
weeks\...
