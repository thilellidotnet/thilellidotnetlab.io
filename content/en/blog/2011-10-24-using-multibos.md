---
date: "2011-10-24T12:44:31Z"
tags:
- Live Upgrade
- Patch
- Upgrade
title: Using multibos
---
+-----------------------------------------------------------------------------+
Setup Operation
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Logical Volumes
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
File Systems
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Mount Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
BOS Files
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Boot Partition Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Mount Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Bootlist Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Multibos Shell Operation
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Mount Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Multibos Root Shell
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Mount Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Customization Operation
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Mount Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Software Update
+-----------------------------------------------------------------------------+
#--------------------------------------------------------------------- # No
filesets on the media could be used to update the currently # installed
software. # # Either the software is already at the same level as on the media,
or # the media contains only filesets which are not currently installed.
#---------------------------------------------------------------------
+-----------------------------------------------------------------------------+
Boot Partition Processing
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Mount Processing
+-----------------------------------------------------------------------------+
/etc/julien.gabel -rw------- 1 root system 0 Jun 22 15:53 /etc/julien.gabel #
bootlist -m normal -o hdisk0 blv=hd5 hdisk0 blv=bos_hd5 hdisk0 blv=hd5 hdisk0
blv=bos_hd5 # shutdown -Fr now # lsvg -l rootvg | grep bos bos_hd5 boot 1 2 2
closed/syncd N/A bos_hd4 jfs2 4 8 2 closed/syncd /bos_inst bos_hd2 jfs2 29 58 2
closed/syncd /bos_inst/usr bos_hd9var jfs2 2 4 2 closed/syncd /bos_inst/var
bos_hd10opt jfs2 1 2 2 closed/syncd /bos_inst/opt # ls -l /etc/julien.gabel
/etc/julien.gabel not found # multibos -R Initializing multibos methods ...
Initializing log /etc/multibos/logs/op.alog ... Gathering system information
...
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Verifying operation parameters ...
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Active boot logical volume is hd5. Standby boot logical volume is bos_hd5.
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Unmounting all standby BOS file systems ...
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Removing all standby BOS file systems ... Removing standby BOS file system
/bos_inst/opt Removing standby BOS file system /bos_inst/var Removing standby
BOS file system /bos_inst/usr Removing standby BOS file system /bos_inst
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Removing all standby BOS logical volumes ... Removing standby BOS logical
volume bos_hd5
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Verifying operation parameters ... Setting bootlist to logical volume hd5 on
hdisk0. ATTENTION: firmware recovery string for active BLV (hd5): boot
/pci@80000002000000b/pci@2,6/pci1014,028C@1/scsi@1/sd@8:2 Log file is
/etc/multibos/logs/op.alog Return Status = SUCCESS

/\* \*
http://www.ibmsystemsmag.com/aix/aprilmay08/tipstechniques/20226p1.aspx
\* http://www.ibmsystemsmag.com/aix/enewsletterexclusive/21438p1.aspx \*
https://sites.google.com/site/torontoaix/aix-commands/aix-upgrades/aix-multibos-mksysb-migration
\*/

Reading this blog, you certainly have seen lots of interest for the
Oracle Solaris [Live Upgrade](/tag/live%20upgrade) (\<= Solaris 10) and
Boot Environment (\>= Solaris 11) features. These really are huge
advantage doing patching, updating, and/or upgrading the operating
system.

In the IBM AIX world, there are two major tools which can be used to
serve a similar purpose. Although I find those not as powerful nor as
feature-rich than Live Upgrade or Boot Environment on Solaris, I got a
real interest for these two products:

1.  `multibos`: Creates, updates, and manages multiple versions of the
    Base Operating System (BOS) on a rootvg.
2.  `alt_disk_copy`: Clones (makes a copy of) the currently running
    system to an alternate disk.

The first I will use in the following test case is `multibos`.

Here is an excerpt from the `multibos(1)` manual page:

> The multibos command allows the root level administrator to create
> multiple instances of AIX(R) on the same rootvg. The multibos setup
> operation creates a standby Base Operating System (BOS) that boots
> from a distinct boot logical volume (BLV). This creates two bootable
> sets of BOS on a given rootvg. The administrator can boot from either
> instance of BOS by specifying the respective BLV as an argument to the
> bootlist command or using system firmware boot operations. Two
> bootable instances of BOS can be simultaneously maintained. The
> instance of BOS associated with the booted BLV is referred to as the
> active BOS. The instance of BOS associated with the BLV that has not
> been booted is referred to as the standby BOS. Currently, only two
> instances of BOS are supported per rootvg.
>
> The multibos command allows the administrator to access, install
> maintenance and technology levels for, update, and customize the
> standby BOS either during setup or in subsequent customization
> operations. Installing maintenance and technology updates to the
> standby BOS does not change system files on the active BOS. This
> allows for concurrent update of the standby BOS, while the active BOS
> remains in production.

\# alog -of /etc/multibos/logs/op.alog
===============================================================================
DATE: 2010.06.22.14:40:58 ID: \[1 6 00C2C9D04C00\] COMMAND: (multibos
-R)
===============================================================================
Gathering system information \... multibos: 0565-077 Unable to locate
standby BOS. Log file is /etc/multibos/logs/op.alog Return Status:
FAILURE \# multibos -sX Initializing multibos methods \... Initializing
log /etc/multibos/logs/op.alog \... Gathering system information \...
Verifying operation parameters \... Creating image.data file \...
Creating standby BOS logical volume bos\_hd5 Creating standby BOS
logical volume bos\_hd4 Creating standby BOS logical volume bos\_hd2
Creating standby BOS logical volume bos\_hd9var Creating standby BOS
logical volume bos\_hd10opt Creating all standby BOS file systems \...
Creating standby BOS file system /bos\_inst on logical volume bos\_hd4
Creating standby BOS file system /bos\_inst/usr on logical volume
bos\_hd2 Creating standby BOS file system /bos\_inst/var on logical
volume bos\_hd9var Creating standby BOS file system /bos\_inst/opt on
logical volume bos\_hd10opt Mounting all standby BOS file systems \...
Mounting /bos\_inst Mounting /bos\_inst/usr Mounting /bos\_inst/var
Mounting /bos\_inst/opt Including files for file system / Including
files for file system /usr Including files for file system /var
Including files for file system /opt Copying files using backup/restore
utilities \... Percentage of files copied: 0.00% \[\...\] Percentage of
files copied: 100.00% Active boot logical volume is hd5. Standby boot
logical volume is bos\_hd5. Creating standby BOS boot image on boot
logical volume bos\_hd5 bosboot: Boot image is 43349 512 byte blocks.
Unmounting all standby BOS file systems \... Unmounting /bos\_inst/opt
Unmounting /bos\_inst/var Unmounting /bos\_inst/usr Unmounting
/bos\_inst Verifying operation parameters \... Setting bootlist to
logical volume bos\_hd5 on hdisk0. ATTENTION: firmware recovery string
for standby BLV (bos\_hd5): boot
/pci\@80000002000000b/pci\@2,6/pci1014,028C\@1/scsi\@1/sd\@8:4
ATTENTION: firmware recovery string for active BLV (hd5): boot
/pci\@80000002000000b/pci\@2,6/pci1014,028C\@1/scsi\@1/sd\@8:2 Log file
is /etc/multibos/logs/op.alog Return Status = SUCCESS \# lsvg -l rootvg
\| grep bos bos\_hd5 boot 1 2 2 closed/syncd N/A bos\_hd4 jfs2 4 8 2
closed/syncd /bos\_inst bos\_hd2 jfs2 29 58 2 closed/syncd
/bos\_inst/usr bos\_hd9var jfs2 2 4 2 closed/syncd /bos\_inst/var
bos\_hd10opt jfs2 1 2 2 closed/syncd /bos\_inst/opt \# bootlist -m
normal -o hdisk0 blv=bos\_hd5 hdisk0 blv=hd5 \# bootlist -m normal -ov
\'ibm,max-boot-devices\' = 0x5 NVRAM variable:
(boot-device=/pci\@80000002000000b/pci\@2,6/pci1014,028C\@1/scsi\@1/sd\@8:4
/pci\@80000002000000b/pci\@2,6/pci1014,028C\@1/scsi\@1/sd\@8:2) Path
name: (/pci\@80000002000000b/pci\@2,6/pci1014,028C\@1/scsi\@1/sd\@8:4)
match\_specific\_info: ut=disk/scsi/scsd hdisk0 blv=bos\_hd5 Path name:
(/pci\@80000002000000b/pci\@2,6/pci1014,028C\@1/scsi\@1/sd\@8:2)
match\_specific\_info: ut=disk/scsi/scsd hdisk0 blv=hd5 \# lslv hd5 \|
grep LAB MOUNT POINT: N/A LABEL: primary\_bootlv \# lslv bos\_hd5 \|
grep LAB MOUNT POINT: N/A LABEL: standby\_bootlv \# multibos -S
Initializing multibos methods \... Initializing log
/etc/multibos/logs/op.alog \... Gathering system information \...
Verifying operation parameters \... Mounting all standby BOS file
systems \... Mounting /bos\_inst Mounting /bos\_inst/usr Mounting
/bos\_inst/var Mounting /bos\_inst/opt Starting multibos root shell \...
Active boot logical volume is hd5. Script started, file is
/etc/multibos/logs/scriptlog.100622153605.txt MULTIBOS\> oslevel -s
MULTIBOS\> touch /etc/julien.gabel 6100-05-01-1016 MULTIBOS\> exit
Script done, file is /etc/multibos/logs/scriptlog.100622153605.txt
Stopping multibos root shell \... Compressing script log file \...
Compressed script log file is
/etc/multibos/logs/scriptlog.100622153605.txt.Z Unmounting all standby
BOS file systems \... Unmounting /bos\_inst/opt Unmounting
/bos\_inst/var Unmounting /bos\_inst/usr Unmounting /bos\_inst Log file
is /etc/multibos/logs/op.alog Return Status = SUCCESS \# multibos -Xac
-l /export/lpp\_source/lpp\_source\_6100-05-01-1016 Initializing
multibos methods \... Initializing log /etc/multibos/logs/op.alog \...
Gathering system information \... Verifying operation parameters \...
Validating install images location
/export/lpp\_source/lpp\_source\_6100-05-01-1016 Mounting all standby
BOS file systems \... Mounting /bos\_inst Mounting /bos\_inst/usr
Mounting /bos\_inst/var Mounting /bos\_inst/opt Installing software to
standby BOS \... install\_all\_updates: Initializing system parameters.
install\_all\_updates: Log file is
/var/adm/ras/install\_all\_updates.log install\_all\_updates: Checking
for updated install utilities on media. install\_all\_updates:
Processing media. install\_all\_updates: Generating list of updatable
installp filesets. install\_all\_updates: Checking for recommended
maintenance level 6100-05. install\_all\_updates: Executing
/usr/bin/oslevel -rf, Result = 6100-05 install\_all\_updates:
Verification completed. install\_all\_updates: Log file is
/var/adm/ras/install\_all\_updates.log install\_all\_updates: Result =
SUCCESS Active boot logical volume is hd5. Standby boot logical volume
is bos\_hd5. Creating standby BOS boot image on boot logical volume
bos\_hd5 bosboot: Boot image is 43349 512 byte blocks. Unmounting all
standby BOS file systems \... Unmounting /bos\_inst/opt Unmounting
/bos\_inst/var Unmounting /bos\_inst/usr Unmounting /bos\_inst Log file
is /etc/multibos/logs/op.alog Return Status = SUCCESS \# shutdown -Fr
now \# lsvg -l rootvg \| grep bos hd4 jfs2 4 8 2 closed/syncd /bos\_inst
hd2 jfs2 29 58 2 closed/syncd /bos\_inst/usr hd9var jfs2 2 4 2
closed/syncd /bos\_inst/var hd10opt jfs2 1 2 2 closed/syncd
/bos\_inst/opt bos\_hd5 boot 1 2 2 closed/syncd N/A bos\_hd4 jfs2 4 8 2
open/syncd / bos\_hd2 jfs2 29 58 2 open/syncd /usr bos\_hd9var jfs2 2 4
2 open/syncd /var bos\_hd10opt jfs2 1 2 2 open/syncd /opt \# ls -l
Remove Operation Boot Partition Processing Mount Processing File Systems
Logical Volumes Bootlist Processing
