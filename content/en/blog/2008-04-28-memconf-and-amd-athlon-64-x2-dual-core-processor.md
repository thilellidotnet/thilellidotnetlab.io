---
date: "2008-04-28T16:25:40Z"
tags:
- memory
- x86
title: memconf And AMD Athlon 64 X2 Dual Core Processor
---
-------------------------------- --------------------------
AMD Athlon(tm) 64 X2 Dual Core Processor 3800+ Socket 939
AMD Athlon(tm) 64 X2 Dual Core Processor 3800+ Socket 939
Memory Units:
Type    Status Set Device Locator      Bank Locator
------- ------ --- ------------------- --------------------

The last update to the excellent `memconf` utility (V2.5 22-Feb-2008)
support properly recent Solaris Express releases, and my recent change
from the stock AMD Opteron Processor 148 to an AMD Athlon 64 X2 Dual
Core Processor 3800+. (I mostly did that change just to be able to
access two run queues separately, not to gain more power *per se*.)

So, here is the new and appropriate `memconf` report:

    # memconf -d
    memconf:  V2.5 22-Feb-2008 http://www.4schmidts.com/unix.html
    hostname: unic
    manufacturer: Sun Microsystems, Inc.
    model:    Sun Ultra 20 Workstation (AMD Athlon(tm) 64 X2 Dual Core \
     Processor 3800+ Socket 939 2010MHz)
    Sun Family Part Number: A63
    Solaris Express Community Edition snv_87 X86, 64-bit kernel, SunOS 5.11
    1 AMD Athlon(tm) 64 X2 Dual Core Processor 3800+ Socket 939 2010MHz cpu
    diagbanner = Sun Ultra 20 Workstation
    cpubanner = AMD Athlon(tm) 64 X2 Dual Core Processor 3800+ Socket 939 2010MHz
    model = Sun Ultra 20 Workstation
    machine = i86pc
    platform = i86pc
    perl version: 5.008004
    CPU Units:
    ==== Processor Sockets ====================================
    Version                          Location Tag
    unknown in use 0   A0                  Bank0/1
    unknown in use 0   A1                  Bank2/3
    unknown in use 0   A2                  Bank4/5
    unknown in use 0   A3                  Bank6/7
    total memory = 2048MB (2GB)

You can check and compare with the previous report [on my
blog](/post/2006/12/04/memconf-Update).
