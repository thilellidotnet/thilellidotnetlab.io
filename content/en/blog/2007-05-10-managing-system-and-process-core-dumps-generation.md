---
date: "2007-05-10T19:32:49Z"
tags:
- Processus
- System
title: Managing System and Process Core Dumps Generation
---

[Core Dump Management on the Solaris
OS](http://developers.sun.com/solaris/articles/manage_core_dump.html)
and [Using the Solaris coreadm utility to control core file
generation](http://prefetch.net/blog/index.php/2007/03/23/using-the-solaris-coreadm-utility-to-control-core-file-generation/)
are clearly two useful recent writings about knowing how to enable and
configure process and system core dumps on a Solaris system.

You will learn what are `SIGSEGV` and `SIGBUS` signals, and the role
they are playing in core generation. You will know how to easily alter
the current process and system configuration files (respectively
`coreadm.conf`, and `dumpadm.conf`) using appropriate system commands
(respectively `coreadm(1M)`, and `dumpadm(1M)`). In the same time, you
will learn some basics about how to extract and interpret core files
content. Then, you will find some tips on how a system dump can be
voluntarily generated on both UltraSPARC and x86 platforms. Last,
[Matty](http://prefetch.net/) will show us that you can even set process
core dump configuration to log to the `syslog` facility. Very nice!
