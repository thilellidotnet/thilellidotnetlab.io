---
date: "2007-05-02T19:59:50Z"
tags:
- SAN
title: Getting Emulex HBA Information on a GNU/Linux System
---

As a GNU/Linux environment behaves always a little differently as with
other UNIX platforms, here is a little sample of what commands I find
useful when working with our SAN administrators. In this example, the
server is connected using an Emulex Fibre Channel HBA (Host Bus Adapter)
and is based on an updated RHEL4U4 system, as can be seen below:

    # cat /etc/redhat-release
    Red Hat Enterprise Linux AS release 4 (Nahant Update 4)
    #
    # uname -a
    Linux test.example.com 2.6.9-42.0.10.ELsmp #1 SMP Fri Feb 16 17:13:42 EST 2007 \
     x86_64 x86_64 x86_64 GNU/Linux

Here are some informations about the HBA itself. You can
see\--respectively\--the HBA description, the firmware revision level,
the WWNN (World Wide Node Name), the WWPN (World Wide Port Name), the
operating system driver version, the associated serial number, and the
current speed and link state.

    # lspci | grep -i emulex
    05:0d.0 Fibre Channel: Emulex Corporation LP9802 Fibre Channel Host Adapter (rev 01)
    #
    # cat /sys/class/scsi_host/host0/fwrev
    1.90A4 (H2D1.90A4)
    #
    # cat /sys/class/scsi_host/host0/node_name
    0x20000000c94f7dd9
    #
    # cat /sys/class/scsi_host/host0/port_name
    0x10000000c94f7dd9
    #
    # cat /sys/class/scsi_host/host0/lpfc_drvr_version
    Emulex LightPulse Fibre Channel SCSI driver 8.0.16.27
    #
    # cat /sys/class/scsi_host/host0/serialnum
    MS54376943
    #
    # cat /sys/class/scsi_host/host0/speed
    2 Gigabit
    #
    # cat /sys/class/scsi_host/host0/state
    Link Up - Ready:
       Fabric
