---
date: "2007-04-27T20:33:07Z"
tags:
- Kernel
title: Compile and Install a New Kernel on Red Hat Advanced Server 2.1
---
--- Makefile.orig    Wed Aug 17 12:51:14 2005
+++ Makefile Tue Aug 16 14:43:16 2005
@@ -1,7 +1,7 @@
 VERSION = 2
 PATCHLEVEL = 4
 SUBLEVEL = 9
-EXTRAVERSION = -e.35
+EXTRAVERSION = -e.35smp-custom
 
 KERNELRELEASE=$(VERSION).$(PATCHLEVEL).$(SUBLEVEL)$(EXTRAVERSION)
 
</pre>
<p>Build and install the corresponding custom modules and kernel:</p>
<pre>
# make dep               /* Set up all the dependencies correctly. */
# make j4 bzImage        /* Create a compressed kernel image. */
# make j4 modules        /* Create the chosen modules. */
# make modules_install   /* Install the corresponding modules. */
# make install           /* Install the newly created kernel. */
</pre>
<p>Add it to the boot loader:</p>
<pre>
# cd /boot/grub
# cp -p grub.conf /boot/grub/grub.conf.orig
# diff -u grub.conf.orig grub.conf     
--- grub.conf.orig      Wed Aug 17 17:26:25 2005

To be able to recompile our Linux kernel for our IBM Blade Center, here
are a listing of the necessary prerequisites:

-   Have the sources installed and available under
    `/usr/src/linux-2.4.9-e.35`
-   The `bcm5700` driver located at `/usr/src/redhat` (provided by IBM)
-   Our standard configuration kernel file, i.e. `.config` provided as
    an
    [attached](/blog/dot.config.rhas.2.1 "Standard configuration kernel file")
    file

Adapt our custom kernel configuration file and the top Makefile:

    # cd /usr/src/linux-2.4.9-e.35   /* Go the top sources directory. */
    # make mrproper                  /* Make sure you have no stale .o files and
                                        dependencies lying around. */
    # sum .config                    /* Verify our customized kernel configuration
    64319    16                         file. */
    # make oldmenu                   /* Default all questions based on the contents
                                        of the existing .config file. */
    # cp Makefile Makefile.orig      /* Check the top Makefile for further
                                        site-dependent configuration. */
    # diff -u Makefile.orig Makefile
    +++ grub.conf   Wed Aug 17 17:29:35 2005
    @@ -10,6 +10,10 @@
     default=0
     timeout=5
     splashimage=(hd0,0)/grub/splash.xpm.gz
    +title Red Hat Enterprise (2.4.9-e.35smp-custom)
    +       root (hd0,0)
    +       kernel /vmlinuz-2.4.9-e.35smp-custom ro root=/dev/hda2
    +       initrd /initrd-2.4.9-e.35smp.img
     title Red Hat Linux (2.4.9-e.35smp)
            root (hd0,0)
            kernel /vmlinuz-2.4.9-e.35smp ro root=/dev/hda2

*Verify the files are present in `/boot`!*

Verify that all these new stuffs are OK, then reinstall the network
driver using the provided `RPM` package:

    # shutdown -ry now
    /*
     * If all is ok, then access the machine through the IBM Management Module
     * Console (MMC).
     */
    # cd /usr/src/redhat
    # rpm bb SPECS/bcm5700.spec
    # rpm ivh --force RPMS/i386/bcm5700-8.1.11-1.i386.rpm
    # grep bcm5700 /etc/modules.conf
    alias eth0 bcm5700
    alias eth1 bcm5700
    # modprobe bcm5700

Test if the network is running OK for now, then reboot.
