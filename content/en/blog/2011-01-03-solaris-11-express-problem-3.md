---
date: "2011-01-03T20:23:31Z"
tags:
- IPS
- Zone
- Bug
- Upgrade
title: 'Solaris 11 Express: Problem #3'
---
--        ------ ---------- ----- ------ -------          
solaris   -      -          9.88M static 2010-12-01 09:32 
solaris-1 NR     /          5.44G static 2011-01-03 19:35 

# zoneadm list -vc                                                                   
  ID NAME             STATUS     PATH                           BRAND    IP    
   0 global           running    /                              ipkg     shared
   - zone1            installed  /dpool/store/zone/zone1        ipkg     shared

# zoneadm -z zone1 detach

# zoneadm -z zone1 attach -u
Log File: /var/tmp/zone1.attach_log.lfa49e
Attaching...

preferred global publisher: solaris
       Global zone version: entire@0.5.11,5.11-0.151.0.1.1:20101222T214417Z
   Non-Global zone version: entire@0.5.11,5.11-0.151.0.1:20101105T054056Z

                     Cache: Using /var/pkg/download.
  Updating non-global zone: Output follows
Creating Plan                          
ERROR: Could not update attaching zone
                    Result: Attach Failed.

# cat /var/tmp/zone1.attach_log.lfa49e
[Monday, January  3, 2011 08:42:24 PM CET] Log File: /var/tmp/zone1.attach_log.lfa49e
[Monday, January  3, 2011 08:42:25 PM CET] Attaching...
[Monday, January  3, 2011 08:42:25 PM CET] existing
[Monday, January  3, 2011 08:42:25 PM CET] 
[Monday, January  3, 2011 08:42:25 PM CET]   Sanity Check: Passed.  Looks like an OpenSolaris system.
[Monday, January  3, 2011 08:42:31 PM CET] preferred global publisher: solaris
[Monday, January  3, 2011 08:42:32 PM CET]        Global zone version: entire@0.5.11,5.11-0.151.0.1.1:20101222T214417Z
[Monday, January  3, 2011 08:42:32 PM CET]    Non-Global zone version: entire@0.5.11,5.11-0.151.0.1:20101105T054056Z

[Monday, January  3, 2011 08:42:32 PM CET]                      Cache: Using /var/pkg/download.
[Monday, January  3, 2011 08:42:32 PM CET]   Updating non-global zone: Output follows
pkg set-publisher: 
Unable to locate certificate '/dpool/store/zone/zone1/root/dpool/store/zone/zone1/root/var/pkg/ssl/Oracle_Solaris_11_Express_Support.certificate.pem' needed to access 'https://pkg.oracle.com/solaris/support/'.
pkg unset-publisher: 
Removal failed for 'za23954': The preferred publisher cannot be removed.

pkg: The following pattern(s) did not match any packages in the current catalog.
Try relaxing the pattern, refreshing and/or examining the catalogs:
        entire@0.5.11,5.11-0.151.0.1.1:20101222T214417Z
[Monday, January  3, 2011 08:44:04 PM CET] ERROR: Could not update attaching zone
[Monday, January  3, 2011 08:44:06 PM CET]                     Result: Attach Failed.
</pre>
<p>FYI, this problem was covered by the Bug ID number 13000, but is always
present at this time, at least for Solaris 11 Express 2010.11.</p>
<ul>
<li><a href="https://defect.opensolaris.org/bz/show_bug.cgi?id=13000" hreflang="en">https://defect.opensolaris.org/bz/show_bug.cgi?id=13000</a></li>
</ul>
<p>So, it seems that the change of repository for the <em>solaris</em>
publisher was not well managed by the non-global zone update mechanism. Just to
be sure, I tried to create a new non-global zone in the new boot environment,
but the problem exists in this case, too:</p>
<pre>
# zoneadm -z zone2 install
A ZFS file system has been created for this zone.
   Publisher: Using solaris (https://pkg.oracle.com/solaris/support/ ).
   Publisher: Using opensolaris.org (http://pkg.opensolaris.org/dev/).
       Image: Preparing at /dpool/store/zone/zone2/root.
 Credentials: Propagating Oracle_Solaris_11_Express_Support.key.pem
 Credentials: Propagating Oracle_Solaris_11_Express_Support.certificate.pem
Traceback (most recent call last):
  File &quot;/usr/bin/pkg&quot;, line 4225, in handle_errors
    __ret = func(*args, **kwargs)
  File &quot;/usr/bin/pkg&quot;, line 4156, in main_func
    ret = image_create(pargs)
  File &quot;/usr/bin/pkg&quot;, line 3836, in image_create
    variants=variants, props=set_props)
  File &quot;/usr/lib/python2.6/vendor-packages/pkg/client/api.py&quot;, line 3205, in image_create
    uri=origins[0])
TypeError: 'set' object does not support indexing

pkg: This is an internal error.  Please let the developers know about this
problem by filing a bug at http://defect.opensolaris.org and including the
above traceback and this message.  The version of pkg(5) is '052adf36c3f4'.
ERROR: failed to create image
</pre>
<p>FYI, this problem is covered by the Bug ID number 17653.</p>
<ul>
<li><a href="https://defect.opensolaris.org/bz/show_bug.cgi?id=17653" hreflang="en">https://defect.opensolaris.org/bz/show_bug.cgi?id=17653</a></li>
</ul>
<p>Well, no luck here. I didn't see a Solaris IPS update for these problems
yet, which are very annoying, at least.</p>
<p><em>Update #1 (2011-02-03):</em> The problem is now fixed in the latest
support pkg repository. Install or attach a non-global ipkg branded zone works
now as expected:</p>
<pre>
# zoneadm -z zone1 install
A ZFS file system has been created for this zone.
   Publisher: Using solaris (https://pkg.oracle.com/solaris/support/ ).
   Publisher: Using opensolaris.org (http://pkg.opensolaris.org/dev/).
   Publisher: Using sunfreeware (http://pkg.sunfreeware.com:9000/).
       Image: Preparing at /dpool/export/zone/zone1/root.
 Credentials: Propagating Oracle_Solaris_11_Express_Support.key.pem
 Credentials: Propagating Oracle_Solaris_11_Express_Support.certificate.pem
       Cache: Using /var/pkg/download. 
Sanity Check: Looking for 'entire' incorporation.
  Installing: Core System (output follows)
               Packages to install:     1
           Create boot environment:    No
[...]
        Note: Man pages can be obtained by installing SUNWman
 Postinstall: Copying SMF seed repository ... done.
 Postinstall: Applying workarounds.
        Done: Installation completed in 332.525 seconds.

  Next Steps: Boot the zone, then log into the zone console (zlogin -C)
              to complete the configuration process.
</pre>
<p>And:</p>
<pre>
# zoneadm -z zone1 detach
# zoneadm -z zone1 attach -u
Log File: /var/tmp/zone1.attach_log.PhaOKf
Attaching...

preferred global publisher: solaris
       Global zone version: entire@0.5.11,5.11-0.151.0.1.2:20110127T225841Z
   Non-Global zone version: entire@0.5.11,5.11-0.151.0.1.2:20110127T225841Z

                     Cache: Using /var/pkg/download.
  Updating non-global zone: Output follows
No updates necessary for this image.   
  Updating non-global zone: Zone updated.
                    Result: Attach Succeeded.
# zoneadm list -vc
  ID NAME             STATUS     PATH                           BRAND    IP    
   0 global           running    /                              ipkg     shared
   - zone1            installed  /dpool/export/zone/zone1       ipkg     shared
</pre>

*In this series, I will report the bugs or problems I find when running
the Oracle Solaris 11 Express distribution. I hope this will give more
visibility on those PR to Oracle to correct them before the release of
Solaris 11 next year.*

I recently switch from the official Oracle *release* repository to the
*support* repository for Solaris 11 Express. Before the switch, one
non-global zone was created. Since there were some updates to this
repository, I `pkg update`\'ed, rebooted to the new boot environment,
and tried to update the non-global zone:

    # beadm list                                                                         
    BE        Active Mountpoint Space Policy Created          
