---
date: "2007-04-08T19:36:44Z"
tags:
- DST
title: Check Support for New Daylight Savings Time
---

Although
[others](http://www.cuddletech.com/blog/pivot/entry.php?id=798 "Y2K7: Daylight Savings Time Changes and OpenSolaris")
already wrote about this very good tip, i found it so useful i though i
must mentioned it here, too. It was first found on the great [Y2K7
update?](http://www.opensolaris.org/jive/thread.jspa?threadID=25516 "Thread: Y2K7 update?")
thread on the `opensolaris-discuss` forum on opensolaris.org.

Here is a live example running `snv_56`:

    $ /usr/sbin/zdump -v ${TZ} | awk '$6 ~ /'"`date '+%Y'`"'/ {print $0}' | \
       grep -v "`date '+%a %b %e'`"
    Europe/Paris Sun Mar 25 00:59:59 2007 UTC = Sun Mar 25 01:59:59 2007 CET isdst=0
    Europe/Paris Sun Mar 25 01:00:00 2007 UTC = Sun Mar 25 03:00:00 2007 CEST isdst=1
    Europe/Paris Sun Oct 28 00:59:59 2007 UTC = Sun Oct 28 02:59:59 2007 CEST isdst=1
    Europe/Paris Sun Oct 28 01:00:00 2007 UTC = Sun Oct 28 02:00:00 2007 CET isdst=0

I can\'t think i miss this utility until today.
