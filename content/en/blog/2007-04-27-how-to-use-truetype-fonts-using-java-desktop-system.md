---
date: "2007-04-27T17:58:54Z"
tags:
- JDS
title: How to Use TrueType Fonts Using Java Desktop System
---

In fact, it is as easy as just say it. Under the JDS3 desktop
environment and running on Solaris 10+ (at least), you just have to
populate your `${HOME}/.fonts` directory with your TrueType fonts (which
generally ends with a `.ttf` extension name) and log in again. Et voila:
any GNOME/GTK software will be able to use them automatically.

There no need anymore to use other `X Window System` tools, such as
`xset(1)` or `mkfontdir(1)`.
