---
date: "2012-07-03T12:41:32Z"
tags:
- Press
title: 'Press Review #13'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### future of AIX is my future too?

-   <http://www.wmduszyk.com/?p=8717>

Maybe all what is required is to have HMC "gui" designers to download a
copy of "virtual box" or VMWare in order to get some inspiration? Maybe
it is time to stop repeating how great AIX is and to return to the
drafting board to make AIX and its components match the times (2012) we
live in? It is not 1990 any more. I am with AIX since 3.2.5 and I have
not seen any improvement in its access and control methods. Why? Are we
perfect already?

### Delegation of Solaris Zone Administration

-   <https://blogs.oracle.com/darren/entry/delegation_of_solaris_zone_administration>

In Solaris 11 \'Zone Delegation\' is a built in feature. The Zones
system now uses finegrained RBAC authorisations to allow delegation of
management of distinct zones, rather than all zones which is what the
\'Zone Management\' RBAC profile did in Solaris 10.

### Red Hat Enterprise Linux to Oracle Solaris Porting Guide

-   <http://www.oracle.com/technetwork/server-storage/solaris11/documentation/o12-026-linux2solaris-guide-1686620.pdf>

The purpose of this document is to enable developers to resolve issues
faced during the migration of Red Hat Enterprise Linux (RHEL)
applications to Oracle Solaris 11. The document describes similarities
and differences between the two environments in terms of architecture,
system calls, tools, utilities, development environments, and operating
system features. Wherever possible, it also provides pointed solutions
and workarounds to address the specific porting issues that commonly
arise due to implementation differences on these two platforms.

### Which Tool Should I Use to Manage Which Virtualization Technology?

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/1688512>

A summary of the interfaces and tools that sysadmins can use to set up
and manage virtual server, operating system, network, and storage
resources from Oracle. This article surveys the interfaces and tools
that sysadmins can use to set up and manage virtual compute, memory,
operating system, network, and storage resources from Oracle.

### Some Insight Into Those Future Power7+ Processors

-   <http://www.itjungle.com/tfh/tfh070912-story01.html>

A few weeks ago, I told you that IBM was getting ready to start talking
about its future Power7+ and System zNext processors at the Hot Chips
conference at the end of August.

### What\'s up with LDoms: Part 3 - A closer look at Disk Backend Choices

-   <https://blogs.oracle.com/cmt/en/entry/what_s_up_with_ldoms3>

In this section, we\'ll have a closer look at virtual disk backends and
the various choices available here. As a little reminder, a disk
backend, in LDoms speak, is the physical storage used when creating a
virtual disk for a guest system. In other virtualization solutions,
these are sometimes called virtual disk images, a term that doesn\'t
really fit for all possible options available in LDoms.

### Solaris11AutomatedInstaller

-   <http://wiki.wesunsolve.net/Solaris11AutomatedInstaller>

This documentation is intented to any Solaris sysadmin who wish to
install from scratch an Automated Installer Server.

This document covers the installation of a SPARC client using AI from
start to finish, including post-install configuration and os-specific
basic configuration.

As the oracle documentation is spreaded accross a lot of different
documents, I decided to write my own HOWTO to setup an AI server\...

### How I Got Started with the Btrfs File System for Oracle Linux

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/gettingstarted-btrfs-1695246.html>

This article describes the basic capabilities that I discovered while
becoming familiar with the Btrfs file system in Oracle Linux, plus the
instructions I used to create a file system, verify its size, create
subdirectories, and perform other basic administrative tasks.

### One More Power Systems Roadmap For The Road

-   <http://www.itjungle.com/tfh/tfh071612-story05.html>

In last week\'s issue of The Four Hundred, I walked you through some
Power processor and systems roadmaps that I was able to find out there
on the Intertubes, as well as some specifics about the forthcoming
Power7+ processors, which are due to come to market between now and the
end of the year. I accidentally left one of the roadmaps I stumbled upon
out of last week\'s story.

### Tips for Hardening an Oracle Linux Server

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/tips-harden-oracle-linux-1695888.html>

Oracle Linux provides a complete security stack, from network firewall
control to access control security policies. While Oracle Linux is
designed \"secure by default,\" this article explores a variety of those
defaults and administrative approaches that help to minimize
vulnerabilities.

### ASMLib

-   <https://blogs.oracle.com/wim/entry/asmlib>

Oracle ASMlib on Linux has been a topic of discussion a number of times
since it was released way back when in 2004. There is a lot of confusion
around it and certainly a lot of misinformation out there for no good
reason. Let me try to give a bit of history around Oracle ASMLib.

### The post-RAID era begins

-   <http://storagemojo.com/2012/07/23/the-post-raid-era-has-begun/>

The post-RAID (noRAID) era has begun. While RAID arrays aren't going
away, the growth is elsewhere, and corporate investment follows growth.

### How to Manage ZFS Data Encryption

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/manage-zfs-encryption-1715034.html>

Oracle Solaris 11 adds transparent data encryption functionality to ZFS.
All data and file system metadata (such as ownership, access control
lists, quota information, and so on) are encrypted when stored
persistently in the ZFS pool.
