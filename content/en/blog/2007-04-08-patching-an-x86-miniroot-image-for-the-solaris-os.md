---
date: "2007-04-08T19:29:54Z"
tags:
- x86
- JumpStart
- Patch
title: Patching an x86 Miniroot Image for the Solaris OS
---

Generally speaking,
[BigAdmin](http://www.sun.com/bigadmin/ "BigAdmin System Administration Portal")
is a great and valuable source for Sun\'s systems administrators. Here
is an awesome article describing how to patch (update) the kernel used
during an installation or system upgrade process, known as `miniroot`,
for x86 based Solaris platform.

At work, we precisely encounter a bug between Solaris 6/06 and the
provided nVidia driver which prevents jumpstarting it on a [Sun Fire
X4100 M2
Server](http://www.sun.com/servers/entry/x4100/ "The Sun Fire X4100 server line").
The support team said we can apply specific patches, already present in
Solaris 11/06 at that time. Because we don\'t really known the exact
procedure to follow to update the `miniroot` accordingly, and because
these machines must be provisioned very quickly, we doesn\'t investigate
much on that way (ending installing them with DVD-ROMs, in servers
room). Now, after reading the proposed
[article](http://www.sun.com/bigadmin/features/hub_articles/patchmini.jsp "Patching an x86 Miniroot Image for the Solaris OS"),
we will certainly take the time to do so\... if we know how to get the
proper bundle of patches to correct our bug.
