---
date: "2012-09-12T15:57:55Z"
tags:
- Press
title: 'Press Review #15'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### Pre-rentrée Oracle Open World 2012 : à vos agendas

-   [https://blogs.oracle.com/EricBezille/entry/pre\_rentrée\_oracle\_open\_world](https://blogs.oracle.com/EricBezille/entry/pre_rentr%C3%A9e_oracle_open_world)

A maintenant moins d\'un mois de l'événement majeur d\'Oracle, qui se
tient comme chaque année à San Francisco, fin septembre, début octobre,
les spéculations vont bon train sur les annonces qui vont y être
dévoilées\... Et sans lever le voile, je vous engage à prendre
connaissance des sujets des \"Key Notes\" qui seront tenues par Larry
Ellison, Mark Hurd, Thomas Kurian (responsable des développements
logiciels) et John Fowler (responsable des développements systèmes) afin
de vous donner un avant goût.

### UPDATED: Volume Group Missing After AIX Migration

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/AIXDownUnder/entry/volume_group_missing_after_aix_migration12>

A customer did a migration from AIX 5.3 to 6.1 and then called me to
report a strange set of symptoms. Some file systems didn\'t mount
following a reboot. When the file systems in the volume group (let\'s
call it datavg) went to mount, they returned the error that there was no
such device. If the customer ran an exportvg and an importvg, all the
datavg file systems became available. But then another reboot was done
and the datavg file systems didn\'t mount.

### Oracle Optimized Solution for Enterprise Cloud Infrastructure --- Implementation Guide for SPARC

-   <http://www.oracle.com/technetwork/server-storage/hardware-solutions/o12-069-oos-cloud-sparc-1739930.pdf>

This white paper describes the implementation details for an Oracle
Solaris Could build around SPARC T4 and ZFS SA, with all the latest
development of Enterprise Manager Ops Center 12c. It covers both Zones
(Branded, S10, S11), and LDOMs (OVM for SPARC) directly managed through
Ops Center.

### Systems Director - What Licences should I have?

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/systems_director_what_licences_should_i_have238>

I have long thought it is rather silly that IBM Systems Director can\'t
give you a list the licenses that you should have! It does, after all,
know what machines it is controlling and the data is in the database.
Perhaps, that will come in the future. What can we do in the short term?

### Oracle SPARC SuperCluster and US DoD Security guidelines

-   <https://blogs.oracle.com/jimlaurent/entry/oracle_sparc_supercluster_and_us>

I\'ve worked in the past to help our government customers understand how
best to secure Solaris. For my customer base that means complying with
Security Technical Implementation Guides (STIGs) from the Defense
Information Systems Agency (DISA). I recently worked with a team to
apply both the Solaris and Oracle 11gR2 database STIGs to a SPARC
SuperCluster. The results have been published in an Oracle White paper.

### Best Practices - Dynamic Reconfiguration

*This post is one of a series of \"best practices\" notes for Oracle VM
Server for SPARC (formerly named Logical Domains)*

-   <https://blogs.oracle.com/jsavit/entry/best_practices_removing_devices_from>

Oracle VM Server for SPARC supports Dynamic Reconfiguration (DR), making
it possible to add or remove resources to or from a domain (virtual
machine) while it is running. This is extremely useful because resources
can be shifted to or from virtual machines in response to load
conditions without having to reboot or interrupt running applications.
For example, if an application requires more CPU capacity, you can add
CPUs to improve performance, and remove them when they are no longer
needed. You can use even use Dynamic Resource Management (DRM) policies
that automatically add and remove CPUs to domains based on load.

### Secure Deployment of Oracle VM Server for SPARC - updated

-   <https://blogs.oracle.com/cmt/entry/secure_deployment_of_oracle_vm>

Quite a while ago, I published a paper with recommendations for a secure
deployment of LDoms. Many things happend in the mean time, and an update
to that paper was due. \[\...\] In a very short few words: With the
success especially of the T4-4, many deployments make use of the
hardware partitioning capabilities of that platform, assigning full PCIe
root complexes to domains, mimicking dynamic system domains if you will.

### Get Unique Access to SPARC Product Strategy and Best Practices at Oracle OpenWorld 2012

-   <http://www.oracle.com/us/dm/h2fy11/sparcatopenworld-1713476.html>

Product strategy, real-world best practices, and customer panels devoted
to Oracle's SPARC servers and related technology are among the
highlights of the upcoming Oracle OpenWorld 2012, taking place from
September 30 to October 4 in San Francisco.

### The first Oracle Solaris 11 book is now available

-   <https://blogs.oracle.com/drcloud/entry/the_first_oracle_solaris_11>

The first Oracle Solaris 11 book is now available: \"Oracle Solaris 11
System Administration - The Complete Reference\", by Michael Jang, Harry
Foxwell, Christine Tran, and Alan Formy-Duval.

### Introducing Oracle System Assistant

-   <https://blogs.oracle.com/hardware/entry/introducing_oracle_system_assistant>

One of the challenges with today\'s servers is getting the server up and
running and understanding what all of the steps are once you plug the
server in for the first time. So many different pieces come into play:
installing drivers, updating firmware, configuring RAID, and
provisioning the operating system. All of these steps must be done
before you can even start using the server.

### ZFS Articles \@nexenta

-   <http://www.nexenta.com/corp/products/what-is-openstorage/zfs-info-center>

This category contains articles that describe the various aspects of
ZFS, the Zettabyte file system. ZFS was originally developed by Sun
Microsystems, and open sourced thru the OpenSolaris project, and now
maintained at Illumos.org.

### DTrace Guide \@illumos

-   <http://dtrace.org/guide/preface.html>

DTrace is a comprehensive dynamic tracing framework for the illumos™
Operating System. DTrace provides a powerful infrastructure to permit
administrators, developers, and service personnel to concisely answer
arbitrary questions about the behavior of the operating system and user
programs. The illumos Dynamic Tracing Guide describes how to use DTrace
to observe, debug, and tune system behavior. This book also includes a
complete reference for bundled DTrace observability tools and the D
programming language.

### By 2015, engineered systems would account for over 30% of physical hardware shipments

-   <http://www.expresscomputeronline.com/interviews/967-by-2015-engineered-systems-would-account-for-over-30-of-physical-hardware-shipments>

Upbeat over Exalogic\'s global uptake, Andrew Lau, Senior Director,
Oracle Exalogic - Asia Pacific, discussed the organization's strategy to
leverage the acquired hardware expertise and extend the Exa family.

### Le Sparc64 X : prochain moteur des serveurs d\'entreprises d\'Oracle ?

-   <http://www.lemagit.fr/technologie/datacenter-technologie/processeurs-composants/2012/09/07/le-sparc64-prochain-moteur-des-serveurs-039-entreprises-039-oracle/>
-   <http://www.lemagit.fr/files/uejju25eiuqnp7qxy66aogvtiy4wljw2.pdf>

A Hot Chips, Fujitsu a dévoilé les spécification de sa puce Sparc64 X,
une puce aux performances détonnantes qui pourrait bien être le fameux
processeur M4 mentionné par Oracle dans ses roadmap. Un processeur censé
motoriser une nouvelle ligne de serveurs Unix haut de gamme à la fin
2012\...

### Oracle hurls Sparc T5 gladiators into big-iron arena

-   <http://www.theregister.co.uk/2012/09/04/oracle_sparc_t5_processor/>

Oracle\'s Sparc processor server biz may be bleeding revenue, but the
company is still working on very innovative chips. Its Sparc T series,
and the Sparc T5 systems that will launch later this year (very likely
at the OpenWorld trade show at the end of September) suggest the company
is growing its multithreaded processors in terms of cores and sockets
and pushing up into the big iron space.

### Oracle VM VirtualBox 4.2 narrows gap with VMware, Parallels

-   <http://www.infoworld.com/d/virtualization/oracle-vm-virtualbox-42-narrows-gap-vmware-parallels-202489>

Many people feared that Oracle would kill off VirtualBox after it
acquired Sun Microsystems, but nothing could be further from the truth.
In fact, it\'s been a little over a year since the last major release
was announced, and Oracle is once again pushing the virtual ball forward
with a major release of Oracle VM VirtualBox 4.2.

### VirtualBox Finds the Meaning of Open Source Life with Version 4.2

-   <http://www.internetnews.com/blog/skerner/virtual-box-finds-the-meaning-of-open-source-life-with-version-4.2.html>

Oracle over the last two years has expanded VirtualBox to make it the
easiest solution for anyone on Linux, Window, Mac or Solaris to get a
baseline level of guest operating system virtualization up and running
quickly.

### What\'s New in Oracle VM VirtualBox 4.2?

-   <https://blogs.oracle.com/fatbloke/entry/what_s_new_in_oracle>

A year is a long time in the IT industry. Since the last VirtualBox
feature release, which was a little over a year ago, we\'ve seen:

-   new releases of cool new operating systems, such as Windows 8,
    ChromeOS, and Mountain Lion;
-   we\'ve seen a myriad of new Linux releases from big Enterprise class
    distributions like Oracle 6.3 and Solaris 11, to accessible desktop
    distros like Ubuntu 12.04 and Fedora 17;
-   and we\'ve also seen the spec of a typical PC or laptop double in
    power.

All of these events have influenced our new VirtualBox version which
we\'re releasing today. Here\'s how\...

### So You Want To Build a SPARC Cloud

-   <https://blogs.oracle.com/stevewilson/entry/so_you_want_to_build>
-   <http://www.oracle.com/technetwork/oem/host-server-mgmt/hghlyavailldomserverpoolsemoc12cv09-1845483.pdf>

Did you ever wish you could get the industrial strength power of
UNIX/RISC with the flexibility of cloud computing? Well, now you can!
With recent advances from Oracle it\'s possible to build an incredibly
high-performance, flexible, available virtualized infrastructure based
on Solaris and SPARC. Here\'s the recipe!

### POWER Double Stuff vs SPARC Critical-Thread

-   <http://netmgt.blogspot.fr/2012/09/power-double-stuff-vs-sparc-critical.html>

Computing processor models differ in architecture from one company to
another, each trying to gain an edge in the market over their
competitors. Often, chip foundries will attempt radical approaches to
conquer a problem, but incremental improvement will often bring radical
ideas back to similar conclusions in the end. A comparison between SPARC
and POWER architectures is no different.

### Oracle forces wesunsolve to close

-   <http://sparcv9.blogspot.fr/2012/09/oracle-forces-wesunsolve-to-close.html>

In another blow against the community and the people who work with
or/and have interest in their products Oracle now has forced
wesunsolve.net to close.

### Current SPARC Architectures

-   <https://blogs.oracle.com/d/entry/current_sparc_architectures>

If an application targets a recent architecture, then the compiler gets
to play with all the instructions that the new architecture provides.
The downside is that the application won\'t work on older processors
that don\'t have the new instructions. So for developer\'s there is a
trade-off between performance and portability.

### A Return to Linux on the Workstation

-   <http://cuddletech.com/blog/?p=770>

Up until about 30 days ago my primary workstation ran some variety of
Solaris for nearly 10 years, starting with Solaris 9 when X86 became
viable on X86, then OpenSolaris and the various Solaris Express releases
and finally Solaris 11 Beta. It was one month ago today that I finally
re-installed it with Ubuntu, returning me to Linux officially.
