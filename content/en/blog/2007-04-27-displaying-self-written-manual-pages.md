---
date: "2007-04-27T20:30:44Z"
tags: []
title: Displaying Self Written Manual Pages
---

Learning about how to write a *well* written manual page, just want to
point out the examples provided by the [FreeBSD
Project](http://www.freebsd.org/ "The Power To Serve"), located under
the base system in `/usr/share/examples/mdoc`.

For those who are not (yet!) under FreeBSD Operating System, you can
browse the publicly available CVS repository via the [web
interface](http://www.freebsd.org/cgi/cvsweb.cgi/src/share/examples/mdoc/ "FreeBSD CVSweb Project: src/share/examples/mdoc/").

If not already in the system man path or the environmental variable
\${MANPATH}, you can display your new manual page using:

    $ tbl example.1 | groff -S -Wall -mtty-char -man -Tlatin1 | col | more

Or:

    $ zcat example.1.gz | tbl | groff -S -Wall -mtty-char -man -Tlatin1 | col | more
