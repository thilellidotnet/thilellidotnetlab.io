---
date: "2013-05-02T08:56:11Z"
tags:
- Press
title: 'Press Review #23'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### How to Monitor Systems in Enterprise Manager using Ops Center

-   <https://blogs.oracle.com/oem/entry/ops_center_and_enterprise_manager>

In this post, we\'ll use Ops Center to add hardware monitoring to
Enterprise Manager. We\'ll discuss the existing capabilities of Host
targets, show how to create an Infrastructure Stack and demonstrate some
of the features it provides to Enterprise Manager.

![](https://blogs.oracle.com/oem/resource/2013-05-01-EM-OC-integration/001-em-host-memory-details.jpg)

### The Clipper Group Navigator

The Clipper Group Navigator, first published in 1993, describes new
technologies and innovative products and the business values that they
bring to the enterprise. It is targeted at business readers, in general,
and decision makers in the information technology community, in
particular.

#### Resetting the Server Bar - Oracle Claims Database Performance Superiority (April 19, 2013)

-   <http://www.clipper.com/research/TCG2013006.pdf>

#### IBM Extends the Reach of POWER7+ - Introducing POWER7+ Servers for the Rest of Us (February 5, 2013)

-   <http://www.clipper.com/research/TCG2013003.pdf>

### Why SysAdmin's Can't Code

-   <http://cuddletech.com/blog/?p=817>

Most systems administrators are quick, perhaps too quick, to tell you
"I'm not a coder." Oddly, this admission normally comes after boasting
about how many programming languages they know or have used. Why is
this? Can this be changed? Here is my 5 step plan on how any SA can
become an honest to goodness programmer.

### P2V in Ops Center

-   <https://blogs.oracle.com/opscenter/entry/p2v_in_ops_center>

You can do P2V using Ops Center, but it\'s not a single-wizard process,
and it\'ll take a bit of tinkering. Basically, you take a flash archive
(FLAR) of an existing system, import it, then deploy a host, ldom, or
branded zone based on that FLAR.

### What is NPIV (N\_Port ID Virtualization) and NPV (N\_Port Virtualization)?

-   <http://www.aixmind.com/?p=2865>

First, though, I need to cover some basics. This is unnecessary for
those of you that are Fibre Channel experts, but for the rest of the
world it might be useful\...

### How to go Physical to Virtual with Oracle Solaris Zones using Enterprise Manager Ops Center

-   <https://blogs.oracle.com/oem/entry/how_to_p2v_to_oracle>

Many customers have large collections of physical Solaris 8, 9 and 10
servers in their datacenters and they are wondering how they are going
to virtualize them. This leads to a commonly asked question. Can
Enterprise Manager Ops Center 12C be used to P2V (Physical to Virtual)
my old servers? Ops Center does not have a single button P2V capability,
but it is possible for Ops Center to deploy physical servers, LDOMs and
branded zones based on flash archives(flars) that have been taken of
your existing physical servers. Ops Center achieves P2V by deploying
flars and leveraging its patching and automation capabilities, to make
the P2V process consistent, repeatable and as cost effective as
possible.

![](https://blogs.oracle.com/oem/resource/OC_P2V_Zones/P2V_zones_image8.png)

### New White Paper Compares SPARC/Solaris to Power/AIX Costs

-   <https://blogs.oracle.com/solaris/entry/go_faster_pay_less>
-   <http://www.oracle.com/us/products/servers-storage/solaris/solaris11/enterprise-server-infrastructure-1944943.pdf>

The Edison Group has just released a white paper. Spoiler alert: the
Oracle systems do better. Key findings:

-   Over a five year period, the Power system solution has a total cost
    of ownership 59 percent higher than the SPARC T5 solution.
-   There\'s even more of a disparity in cost of acquisition, where the
    IBM solution is twice as expensive right out of the chute.

### Why OS matters: Solaris Users Group testimony

-   <https://blogs.oracle.com/EricBezille/entry/french_solaris_users_group_sap>
-   <http://brain.so/4d8jp> (presentation slides)
-   <http://www.itplace.tv/techday-sparc-solaris-2013> (video)

Wednesday evening, a month after the new SPARC servers T5 & M5 launch in
Paris, the french Solaris users group, get together to get the latest
from Oracle experts on SPARC T5 & M5, Oracle Virtual Network, as well as
the new enhancements inside Solaris 11.1 for Oracle Database. They also
came to share their projects experiences and lessons learn, leveraging
Solaris features : René Garcia Vallina from PSA, did a deep dive on ZFS
internal and best practices around SAP deployment and Bruno Philippe
explained how he managed to consolidate 100 Solaris servers into 6
thanks to Solaris 11 specific features.

![](https://blogs.oracle.com/EricBezille/resource/guses.jpg)

### Oracle Solaris Sparc TechDay 2013 : Retour sur l'événement

-   <http://www.oracle-solaris.fr/oracle-solaris-sparc-techday-retour-sur-levenement/>

Oracle-Solaris.fr était présent à cet événement d'envergure pour les
technologies Oracle Solaris et Sparc qui s'est déroulé le 15 mai à
Paris. L'objectif premier était d'y faire des rencontres, entre
administrateurs et ingénieurs systèmes Solaris mais aussi d'y partager
ses expériences.

### Entitlements and VPs---Why You Should Care

-   <http://www.ibmsystemsmag.com/aix/administrator/lpar/vp_entitlements/>

With the POWER7 processor, many changes were integrated into the chip
that must be taken into account when defining LPARs. First, understand
that POWER7 technology and simultaneous multithreading (SMT4) are all
about throughput, and rPerf is a throughput-based performance
measurement. Therefore, to take advantage of the server's full rPerf
it's important to drive all of the threads. However, the way the threads
get kicked off has changed with POWER7.

### Revealing Hidden Latency Patterns

-   <http://dtrace.org/blogs/brendan/2013/05/19/revealing-hidden-latency-patterns/>

Response time -- or latency -- is crucial to understand in detail, but
many of the common presentations of this data hide important details and
patterns. Latency heat maps are an effective way to reveal these. I
often use tools that provide heat maps directly, but sometimes I have
separate trace output that I'd like to convert into a heat map. To
answer this need, I just wrote trace2heatmap.pl, which generates
interactive SVGs.

![](http://dtrace.org/blogs/brendan/files/2013/05/heatmap-explained-1200.png)

### Exemple: ldom migration with tool ldmp2v

-   <http://www.gloumps.org/article-exemple-ldom-migration-with-tool-ldmp2v-117932944.html>

I describe only migration P2V of a physical server in a ldom, the
installation and the configuration of Oracle VM Server for Sparc are not
specified in this article.

### How to Update Only Java on Your Oracle Solaris 11 System

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/sol-howto-update-only-java-1948328.html>

This article describes how to update Java without updating any other
software. Java might be updated multiple times in an Oracle Solaris
release, so you should follow this procedure periodically.

### The Greatest Tool that Never Worked: har

-   <http://dtrace.org/blogs/brendan/2013/05/27/the-greatest-tool-that-never-worked-har/>

har is the Hardware Activity Reporter. I've never seen it work, but it
did help me solve a crucial performance issue.

### Power Management on SPARC T5 Servers

-   <http://youtu.be/GUa8n_Jh3Cg>

This video demonstrates the server power management features of the
Oracle SPARC T5 platform running Solaris 11 and Oracle VM Server for
SPARC.

### NSS\_OPTIONS

-   <http://milek.blogspot.com/2013/05/nssoptions.html>

Sometimes developers put undocumented options in their code to help with
debugging issues. This morning I came across one of such options which
prints extra debug information when executing NSS queries.

### Oracle Solaris 11 Certification Study Tips

-   <https://blogs.oracle.com/certification/entry/0854_05>

Today on UnixEd.com, author and educator Bill Calkins posts some tips
for taking Oracle Solaris 11 certification exams. In this helpful
article, he outlines key things to focus on for the most effective use
of your study time, including type of exam questions, recommended
practice exercises, how to approach and interpret scenarios, as well as
recommended reading/study material within his new book, \"Oracle Solaris
11 System Administration\" which comes out in Jun 2013.

![](https://blogs.oracle.com/certification/resource/0854.05.02.jpg)
