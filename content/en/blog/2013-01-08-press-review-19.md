---
date: "2013-01-08T10:26:03Z"
tags:
- Press
title: 'Press Review #19'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### Oracle Linux Ksplice offline client

-   <https://blogs.oracle.com/wim/entry/oracle_linux_ksplice_offline_client>

By introducing the offline client, customers with Oracle Linux Premier
or Oracle Linux Premier Limited support can create a local intranet yum
repository that creates a local mirror of the ULN ksplice channel and
just use the yum update command to install the latest ksplice updates.
This will allow customers to have just one server connected to the
oracle server and every other system just have a local connection.

### Resource Management as an Enabling Technology for Virtualization

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/resource-mgmt-for-virtualization-1890711.html>

In this article and the next, we will cover some enabling technologies
for virtualization. Here, we discuss IT resource management as an
enabling technology for virtualization.

### Oracle Premier Support for Oracle Solaris 10 extended by 3 years - now ends January 2018

-   <http://www.oracle.com/us/support/library/lifetime-support-hardware-301321.pdf>

<!-- -->

    Release        GA Date         Premier Support Ends    Extended Support Ends   Sustaining Support Ends
    Solaris 10      Jan 2005        Jan 2018                Jan 2021                Indefinite
    Solaris 11      Nov 2011        Nov 2021                Nov 2024                Indefinite

### BrandZ Solaris 10 (exemples de migration P2V et V2V)

-   <http://www.gloumps.org/article-brandz-solaris-10-exemples-de-migrations-p2v-et-v2v-114194764.html>

Comment réduire efficacement les coûts d\'une infrastructure de serveurs
sous Solaris 10 ? En les consolidant !! Cela va de soi, la forte
utilisation des zones en est un très bonne exemple. Oui mais\...
Pourquoi ne pas utiliser les nouvelles possibilités qui s\'offrent à
nous : Un serveur sous Solaris 11 et des brandZ Solaris 10.

Je vous présente deux exemples rapides de migration d\'environnements
Solaris 10 physique (P2V) et Solaris 10 zone (V2V) sur un serveur
Solaris 11.

### Virtualization Performance: Zones, KVM, Xen

-   <http://dtrace.org/blogs/brendan/2013/01/11/virtualization-performance-zones-kvm-xen/>

At Joyent we run a high-performance public cloud based on two different
virtualization technologies: Zones and KVM. We have historically run Xen
as well, but have phased it out for KVM on SmartOS. My job is to make
things go fast, which often means using DTrace to analyze the kernel,
applications, and those virtualization technologies. In this post I'll
summarize their performance in four ways: characteristics, block
diagrams, internals, and results.

### TPM Key Migration in Solaris

-   <https://blogs.oracle.com/DanX/entry/tpm_key_migration_in_solaris>

\"TPM\" stands for \"Trusted Platform Module,\" a hardware device that
provides many security functions, including storage of encryption keys.
\[\...\] In Solaris 11.1, Wyllys Ingersoll added the ability to migrate
keys from one TPM to another TPM. TPM migration is is especially useful
when upgrading hardware, migrating a system to a new platform, or after
reinitializing the TPM. The following describes this new feature.

### What\'s new in User Rights Management

-   <https://blogs.oracle.com/gfaden/entry/what_s_new_in_user>

Way back in Solaris 8 we introduced an extensible database,
user\_attr(4), where we could maintain the security attributes of each
user. Originally the database included just three properties: roles,
auths, and profiles. \[\...\] Since then we have been adding new
properties in each Solaris release, while preserving backward
compatibility in both the file /etc/user\_attr and the corresponding
LDAP schema. To avoid dealing with an alphabet full of new options, we
standardized on the -K option, which can be used to set the values of
any property.

### LDoms IO Best Practices

-   <https://blogs.oracle.com/cmt/resource/downloads/ldoms.io.best.practices.pdf>

Stefan Hinker discuss different IO options for both disk and networking
and give some recommends on how you to choose the right ones for your
environment. A couple hints about performance are also included.

### T4 & the Red Crypto Stack

-   <https://blogs.oracle.com/cmt/resource/downloads/t4.red.crypto.stack-en.pdf>

Heinz-Wilhelm Fabry and Stefan Hinker show how to use encryption and
other security mechanisms throughout the red stack to deploy a quite
well secured database.

### Why Oracle CEO Larry Ellison Is So Bullish on Sun Hardware

-   <http://www.forbes.com/sites/oracle/2013/01/11/why-oracle-ceo-larry-ellison-is-so-bullish-on-sun-hardware/>

In the past decade, Oracle has acquired about 90 companies. And out of
all of those deals, Larry Ellison recently cited the 2009 acquisition of
Sun Microsystems as "the most strategic and profitable acquisition
Oracle has ever made."

### ZFS Compression Enhancements in illumos

-   <http://gdamore.blogspot.com/2013/01/zfs-compression-enhancements-in-illumos.html>

The ZFS code in illumos has gained another differentiator thanks to the
hard work of Saso, who integrated LZ4 improved compression into our code
base.

### Solaris 11 IPS Concepts, Issues, and Workarounds

-   <https://blogs.oracle.com/Solaris11Life/entry/solaris_11_and_ips_potential>

Image Packaging System (IPS) is a single tier packaging architecture
which in Oracle Solaris 11, and other Oracle Sun products such as Oracle
Solaris Cluster 4.x, replaces the previous SVR4-based dual tier
packaging and patching architecture.

### Oracle Delivers On SPARC Promises

-   <http://blogs.forrester.com/richard_fichera/13-01-13-oracle_delivers_on_sparc_promises>

In a nutshell, SPARC/Solaris customers are looking at a predictable
long-term future of improved and very competitive performance and
price-performance scaling for Oracle hardware, especially when coupled
with Oracle software in its "engineered systems." Oracle and IBM will
remain in a strong competition for performance and price-performance
across a variety of workloads, and competitive pressures will ensure a
decade of strong systems platforms for high-end UNIX workloads
regardless of the eventual fate of HP's Itanium platforms.

### How to Configure a Failover Guest Domain in an Oracle Solaris Cluster

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-configure-failover-guest-1890750.html>

The configuration described in this article enables the protection of
guest domains from planned and unplanned downtime by automating the
failover of a guest domain through restart on an alternate cluster node.
Automated failover provides protection in case there is a component
outage or the guest domain needs to be migrated for preventive
maintenance.

### Compliance reporting with SCAP

-   <https://blogs.oracle.com/darren/entry/compliance_reporting_with_scap>

In Solaris 11.1 we added the early stages of our (security) Compliance
framework. We have (like some other OS vendors) selected to use the SCAP
(Security Content Automation Protocol) standard from NIST. There are a
number of different parts to SCAP but for Compliance reporting one of
the important parts is the OVAL (Open Vulnerability Assesment Language)
standard. This is what allows us to write a checkable security policy
and verify it against running systems.

### Fujitsu launches \'Athena\' Sparc64-X servers in Japan

-   <http://www.theregister.co.uk/2013/01/25/fujitsu_racle_athena_sparc64_x_servers/>
-   <http://www.lemagit.fr/technologie/datacenter-technologie/serveurs-datacenter/2013/01/25/fujtsu-lance-les-serveurs-oracle-sparc-m10/>

Japanese IT giant and long-time Sparc partner with Sun Microsystems and
now Oracle has let slip the details on its \"Athena\" line servers based
on its own sixteen-core Sparc64-X processors, which bear the same
code-name inside Fujitsu. And it looks like Oracle is going to be
reselling them, too.

### Evaluating Oracle Solaris 11 from Inside Oracle VM VirtualBox

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/evaluating-s11-inside-vmv-1900902.html>

This article describes how to evaluate Oracle Solaris 11---without
having to install it on the bare metal---by importing it into Oracle VM
VirtualBox, configuring it, exploring basic administrative tasks, and
connecting to the network.

### grub2 : configuration de la console série

-   <http://www.gloumps.org/article-grub2-configuration-de-la-console-serie-114899043.html>

Depuis Solaris 11 update 1, le chain loader utilisé pour les plateforme
x86 est GRUB2. Le fichier de configuration présent dans GRUB (menu.lst)
est remplacé par un nouveau fichier nommé grub.cfg. L\'édition de ce
fichier est normallement déconseillé, du coup la mise à jour s\'effectue
via la commande bootadm.

### How to Use Signed Kernel Modules

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/signed-kernel-modules-1891111.html>

Loadable kernel modules allow you to add code to a running Linux kernel.
Oracle\'s Unbreakable Enterprise Kernel provides signed kernel modules
to further protect the kernel. This article explains how to use this
feature.

### Création pas-à-pas des repos sous Solaris 11

-   <http://www.gloumps.org/article-creation-pas-a-pas-des-repos-sous-solaris-11-114928364.html>

Ci-joint la procédure de création pas-à-pas des principales repo pour
Solaris 11. Rien de plus simple\... vous allez voir !

### Deep Inside Every Sysadmin Is\...

-   <https://blogs.oracle.com/OTNGarage/entry/deep_inside_every_sysadmin_is>

\... an Oracle ACE! The thrills. The glory. The fame. Who can resist?
Turns out sysadmins can.

### Updating and patching Oracle Linux using yum and Ksplice

-   <https://blogs.oracle.com/linux/entry/updating_and_patching_oracle_linux>

Oracle Linux provides two complimentary technologies for patching and
updating the operating system. \[\...\] This is where Ksplice enters the
picture. It is a technology that allows you to apply critical fixes to
the Linux kernel at run time, without the need to reboot your system.
This is a feature that is unique to Oracle Linux.
