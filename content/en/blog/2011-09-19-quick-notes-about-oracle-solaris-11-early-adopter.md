---
date: "2011-09-19T09:53:01Z"
tags:
- Oracle
- System
title: Quick Notes About Oracle Solaris 11 Early Adopter
---

The Oracle Solaris 11 Early Adopter release is available for some days
by now. This EA release provides access to the final (complete)
functionality which will be delivered in Oracle Solaris 11 GA. Although
I only played with it for a few days, here are my very, very first notes
about things I found interesting to mention, in no particular order.

-   I noticed that the Oracle Solaris 11/11 release (and not EA) was
    mentioned in one of the subsections of the provided draft for the
    documentation. Was this inadvertently forgotten\... on purpose? ;)
-   The support for Flash Archives seems to have finally disappeared. I
    know about the *Distribution Constructor* argument, but a `flar(1M)`
    (as an `mksysb(1)` for AIX) definitely has a special place in the
    Solaris ecosystem (particularly for crash recovery scenario).
-   The `-x` option has been removed from the `vi(1)` command (among
    others), and is now replaced by the use of the `encrypt(1)` command.
    I know a place where she will be missed: you know who you are :)
-   It seems that the `network-boot-arguments` command is now supported
    to be able to set IP configuration directly from the OBP, just in
    case a DHCP server is not an option to get this information at
    installation time (as we can do on IBM AIX using the IPL
    configuration from the SMS menu).
-   Automated Installer is now able to install Zones along with the main
    system.
-   New utilities are provided to help migrating JumpStart configuration
    files to AI manifests (I did not use them yet though).
-   RBAC things have changed a little, for example the provided profiles
    are now defined under different files under the
    `/etc/security/prof_attr.d` directory instead of a single file
    (`/etc/security/prof_attr`) before that (even in Solaris 11
    Express). More, there is no *Primary Administrator* profile anymore,
    but a new *System Administrator* profile which doesn\'t have some
    security privileges the old profile has (can not read the
    `/etc/shadow` file for example).
-   The `useradd(1m)` command has well evolved. This utility is now able
    to automatically create a dedicated ZFS dataset as the home
    directory (which is not a directory anymore :)) if the `-d` flag is
    given, to populate the `/etc/auto_home` file, and to enable to
    `autofs` service to serve the `/home` content automatically as
    needed.
-   Although the default shell is now `bash(1)` (why not the newly
    integrated `ksh93(1)`?), the default PATH seen in OpenSolaris
    releases and Solaris 11 Express, which used to set GNU tools in
    front of SYSV commands, is reverted back to a more classical and
    fully functional paths: `/usr/bin:/usr/sbin`. At least the `ls -v`
    is OK again by default. Nonetheless, the path `/usr/gnu/bin` is here
    for whoever is interested.
-   An interesting change is the motivation to put out some old and well
    known configuration file. For better or for worse, the
    `/etc/nodename` is dead in Solaris 11. It is replaced by a property
    of a new SMF. So in order to change the nodename of a host, you must
    now do:

        # svccfg -s node setprop config/nodename = "mynewnodename"
        # svcadm refresh node

-   In the same vein, the `/etc/default/init` is replaced by a SMF too.
    The SMF is named `system/environment:init`, and the corresponding
    environment properties are `environment/LANG`, `environment/LC_*`,
    and `environment/TZ`.
-   If you want to be able to manually configure the network, you have
    to disable NWAM, to change the active Network Configuration Profiles
    (NCP) and enable traditional configuration:

        # netadm enable -p ncp DefaultFixed

-   The old `sys-unconfig(1m)` command is now replaced by a more
    powerful `sysconfig(1m)` utility which can unconfigure or
    reconfigure a Solaris instance, and generate a configuration profile
    which can be used to configure a system, or a Zone (exit the
    `sysidcfg` file).
-   Shares (NFS, SMB) are now supported inside a non-global zone.
-   The default networking mode is switched to exclusive-IP.
-   Similarly as can be found for SRM and privileges configuration
    settings with automatic Resources Pools, a VNIC can now be
    automatically instantiated for the time the Zone is booted, and
    automatically removed when she shuts down.
-   A new mode for the Zone known as Read-Only permits to create some
    instance which may be more or less writable, i.e. some parts may not
    be changed (configuration, file systems, etc.).
-   The IPS packages are now automagically updated in each Zones using
    Boot Environments.
-   Last point in this quick entry, the default locale positioned is
    en\_US.UTF-8, and not just the old C. Well, not a big deal, but I
    found some tools which have issued some warnings against this locale
    such as `expect` for example.

So, I think that Solaris 11 is getting better, even from a Solaris 11
Express experience standpoint. Some choices are surprising, but the
overall seems coherent and works as expected. A more longer experience
in real user cases will be necessary to judge of this (very big)
release, but I am mostly pleased with the direction taken by Solaris,
and I am exited to put all of this new stuff in production!
