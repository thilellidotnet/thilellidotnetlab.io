---
date: "2007-04-27T20:42:14Z"
tags:
- Nanoblogger
- Patch
title: Adding a New Plugin Using the NanoBlogger Framework
---
--- templates/main_index.htm Wed Jul 6 14:49:05 2005
+++ templates/main_index.htm.hardwritten Mon Aug 8 13:12:05 2005
@@ -29,6 +29,10 @@
$NB_Entries
&lt;/div&gt;

+&lt;div class=&quot;side&quot;&gt;
+All content herein © 2006, Your Company, Inc. All right reserved.
+&lt;/div&gt;
+
&lt;div id=&quot;menu&quot;&gt;
$NB_PageLinks
&lt;/div&gt;
</pre></li>
<li>Or writing a little <strong>plugin</strong> to do that... (based on the
<code>plugins/fortune.sh</code> code)
<pre>
# cat &lt;&lt; EOF &gt; ${NB_INST_PATH}/plugins/copyright.sh
# NanoBlogger Copyright plugin.

# sample code for templates, based off default stylesheet
#
# &lt;div class=&quot;side&quot;&gt;
# $NB_Copyright
# &lt;/div&gt;

PLUGIN_OUTFILE=&quot;$BLOG_DIR/$PARTS_DIR/copyright.$NB_FILETYPE&quot;

nb_msg &quot;generating copyright ...&quot;
echo '&lt;div class=&quot;copyright&quot;&gt;' &gt; &quot;$PLUGIN_OUTFILE&quot;
echo 'All content herein © 2006, Your Company, Inc. All right reserved.' &gt;&gt; &quot;$PLUGIN_OUTFILE&quot;
echo '&lt;/div&gt;' &gt;&gt; &quot;$PLUGIN_OUTFILE&quot;
NB_Copyright=$(&lt; &quot;$PLUGIN_OUTFILE&quot;)
EOF
$
$ diff -u templates/main_index.htm templates/main_index.htm.plugin
--- templates/main_index.htm Wed Jul 6 14:49:05 2005

Discussing how to add a copyright notice at each page automatically on
the NanoBlogger [mailing
list](http://groups.yahoo.com/group/nanoblogger/message/550 "Inserting a site-wide Copyright notice"),
here are the two propositions i can think of.

It can be done by adding directly the copyright notice in the desired
template files (i.e. `templates/main_index.htm`, etc.):

1.  Either writing it in the corresponding files\...

        $ diff -u templates/main_index.htm templates/main_index.htm.hardwritten
        +++ templates/main_index.htm.plugin Mon Aug 8 13:12:29 2005
        @@ -29,6 +29,10 @@
        $NB_Entries
        </div>

        +<div class="side">
        +$NB_Copyright
        +</div>
        +
        <div id="menu">
        $NB_PageLinks
        </div>

The advantage of the second proposition is the capability to add a
general feature using the
[NanoBlogger](http://nanoblogger.sourceforge.net/ "Small weblog engine for the UNIX command line")
framework, and the use of the CSS to modify the new *copyright* class.
