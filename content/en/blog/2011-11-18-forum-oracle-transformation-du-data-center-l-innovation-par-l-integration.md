---
date: "2011-11-18T17:29:39Z"
tags:
- Oracle
- Press
title: 'Forum Oracle : Transformation du Data Center, l''innovation par l''intégration'
---

I had the great opportunity to assist to the french event "Forum Oracle
: Transformation du Data Center, l\'innovation par l\'intégration",
which took place in Paris last Tuesday, November 8, 2011.

Follow the slides corresponding to this event.

### Title: Changing the Game by Simplifying I.T.

*Speaker: Olivier Brot, Senior Sales Director Hardware BU Country
Leader*

URL:
<http://www.oracle.com/us/dm/h2fy11/1-olivier-brot-intro-1368628.pdf>
(fr) (1,5 MB)

### Title: The Oracle Story

*Speaker: John Abel, Chief Technology Architect; EMEA Server and
Storage*

URL:
<http://www.oracle.com/us/dm/h2fy11/2-john-abel-oraclestory-1368637.pdf>
(en) (5,8 MB)

### Title: Datacenter Transformation with Oracle Engineered Systems

*Speaker: Dario Wiser, Head of Datacenters & Servers, HW Business
Development EMEA*

URL: <http://www.oracle.com/us/dm/h2fy11/3-dct-keynote-1368643.pdf> (en)
(2,7 MB)

### Title: Comment construire votre Cloud avec Oracle ?

*Speaker: Eric Bezille, CTO Oracle HW Business Unit France*

URL:
<http://www.oracle.com/us/dm/h2fy11/4-eric-bezille-dct-pvt-cloud-key-1368654.pdf>
(fr) (1,3 MB)

### Title: Oracle Exadata Database Machine & Oracle Exalogic

*Speaker: Denis Martin, Exadata Business Development Manager; Antonio
Ferreira, Architect Software*

URL:
<http://www.oracle.com/us/dm/h2fy11/5-denismartin-ant-fer-exa-exalogic-1368666.pdf>
(en) (3,5 MB)

### Title: New Engineered Solutions : BigData et Exalytics pour anticiper l'explosion de nos données

*Speaker: Pascal Guy, Architecte groupe EMEA Expert Server*

URL:
<http://www.oracle.com/us/dm/h2fy11/6-pascal-guy-new-eng-sol-1368670.pdf>
(en) (1,5 MB)

### Title: Déploiements rapides, performants et sécurisés des applications avec la nouvelle génération des systèmes SPARC

*Speaker: Jean-Yves Migeon; EMEA Oracle HW Business Development; Eric
Bezille*

URL:
<http://www.oracle.com/us/dm/h2fy11/7-ericbezille-jym-supercluster-1368680.pdf>
(fr) (2,6 MB)

### Title: Transformative Oracle Storage Solutions For Datacenter Consolidation, Virtualization and Cloud

*Speaker: Jacques Villain, Principal Sales Consultant, EMEA Long Term
Storage TEAM*

URL:
<http://www.oracle.com/us/dm/h2fy11/8jacques-transformative-storage-1368634.pdf>
(en) (2,5 MB)

### Title: Présentation ALTARES: Optimisation du Datacenter

*Speaker: M. Christophe Le Caignec, Directeur Système et Sécurité
Informatique, ALTARES*

URL:
<http://www.oracle.com/us/dm/h2fy11/9altares-c-lecaignec-1368648.pdf>
(fr) (0,2 MB)

### Title: Realize The Full Potential of Your Application Infrastructure with Oracle's Virtualized Systems

*Speaker: Christophe PAULIAT, Sales Consultant, Hardware BU*

URL:
<http://www.oracle.com/us/dm/h2fy11/10-christophe-servervirtualization-1368683.pdf>
(en) (1,2 MB)

### Title: Total Cloud Control with Oracle EM 12c

*Speaker: Alain Scazzola, Business Development Manager*

URL:
<http://www.oracle.com/us/dm/h2fy11/11alain-scazzola-em12c-1368695.pdf>
(en) (4,4 MB)
