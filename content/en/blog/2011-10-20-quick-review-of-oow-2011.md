---
date: "2011-10-20T16:50:13Z"
tags:
- Oracle
- Press
title: Quick Review Of OOW 2011
---

Here is a quick review on the Oracle OpenWorld 2011:

### OOW Oracle San Fransisco 2011

*OpenWorld Content Catalog*

-   <https://oracleus.wingateweb.com/scheduler/eventcatalog/eventCatalog.do>

If you missed the show or want to review content you saw during the
sessions, you can now download many of the presentations from the
OpenWorld site here. This site is open to the public, not just OpenWorld
attendees. So even if you weren\'t able to join us in San Francisco, you
can go download PDFs of the content that was presented.

### First days of OOW

-   <http://sparcv9.blogspot.com/2011/10/first-days-of-oow.html>

### More news from OOW 2011

-   <http://sparcv9.blogspot.com/2011/10/more-news-from-oow-2011.html>

### A new dawn for SPARC

-   <http://sparcv9.blogspot.com/2011/10/new-dawn-for-sparc.html>

### SPARC presentations from OOW

-   <http://sparcv9.blogspot.com/2011/10/sparc-presentations-from-oow.html>

### Oracle Previews Oracle Solaris 11 at Oracle OpenWorld

*Prepares for Planned Release of Oracle Solaris 11, the \#1 Enterprise
OS -- Built for Clouds, Later This Year*

-   <http://www.oracle.com/us/corporate/press/512668>

![solaris11.5.png](/blog/solaris11.5.jpg "solaris11.5.png, Oct 2011")

Solaris 11 is the operating system for Oracle's recently announced SPARC
SuperCluster T4-4 engineered system and Oracle's SPARC T4 server line.
It also powers the Oracle Exadata Database Machine X2-2 and X2-8
systems, as well as Oracle Exalogic Elastic Cloud.

Oracle Solaris is developed, tested and supported as an integrated
component of Oracle\'s \"applications-to-disk\" technology stack, which
includes the Oracle Certification Environment, representing over 50,000
test use cases for every Oracle Solaris patch and platform released.

Oracle Solaris provides customers with the most choice in supported
enterprise applications with over 11,000 third-party applications on a
wide range of SPARC and x86 systems.

![solaris11.6.png](/blog/solaris11.6.jpg "solaris11.6.png, Oct 2011")

Oracle Solaris 11 guarantees binary compatibility with previous Oracle
Solaris versions, through the Oracle Solaris Binary Application
Guarantee Program and development compatibility between SPARC and x86
platforms, providing customers with a seamless upgrade path and the
industry's best investment protection.

### Oracle\'s Larry Ellison unveils \'Exalytics\' in-memory machine

-   <http://www.computerworlduk.com/news/infrastructure/3307668/oracles-larry-ellison-unveils-exalytics-in-memory-machine>

\"We\'re determined to deliver best-of-breed in every aspect of our
computing architecture,\" Ellison said. \"We\'re in the business of
catching up \[with IBM\] in the microprocessor business. If we don\'t
pass them we\'ll be very, very close. If our microprocessor is the same
speed and we move data a hundred times faster than they do, I like our
chances in the marketplace.\"

From the beginning, Oracle\'s design goals for its systems were the
highest performance for the lowest cost, Ellison said. \"For a given
task, it will cost you less on an Exadata than it would on a plain old
commodity server.\"

The main idea was a \"parallel everything\" architecture, with various
set of components working in unison for more power and reliability, he
said. \"These machines should never, ever fail,\" he said. \"Hardware
breaks. Software breaks too. But if you have a parallel architecture you
should be tolerant of those failures.\"

Meanwhile, faster chips aren\'t the best way to make software run
faster, because the real bottleneck is storage, according to Ellison.
Database performance is \"about moving data, and not doing arithmetic on
a microprocessor,\" he said.

Overall, \"we move data around a hundred times faster than anyone else
in this business,\" Ellison claimed. Ellison cited a series of companies
such as Proctor & Gamble, BNP Paribas and AFG, which experienced vast
performance and cost savings through Exadata. Some 1,000 Exadata
machines have been installed and 3,000 more will be sold this year,
Ellison said.

### Oracle enters BI with Exalytics appliance

-   <http://www.zdnet.co.uk/news/enterprise-apps/2011/10/03/oracle-enters-bi-with-exalytics-appliance-40094079/>

The Exalytics appliance, revealed by Oracle chief Larry Ellison on
Sunday, is designed to run business intelligence analytics at high
speeds via a terabyte of DRAM for in-memory computing.

\"\[Exalytics is\] hardware and software engineered together to deliver
data analysis at the speed of thought,\" Ellison said in a keynote
speech at Oracle OpenWorld in San Francisco. \"Everything runs faster if
you keep it in DRAM, if you keep it in main memory.\"

Although storage costs are notably volatile, DRAM costs around \$10
(£6.40) a gigabyte, compared with flash at around \$1 a gigabyte and
hard disk at 4 cents. However, DRAM has the advantage of allowing data
to be processed and the results sent to the consumer at speeds that are
orders of magnitude faster than data stored in other technologies.

### Fast, Safe, Cheap : Pick 3

-   <http://blogs.oracle.com/roch/entry/fast_safe_cheap_pick_3>

Today, we\'re making performance headlines with Oracle\'s ZFS Storage
Appliance:

1.  SPC-1 : Twice the performance of NetApp at the same latency; Half
    the \$/IOPS.
2.  2X the absolute performance, 2.5X cheaper per SPC-1 IOPS, almost 3X
    lower latency, 30% cheaper per user GB with room to grow\... So, If
    you have a storage decision coming and you need, FAST, SAFE, CHEAP :
    pick 3, take a fresh look at the ZFS Storage appliance.

![ZFSSA.1.png](/blog/ZFSSA.1.jpg "ZFSSA.1.png, Oct 2011")\
![ZFSSA.2.png](/blog/ZFSSA.2.jpg "ZFSSA.2.png, Oct 2011")

We are announcing that Oracle\'s 7420C cluster acheived 137000 SPC-1
IOPS with an average latency of less than 10 ms. That is double the
results of NetApp\'s 3270A while delivering the same latency. As
compared to the NetApp 3270 result, this is a 2.5x improvement in
\$/SPC-1-IOPS (2.99\$/IOPS vs \$7.48/IOPS). We\'re also showing that
when the ZFS Storage Appliance runs at the rate posted by the 3270A
(68034 SPC-1 IOPS), our latency of 3.26ms is almost 3X lower than theirs
(9.16ms). Moreover, our result was obtained with 23700 GB of user level
capacity (internally mirrored) for 17.3 \$/GB while NetApp\'s , even
using a space saving raid scheme, can only deliver 23.5\$/GB. This is
the price per GB of application data actually used in the benchmark. On
top of that the 7420C still had 40% of space headroom whereas the 3270A
was left with only 10% of free blocks.
