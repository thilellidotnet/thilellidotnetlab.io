---
date: "2008-03-12T17:46:22Z"
tags:
- SVM
- GRUB
- Boot
- Failsafe
title: Update A Corrupted GRUB Boot Archive, With SVM
---

In a [previous
discussion](/post/2008/03/09/Update-A-Corrupted-GRUB-Boot-Archive-Without-SVM)
about the GRUB boot archive and how it can be regenerated, I mentioned
that it will not be as easy as it can be when the root file system use
the `md` driver. I will now show two different methods to do the same
thing when the root file system is build upon a SVM mirror (RAID-1):

1.  Unmirror the root file system only.
2.  Unmirror the entire system, i.e. all devices.

*Note: Although this test case was done using Solaris 10 8/07 under a
virtual machine build upon [VirtualBox](http://www.virtualbox.org/) on
latest Solaris Express Community Edition, the instructions must be valid
for Solaris 10 1/06 and later.*

### Initial setup

As we can see, the system use only a root file system, and a swap
device. Both are encapsulated with SVM.

    # df -k -F ufs
    Filesystem     kbytes    used   avail capacity  Mounted on
    /dev/md/dsk/d0 6147798 3455578 2630743      57%  /
    # swap -l
    swapfile             dev  swaplo blocks   free
    /dev/md/dsk/d1      85,1       8 4194288 4194288
    # metastat -c d0 d1
    d0               m  6.0GB d10 d20
        d10          s  6.0GB c0d0s0
        d20          s  6.0GB c1d1s0
    d1               m  2.0GB d11 d21
        d11          s  2.0GB c0d0s1
        d21          s  2.0GB c1d1s1

### Unmirror the root file system only

The idea is to boot on the GRUB Failsafe mode, select the first side of
the mirror, and modify the `system` and `vfstab` configuration files to
use the correct device path. For the `system` file, this means to
actually remove the `rootdev:/pseudo/md@0:0,0,bl` entry, not just
comment it. For the `vfstab` file, this means replacing the root file
system metadevice path `/dev/md/[r]dsk/d0` by the first underlying
device path, i.e. `/dev/[r]dsk/c0d0s0`. Last, regenerate the boot
archive on the alternate root path.

    [...]
    Booting to milestone "milestone/single-user:default".
    Configuring devices.
    Searching for installed OS instances...
    /dev/dsk/c0d0s0 is under md control, skipping.
    /dev/dsk/c1d1s0 is under md control, skipping.
    No installed OS instance found.

    Starting shell.
    # fsck /dev/rdsk/c0d0s0
    # mount -F ufs /dev/dsk/c0d0s0 /a
    # cp /a/etc/system /a/etc/system.bckp
    # cp /a/etc/vfstab /a/etc/vfstab.bckp
    # TERM=vt100 vi /a/etc/system
    # TERM=vt100 vi /a/etc/vfstab
    # bootadm update-archive -R /a
    # umount /a
    # fsck /dev/rdsk/c0d0s0
    # reboot

Then, boot into `milestone/multi-user:default` level and detach the
second half of the mirror, since the first half correspond to the valid
and updated underlying device. Next, restore the original configuration
files which refers to the encapsulated metadevices, and reboot.

    # df -k -F ufs
    Filesystem            kbytes    used   avail capacity  Mounted on
    /dev/dsk/c0d0s0      6147798 3458810 2627511    57%    /
    # swap -l
    swapfile             dev  swaplo blocks   free
    /dev/md/dsk/d1      85,1       8 4194288 4194288
    # metastat -c d0
    d0               m  6.0GB d10 d20
        d10          s  6.0GB c0d0s0
        d20          s  6.0GB c1d1s0
    # metadetach d0 d20
    d0: submirror d20 is detached
    # metastat -c d0
    d0               m  6.0GB d10
        d10          s  6.0GB c0d0s0
    # cp /etc/system.orig /etc/system
    # cp /etc/vfstab.orig /etc/vfstab
    # shutdown -y -i 6 -g 0

After the reboot, just reattach the second half of the mirror, and wait
for complete synchronization to be fully redundant again.

    # df -k -F ufs
    Filesystem            kbytes    used   avail capacity  Mounted on
    /dev/md/dsk/d0       6147798 3458714 2627607    57%    /
    # swap -l
    swapfile             dev  swaplo blocks   free
    /dev/md/dsk/d1      85,1       8 4194288 4194288
    # metattach d0 d20
    d0: submirror d20 is attached
    # metastat -c d0
    d0               m  6.0GB d10 d20 (resync-29%)
        d10          s  6.0GB c0d0s0
        d20          s  6.0GB c1d1s0

### Unmirror the entire system, i.e. all devices

The idea is exactly the same as for unmirroring the root file system
only, but adapting the `vfstab` file to change the swap entry, too. (So,
I didn\'t reproduce the code listing here.)

Then, boot into `milestone/single-user:default` level modifying the
corresponding GRUB entry as follow:
`kernel /platform/i86pc/multiboot -s`. Completely delete all the
metadevices and `metadb` configurations to clear SVM settings. Last,
continue into `milestone/multi-user:default` level to boot unmirrored.

    # metaclear -f -r d0 d1
    # metadb -f -d  c1d0s4 c1d0s4
    # ^D

Now, the system must be fully encapsulate by SVM again. Please refer to
online [Sun
Documentation](http://docs.sun.com/app/docs/doc/816-4520/tasks-mirrors-21?a=view),
or some past entries on this subject, depending on the system\'s
architecture: [SPARC
systems](/post/2005/06/06/Encapsulation-of-the-Systems-Disk-Using-SVM),
or [x86
platforms](/post/2007/05/30/RAID-1-Volume-From-the-root-File-System-Using-SVM-on-x86-Platform).
