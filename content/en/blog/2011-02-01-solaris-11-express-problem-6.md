---
date: "2011-02-01T10:05:56Z"
tags:
- Bug
- Network
title: 'Solaris 11 Express: Problem #6'
---

*In this series, I will report the bugs or problems I find when running
the Oracle Solaris 11 Express distribution. I hope this will give more
visibility on those PR to Oracle to correct them before the release of
Solaris 11 next year.*

Well, this one is not a really a bug per-se, but it is really annoying
nonetheless. The new interface where most network things are managed
through `dladm` and `ipadm` is really interesting because this leads to
a single point of management for the stack IP. Most things works pretty
well, unless setting the mode and speed of an interface, were we always
need to use the old `ndd` command, or the system kernel configuration
file, or set parameters in the specific interface configuration file, as
in old days on old Solaris releases.

Here is an example of such annoying behavior when hard setting a mode
and speed for an interface is required:

    # dladm show-link
    LINK        CLASS     MTU    STATE    BRIDGE     OVER
    eri0        phys      1500   up       --         --

    # dladm show-phys
    LINK         MEDIA                STATE      SPEED  DUPLEX    DEVICE
    eri0         Ethernet             up         100    half      eri0

    # dladm show-linkprop -p duplex eri0
    LINK         PROPERTY        PERM VALUE          DEFAULT        POSSIBLE
    eri0         duplex          r-   full           full           half,full

As you can see, the auto-negotiation leads to an unusual 100Mbps
half-duplex mode. And because the associated link property is read-only,
the only way to put the link at 100Mbps full-duplex is to set it another
way as before Solaris 11:

    # ndd -set /dev/eri0 adv_100fdx_cap 1
    # ndd -set /dev/eri0 adv_autoneg_cap 0

    # cat << EOF >> /etc/system
    set eri:adv_autoneg_cap=0
    set eri:adv_100fdx_cap=1
    set eri:adv_100hdx_cap=0
    set eri:adv_10fdx_cap=0
    set eri:adv_10hdx_cap=0
    EOF

As I said previously it is not a bug, but is is really a pity it not
possible to use the new way to set these parameters.
