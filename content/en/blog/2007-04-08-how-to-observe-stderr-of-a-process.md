---
date: "2007-04-08T19:25:04Z"
tags:
- Utility
title: How to Observe stderr of a Process
---

\"How to observe stderr of a process\" is a recently asked question seen
on the the `observability-discuss` forum on opensolaris.org, ending
with:

> I\'ve tried \"truss\", but it outputs only part of the buffers being
> written.

As always, a simple answer show up very quickly from a community member,
in this case James Carlson:

> Have you tried the \"-w\" option on truss? \"truss -t\\!all -w2\"
> should do what you\'re asking.

Feel good to have lots of nice people on these lists!
