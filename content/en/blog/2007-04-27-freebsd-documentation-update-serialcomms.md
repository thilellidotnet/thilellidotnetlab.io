---
date: "2007-04-27T18:11:30Z"
tags:
- PR
title: 'FreeBSD Documentation Update: serialcomms'
---

Although it was a long time ago, i think it may be worth to be noted
here, just in case someone is interested. I work hard (at least, i hope
;)) to update the *Serial Communications* chapter of the FreeBSD
Handbook in the past months, in particular the *Introduction* section.
The work was heavily done with [Gary W.
Swearingen](mailto:garys%20at%20freebsd%20dot%20org) which take the PR
for himself, helped by his mentor [Giorgos
Keramidas](mailto:keramida%20at%20freebsd%20dot%20org) (not less), and
the participation of [Yar
Tikhiy](mailto:yar%20at%20freebsd%20dot%20org).

This work was done under the FreeBSD problem report
[docs/85355](http://www.freebsd.org/cgi/query-pr.cgi?pr=85355 "PR docs/85355").

For reference only, here are most links to the different steps which
drive us to the current committed code, as found in the documentation
project source tree:

1.  [chapter.sgml.orig](/blog/chapter.sgml.orig)
2.  [chapter.sgml.diff.0](/blog/chapter.sgml.diff.0)
3.  [chapter.sgml.diff.1](/blog/chapter.sgml.diff.1)
4.  [chapter.sgml.diff.2](/blog/chapter.sgml.diff.2)
5.  [chapter.sgml.diff.3](/blog/chapter.sgml.diff.3)
6.  [chapter.sgml.diff.4](/blog/chapter.sgml.diff.4)

So, here it is: [Serial
Communications](http://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/serialcomms.html "Serial Communications, in the FreeBSD Handbook")
chapter.
