---
date: "2007-04-08T19:40:21Z"
tags:
- Resource Management
- Zone
title: Zones plus Resource Management Features in SX
---

Containers (in fact Zones plus Resource Management features) is a
wonderful technology, particularly in virtualized environments. But the
fact is that it is not very usable\--speaking of the use of RM feature
in particular\--since it exists many different RM technologies, not
always well integrated with Zones capability.

This is why the upcoming work on `memory sets` and `cpu caps` are so
interesting, and a more-than-welcome work in order to enhance and ease
the use and deployment of Containers on powerful Solaris systems.

Enjoy reading Jerry Jelinek\'s [blog
entry](http://blogs.sun.com/jerrysblog/entry/containers_in_sx_build_56 "Containers in SX build 56")
about this work (already in SXCE build 56!).
