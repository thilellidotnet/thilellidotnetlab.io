---
date: "2007-04-27T18:25:43Z"
tags:
- man
title: About Installing Manual Pages Under AIX 5L
---

### History

Installing manual pages under an OS seems trivial at first, but may be
problematic, especially when using the 5.X series of AIX.

In fact, there is two reasons for this:

1.  First, IBM change the corresponding filesets name and number at each
    revision, sometime less (for example a new AIX distribution CDROM
    labeled AIX 5L - Version 5.3 - 5765-G03 - ML 5300-03, 10/2006 -
    LCD4-7463 - LCD4-7489-02 is very different from one labeled AIX 5L -
    Version 5.3 - 5765-G03 - ML 5300-03, 10/2006 - LCD4-7463-02 -
    LCD4-7489-02);
2.  Secondly, IBM is going to push all kind of documentation through
    their InfoCenter product line, a web based interface using Eclipse
    (also accessible on the IBM web site).

### Installing manual pages under AIX 5.3ML3

Installing the manual pages is finally very simple: get the first and
the third installation\'s CDs for AIX 5.3.ML3, and install the following
filesets:

    infocenter.man.EN_US.commands
    infocenter.man.EN_US.files
    infocenter.man.EN_US.libs

Eventually, don\'t forget to (re)create the `windex` database that is
used by `whatis(1)` and the `man(1) -f` and `-k` options:

    # catman -w

### External reference links

-   Google Groups post:\
    [Install man pages on AIX
    5.3?](http://groups.google.com/group/comp.unix.aix/browse_thread/thread/b0ab1834a5887937/)
-   Paragraph on the Subscription service: Bulletons on ibm.com:\
    [Installation of man Command Content for
    AIX](http://www14.software.ibm.com/webapp/set2/subscriptions/pqvcmjd?mode=18&ID=2304#Man)
