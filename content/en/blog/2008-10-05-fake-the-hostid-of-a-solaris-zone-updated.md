---
date: "2008-10-05T12:30:35Z"
tags:
- DTrace
- Zone
- Hostid
- Resource Management
title: Fake The hostid Of A Solaris Zone, Updated
---

As a little follow-up to [Fake The hostid Of A Solaris
Zone](/post/2006/06/17/Fake-the-hostid-of-a-Solaris-Zone), and regarding
the
[discussion](http://www.opensolaris.org/jive/thread.jspa?threadID=77246)
on the capacity to change the `hostid` of a Solaris non-global zone, it
is interesting to mention these (updated) informations:

1.  The `LD_PRELOAD` trick proposed before is not a proper option, and
    is really ugly (and intrusive if you didn\'t unset it before
    continuing the execution of a program).
2.  When using Solaris 8 or Solaris 9 Containers, there is a feature
    called [Host ID
    Emulation](http://docs.sun.com/app/docs/doc/820-4490/gfrzp?l=en&a=view)
    from the `zonecfg` utility which can do exactly that.
3.  Before the introduction of the privileges in a non-global zone with
    Solaris 11/06 (a.k.a. Solaris Update 3), you must run the DTrace
    `zhostid` script (daemon) within the global zone. It is not
    mandatory to run it from the global zone anymore. Using the
    appropriate `dtrace_user` privilege only, you can run it directly
    from the non-global zone:

        # zonecfg -z ngzone set limitpriv=default,dtrace_user
        # zoneadm -z ngzone boot
        # zlogin ngzone
        [Connected to zone 'ngzone' pts/5]
        Last login: Sat Oct  4 18:57:17 on pts/5
        Sun Microsystems Inc.   SunOS 5.11      snv_99  November 2008
        # /sbin/zonename 
        ngzone
        # /usr/bin/hostid
        837d47dd
        # ./zhostid &
        [1] 21506
        # /usr/bin/hostid
        20a82f32
        # ^D
        [Connection to zone 'ngzone' pts/5 closed]
