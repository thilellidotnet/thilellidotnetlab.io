---
date: "2010-04-26T12:49:55Z"
tags:
- Oracle
- Zone
- Scheduler
title: Problem Starting OCCSD In A Non-Global Zone
---

If you are not able to start the Oracle Cluster Synchronization Services
Daemon (OCCSD) in a non-global zone on Solaris 10, I bet you are running
Oracle 10.2.0.3 and higher. In this case, you will see something similar
in the the `/var/adm/messages` file\--but nothing is coming up:

    Apr 26 10:39:51 zonename oracle: [ID 702911 user.error] Oracle Cluster Synchronization Service starting by user request.
    Apr 26 10:39:52 zonename root: [ID 702911 user.error] Cluster Ready Services completed waiting on dependencies.

Trying to trace the `ocssd.bin` process during start-up give you
something similar to:

    [...]
    12564:   0.0803 setrlimit(RLIMIT_CORE, 0xFFFFFFFF7FFFF900)      = 0
    12564:   0.0804 priocntlsys(1, 0xFFFFFFFF7FFFF694, 6, 0xFFFFFFFF7FFFF768, 0) Err#1 EPERM [proc_priocntl]
    12564:   0.0810 fstat(2, 0xFFFFFFFF7FFFE870)                    = 0
    12564:   0.0811 brk(0x100229E80)                                = 0
    12564:   0.0813 brk(0x10022DE80)                                = 0
    12564:   0.0815 fstat(2, 0xFFFFFFFF7FFFE740)                    = 0
    12564:   0.0816 ioctl(2, TCGETA, 0xFFFFFFFF7FFFE7AC)            Err#25 ENOTTY
    12564:   0.0818 write(2, " s e t p r i o r i t y :".., 52)      = 52
    12564:   0.0821 _exit(100)

So, in this case you just hit a privilege restriction, which did not
apply before with older release of Oracle. As clearly mentioned in the
output of `truss`, the `proc_priocntl` is not available in the
non-global zone for use by Oracle. A clean solution, available only with
Solaris 10 11/06 (U3) and later, is to use the `limitpriv` configuration
property to extend the basic privileges provided by the zone framework.

As stated in the `privileges(5)` man page:

> PRIV\_PROC\_PRIOCNTL Allow a process to elevate its priority above its
> current level. Allow a process to change its scheduling class to any
> scheduling class, including the RT class.

Interestingly, this seems to be exactly the case for the Oracle Cluster
Synchronization Services Daemon:

    # zonecfg -z zonename set limitpriv=default,proc_priocntl
    # zoneadm -z zonename reboot
    # zlogin zonename "ps -o class,args -p `pgrep ocssd.bin`"
     CLS COMMAND
      RT /soft/oracle/10.2.0/asm_1/bin/ocssd.bin

Ok, that\'s fine right now.
