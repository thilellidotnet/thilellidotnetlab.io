---
date: "2010-03-25T11:05:14Z"
tags:
- Zone
- Network
title: Prevent A Non-Global Zone Reaching Others
---

When using non-global zones, the network stream didn\'t leave the global
zone. Although very interesting when looking for performance for
multi-tiers applications hosted on non-global zones from the same
system, it can be a problem when it comes to segregate different
networks used by the different non-global zones.

To my knowledge, IP Filter can be use from the global zone to help in
this case. But a more cleaner approach would be to block (reject) the
route between those non-global zones. For example, if one non-global
zone has an IP address of `addrX`, and the second non-global zone has an
address of `addrY`, then the following commands will prevent network
traffic from passing between the two zones.

    # route add addrX addrY -interface -reject
    # route add addrY addrX -interface -reject

The problem is, when there is a lot of non-global zones you need to
segregate, you need to add 2\^n routes, which represents 32 routes for 5
non-global zones\... Not very scalable, and not manageable. If someone
know a better solution, please feel free to comment this post.
