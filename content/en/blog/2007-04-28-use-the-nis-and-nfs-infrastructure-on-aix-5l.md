---
date: "2007-04-28T09:53:16Z"
tags:
- NIS
- NFS
- Service
- Network
title: Use the NIS and NFS Infrastructure on AIX 5L
---

Here are the steps to be able to use the current NIS and NFS
infrastructure from an AIX server:

    # cat /etc/resolv.conf  
    domain          dev.example.com
    nameserver      10.239.208.24
    nameserver      10.251.140.96
    search          dev.example.com int.example.com prod.example.com
    #
    # TERM=vt220 smitty
    /*
     * Communications Applications and Services
     *  TCP/IP
     *   Further Configuration
     *    Name Resolution
     *     Hosts Table (/etc/hosts)
     *      Add a Host
     *       INTERNET ADDRESS (dotted decimal)               [10.254.234.22]
     *       HOST NAME                                       [neptune.dev.example.com]
     *       ALIAS(ES) (if any - separated by blank space)   [neptune]
     *       COMMENT (if any - for the host entry)           [NIS server for domain devex]
     *  NFS
     *   Network Information Service (NIS)
     *    Configure / Modify NIS
     *     Change NIS Domain Name of this Host
     *      Domain name of this host                        [devex]
     *     Configure this Host as a NIS Client
     *      NIS server - required if there are              [neptune]
     *   Network File System (NFS)
     *    Configure NFS on This System
     *     Start Automounter
     *      PARAMETERS to be used for the automount daemon  [-n]
     */

Launch the automountd at run-level \#2:

    # cat << EOF > /etc/rc.d/rc2.d/Sautomountd
    #!/usr/bin/env ksh
    #################################################################
    # name: {K|S}automountd
    # purpose: script that will start or stop the automountd service.
    #################################################################

    case "$1" in
    start)
      /usr/sbin/automount -n
      ;;
    stop)
      stopsrc -g autofs
      ;;
    *)
      echo "Usage: $0 {start|stop}"
      exit 1
    esac

    exit 0
    EOF
    # ln /etc/rc.d/rc2.d/Sautomountd /etc/rc.d/rc2.d/Kautomountd
    # chmod 754 /etc/rc.d/rc2.d/?automountd

In the same time, modify the `automountd` service to add some arguments
that must be passed to the program. This is a necessary step to be able
to automount the correct remote path using our
[customized](/post/2005/08/29/How-to-Set-Up-the-NFS-Server-for-the-Use-of-the-Automounter-Capability "How to Set Up the NFS Server, ...")
`autofs` server. Here is how to do so:

    # chssys -s automountd -a "-DOSNAME=`uname -s` -DCPU=`uname -p` -DNATISA=`bootinfo -K` -DOSREL=`uname -v`.`uname -r`"
    # stopsrc -g autofs
    # /usr/sbin/automount -n

**Very important**

To resolve information correctly, it was needed to *explicitly* specify
the ordering of name resolution and hosts setting in `/etc/netsvc.conf`.
This file corresponds to `/etc/nsswitch.conf` under Solaris, GNU/Linux
or the BSDs for hosts name resolution. For example:

    # cat << EOF >> /etc/netsvc.conf
    hosts = local, nis, bind
    EOF
