---
date: "2007-04-08T19:53:36Z"
tags:
- Patch
title: Patching a Solaris 10 Single-System
---

In order to be able to access Software Updates via the official Sun
server, you need to register your system using a valid account. You can
use the `sconadm` command to do so. Note that this one is the command
line version of the `/usr/bin/updatemanager` program.

First, prepare a registration profile, enabling authenticated web proxy
support:

    # cat /tmp/registrationprofile.properties
    userName=aaaaaaaa
    password=bbbbbbbb
    hostName=cccccccc
    subscriptionKey=
    portalEnabled=false
    proxyHostName=dd.ee.ff.gg
    proxyPort=hhhh
    proxyUserName=iiiiiiii
    proxyPassword=jjjjjjjj

Then, be sure to have correct permissions and mode on the profile
(especially since passwords are stored in clear text). Last register the
system:

    # chown root:root /tmp/registrationprofile.properties && \
      chmod 400 /tmp/registrationprofile.properties
    #
    # sconadm register -a -r /tmp/registrationprofile.properties
    sconadm is running
    Authenticating user ...
    finish registration!
    #
    # rm /tmp/registrationprofile.properties

You can now configure the `sconadm` CLI utility to use the authenticated
web proxy configuration permanently, as follow:

    # smpatch get
    patchpro.backout.directory      -       ""
    patchpro.baseline.directory     -       /var/sadm/spool
    patchpro.download.directory     -       /var/sadm/spool
    patchpro.install.types          -       rebootafter:reconfigafter:standard
    patchpro.patch.source           -       https://getupdates1.sun.com/
    patchpro.patchset               -       current
    patchpro.proxy.host             -       ""
    patchpro.proxy.passwd           ****    ****
    patchpro.proxy.port             -       8080
    patchpro.proxy.user             -       ""
    #
    # smpatch set patchpro.proxy.host=dd.ee.ff.gg
    # smpatch set patchpro.proxy.port=hhhh
    # smpatch set patchpro.proxy.user=iiiiiiii
    # smpatch set patchpro.proxy.passwd=jjjjjjjj
    #
    # smpatch get
    patchpro.backout.directory      -               ""
    patchpro.baseline.directory     -               /var/sadm/spool
    patchpro.download.directory     -               /var/sadm/spool
    patchpro.install.types          -               rebootafter:reconfigafter:standard
    patchpro.patch.source           -               https://getupdates1.sun.com/
    patchpro.patchset               -               current
    patchpro.proxy.host             dd.ee.ff.gg     ""
    patchpro.proxy.passwd           ****            ****
    patchpro.proxy.port             hhhh            8080
    patchpro.proxy.user             iiiiiiii        ""

So you can now easily analyze your system and see what patches are
published online and ready to be applied:

    # smpatch analyze
    [...]
    118855-36 SunOS 5.10_x86: kernel patch
    119999-02 SunOS 5.10_x86: arp, ip, ipsecah drivers patch
    124923-02 SunOS 5.10_x86: ld.so.1 patch
    122033-03 SunOS 5.10_x86: Update timezones patch
    125012-01 SunOS 5.10_x86: sendmail patch

At this time, you can choose to download and apply them all. I some
updates required a (clean) reboot to be effective, `smpatch` will tell
you how to do properly:

    # smpatch update
    Update 122033-03 will not be downloaded since it already exists in the download
     directory.
    Update 124207-02 will not be downloaded since it already exists in the download
     directory.
    118844-30 has been validated.
    118855-36 has been validated.
    119999-02 has been validated.
    124923-02 has been validated.
    125012-01 has been validated.
    Installing patches from /var/sadm/spool...
    NOTICE: Update 118844-30 cannot be applied at this time since it is typed as
     "single user, reconfig immediate" which is disallowed by installation policy.
    NOTICE: Patch 118844-30 cannot be installed until the next system shutdown.
    NOTICE: Update 122033-03 cannot be applied at this time since it is typed as
     "single user" which is disallowed by installation policy.
    NOTICE: Patch 122033-03 cannot be installed until the next system shutdown.
    NOTICE: Update 124207-02 cannot be applied at this time since it is typed as
     "single user, reconfig immediate" which is disallowed by installation policy.
    NOTICE: Patch 124207-02 cannot be installed until the next system shutdown.
    /var/sadm/spool/patchpro_dnld_2007.01.31@13:10:10:CET.txt has been moved to
    /var/sadm/spool/patchproSequester/patchpro_dnld_2007.01.31@13:10:10:CET.txt

    ID's of the updates that are disallowed by installation policy have been
    written to file
            /var/sadm/spool/disallowed_patch_list

    One or more updates that you installed requires a system shutdown to activate
     it. To initiate the system shutdown, you must use one of the following
     commands:
    o Drop to the firmware prompt - init 0 or shutdown -i 0
    o Power down the system - init 5 or shutdown -i 5
    o Restart the system - init 6 or shutdown -i 6

The bad thing is that this tool doesn\'t always as expected\...
particularly on older releases. Please refer to Matty\'s
[entries](http://prefetch.net/blog/index.php?s=smpatch "Blog O' Matty")
on this subject for more information.
