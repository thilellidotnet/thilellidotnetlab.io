---
date: "2007-05-08T13:51:39Z"
tags:
- Nanoblogger
title: Not the NanoBlogger Port Maintainer Anymore
---

Well, since I didn\'t use
[nanoblogger](http://nanoblogger.sourceforge.net/) anymore, I decided to
[resign](http://www.freebsd.org/cgi/query-pr.cgi?pr=ports/112302) as its
FreeBSD port maintainer.

And because a bad news doesn\'t come alone, it seems that nanoblogger
will [not get much
interest](http://nanoblogger.sourceforge.net/archives/2007/04/19/not_so_nano_anymore/)
in the future. I really enjoy used it for two years now, but because of
recent
[changes](/post/2007/04/28/Moving-away-thnet-resources-from-self-made-self-hosted-solutions)
with the thnet project, I can\'t provide help with it anymore.
