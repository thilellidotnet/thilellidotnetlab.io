---
date: "2007-04-13T21:46:26Z"
tags:
- GUSES
title: OpenSolaris Day at Sun Tech Days - Paris, France
---

The fact is I failed to mention I had the opportunity to assist to the
free sessions given late March at the [OpenSolaris
Day](http://fr.sun.com/sunnews/events/2007/mar/techdays/) at Sun Tech
Days, at Paris La Défense. It was a very pleasant moment, with really
[nice
presentations](http://www.opensolaris.org/os/community/advocacy/events/techdays/paris/)
by great well-known speakers. I was particularly interested (and
impressed) by "The Zen of Free" and "New Security Features in
OpenSolaris" presos, respectively given by [Simon
Phipps](http://www.webmink.net/), and [Darren J.
Moffat](http://blogs.sun.com/darren/). I *knew* them from OpenSolaris
mailing lists, and enjoyed to be able to see them in real life.

The end of the days was punctuate by a French talk given by Bruno
Bonfils which, with Laurent Blume, are the leaders of the French
OpenSolaris User Group ([FOSUG](http://fr.opensolaris.org/)).

I now hope to be available to see the first [data center in a
box](http://www.sun.com/events/st/), code-named Project Blackbox\... at
Paris [Quai de
Bercy](http://fr.sun.com/sunnews/events/2007/may/blackbox-tour/agenda_details.jsp?cc=fr)
this time.

As a last note, you can read what William Bonnet (from
[GUSES](http://www.guses.org/), "Groupe d'Utilisateurs du Système
d'Exploitation Solaris") [have to
say](http://www.wbonnet.net/2007/05/12/de-retour-des-techdays-2007/) on
the first two days of this particular event.
