---
date: "2007-12-23T14:04:35Z"
tags:
- System
title: Tuning Is Evil
---

Each month, I hear many coworkers or specific application management
teams asking about putting some system tunings in place, even on very
recent operating system releases. All the time. Most of these settings
comes from the Internet, are found in forum posts, or articles related
to a subsystem, or in technical publications. And some of them comes
from third party software providers, or editors. A very, very few
settings are proposed or recommended by system administrators, or by
knowledgeable people in tuning area.

The problem is that, most of the time, these tunings are related to
another release of the operating system, are not updated to keep current
with the Best Of Practices for a given OS release, or simply are not
well understood and not applicable without affecting (badly) current
running environments. More, already present tunings are reported as-is
on upgraded and fresh installed systems without more thinking, or
without be assured these are always applicable (or obsolete) and what
are the new defaults (if not dynamic). One of the most representative
example today of this is the new [System V IPC
facilities](http://docs.sun.com/app/docs/doc/817-0404/6mg74vs92?a=view#chapter1-33)
found from the GA Solaris 10, and later, operating system, where some
Oracle DBAs always ask SA team for shared memory settings as found on
Solaris 8 systems.

Although extract from the [Solaris Internals and Performance
FAQ](http://www.solarisinternals.com/) for ZFS, here is a great excerpt
we all must read carefully and try to keep in mind when modifying
default behavior of a system:

> Tuning is evil and should not be done\...in general.
>
> First, consider that the default values are set by the people who know
> most things about the effects of the tuning. If a better value exists,
> it would be the default. While alternative values might help a given
> workload, it could quite possibly degrade some other aspects of
> performance. Maybe, catastrophically so.
>
> Over time, tuning recommendations might become stale at best or might
> lead to performance degradations. Customers are leery of changing a
> tuning that is in place and the net effect is a worse product than
> what it could be. Moreover, tuning enabled on a given system might
> spread to other systems, where it might not be warranted at all.
