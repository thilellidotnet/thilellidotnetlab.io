---
date: "2007-04-27T20:21:42Z"
tags:
- NIS
- NFS
- Network
title: Use the NIS and NFS Infrastructure on Red Hat Advanced Server 2.1
---
--- /etc/init.d/autofs.orig     Thu Aug 25 13:12:38 2005
+++ /etc/init.d/autofs  Thu Aug 25 17:25:47 2005
@@ -67,7 +67,10 @@
 # We can add local options here
 # e.g. localoptions='rsize=8192,wsize=8192'
 #
-localoptions=''
+localoptions=&quot;-DOSNAME=`uname -s` \
+              -DCPU=x86 \
+              -DNATISA=32 \
+              -DOSREL=`uname -r | awk -F\. '{print $1\&quot;.\&quot;$2}'`&quot;
 
 # Daemon options
 # e.g. --timeout 60
# service autofs restart
</pre>
<p>Verify if all is ok:</p>
<pre>
# service autofs status
Configured Mount Points:
------------------------
--------------------
/usr/sbin/automount /Soft yp auto.soft -ro,hard,bg,intr -DOSNAME=Linux -DCPU=x86 -DNATISA=32 -DOSREL=2.4
/usr/sbin/automount /NTFS yp auto.nt -DOSNAME=Linux -DCPU=x86 -DNATISA=32 -DOSREL=2.4
/usr/sbin/automount /Home yp auto.home -rw,hard,bg,intr -DOSNAME=Linux -DCPU=x86 -DNATISA=32 -DOSREL=2.4
/usr/sbin/automount /Apps yp auto.apps -ro,hard,bg,intr -DOSNAME=Linux -DCPU=x86 -DNATISA=32 -DOSREL=2.4
/usr/sbin/automount /- yp auto.direct -DOSNAME=Linux -DCPU=x86 -DNATISA=32 -DOSREL=2.4
</pre>

Here are the steps to be able to use the current NIS and NFS
infrastructure from a Linux server.

### NIS

Be sure to resolve the NIS servers (slave and/or master) for the *int*
domain name:

    # egrep "nasty|bigup" /etc/hosts
    192.168.4.74           nasty
    192.168.4.23           bigup

Configure the NIS client:

    # cat << EOF >> /etc/yp.conf
    domain int server nasty
    domain int server nasty
    EOF
    # grep NIS /etc/sysconfig/authconfig /etc/sysconfig/network
    /etc/sysconfig/authconfig:USENIS=yes
    /etc/sysconfig/network:NISDOMAIN=int

### NFS

The NFS part is relatively simple since the autofs maps is looked up in
the NIS maps (already managed by the corresponding boot script\'s
service).

So, it is just needed to modify the `automountd` service to add some
arguments that must be passed to the program. This is a necessary step
to be able to automount the correct remote path using our
[customized](/post/2005/08/29/How-to-Set-Up-the-NFS-Server-for-the-Use-of-the-Automounter-Capability "How to Set Up the NFS Server, ...")
`autofs` server. Here is how to do so.

Check the configuration of the run-level informations for the `autofs`
service:

    # chkconfig --list autofs
    autofs          0:off   1:off   2:off   3:on    4:on    5:on    6:off

Modify the initial service configuration and reload it:

    # diff -u /etc/init.d/autofs.orig /etc/init.d/autofs
    /usr/sbin/automount /Soft yp auto.soft -ro,hard,bg,intr -DOSNAME=Linux        -DCPU=x86        -DNATISA=32        -DOSREL=2.4
    /usr/sbin/automount /NTFS yp auto.nt  -DOSNAME=Linux        -DCPU=x86        -DNATISA=32        -DOSREL=2.4
    /usr/sbin/automount /Home yp auto.home -rw,hard,bg,intr -DOSNAME=Linux        -DCPU=x86        -DNATISA=32        -DOSREL=2.4
    /usr/sbin/automount /Apps yp auto.apps -ro,hard,bg,intr -DOSNAME=Linux        -DCPU=x86        -DNATISA=32        -DOSREL=2.4
    /usr/sbin/automount /- yp auto.direct  -DOSNAME=Linux        -DCPU=x86        -DNATISA=32        -DOSREL=2.4

    Active Mount Points:
