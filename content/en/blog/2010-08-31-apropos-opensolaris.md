---
date: "2010-08-31T09:45:59Z"
tags:
- Oracle
title: Apropos OpenSolaris
---

As a follow-up to the [About Solaris](/post/2010/08/29/Apropos-Solaris)
part, and as everybody know by now the OpenSolaris project has [evolved
recently](http://sstallion.blogspot.com/2010/08/opensolaris-is-dead.html),
sort of. I really didn\'t have time to write about this, but because
others have done a really good job at express themselves on this
subject, I will aggregate some of them here since the whole opinion of
them all summarize mine pretty well.

About the leaked (?) information itself:

-   [OpenSolaris R.I.P.: The Day is Finally
    Here.](http://www.cuddletech.com/blog/pivot/entry.php?id=1140)
-   [Some thoughts about the mail on osol-discuss and the reactions to
    that\...](http://www.c0t0d0s0.org/archives/6842-Some-thoughts-about-the-mail-on-osol-discuss-and-the-reactions-to-that-....html)

About the position on the end of the OpenSolaris per-se:

-   [The liberation of
    OpenSolaris](http://dtrace.org/blogs/bmc/2010/08/19/the-liberation-of-opensolaris/)
-   [Illumos Shines New
    Light](http://www.cuddletech.com/blog/pivot/entry.php?id=1138)

About the succession:

-   [The Hand May Be
    Forced](http://gdamore.blogspot.com/2010/08/hand-may-be-forced.html)
-   [A new light](http://ptribble.blogspot.com/2010/08/new-light.html)
-   [The future of
    Solaris](http://dtrace.org/blogs/ahl/2010/08/27/the-future-of-solaris/)

Overall thoughts, which tend to describe very well my mood after
thinking about this subject as a whole:

-   [Oracle, OpenSolaris and
    Acceptance](http://blog.sysdroid.com/2010/08/oracle-opensolaris-and-acceptance/)
-   [OpenSolaris dead as
    distribution](http://sparcv9.blogspot.com/2010/08/opensolaris-dead-as-distribution.html)
