---
date: "2007-04-27T21:08:26Z"
tags:
- Nanoblogger
- Patch
title: Updating the FreeBSD's Port of NanoBlogger to Release 3.2.3
---

After deciding to use the NanoBlogger tool to maintain my little space
on the web, i had updated the
[corresponding](http://www.freshports.org/www/nanoblogger/ "www/nanoblogger port")
`ports(7)` on [FreeBSD](http://www.freebsd.org/ "The Power To Serve"),
currently orphaned at the time of this writing.

The all in one [patch](/blog/nanoblogger.patch)
produced was
[committed](http://www.freebsd.org/cgi/query-pr.cgi?pr=83033 "Problem Report ports/83033")
by [Pav Lucistnik](mailto:pav%20at%20freebsd%20dot%20org), many thanks
to him.

*As a side note*, it may be pointed out that this port include a local
FreeBSD port\'s patch in order to bypass a little bug when creating a
new weblog in release greater than 3.2.1 (and so present in 3.2.3). See
bug report
[1232755](http://sourceforge.net/tracker/index.php?func=detail&aid=1232755&group_id=103576&atid=635240 "[ 1232755 ] Little bug when creating a new weblog in release 3.2.3")
for more information about this issue.
