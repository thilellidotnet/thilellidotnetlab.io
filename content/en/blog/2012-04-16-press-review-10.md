---
date: "2012-04-16T15:51:51Z"
tags:
- Press
title: 'Press Review #10'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### IOPS and latency are not related - HDD performance explored

-   <http://blog.richardelling.com/2012/03/iops-and-latency-are-not-related-hdd.html>

Today, we routinely hear people carrying on about IOPS-this and
IOPS-that. Mostly this seems to come from marketing people: 1.5 million
IOPS-this, billion IOPS-that. Right off the bat, a billion IOPS is not
hard to do, the metric lends itself rather well to parallelization\...

### How to Use the Power Management Controls on SPARC Servers

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/pwr-mgmt-sparc-cmt-1563618.html>

SPARC T-Series systems have power-saving features designed into the
hardware and software. These features allow you to reduce server power
consumption, which leads to a cost reduction for environmental cooling
and reduced power usage by other infrastructure components. The SPARC
T-Series power management (PM) interfaces make it easy to manage these
PM features.

### Solaris: What comes next?

-   <https://blogs.oracle.com/alanc/entry/solaris_what_comes_next>

As you probably know by now, a few months ago, we released Solaris 11
after years of development. That of course means we now need to figure
out what comes next - if Solaris 11 is "The First Cloud OS", then what
do we need to make future releases of Solaris be, to be modern and
competitive when they\'re released? So we\'ve been having planning and
brainstorming meetings, and I\'ve captured some notes here from just one
of those we held a couple weeks ago with a number of the Silicon Valley
based engineers.

### Solaris 11 features: nscfg

-   <http://www.c0t0d0s0.org/archives/7452-Solaris-11-features-nscfg.html>

As you may have noticed many configuration tasks around name services
have moved into the SMF in Solaris 11. However you don\'t have to use
the svccfg command in order to configure them, you could still use the
old files. However you can\'t just edit them, you have to import the
data into the SMF repository. There are many reasons for this need but
the ultimate one is in the start method. I will explain that later. In
this article i want to explain, how nscfg can help you with with the
naming service configuration of your system.

### New Oracle VM Hardware Certifications

-   <https://blogs.oracle.com/virtualization/entry/new_oracle_vm_hardware_certifications>

We\'ve received inquiries from the community on certification of Oracle
VM 3.0 on HP Proliant systems. We\'re pleased to update that we\'ve
recently completed certification of the HP Proliant systems for Oracle
VM 3.0.

### Oracle's SPARC SuperCluster is now Supported by SAP

-   <http://www.oracle.com/us/solutions/sap/infrastructure/sparc-supercluster-for-sap-1559291.pdf>

Oracle's SPARC SuperCluster now runs the Oracle Database, the SAP
central instance, application or web server, and Oracle Enterprise
Manager management software along with all your SAP applications.

### POWER, AMD, Itanium, and SPARC

-   <http://netmgt.blogspot.fr/2012/04/power-amd-itanium-and-sparc.html>

The addition of compression engines (in the T5), in addition to the
well-know crypto engines in the SPARC T Series will be a welcome
capability addition for general purpose computing. Fewer proprietary
crypto cards, proprietary network devices with crypto engines, and
proprietary disk arrays (sporting compression, encryption, and dedup)
will be needed - to achieve outstanding performance of general purpose
applications running under SPARC.

### How the SPARC T4 Processor Optimizes Throughput Capacity: A Case Study

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/t-series-latency-1579242.pdf>

Latency is the time delay between when a request is sent and the moment
the result of the request is delivered. Such delays can be found between
many components within a computer system and can have a significant
impact on performance.

In this paper, the focus is on instruction-level latencies, which are
usually expressed in processor cycles. Many instructions have a fixed
latency, but there are also instructions with a latency that depends on
the runtime conditions or data type format.

### AIX to Oracle Solaris 11 Evaluation

-   <http://www.oracle.com/technetwork/server-storage/solaris11/overview/aix-mapping-guide-1566108.html>

The following guide gives an overview of some of the technologies
included in Oracle Solaris 11 and the direct benefit you can get by
using some of these features. This guide also provides a similar
technology mapping, where possible, between IBM AIX and Oracle Solaris
11, so that administrators with knowledge in the former can kick start
their learning experience if planning deploy the latter.

### dtrace.conf(12) wrap-up

-   <http://dtrace.org/blogs/ahl/2012/04/09/dtrace-conf12-wrap-up/>

For the second time in as many quadrennial dtrace.confs, I was impressed
at how well the unconference format worked out. Sharing coffee with the
DTrace community, it was great to see some of the oldest friends of
DTrace --- Jarod Jenson, Stephen O'Grady, Jonathan Adams to name a few
--- and to put faces to names --- Scott Fritchie, Dustin Sallings, Blake
Irvin, etc --- of the many new additions to the DTrace community. You
can see all the slides and videos; these are my thoughts and notes on
the day.

### Ksplice: Kernal Update Without Reboot

-   <http://netmgt.blogspot.fr/2012/04/ksplice-kernal-update-without-reboot.html>

Operating Systems normally comprise two distinct layers: the kernel and
the user space. Normally, updating the kernel would require a reboot, so
the OS can apply a new kernel module. Operating Systems like Solaris
created a mechanism called \"live update\" to update OS Kernel, OS User
Space, or even third-party applications (not to mention provide
rollback) with merely a reboot. Oracle Solaris 11 facilitates virtually
unlimited patch/rollback cycles leveraging ZFS. The new Ksplice tool
from Oracle allows for Linux to get closer to Solaris uptime
requirements by providing for kernel updates without reboot, leaving OS
User Space and Applications to normal reboot or application restart
cycles.

### How to Avoid Your Next 12-Month Science Project

-   <http://constantin.glez.de/blog/2012/04/how-avoid-your-next-12-month-science-project>

Could it be there\'s more going on behind the scenes than merely putting
together a bunch of servers, a storage array and an InfiniBand network
into a rack? Let\'s explore some of the special sauce that makes
Exalogic unique and un-copyable, so you can save yourself from your next
6- to 12-month science project that distracts you from doing real work
that adds value to your company.

### How to Update Oracle Solaris 11 Systems Using Support Repository Updates

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/o11-018-howto-update-s11-1572261.html>

Oracle Solaris 11 includes a new package management system that greatly
simplifies the process of managing system software helping to reduce the
risk of operating system maintenance, including planned and unplanned
system downtime. Image Packaging System (IPS) takes much of the
complexity out of software administration with its ability to
automatically calculate dependencies, and it merges both the package and
patch management into a single administrative interface.

This article steps through updating an Oracle Solaris 11 system with
software packages that are provided with an active Oracle support
agreement. In the process, it covers some of the basics that you should
know to ensure an update goes successfully and safely.

### IBM Launches Hybrid, Flexible Systems Into The Data Center

-   <http://www.itjungle.com/tfh/tfh041612-story01.html>

It takes a little time and a lot of money to roll out a new server
architecture, and even a company as large as IBM can\'t do it very
often. The System/360 in 1964. The System/38 in 1979 and its follow-on,
the AS/400, in 1988. The RS/6000 in 1990. The BladeCenter in 2002, and
the Sequent-inspired clustered server nodes in the xSeries and pSeries
in the mid-2000s. iDataplex in 2008. And now the PureSystem converged
infrastructure launched last week, in 2012.

### OmniOS builds on Illumos to make a complete operating system

-   <http://omnios.omniti.com/>

\"OmniOS is our vision of what OpenSolaris could have been had it
remained in the open. It runs better, faster and has more innovations,\"
continued Schlossnagle. \"OmniTI did not want to lose the benefits that
OpenSolaris technologies brought to customers, so we decided to pursue
the continuation of the OS on our own. \[\...\]\"

### Latency and I/O Size: Cars vs Trains

-   <http://blog.richardelling.com/2012/04/latency-and-io-size-cars-vs-trains.html>

A legacy view of system performance is that bigger I/O is better than
smaller I/O. This has led many to worry about things like \"jumbo\"
frames for Ethernet or setting the maximum I/O size for SANs. Is this
worry justified? Let\'s take a look\...

### PureSystems Giving You Job Jitters? The Doctor is In!

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/AIXDownUnder/entry/puresystems_job_jitters_the_doctor_is_in25>

Will these IBM PureSystems put me out of a job?

After all, you\'ve had a successful career configuring systems, tuning
them, rebuilding them, finding performance bottlenecks, writing scripts,
and juggling high user expectations with a budget that would starve a
mouse. Now there are these new systems that are configured in the blink
of an eye, tune themselves and (you may be thinking), don\'t need you.

Well, before you sign the extinction certificate and set up the world\'s
first museum for experienced AIX sys admins, have a think about the
number of reasons your career is not doomed as of 11 April 2012, the day
the new IBM PureSystems were launched.

### How I Decide Which Virtualization Technology to Use

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/which-virt-1592222.html>

When the time comes to update your hardware, some workloads that were
comfortably ensconced in their own hardware need to be consolidated onto
systems with virtual environments. But how do you choose between the
various methods of virtualization? For instance, Oracle Solaris lets you
create a virtual network. And virtual storage. You can allocate memory
and CPUs to workloads in interesting ways. Oracle Solaris Zones
(previously called Oracle Solaris Containers) lets you virtualize entire
systems. You also have different hypervisors to choose from. And what
about the hardware virtualization options in SPARC and x86 platforms \--
how do they add to your options, and when should you use them?

### Performability Analysis

-   <http://blog.richardelling.com/2012/04/performability-analysis.html>

Modern systems are continuing to evolve and become more tolerant to
failures. For many systems today, a simple performance or availability
analysis does not reveal how well a system will operate when in a
degraded mode. A performability analysis can help answer these questions
for complex systems. In this blog, an updated version of an old blog
post on performability, I\'ll show one of the methods we use for
performability analysis.

### How to Live Install from Oracle Solaris 10 to Oracle Solaris 11 11/11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-solaris-live-install-1599365.html>

First, you create a set of ZFS send archives---golden image---on an
Oracle Solaris 11 11/11 system that is the same model as your Oracle
Solaris 10 system. Then you install this golden image on an unused disk
of the system running Oracle Solaris 10 to enable it to be rebooted into
Oracle Solaris 11 11/11. The basic system configuration parameters from
the Oracle Solaris 10 image are stored and applied to the Oracle Solaris
11 11/11 image.

### Get Ready to Change your Job

-   <http://constantin.glez.de/blog/2012/04/get-ready-change-your-job>

As new IT concepts like virtualization, Engineered Systems, Cloud
Computing, DevOps, new services, patterns and languages emerge, they
force IT organizations to re-think and adapt roles, responsibilities and
jobs to the new reality. Change is a constant in IT, and the current
times are likely to see a lot more change than we have seen before.

### Enable DTrace hooks in GENERIC

-   <http://lists.freebsd.org/pipermail/svn-src-all/2012-April/052347.html>

This commit enables DTrace in FreeBSD-10 GENERIC kernel!
