---
date: "2007-04-28T09:45:45Z"
tags:
- management
- Password
title: Password Management for Hosts in the Demilitarized Zone (DMZ)
---

Based on existing procedures, here is a new tool which aim is to help
managing centralized passwords for all servers hosted in the
demilitarized zone. As for the [Static Route Management for Hosts in the
Demilitarized Zone
(DMZ)](/post/2005/06/29/Static-Route-Management-for-Hosts-in-the-Demilitarized-Zone-DMZ),
this script is managed using the `cvs(1)` concurrent management system.

Follow is the named script:

-   `dmz_passwd.sh` this one is able to get and set some password
    configurations and/or parameters remotely

Assuming that the environment variables `${CVSROOT}` and `${CVS_RSH}`
are properly sets, here are little samples of usage:

    # cvs checkout -P dmz_passwd && cd dmz_passwd
    #
    # sh ./dmz_passwd.sh
    usage: dmz_passwd.sh [-hd] [-s servername,...] [-n min_day] [-x max_day] [-w warn_day] -u username,... {status|set|unlock|lock|reset}
    #
    # sh ./dmz_passwd.sh -s unic -u greg set
    * server: unic
     => *set* password attributes and policy for user: greg
    #
    # sh ./dmz_passwd.sh -s unic,beastie -u greg status
    * server: unic
     => *get* password attributes and policy for user: greg
        .password status: locked [LK]
        .last changed: 13 September 2004
        .minimum between password changes: 0 day(s)
        .maximum between password changes: 91 day(s)
        .warning before the password expires: 7 day(s)
        .password will expire at: 13 December 2004

    * server: beastie
     => *get* password attributes and policy for user: greg
        .password status: passworded or locked [PS]

Need more help?\... See the command line help switch:

    # sh ./dmz_passwd.sh -h
