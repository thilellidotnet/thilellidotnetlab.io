---
date: "2007-04-07T16:09:04Z"
tags:
- Boot
- ZFS
title: ZFS Recent News
---

Well. More than a real blog entry, this post is more about keeping in
touch with some recent add-ons in ZFS area. First, you can read the [ZFS
Overview and
Guide](http://www.sun.com/bigadmin/features/articles/zfs_overview.jsp "ZFS Overview and Guide")
just published on BigAdmin. Second, you must watch the excellent
[Thumper do it
yourself](http://blog.tukker.org/2007/03/22/solaris-zfs-thumper-do-it-yourself/ "Solaris ZFS / Thumper do it yourself"),
which is a very nice showcase of ZFS use. Third, a great listing of
recent
[add-ons](http://milek.blogspot.com/2007/03/latest-zfs-add-ons.html " Latest ZFS add-ons")
put in latest SXCE builds is available at Robert Milkowski\'s blog.

Last, be sure to check Tim Foster explanations
[about](http://blogs.sun.com/timf/entry/zfs_bootable_datasets_happily_rumbling "ZFS Bootable datasets - happily rumbling")
the recently announced ZFS Boot support in build 62, for the x86
platform. All interesting links included. His script to set up ZFS root
automatically too! (Since all bits not yet well integrated\...)
