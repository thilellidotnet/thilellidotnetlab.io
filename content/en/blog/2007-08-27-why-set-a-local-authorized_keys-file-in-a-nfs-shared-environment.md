---
date: "2007-08-27T18:44:19Z"
tags:
- SSH
title: Why Set a Local authorized_keys File in a NFS Shared Environment
---

Why set the `authorized_keys` file to a local pathname on large UNIX
environments, especially when NFS shares are used for home directories?
Because this can address security problems.

First, you must remember that this special SSH file stores the public
key of a remote account, letting the owner to be able to log-in using
asymmetric keys along with the corresponding passphrase instead of the
more classical challenge with appropriate password mechanism. (This
eventually enable for non-interactive login through the use of an SSH
agent, latter.)

The default path for the `authorized_keys` file is in a subdirectory of
the home directory. This means that when the home of a UNIX account is
hosted on a NFS share, all servers available in the same domain as the
NFS resource will have access to the very same `authorized_keys` file,
thus opening a security flaw. This is a security concern since by
allowing one account on one server, you open this account to all servers
in the same domain.

So, the first benefit to store the `authorized_keys` file in a local
name space on each server is to authorize one\--and only one\--access to
a given machine. The direct drawback is that there will be as many
`authorized_keys` file as the number of servers in a domain (if a SSH
access is needed on all servers). A side effect is that the path, mode
and owner of the directory which will host the `authorized_keys` file
may be better managed and hardened than before (even if SSH already
check those things for sane defaults). It is particularly of interest
when managing thousands of servers in heterogeneous UNIX environment,
when Solaris, AIX, Linux and HP-UX doesn\'t have the same ownership same
system paths (such as `/var`, for example).
