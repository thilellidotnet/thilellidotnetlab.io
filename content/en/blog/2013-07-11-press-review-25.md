---
date: "2013-07-11T08:12:51Z"
tags: []
title: 'Press Review #25'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### How to Get Started Using DTrace on Oracle Linux

-   <https://blogs.oracle.com/OTNGarage/entry/how_to_get_started_using>

We all hate slow code. Bunch of princesses is what we\'ve become. During
the American Civil War, they had to deliver their text messages by
horseback! It took weeks! And half the time, they got blown off their
horse by a cannonball to the neck!

Today? Today we have to have our stuff back in milliseconds, or we start
tweeting about it. So, if you\'re developing or deploying applications,
how do you keep them performing at the speed to which we have become
accustomed? DTrace, of course.

### What\'s up with LDoms: Part 7 - Layered Virtual Networking

-   <https://blogs.oracle.com/cmt/entry/what_s_up_with_ldoms7>

Back for another article about LDoms - today we\'ll cover some tricky
networking options that come up if you want to run Solaris 11 zones in
LDom guest systems. So what\'s the problem?

![](https://blogs.oracle.com/cmt/resource/ldoms.images/mac.tables3.png)

### Core files filling important filesystems? Want email alerts about each core dump?

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/core_files_filling_important_filesystems_want_email_alerts_about_each_core_dump>

I got asked these questions recently and had to go look the subject up
\... again! I seem to have forgotten some of the details and then I
thought I would use some new features of AIX for the second part. In the
distant past there was various way to stop core files being dumped in to
the current working directory of the program that failed. In AIX 5.3,
AIX 6 and 7, the \"chcore\" command does all the hard work for us by
letting us\...

### Availability Best Practices on Oracle VM Server for SPARC

-   <https://blogs.oracle.com/jsavit/entry/availability_best_practices_on_oracle>

This is the first of a series of blog posts on configuring Oracle VM
Server for SPARC (also called Logical Domains) for availability. This
series will show how to how to plan for availability, improve
serviceability, avoid single points of failure, and provide resiliency
against hardware and software failures.

### Detecting Outliers

-   <http://dtrace.org/blogs/brendan/2013/07/01/detecting-outliers/>

In computer performance, we're especially concerned about latency
outliers: very slow database queries, application requests, disk I/O,
etc. The term "outlier" is subjective: there is no rigid mathematical
definition.

![](http://dtrace.org/blogs/brendan/files/2013/06/frequencytrail_waterfall_maxsigma_synth50_color_500.png)

### How Oracle Solaris Makes Oracle Database Fast

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/sol-why-os-matters-1961737.html>

Is choosing an operating system all that\'s important any more? After
all, virtualization lets sysadmins choose the OS that best matches the
workload they plan to put on each virtual machine. And Red Hat Linux on
Intel is cheap and ubiquitous enough to work for most anything. Right?

We don\'t think so.

### National Security, the \"two man rule\" and Solaris

-   <https://blogs.oracle.com/jimlaurent/entry/national_security_the_two_man>

\[\...\] Luckily, Solaris 10 and 11 have all the tools to assist in
creating a \"two man rule.\" In fact, we published a paper on the topic
in 2005. Its comprehensive role and profile based collection of
authorizations ensure that only user with the proper authorizations are
allowed access to administrative tools. Solaris can be configured so
that one user has the role of \"Security Admin\" while another user has
the role of \"System Admin.\"

### How to Initialize an Oracle Linux System

*SUSE Linux to Oracle Linux Migration Guide*

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/linux-system-init-1967560.html>

This article, Part 1 in a series that compares and contrasts Oracle
Linux with SUSE Linux, describes registering the software, booting, and
system initialization.

### How to Install Oracle 12c RAC: a Step-by-Step Guide

-   <http://www.pythian.com/blog/oracle-12c-rac-on-your-laptop-step-by-step-guide/>

Okay, back to school folks, back to school :). As with any new product,
our long learning journey starts with an installation on a sandbox
environment. To get you up to speed with Oracle 12c RAC and let you
focus on important stuff (e.g. features research), I have put together
detailed step-by-step installation instructions.

### AIX2Solaris

-   <https://blogs.oracle.com/JeffV/entry/aix2solaris>

Are you among the people migrating workloads from IBM AIX to Oracle
Solaris 11? Even if you have not yet begun this migration, you will
benefit from the knowledge contained in these resources\...

### Overview of Linux Kernel Security Features

-   <https://www.linux.com/learn/docs/727873-overview-of-linux-kernel-security-features/>

In this article, we\'ll take a high-level look at the security features
of the Linux kernel. We\'ll start with a brief overview of traditional
Unix security, and the rationale for extending that for Linux, then
we\'ll discuss the Linux security extensions.

### Extra huge database IOs, part 3

-   <http://fritshoogland.wordpress.com/2013/07/14/extra-huge-database-ios-part-3/>

This is part 3 of a number of blogposts about huge Oracle database IO's.

### Oracle Continues Linux Leadership with 80% Year over Year Growth for Oracle Linux

-   <https://blogs.oracle.com/linux/entry/oracle_continues_linux_leadership_with>

Oracle today announced that customers and partners continue to rapidly
adopt Oracle Linux to increase the scalability, reliability and
performance of their critical applications and systems.

### Availability Best Practices - Availability Using Multiple Service Domains

-   <https://blogs.oracle.com/jsavit/entry/availability_best_practices_availability_using>

Oracle VM Server for SPARC lets the administrator create multiple
service domains to provide virtual I/O devices to guests. While not
mandatory, this powerful feature adds resiliency and reduces single
points of failure. Resiliency is provided by redundant I/O devices,
strengthened by providing the I/O from different service domains.

![](https://blogs.oracle.com/jsavit/resource/PCIe_Bus_Assignment.png)

### Availability Best Practices - Example configuring a T5-8

-   <https://blogs.oracle.com/jsavit/entry/availability_best_practices_example_configuring>

This article continues the series on availability best practices. In
this post we will show each step used to configure a T5-8 for
availability with redundant network and disk I/O, using multiple service
domains.

![](https://blogs.oracle.com/jsavit/resource/two-service-domains_600x448.png)

### Power Ask the Experts Technical Event UK - Session Downloads

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/power_ask_the_experts_technical_event_uk_session_downloads>

The fifth POWER Ask the Experts is a one day customer technical event in
the UK which ran in Manchester and London on 15th/16th July 2013. It
proved to be a very popular free event. We had well over a 100 people
attend which was a mixture of customers, a few Business partners and
some IBMers.

### Oracle Linux Gains Momentum

-   <http://www.serverwatch.com/server-trends/oracle-linux-gains-momentum.html>

In October of 2006, Oracle\'s CEO Larry Ellison announced a bold new
venture, with his company\'s own Linux distribution.

Nearly seven years later, Oracle Linux continues to power ahead.

\"There are nearly 11,000 customers running Oracle Linux on non-Oracle
hardware,\" Wim Coekaerts, senior vice president of Linux and
Virtualization Engineering, Oracle, told ServerWatch.

### Top 5 Processor Myths

-   <http://www.forbes.com/sites/oracle/2013/06/20/top-5-processor-myths/>

When the microprocessor was invented in 1974, the computational power
packed into the 2.6-GHz, dual-core Intel Core i5 that's in the laptop I
got when I joined Oracle two weeks ago would have been unimaginable.

### The Ksplice differentiator

-   <https://blogs.oracle.com/wim/entry/the_ksplice_differentiator>

It\'s been exactly two years since we acquired a small startup called
Ksplice. Ksplice as a company created the zero downtime update
technology for the Linux kernel and they provided a service to their
customers which tracked Linux kernel security fixes and providing these
fixes as zero downtime Ksplice updates.
