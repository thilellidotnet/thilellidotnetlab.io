---
date: "2007-04-27T19:46:59Z"
tags:
- Kernel
- Bug
title: FreeBSD LORs (Lock Order Reversal)
---

I encountered two LORs in the past, especially during the testing phase
of the *just behind the corner* FreeBSD 6.0 release.

I already
[reported](/post/2005/08/18/Update-the-Notebook-to-FreeBSD-60-BETA2)
about two of them, but wanted to point out that the second seems to be
fixed, as shown in this
[patch](http://www.freebsd.org/cgi/cvsweb.cgi/src/sys/vm/uma_core.c.diff?r1=1.129&r2=1.130 "FreeBSD lock order reversal (LOR) #109").
Regrettably, the MFC seem not to have taken place in the RELENG\_6
branch\... yet. On the other side, this one is not known to generate
panic or some other bad behavior, so i think it is ok.
