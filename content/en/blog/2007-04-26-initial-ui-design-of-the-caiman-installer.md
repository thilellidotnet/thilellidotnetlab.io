---
date: "2007-04-26T20:21:17Z"
tags:
- Install
title: Initial UI Design of the Caiman Installer
---

Just a little news to point out some fresh
[mockup](http://opensolaris.org/os/community/install/mockupScreenshots/ "Wireframes of Caiman")
screen shots about the *upcoming* OpenSolaris and future Solaris
installer. All this work came from the [Installation and
Packaging](http://opensolaris.org/os/community/install/ "OpenSolaris community")
OpenSolaris community.

Interesting, but i will wait and try an eventual LiveCD/LiveDVD to see
it in action, if possible.
