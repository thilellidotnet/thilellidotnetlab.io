---
date: "2007-04-28T09:41:23Z"
tags:
- management
- Network
title: Static Route Management for Hosts in the Demilitarized Zone (DMZ)
---
-------------------- --------------- -------------------- ------ ----- ----- --- --- ----- ------
10.126.220.40        255.255.255.255 192.168.138.33               1500*    0   1 UGH    790     0
10.126.220.41        255.255.255.255 192.168.138.33               1500*    0   1 UGH   2862     0
10.126.215.162       255.255.255.255 192.168.138.33               1500*    0   1 UGH     65     0
[...]
#
# sh ./dmz_route.sh -s beastie push
* server: beastie
 =&gt; last configuration backuped
 =&gt; route_admin.cfg pushed
 =&gt; route_admin pushed
</pre>
<p>Need more help?... See the command line help switch:</p>
<pre>
# sh ./dmz_route.sh -h
</pre>

Based on existing procedures, here is a new tool which aim is to help
adding centralized managed static routes for all servers hosted in the
demilitarized zone. As for the [Password Management for Hosts in the
Demilitarized Zone
(DMZ)](/post/2005/06/25/Password-Management-for-Hosts-in-the-Demilitarized-Zone-DMZ),
this script is managed using the `cvs(1)` concurrent management system.

Follow are the three necessary files:

1.  `dmz_routes.sh` this one is able to get, push and apply new static
    route(s) remotely
2.  `route_admin` this script can list and apply new static route(s)
    locally and is used from rc script at boot time
3.  `route_admin.cfg` current static routes commented configuration
    file; used by `route_admin`

Assuming that the environment variables `${CVSROOT}` and `${CVS_RSH}`
are properly sets, here are little samples of usage:

    # cvs checkout -P dmz_route && cd dmz_route
    #
    # sh ./dmz_route.sh
    usage: dmz_route.sh [-hd] [-s servername,...] [-c config_file] [-i init.d_file] {push|add|status}
    #
    # sh ./dmz_route.sh -s beastie status
    * server: beastie
     => state of files:
    /data/system/etc/
    /data/system/etc/init.d/
    /data/system/etc/route_admin.cfg:
         $Id: route_admin.cfg,v 1.10 2005/02/14 14:14:14 root Exp $
    /data/system/etc/init.d/route_admin:
         $Id: route_admin,v 1.9 2004/09/14 08:19:29 root Exp $
     => show the routing tables:IRE Table: IPv4
      Destination             Mask           Gateway          Device Mxfrg  Rtt  Ref Flg  Out  In/Fwd
