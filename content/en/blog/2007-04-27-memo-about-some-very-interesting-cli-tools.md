---
date: "2007-04-27T21:13:16Z"
tags:
- Boot
- ODM
title: Memo About Some Very Interesting CLI Tools
---
 8231 crw-------   2 root     system       17,  0 Apr 21 14:37 /dev/ipldevice
 8231 crw-------   2 root     system       17,  0 Apr 21 14:37 /dev/rhdisk0
-------- ---------------  ---------------  ------ ------------ --------------- 
[...]
         /dev/fslv07      /files/tmpcdinst jfs2   Jun 27 10:14 rw,log=/dev/loglv01
</pre>
<p>Here are the corresponding information found in the ODM:</p>
<pre>
# odmget -q &quot;name=fslv07 and attribute=type&quot; CuAt 

CuAt:
        name = &quot;fslv07&quot;
        attribute = &quot;type&quot;
        value = &quot;jfs2&quot;
        type = &quot;R&quot;
        generic = &quot;DU&quot;
        rep = &quot;s&quot;
        nls_index = 639
</pre>
<p>This can be compared with the status returned by the <code>lsvg</code>
command:</p>
<pre>
# lsvg -l colombvg
colombvg:
LV NAME             TYPE       LPs   PPs   PVs  LV STATE      MOUNT POINT
[...]
fslv07              jf2        280   280   2    open/syncd    /files/tmpcdinst
</pre>
<p><em>More on this particular subject in the story <a href="/post/2005/07/06/Export-and-Import-a-Volume-Group-When-Things-Goes-the-Wrong-Way">
Export and Import a Volume Group... When Things Goes the Wrong
Way</a>.</em></p>
<p>Get the ODM volume group information for a given disk:</p>
<pre>
# lqueryvg -p hdisk0 -Avt
Max LVs:        256
PP Size:        27
Free PPs:       0
LV count:       12
PV count:       2
Total VGDAs:    3
Conc Allowed:   0
MAX PPs per PV  1016
MAX PVs:        32
Conc Autovaryo  0
Varied on Conc  0
Logical:        00ce3a0c00004c000000010364ba67bc.1   hd5 1  
                00ce3a0c00004c000000010364ba67bc.2   hd6 1  
                00ce3a0c00004c000000010364ba67bc.3   hd8 1  
                00ce3a0c00004c000000010364ba67bc.4   hd4 1  
                00ce3a0c00004c000000010364ba67bc.5   hd2 1  
                00ce3a0c00004c000000010364ba67bc.6   hd9var 1  
                00ce3a0c00004c000000010364ba67bc.7   hd3 1  
                00ce3a0c00004c000000010364ba67bc.8   hd1 1  
                00ce3a0c00004c000000010364ba67bc.9   hd10opt 1  
                00ce3a0c00004c000000010364ba67bc.10  loglv00 1  
                00ce3a0c00004c000000010364ba67bc.11  fslv00 1  
                00ce3a0c00004c000000010364ba67bc.12  fslv04 1  
Physical:       00ce3a0c64ba5da3                2   0  
                00ce3a0c8df2265d                1   0  
VGid:           00ce3a0c00004c000000010364ba67bc
Total PPs:      158
LTG size:       128
HOT SPARE:      0
AUTO SYNC:      0
VG PERMISSION:  0
SNAPSHOT VG:    0
IS_PRIMARY VG:  0
PSNFSTPP:       4352
VARYON MODE:    0
VG Type:        0
Max PPs:        32512
</pre>
<h3>Operating system general status and information</h3>
<p>Gather system configuration information:</p>
<pre>
# snap -r    /* Remove snap command output from the /tmp/ibmsupt directory. */
# snap -ac   /* Creates a compressed pax image (snap.pax.Z file) of all files
                in the /tmp/ibmsupt. */
</pre>
<p><em>This tool can be compared to the <code>explorer</code> (known as the
SUNWexplo package) on Sun Solaris OE.</em></p>
<h3>About starting services at boot time</h3>
<p>List the content of the <code>inittab</code> file:</p>
<pre>
# lsitab -a   /* Use this command instead of `cat /etc/inittab`. */
</pre>
<h3>Create a new file system</h3>
<p>Create a new Enhanced Journaled File System in the the <code>colombvg</code>
volume group with a size of 5 gigabytes in read-write mode, using the mount
point <code>/files/ddaeurd1/DATA</code> and being automatically mounted at boot
time:</p>
<pre>
# crfs -v jfs2 -g colombvg -a size=5G -m /files/ddaeurd1/DATA -p rw -A yes
</pre>

### Boot disk configuration

After verifying there are two disks in the boot list\...

    # bootlist -m normal -o
    hdisk0
    hdisk1

\... verify and create a boot image on the second mirrored boot disk:

    # bosboot -vd hdisk1 && bosboot -ad hdisk1

How to know on which disk the OS has booted (bootblock used and kernel
loaded):

    # bootinfo -b
    hdisk0

How to know on which mode the OS has booted (kernel in 32-bit or
64-bit):

    # bootinfo -K
    64

If there is some problem booting on one disk, be sure that the
corresponding raw device are the same device as `ipldevice`:

    # bootinfo -b
    hdisk0
    #
    # ls -ilF /dev/ipldevice /dev/rhdisk0

### VM information vs. ODM information

Assuming the following mounted file system:

    # mount
      node       mounted        mounted over    vfs       date        options      
