---
date: "2011-02-10T16:43:12Z"
tags:
- Zone
- Bug
- Live Upgrade
- Patch
title: Customized Solaris installation and patching experience
---

I recently faced a curious problem when trying to patch an Alternate
Boot Environment created with Live Upgrade on Solaris 10. Although I
initially though it was a LU problem, the solution is finally related to
the patches to be applied and the way a Solaris is installed.

Assuming the ABE is named s10u9, I first tried to apply the Critical
Patch Updates the new way, i.e. through a switch to the `installcluster`
script , which quickly failed like this:

    # cd /net/jumpstart/export/media/patch/cpu/10_Recommended_CPU_2010-10
    # ./installcluster --apply-prereq --s10cluster
    [...]
    # ./installcluster -B s10u9 --s10cluster
    ERROR: Patch set cannot be installed from a live boot environment without zones
           support, to a target boot environment that has zones support.

So, I tried to apply the patches using the `luupgrade` command, but it
failed with a very similar message:

    # ptime luupgrade -t -n s10u9 -s /net/jumpstart/export/media/patch/cpu/10_Recommended_CPU_2010-10/patches `cat patch_order`
    Validating the contents of the media .
    The media contains 198 software patches that can be added.
    All 198 patches will be added because you did not specify any specific patches to add.
    Mounting the BE .
    ERROR: The boot environment  supports non-global zones. The current boot
    environment does not support non-global zones. Releases prior to Solaris 10 cannot be
    used to maintain Solaris 10 and later releases that include support for non-global zones.
    You may only execute the specified operation on a system with Solaris 10 (or later)
    installed.

The fact is, the Primary Boot Environment is a Solaris 10 installation.
So, why complaining that the PBE is an older release? Looking on OTN
discussion forums and in the README file which came with the Critical
Patch Updates release, there is a known bug which can end this way. This
will occur when `/etc/zones/index` in the inactive boot environment has
an incorrect setting for the state for the global zone. The correct
setting is `installed`. So, get check this one:

    # lumount -n s10u9
    /.alt.s10u9
    # grep "^global:configured:" /.alt.s10u9/etc/zones/index
    # luumount -n s10u9

So no luck here. But wait: if the PBE is a customized Solaris 10
installation, it may be that the installed packages missed the Zone
feature, which seems to be mandatory by `installcluster` or
`liveupgrade -t` to figure out if the PBE is a proper (usable) Solaris
10 installation. So, I just installed the missing packages from the
install media\...

    # mount -r -F hsfs `lofiadm -a /net/jumpstart/export/media/iso/sol-10-u9-ga-sparc-dvd.iso` /mnt
    # pkginfo -d /mnt/Solaris_10/Product | nawk '$2 ~ /zone/ || $2 ~ /pool$/ {print $0}'
    application SUNWluzone                       Live Upgrade (zones support)
    system      SUNWpool                         Resource Pools
    system      SUNWzoner                        Solaris Zones (Root)
    system      SUNWzoneu                        Solaris Zones (Usr)
    # yes | pkgadd -d /mnt/Solaris_10/Product SUNWluzone SUNWzoner SUNWzoneu SUNWpool
    [...]
    # umount /mnt
    # lofiadm -d /dev/lofi/1

\... and this must be OK right now:

    # ./installcluster -B s10u9 --s10cluster
    Setup ...
    CPU OS Cluster 2010/10 Solaris 10 SPARC (2010.10.06)
    Application of patches started : 2011.02.07 11:17:08

    Applying 120900-04 (  1 of 198) ... skipped
    [...]
    Installation of patch set to alternate boot environment complete.

    Please remember to activate boot environment s10u9 with luactivate(1M)
    before rebooting.
    Install log files written :
      /.alt.s10u9/var/sadm/install_data/s10s_rec_cluster_short_2011.02.07_11.17.08.log
      /.alt.s10u9/var/sadm/install_data/s10s_rec_cluster_verbose_2011.02.07_11.17.08.log

And it is\... The question is, why is the Zone feature necessary and
mandatory in this case?
