---
date: "2007-04-07T15:41:07Z"
tags:
- Utility
- Processus
title: Notes About Interesting ps(1b) Behaviour
---

I encounter a strange behavior using `/usr/ucb/ps` command recently.
Normally, the `COMMAND` header of the output of the `ps(1b)` is
truncated to the size of a default terminal. Adding one `w` flag can
grow this output to 132 characters. Adding a second `w` flag doesn\'t
truncate the output at all, reading the complete arguments list in the
`/proc/${PID}/as` special file; in fact, a representation of the address
space for the corresponding process. The restriction that apply here is
the ability to read this file, generally the owner of the running
process only.

Interestingly, at least on Solaris 8 and 9, you can print untruncated
arguments list\--even if you didn\'t have the the right to read the
content of the address space special file\--if the `SUNWscpux` package
is installed. This package seems not delivered with Solaris 10.

As I remember, this package is a prerequisite for 64-bit architecture
platform or you will see error messages such as *Data Type Too Large* if
executed without it installed, but it seems to be able to behave in a
very different manner as I expected.

Don\'t known what to think about this right now. Any though?
