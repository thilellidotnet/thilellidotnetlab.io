---
date: "2007-04-27T20:55:31Z"
tags:
- POWER
- Compiler
title: Installation and Configuration of the "C for AIX Compiler" (server and Client)
---

To be able to use the C for AIX Compiler, a server-side license and
configuration must done as well as a client-side installation. Here are
the steps to achieve this goal.

1.  *madre* is the hostname of the AIX server on which to install the C
    for AIX Compiler (license and tools)

First, check the necessary AIX filesets (and install them accordingly):

    # lslpp -l | egrep "vac|bos.rte |bos.adt.libm" | sort -u
      bos.adt.libm              5.3.0.10  COMMITTED  Base Application Development
      bos.rte                   5.3.0.10  COMMITTED  Base Operating System Runtime
      vac.C                      6.0.0.0  COMMITTED  C for AIX Compiler
      vac.C.readme.ibm           6.0.0.0  COMMITTED  C for AIX iFOR/LS Information
      vac.lic                    6.0.0.0  COMMITTED  C for AIX Licence Files

*For simplicity purpose, the same packages will be installed on each of
the server and the client for the C for AIX Compiler.*

*Note \#1: the `bos.*` filesets can be found in the lpp source
`lpp_source530`.*

*Note \#2: the `vac.*` filesets can be found in the lpp source
`C_for_AIX_60`.*

### Configuration of the license server: Concurrent Network License Server

Based on information found in the file `/usr/vac/README.password`:

Check for the necessary License Use Runtime filesets:

    # lslpp -l | egrep "ifor" | sort -u
      bos.rte.ifor_ls            5.3.0.0  COMMITTED  iFOR/LS Libraries
      ifor_ls.base.cli          5.3.0.10  COMMITTED  License Use Management Runtime

To configure LUM, perform the following steps (based on the content of
`/usr/vac/cforaix_c.lic`):

    # cd /usr/opt/ifor/ls/os/aix/bin
    # ./i4config
    /*
     * Answer the LUM configuration questions as appropriate (configure a Concurrent Network license server):
     * a) Select 4 "Central Registry (and/or Network and/or Nodelock) License Server" on the first panel
     * b) Answer y to "Do you want this system be a Network License Server too?"
     * c) Answer n to "Do you want this system be a Nodelock License Server too?"
     * d) Answer n to "Do you want to disable remote administration of this Network License"
     * e) Select 2 "Direct Binding only"
     * f) Answer n to "Do you want to change the Network License Server ip port number?"
     * g) Answer n to "Do you want to change the Central Registry License Server ip port number?"
     * h) Select 1 "Default" as the desired server(s) logging level
     * i) Enter blank to accept the default path for the default log file(s)
     * j) Answer y to the Network Server question "Do you want to modify the list of remote Nodelock and/or
     *    Network License Servers this system can connect to in direct binding mode (both for Administration purposes and for
     *    working as Network License Client)?"
     * k) Select 3 "Create a new list" to the direct binding list menu
     * l) Enter the hostname, without the domain, of the system you are configuring LUM when prompted for the "Server network
     *    name(s)", for example: madre
     * m) Answer n to "Do you want to change the default ip port number?"
     * n) Select 1 "preserve the current list" to the direct binding list menu
     * o) Answer y to "Do you want the License Server(s) automatically start on this system at boot time?"
     * p) Answer y to continue the configuration setup and write the updates to the i4ls.ini file
     * q) Answer y to "Do you want the License Server(s) start now?"
     */

The Concurrent Network license certificate is contained in the file
`/usr/vac/cforaix_c.lic`. To enroll a license certificate:

    # cd /usr/opt/ifor/ls/os/aix/bin
    # ./i4blt -a \
              -v "'IBM Software Solutions Toronto' 5da54a553b4c.02.09.15.31.05.00.00.00 p9gb3ycs6ydpw" \
              -p "'C for AIX' '6.0.c' bcmefhz6zkdmdaaxe524si3kbgb2zwxsaqskixs222 " \
              -T 1 \
              -R "DSI/ASP/SUT"
    i4blt Version 4.6.8 AIX -- LUM Basic License Tool
    (c) Copyright 1995-2004, IBM Corporation, All Rights Reserved
    US Government Users Restricted Rights - Use, duplication or disclosure
    restricted by GSA ADP Schedule Contract with IBM Corp.
    (c) Copyright 1991-1997 Gradient Technologies Inc., All Rights Reserved
    (c) Copyright 1991,1992,1993, Hewlett-Packard Company, All Rights Reserved

    ADM-10099: Product successfully enrolled

After enrolling a concurrent-network certificate, the licenses **must**
be *distributed* to one or more LUM license servers (only for
Concurrent-Network Certificate Enrollment):

    # ./i4blt -E \
              -n madre \
              -v "'IBM Software Solutions Toronto'" \
              -p "'C for AIX' '6.0.c'" \
              -A 1 \
              -w "madre"
    i4blt Version 4.6.8 AIX -- LUM Basic License Tool
    (c) Copyright 1995-2004, IBM Corporation, All Rights Reserved
    US Government Users Restricted Rights - Use, duplication or disclosure
    restricted by GSA ADP Schedule Contract with IBM Corp.
    (c) Copyright 1991-1997 Gradient Technologies Inc., All Rights Reserved
    (c) Copyright 1991,1992,1993, Hewlett-Packard Company, All Rights Reserved

    ADM-10723: The license was successfully distributed.

### Client configuration to access to the License and Central Registry servers

To configure a LUM Network Client, perform the following steps:

    # cd /usr/opt/ifor/ls/os/aix/bin
    # ./i4config
    /*
     * a) Answer y to "Do you want to terminate them NOW?"
     * b) Select 1 "Network License Client" on the first panel
     * c) Answer y to "Do you want this system be a Network License Server too?"
     * d) Select 2 "Direct Binding only"
     * e) Select 3 "Create a new list" to the direct binding list menu
     * f) Enter the "Server network name(s)", for example: madre
     * g) Enter the "Please specify the Central Registry License Server name", for example: madre
     * h) Answer n to "Do you want to change the default ip port number?" (to locate License Servers)
     * i) Answer n to "Do you want to change the default ip port number?" (to locate the Central Registry License)
     * j) Select 1 "preserve the current list" to the direct binding list menu
     * k) Answer y to continue the configuration setup and write the updates to the i4ls.ini file
     */

Verify the license configuration\... say, on the client side for
example:

    # TERM=vt220 smitty
    /*
     * Software License Management
     *  Show Available License Servers
    i4tv Version 4.6.8 AIX -- LUM Test and Verification Tool
    (c) Copyright 1995-2004, IBM Corporation, All Rights Reserved
    US Government Users Restricted Rights - Use, duplication or disclosure
    restricted by GSA ADP Schedule Contract with IBM Corp.
    (c) Copyright 1991-1997, Gradient Technologies Inc., All Rights Reserved
    (c) Copyright 1991,1992,1993, Hewlett-Packard Company, All Rights Reserved

    Completed license transaction on node  ce3a0c51 running LUM  4.6.8 AIX
    Active License Servers:
        ip:madre.dev.example.com (IBM/AIX) running LUM 4.6.8 AIX
    Active Central Registry License Server:
        ip:madre.dev.example (IBM/AIX) running LUM 4.6.8 AIX
     */
