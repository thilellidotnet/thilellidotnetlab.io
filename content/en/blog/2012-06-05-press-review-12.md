---
date: "2012-06-05T08:46:47Z"
tags:
- Press
title: 'Press Review #12'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a lot more:

### How to Set Up a Load-Balanced Application Across Two Oracle Solaris Zones

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/loadbalancedapp-1653020.html>

This article describes how to combine the built-in Integrated Load
Balancer (ILB) with Oracle Solaris Zones and the new network
virtualization capabilities of Oracle Solaris 11 to set up a virtual
server on a single system. This article starts with a brief overview of
ILB and follows with an example of setting up a virtual Apache Tomcat
server instance. You will need a basic knowledge of Oracle Solaris Zones
and networking administration.

### How to Use Oracle VM Templates

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/configure-vm-templates-1656261.html>

This article describes how to prepare an Oracle VM environment to use
Oracle VM Templates, how to obtain a template, and how to deploy the
template to your Oracle VM environment. It also describes how to create
a virtual machine based on that template and how you can clone the
template and change the clone\'s configuration. As an example, the
article uses a template for Oracle Database that contains two disk
images: an Oracle Linux system image and an Oracle Database image.

### NUM\_PARALLEL\_LPS for AIX and for PowerHA

-   <http://www.wmduszyk.com/?p=8556>

This logical volume has stale partitions, so sync it. Doing 4 stale
partitions at a time seems to be a win most of the time. However, we
will honor the NUM\_PARALLEL\_LPS value in /etc/environment, if set.

### DTrace in the zone

-   <http://dtrace.org/blogs/bmc/2012/06/07/dtrace-in-the-zone/>

So with these three changes, I am relieved to report that DTrace is now
completely usable in the non-global zone --- and all without sacrificing
the security model of zones! If you are a Joyent cloud customer, we will
be rolling out a platform with this modification across the cloud (it
necessitates a reboot, so don't expect it before your next scheduled
maintenance window); if you are a SmartOS user, look for this in our
next SmartOS release; and if you are using another illumos-based distro
(e.g., OpenIndiana or OmniOS) look for it in an upcoming release --- we
will be integrating these changes into illumos, so you can expect them
to be in downstream distros soon. And here's to DTrace in the zone!

### What happened to the MAUs on T4?

-   <https://blogs.oracle.com/jsavit/entry/no_mau_required_on_a>

Besides being much faster than its predecessors, the T4 also integrates
hardware crypto acceleration so its seamlessly available to
applications, whether domains are being used or not. Administrators no
longer have to control how they are allocated - it is available to all
CPUs and virtual environments without any administrative effort.

### FOSS Support In Oracle Solaris

-   <https://blogs.oracle.com/solarisfoss/entry/foss_support_in_oracle_solaris>

Support of Free and Open Source Software in Oracle Solaris is described
inside a knowledge article \[ID 1400676.1\], which can be found inside
My Oracle Support (MOS). This knowledge article is the most definitive
source of information concerning FOSS support in Oracle Solaris and
shall be used by Oracle Solaris customers.

### Virtualise Storage with Style: Shared Storage Pools

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/AIXDownUnder/entry/virtualise_with_style_powervm_webinars20>

The team from IBM\'s Advanced Technology Support, Europe have done it
again! Their free Webinar Series on Power Systems Virtualisation from
IBM has another contribution from Nigel Griffiths, (a.k.a. Mr NMon).
This presentation is on Shared Storage Pools from Experience - a kind of
walk through of where the rubber hits the road. On the PowerVM
Virtualisation Webinar Series Wiki, scroll down to Session 13: Shared
Storage Pools \... from Experience.

### Best Practices - Core allocation

-   <https://blogs.oracle.com/jsavit/entry/best_practices_core_allocation>

SPARC T-series servers currently have up to 4 CPU sockets, each of which
has up to 8 or (on SPARC T3) 16 CPU cores, while each CPU core has 8
threads, for a maximum of 512 dispatchable CPUs. The defining feature of
Oracle VM Server for SPARC is that each domain is assigned CPU threads
or cores for its exclusive use. This avoids the overhead of
software-based time-slicing and emulation (or binary rewriting) of
system state-changing privileged instructions used in traditional
hypervisors.

### Oracle Solaris Zones Physical to virtual (P2V)

-   <https://blogs.oracle.com/vreality/entry/oracle_solaris_zones_physical_to>

This document describes the process of creating and installing a Solaris
10 image build from physical system and migrate it into a virtualized
operating system environment using the Oracle Solaris 10 Zones
Physical-to-Virtual (P2V) capability.

### Networking in VirtualBox

-   <https://blogs.oracle.com/fatbloke/entry/networking_in_virtualbox1>

Networking in VirtualBox is extremely powerful, but can also be a bit
daunting, so here\'s a quick overview of the different ways you can
setup networking in VirtualBox, with a few pointers as to which
configurations should be used and when.

### Vendors, Systems, Processors, and Vendors Update

-   <http://netmgt.blogspot.fr/2012/06/vendors-systems-processors-and-vendors.html>

Hot Chips 24: A Symposium on High Performance Chips is right around the
corner, and the agenda looks pretty exciting.

### Upgrading a very, very, very old VIOS

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/AIXDownUnder/entry/upgrading_a_very_very_very_old_vios>

This is the epic of the upgrade for the Virtual I/O server (VIOS) from
version 1.4 to version 2.2.

### Fragmentation de l\'Intimate Shared Memnory (ISM)

-   <http://www.gloumps.org/article-fragmentation-de-l-intimate-shared-memory-ism-106867569.html>

Petit compte rendu sur un problème de performance que je viens de
rencontrer sur un serveur Solaris Sparc avec une base de donnée Oracle.
Le contexte étant le suivant : temps de réponse dégradés suite au reboot
du serveur. Bien entendu, aucun changement entre les deux reboot et
pourtant l\'application fonctionne moins bien qu\'avant.

### Workload Partition (WPAR) - Answers

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/workload_partition_wpar_answers305>

This week I spent 4 hours with a customer covering many advanced WPAR
topics and took way a bunch of questions that I had to check the answers
and ask the WPAR developers themselves to be sure I had the right
answers. If the questions were not clear to my customers and I did know
initially know the answers then there may be others with similar issues
so I thought I would share the answers with everyone.

### Workload Partition (WPAR) - Sharing filesystem Global AIX to WPAR

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/workload_partition_wpar_sharing_filesystem_global_aix_to_wpar73>

From the Global AIX, I can add a filesystem to /wpars/WPARname/directory
so the WPAR has access but what if I don\'t want to have the filesystem
mounted there in the Global AIX?

### POWER System Firmware Warnings & Red Lights on the Dashboard

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/power_system_firmware_warnings_red_lights_on_the_dashboard10>

Earlier today I received email from a customer reporting their large
POWER7 based machines where on firmware 720\_64 to 720\_90 and their
reluctance to take the outage to upgrade it. They were asking for fine
details of newer firmware levels and what advantages this would bring to
\"justify the outage to their user departments\".

### Breaking the telnet addiction with netcat

-   <http://prefetch.net/blog/index.php/2012/06/26/breaking-the-telnet-addiction-with-netcat/>

After many years of use it's become almost second nature to type 'telnet
' when I need to see if a system has TCP port open. Newer systems no
longer install telnet by default.

### To extreme micro-partition or to Workload-Parttion, that is the question?

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/to_extreme_micro_partition_or_to_workload_parttion_that_is_the_question137>

So I got asked, just as an example configuration which forces lots of
workload per CPU:\
Given a 16 CPU POWER machine and a need to run 100 workloads, would I
recommend 100 LPARs or 100 WPARs?

### Oracle VM Server for SPARC on YouTube

-   <http://www.youtube.com/user/OVMSPARC>

### What\'s up with LDoms: Part 1 - Introduction & Basic Concepts

-   <https://blogs.oracle.com/cmt/en/entry/what_s_up_with_ldoms>

LDoms - the correct name is Oracle VM Server for SPARC - have been
around for quite a while now. But to my surprise, I get more and more
requests to explain how they work or to give advise on how to make good
use of them. This made me think that writing up a few articles
discussing the different features would be a good idea.

### What\'s up with LDoms: Part 2 - Creating a first, simple guest

-   <https://blogs.oracle.com/cmt/en/entry/what_s_up_with_ldoms1>

In the first part, we discussed the basic concepts of LDoms and how to
configure a simple control domain. We saw how resources were put aside
for guest systems and what infrastructure we need for them. With that,
we are now ready to create a first, very simple guest domain. In this
first example, we\'ll keep things very simple. Later on, we\'ll have a
detailed look at things like sizing, IO redundancy, other types of IO as
well as security.

### Magic Quadrant for x86 Server Virtualization Infrastructure

-   <http://www.gartner.com/technology/reprints.do?id=1-1AVRXJL&ct=120612>

Oracle VM moves into Challenger Position in Gartner x86 Server
Virtualization Infrastructure Magic Quadrant Report.

### Présentation Automated Installer, TechDay Solaris 11, Guses, par Bruno Philippe

-   <http://www.gloumps.org/photo-2085386-001_jpg.html>
