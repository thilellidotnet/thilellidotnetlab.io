---
date: "2007-04-22T17:38:00Z"
tags:
- memory
- x86
title: memconf Update!
---
-------------------------------- --------------------------
AMD Opteron(tm) Processor 148    Socket 939
Memory Units:
Type    Status Set Device Locator      Bank Locator
------- ------ --- ------------------- --------------------
-------------------------------- --------------------------
AMD Opteron(tm) Processor 148    Socket 939
Memory Units:
Type    Status Set Device Locator      Bank Locator
------- ------ --- ------------------- --------------------

AS you may notice, there is a new version of the `memconf` utility. I am
proud that the improved x86 support was done using my Sun Ultra 20 as
the reference Opteron (amd64) system ;-)

Here are the outputs before, and after Tom Schmidt\'s great work (look
at the bold informations):

    $ memconf -d
    memconf:  V2.0 17-Oct-2006 http://www.4schmidts.com/unix.html
    hostname: unic.thilelli.net
    model:    Sun Ultra 20 Workstation (Solaris x86 machine)
    Solaris Nevada snv_51 X86, 32-bit kernel, SunOS 5.11
    2 x86 2211MHz cpus
    diagbanner = Sun Ultra 20 Workstation
    model = Sun Ultra 20 Workstation
    modelmore = (Solaris x86 machine)
    machine = i86pc
    platform = i86pc
    perl version: 5.008004
    CPU Units:
    ==== Processor Sockets ====================================
    Version                          Location Tag
    unknown in use 0   A0                  Bank0/1
    unknown in use 0   A1                  Bank2/3
    unknown in use 0   A2                  Bank4/5
    unknown in use 0   A3                  Bank6/7
    total memory = 2047MB (1.9990234375GB)
    $
    $ memconf -d
    memconf:  V2.1 29-Nov-2006 http://www.4schmidts.com/unix.html
    hostname: unic
    model:    Sun Ultra 20 Workstation (Solaris x86 machine)
    Solaris Nevada snv_52 X86, 64-bit kernel, SunOS 5.11
    1 AMD Opteron(tm) Processor 148 2211MHz cpu
    diagbanner = Sun Ultra 20 Workstation
    model = Sun Ultra 20 Workstation
    modelmore = (Solaris x86 machine)
    machine = i86pc
    platform = i86pc
    perl version: 5.008004
    CPU Units:
    ==== Processor Sockets ====================================
    Version                          Location Tag
    unknown in use 0   A0                  Bank0/1
    unknown in use 0   A1                  Bank2/3
    unknown in use 0   A2                  Bank4/5
    unknown in use 0   A3                  Bank6/7
    total memory = 2048MB (2GB)
