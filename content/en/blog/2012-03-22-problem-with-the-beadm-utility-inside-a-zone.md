---
date: "2012-03-22T12:56:50Z"
tags:
- Boot Environment
- Boot
- Bug
- Zone
title: Problem with  the beadm utility inside a Zone
---
--      ------ ---------- -----   ------ -------
solaris NR     /          917.06M static 2012-03-21 14:04

ZNG# beadm create solaris-1

ZNG# beadm activate solaris-1

ZNG# beadm list
BE        Active Mountpoint Space   Policy Created
--        ------ ---------- -----   ------ -------
--        ------ ---------- -----   ------ -------
solaris   -      -          3.06M   static 2012-03-21 14:04
solaris-1 NR     /          979.46M static 2012-03-21 17:56
</pre>
<p>All works very well, I didnt get any problem and can do whatever I want
after that: fallback on the other BE, go on with this one installing new
packages, create more new BE, etc.</p>
<p>But if I tried to automagically create a new BE from the <code>pkg</code>
utility, the created BE seems not have all the good stuff it must had.</p>
<pre>
ZNG# pkg install --require-new-be site/application/testpkg
           Packages to install:   1
       Create boot environment: Yes
Create backup boot environment:  No

DOWNLOAD                                  PKGS       FILES    XFER (MB)
Completed                                  1/1       20/20      0.0/0.0

PHASE                                        ACTIONS
Install Phase                                  73/73

PHASE                                          ITEMS
Package State Update Phase                       1/1
Image State Update Phase                         2/2
<strong>pkg: '/sbin/bootadm update-archive -R /tmp/tmpCqUVIT' failed.
with a return code of 1.</strong>

A clone of solaris exists and has been updated and activated.
On the next boot the Boot Environment solaris-1 will be
mounted on '/'.  Reboot when ready to switch to this updated BE.

ZNG# beadm list
BE        Active Mountpoint Space   Policy Created
--        ------ ---------- -----   ------ -------
--         ------ ---------- -----   ------ -------
newsolaris R      -          864.50M static 2012-03-21 17:52
solaris    N      /          80.84M  static 2012-03-21 14:04
ZNG# beadm mount newsolaris /mnt

ZNG# bootadm  update-archive -vn -R /mnt
<strong>file not found: /mnt//boot/solaris/bin/create_ramdisk
/mnt/: not a boot archive based Solaris instance</strong>

ZNG# ls -l /mnt/boot/solaris/bin/create_ramdisk
/mnt/boot/solaris/bin/create_ramdisk: No such file or directory

ZNG# ls -l /mnt/boot
<strong>/mnt/boot: No such file or directory</strong>

ZNG# ls -l /mnt
total 72
lrwxrwxrwx   1 root     root           9 Mar 21 14:17 bin -&gt; ./usr/bin
drwxr-xr-x  17 root     sys           18 Mar 21 17:18 dev
drwxr-xr-x   2 root     root           2 Mar 21 14:26 dpool
drwxr-xr-x  48 root     sys          114 Mar 21 17:52 etc
drwxr-xr-x   2 root     sys            2 Mar 21 14:11 export
dr-xr-xr-x   2 root     root           2 Mar 21 14:11 home
drwxr-xr-x  12 root     bin          185 Mar 21 14:17 lib
drwxr-xr-x   2 root     sys            2 Mar 21 14:11 mnt
dr-xr-xr-x   2 root     root           2 Mar 21 14:26 net
dr-xr-xr-x   2 root     root           2 Mar 21 14:26 nfs4
drwxr-xr-x   2 root     sys            2 Mar 21 14:11 opt
dr-xr-xr-x   2 root     root           2 Mar 21 14:11 proc
drwx------   2 root     root           5 Mar 21 16:50 root

Although `beadm` utility is now supported [inside a non-global
zone](http://docs.oracle.com/cd/E23824_01/html/E21801/zonelimit.html), I
find a case where its behavior seems not works as expected. So,
connected inside a Zone (say, `myzone`), I can create a new BE (say,
solaris-1), activate it, and reboot on it.

    ZG# zoneadm list -vc
      ID NAME             STATUS     PATH                           BRAND    IP
       0 global           running    /                              solaris  shared
       4 myzone           running    /zones/myzone                  solaris  excl

    ZG# zlogin myZone
    [Connected to zone 'myzone' pts/7]
    Oracle Corporation      SunOS 5.11      11.0    February 2012

    ZNG# beadm list
    BE      Active Mountpoint Space   Policy Created
    solaris   N      /          43.0K   static 2012-03-21 14:04
    solaris-1 R      -          917.19M static 2012-03-21 16:48

    ZNG# init 6
    [Connection to zone 'myzone' pts/9 closed]

    ZG# zlogin myzone
    [Connected to zone 'myzone' pts/7]
    Oracle Corporation      SunOS 5.11      11.0    February 2012

    ZNG# beadm list
    BE        Active Mountpoint Space   Policy Created
    solaris   N      /          102.0K  static 2012-03-21 14:04
    solaris-1 R      -          950.52M static 2012-03-21 17:39

So a new BE was created, but this time there is something wrong. Try to
see what\'s missing:

    ZNG# beadm list
    beadm mount newsolaBE         Active Mountpoint Space   Policy Created
    drwxr-xr-x   2 root     root           2 Mar 21 14:26 rpool
    lrwxrwxrwx   1 root     root          10 Mar 21 14:17 sbin -> ./usr/sbin
    drwxr-xr-x   5 root     root           5 Mar 21 14:11 system
    drwxrwxrwt   2 root     sys            2 Mar 21 17:19 tmp
    drwxr-xr-x   2 root     root           2 Mar 21 14:26 tools
    drwxr-xr-x  22 root     sys           32 Mar 21 14:26 usr
    drwxr-xr-x  28 root     sys           29 Mar 21 14:17 var

Well, I don\'t why there is a difference between those two BE, but the
differences are significant enough to be a problem.

Comments welcome!
