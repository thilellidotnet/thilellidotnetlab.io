---
date: "2011-10-04T15:53:02Z"
tags:
- Press
title: 'Press Review #2'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### New OTN Demo - New fast reboot process for x86 systems and how it compares to standard reboot

-   <http://download.oracle.com/otndocs/tech/OTN_Demos/x86/x86-OTN-Demo/x86-OTN-Demo.html>

The Fast Reboot feature enables you to reboot an x86 based system,
bypassing the firmware and boot loader processes. Fast Reboot implements
an in-kernel boot loader that loads the kernel into memory and then
switches to that kernel, so that the reboot process occurs within
seconds. This feature is implemented on both 32-bit and 64-bit kernels.

If you are running Oracle Solaris 11 Express, Fast Reboot is enabled by
default on the x86 platform, without the need to use the -f option with
the reboot command. The Fast Reboot feature of Oracle Solaris,
previously introduced on the x86 platform, is now supported on the SPARC
platform. The integration of Fast Reboot on the SPARC platform enables
the -f option to be used with the reboot command to accelerate the boot
process by skipping certain POST tests.

On both the x86 and SPARC platforms, Fast Reboot is managed through SMF
and implemented through a boot configuration service,
svc:/system/boot-config. The boot-config service provides a means for
setting or changing the default boot configuration parameters. When the
config/fastreboot\_default property is set to true, the system performs
a fast reboot automatically, without the need to use the reboot -f
command. By default, this property value is set to true on the x86
platform and to false on the SPARC platform.

### Oracle Buys Ksplice to Become Only Enterprise Linux Provider with Zero Downtime Updates

-   <http://www.oracle.com/us/corporate/Acquisitions/ksplice/customer-letter-430127.html>
-   <http://www.oracle.com/us/corporate/Acquisitions/ksplice/general-presentation-430138.pdf>

With its acquisition of Ksplice, Oracle is now in the position of being
the only enterprise Linux provider that can offer zero downtime updates.
In its customer letter Oracle announces that it plans to make the
Ksplice technology a standard feature of Oracle Linux Premier Support.
The letter also mentions that Oracle does not plan to support the use of
Ksplice technology with Red Hat Enterprise Linux or SUSE Enterprise
Linux. Rather, the Oracle Linux Premier Support subscription applies to
Unbreakable Enterprise Kernel.

### Oracle x86 Infrastructure TCO Study

-   <http://www.oracle.com/us/products/servers-storage/servers/x86/edison-x86-infrastructure-tco-wp-431547.pdf>

The Edison Group\'s white paper, \"Oracle x86 Infrastructure: The
Optimized Stack: Reducing Total Cost of Ownership through Vertical
Integration\" examines the cost structures across a range of system
sizes and deployments for the core x86 system stack by comparing
Oracle\'s integrated complete infrastructure with alternatives from HP
and HP, all deployed with Red Hat Enterprise Linux and VMware vSphere,
both together and separately. Among the findings is that TCO with an
Oracle solution is as much as 57 percent lower than comparable
deployments. Edison evaluated two, four and eight socket systems over
three and five year periods using the Oracle Sun Fire X4170 M2 server
for the two socket study and the Sun Fire X4470 M2 for the four socket
system. For the eight socket study, Edison Group used the Sun Fire X4800
M2, and for a ten two socket study of blades with networking Edison used
the Sun Blade X6270 M2 server module in a Sun Blade 6000 chassis with
Sun Blade 6000 10 gigabyte (GB) switched NEM 24p. Equipment from IBM and
HP was comparable in every case.

The white paper concludes that, \"By engineering the entire
infrastructure with service and support in mind, Oracle can deliver
lower TCO in the design and operation of its system, in the ease of
deployment enabled by VM Templates and Validated Configurations \... and
in the efficiency and effectiveness of its \... Premier Support
package.\"

### Oracle Solaris Studio 12.3 Beta Program

-   <http://www.oracle.com/technetwork/server-storage/solarisstudio/downloads/beta-program-408129.html>

Feature highlights in the Oracle Solaris Studio 12.3 Beta Release
include:\
*Application Performance*\
- Compiler and library optimizations for the newest SPARC and x86 Oracle
Sun Enterprise servers\
*Application Observability*\
- New application quality tool, Code Analyzer, to identify application
vulnerabilities,\
including memory, code coverage and other errors\
- Improved performance analysis tooling for multi-core applications\
*Developer Productivity*\
- Compile time performance enhancements\
- Updated support for key open source runtime libraries, including BOOST
and Apache C++\
- Feature enhancements throughout toolchain to simplify multi-core
development\
- New support for Oracle Database development with Pro\*C support within
IDE\
*Platform support for Oracle Solaris and Oracle Linux*\
- Includes Oracle Solaris 11 Express and Oracle Linux\
- New IDE support on Windows enables remote development

### Best Way to Update Software With IPS in Oracle Solaris 11

### Best Way to Automate ZFS Snapshots and Track Software Updates

### Best Way to Update Software in Zones

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/updatesoftwareips-367407.html>
-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/autosnapshots-397145.html>
-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/updatesoftwarezones-450942.html>

This is the third article in a series highlighting best practices for
software updates in Oracle Solaris 11 Express. The first article
introduced the IPS software packaging model and highlighted best
practices for creating a new Boot Environment (BE) before performing an
update. The second article discussed the Time Slider and auto-snapshot
services, describing how to initialize and use these services to
periodically snapshot BEs and other ZFS volumes.

This third article dives more deeply into the topic of software updates,
exploring the process of updating an Oracle Solaris 11 Express system
configured with zones. This topic is especially pertinent since zones in
this release differ somewhat from those in Oracle Solaris 10, as does
the software upgrade process for zoned systems.

### ZFS: To Dedupe or not to Dedupe\...

-   <http://constantin.glez.de/blog/2011/07/zfs-dedupe-or-not-dedupe>

\...that is the question.

Ever since the introduction of deduplication into ZFS, users have been
divided into two camps: One side enthusiastically adopted deduplication
as a way to save storage space, while the other remained skeptical,
pointing out that dedupe has a cost, and that it may not be always the
best option.

Let\'s look a little deeper into the benefits of ZFS deduplication as
well as the cost, because ultimately it boils down to running a
cost/benefit analysis of ZFS deduplication. It\'s that simple.

### First Steps with Oracle Solaris 11 Express

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/gettingstarteds11ex-405412.html>

Oracle Solaris 11 Express is distributed in several formats: a
hands-free server based format that\'s used for automatic installation,
an interactive installer format that only has console access, and an
interactive graphical installer that includes a full desktop
environment.

This article focuses on the third format with the full desktop
environment, although most of the concepts discussed in this article
will apply to any of the Oracle Solaris 11 Express formats. The
graphical format is officially known as the LiveCD. This means that
Oracle Solaris can be booted into RAM, causing zero impact on your
existing operating system. Once it is loaded, you are then free to
experiment with Oracle Solaris to determine whether it is something that
you would like to install.

The LiveCD is not intended for long-term use. For example, any changes
that you make to the system will be lost when the system is shut down.
Therefore, the next logical step is to install Oracle Solaris on the
system, which the LiveCD makes easy by placing an Install Oracle Solaris
icon right on the desktop. But before we head down that road, let\'s
step back a bit and consider the installation options.

### Solaris 11 Express Network Tunables

-   <http://blogs.oracle.com/stw/entry/solaris_11_express_network_tunables>

For years I, and many others, have been tuning TCP, UDP, IP, and other
aspects of the Solaris network stack with ndd(1M). The ndd command is
documented, however, most of the tunables were really private interface
implementations, subject to change, and lacked documentation in many
cases. Also, ndd does not show the default values, nor the possible
values or ranges.

That is changing with Solaris 11 Express. A new command ipadm(1M) allows
persistent and temporary (with the -t option) setting of key tunable
values. This is a major improvement over ndd, where it is customary to
create an /etc/rc2.d/S69ndd or similar script to set the parameter on
every reboot. Another benefit is that ipadm shows the default value and
the values that the property can be set to.

The ipadm has many features to configure the IP settings of interfaces.
This blog entry focuses on how ipadm replaces ndd. Note that ipadm only
supports the IP, TCP, UDP, SCTP, and ICMP protocols. Other protocols
such as ipsecah and keysock still required the use of ndd.

### VirtualBox 4.1.2 released!

-   <http://www.virtualbox.org/wiki/Changelog>

Oracle made a maintenance release of Oracle VM VirtualBox version 4.1.2.

This release fixes lots of stuff including a significant problem on AMD
Fusion CPUs, and also adds Linked Clone support to the VirtualBox
Manager (GUI) which we couldn\'t get done in time for 4.1.0.

### The Compiler Detective - What Compiler and Options Were Used to Build This Application?

-   <http://blogs.oracle.com/timc/entry/the_compiler_detective_what_compiler>

Performance engineers often look at improving application performance by
getting the compiler to produce more efficient binaries from the same
source. This is done by changing what compiler options are used. In this
modern era of Open Source Software, you can often get your hands on a
number of binary distributions of an application, but if you really want
to roll your sleeves up, the source is there, just waiting to be
compiled with the latest compiler and optimizations.

Now, it might be useful to have as a reference the compiler version and
flags that were originally used on the binary distribution you tried
out, or you just might be interested to know. Read on for details on the
forensic tools.
