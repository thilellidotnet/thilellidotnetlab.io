---
date: "2008-02-09T10:53:09Z"
tags:
- EMC
- SAN
- MPxIO
title: Deleting SCSI Device Paths For A Multipath SAN LUN
---

When releasing a multipath device under RHEL4, different SCSI devices
corresponding to different paths must be cleared properly before
removing the SAN LUN effectively. When the LUN was delete before to
clean up the paths at the OS level, it is always possible to remove them
afterwards. *In the following example, it is assume that the freeing LVM
manipulations were already done, and that the LUN is managed by EMC
PowerPath.*

1.  First, get and verify the SCSI devices corresponding to the
    multipath LUN:

        # grep "I/O error on device" /var/log/messages | tail -2
        Feb  4 00:20:47 beastie kernel: Buffer I/O error on device sdo, \
         logical block 12960479
        Feb  4 00:20:47 beastie kernel: Buffer I/O error on device sdp, \
         logical block 12960479
        # powermt display dev=sdo
        Bad dev value sdo, or not under Powerpath control.
        # powermt display dev=sdp
        Bad dev value sdp, or not under Powerpath control.

2.  Then, get the appropriate `scsi#:channel#:id#:lun#` informations:

        # find /sys/devices -name "*block" -print | \
         xargs \ls -l | awk -F\/ '$NF ~ /sdo$/ || $NF ~ /sdp$/ \
         {print "HBA: "$7"\tscsi#:channel#:id#:lun#: "$9}'
        HBA: host0      scsi#:channel#:id#:lun#: 0:0:0:9
        HBA: host0      scsi#:channel#:id#:lun#: 0:0:1:9

3.  When the individual SCSI paths are known, remove them from the
    system:

        # echo 1 > /sys/bus/scsi/devices/0\:0\:0\:9/delete
        # echo 1 > /sys/bus/scsi/devices/0\:0\:1\:9/delete
        # dmesg | grep "Synchronizing SCSI cache"
        Synchronizing SCSI cache for disk sdp:
        Synchronizing SCSI cache for disk sdo:
