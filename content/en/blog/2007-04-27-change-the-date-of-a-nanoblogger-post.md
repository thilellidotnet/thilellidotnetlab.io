---
date: "2007-04-27T20:44:23Z"
tags:
- Nanoblogger
title: Change the date of a NanoBlogger Post
---

Here are the complete steps to change the date of a post, to look as if
it had been written on the changed date.

1.  Rename the entry\'s data file name to the desired date
2.  Rename, accordingly to the entry\'s data file name, the content of
    the corresponding `cat_X.db` file in which the entry reside (it
    seems not to be updated by `nb -u all` or anything else)
3.  Eventually correct the date in the DATE field of the entry using
    `nb -e #id`, correcting the *day* name too (thanks to the `cal(1)`
    utility)

Hum. I think there may be a better way to do this.
