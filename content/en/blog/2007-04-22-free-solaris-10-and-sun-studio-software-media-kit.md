---
date: "2007-04-22T17:29:17Z"
tags:
- Compiler
title: Free Solaris 10 and Sun Studio Software Media Kit
---

On the excellent [Solaris Developer
Center](http://developers.sun.com/solaris/ "The Source for Solaris Application Resources...")
web site, you can read this:

> While we\'re on break, consider ordering your Solaris and Sun Studio
> free media kit. It\'s on backorder but new kits will be available in
> the New Year.

So it is Christmas time, don\'t
[hesitate](http://www2.sun.de/dc/forms/reg_us_2211_391.jsp "Free Solaris 10 and Sun Studio...")
!

Side note: Since i was one of the very first to register for this kit, i
get one (shipped in France) as expected. Sadly, it was the 6/06 Solaris
update, in the same time the 11/06 was released\...
