---
date: "2012-01-20T09:55:39Z"
tags:
- Press
title: 'Press Review #7'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### Analyzing Interrupt Activity with DTrace

-   <http://blogs.oracle.com/timc/entry/analyzing_interrupt_activity_with_dtrace>

Interrupts are events delivered to CPUs, usually by external devices
(e.g. FC, SCSI, Ethernet and Infiniband adapters). Interrupts can cause
performance and observability problems for applications.

Performance problems are caused when an interrupt \"steals\" a CPU from
an application thread, halting its process while the interrupt is
serviced. This is called pinning - the interrupt will pin an application
thread if the interrupt was delivered to a CPU on which an application
was executing at the time.

### ZFSSA/S7000 major update

-   <http://sparcv9.blogspot.com/2012/01/zfssas7000-major-update.html>

The first major software update of S7000/ZFSSA/Fishwork in over a year
is now available. With the original version \"2011.Q1\" it seems a bit
delayed, perhaps due to the departure several key persons behind the
software post Oracle acquisition of Sun.

### The all-seeing eye of DTrace

-   <http://sparcv9.blogspot.com/2012/01/all-seeing-eye-of-dtrace.html>

I was recently involved with a problem related to backup software
running on Solaris, as part of a general health check of the system I
stumbled on something interesting that was not visible using
conventional tools.

### Windows 8: Getting More ZFS\'ish

-   <http://netmgt.blogspot.com/2012/01/windows-8-getting-more-zfsish.html>

Storage has always been a part of operating systems. Over time, storage
capabilities have increasingly became more sophisticated in operating
systems, consuming features from 3rd party partners. Occasionally, a
vendor will release very sophisticated increments to their operating
systems. Windows 8 is projected to receive more ZFS-like features, to
make it more competitive with Solaris, when Sun release ZFS over a
half-dozen years ago.

### Big Data : opportunité Business et (nouveau) défi pour la DSI ?

-   [http://blogs.oracle.com/EricBezille/entry/big\_data\_opportunité\_business\_et](http://blogs.oracle.com/EricBezille/entry/big_data_opportunit%C3%A9_business_et)

Comme le souligne une étude de McKinsey (\"Big Data: The next frontier
for innovation, competition, and productivity\"), la maîtrise des
données (dans leur diversité) et la capacité à les analyser à un impact
fort sur l'apport que l'informatique (la DSI) peut fournir aux métiers,
pour trouver de nouveaux axes de compétitivité. Pour ne citer que 2
exemples, ils estiment que l\'exploitation du Big Data pourrait
permettre d\'économiser plus de €250 milliards sur l\'ensemble du
secteur publique Européens (identification des fraudes, gestion et
mesures de l\'efficacité des affectations des subventions et des plans
d\'investissements, \...). Quant au secteur marchand, la simple
utilisation des données de géolocalisation pourrait permettre un surplus
globale de \$600 milliards\[\...\]

### Activity of the ZFS ARC

-   <http://dtrace.org/blogs/brendan/2012/01/09/activity-of-the-zfs-arc/>

Disk I/O is still a common source of performance issues, despite modern
cloud environments, modern file systems and huge amounts of main memory
serving as file system cache. Understanding how well that cache is
working is a key task while investigating disk I/O issues. In this post,
I'll show the activity of the ZFS file system Adaptive Replacement Cache
(ARC).

### Engineered Systems and Enterprise Architecture (or: How to Sell Dog Food Online)

-   <http://constantin.glez.de/blog/2012/01/engineered-systems-and-enterprise-architecture-or-how-sell-dog-food-online>

One of the first things that customers and sales teams realize when
dealing with Engineered Systems is: They fundamentally change the IT
architecture of a business.

Change is good, it means progress. But change is sometimes seen as a bad
thing: Change comes with fear.

The truth is that Engineered Systems really empower IT architects to add
value to their business, application and data architectures, without
worrying about the technology architecture.

### New My Oracle Support User Interface to Replace HTML-Based User Interface on January 27, 2012

-   <http://www.oracle.com/us/dm/100042350-wwsu11104830mpp001-oem-1489365.html>

On January 27, 2012, we will upgrade My Oracle Support's current
HTML-based user interface (UI) to a new one built using Oracle's
Application Development Framework (ADF). This upgrade is driven by
customer feedback, and will help provide our My Oracle Support
HTML-based users more streamlined access to support information and
services.

### NEW CERTIFICATION: Oracle Certified Associate (OCA), Oracle Solaris 11 System Administrator

-   <http://blogs.oracle.com/certification/entry/0684a>

Oracle Certification announces the release of the new \"Oracle Certified
Associate, Oracle Solaris 11 System Administrator\" certification. This
certification is for Oracle Solaris system administrators who possess a
strong foundation in the administration of the Oracle Solaris 11
Operating System and are proficient in essential system administration
skills such as managing local disk devices, managing file systems,
installing and removing Solaris packages and patches, performing system
boot procedures and system processes.

### Solaris Tip: How-To Identify Memory Mapped Files

-   <http://blogs.oracle.com/mandalika/entry/solaris_tip_how_to_identify>

A memory mapped (mmap\'d) file is a shared memory object, or a file with
some portion or the whole file was mapped to virtual memory segments in
the address space of an OS process. Here is one way to figure out if a
given object (file or shared memory object) was memory mapped in a
process or not.

### New Storage Magazine awards for NAS\...

-   <http://blogs.oracle.com/7000tips/entry/new_storage_magazine_awards_for>

Storage Magazine just came out with the January 2012 issue, showing
Oracle Storage doing quite well (\#1) with the Oracle ZFSSA 7420 and
7320 family. Check out pages 37-43 of this month\'s Storage Magazine.
Storage Magazine:
<http://docs.media.bitpipe.com/io_10x/io_103104/item_494970/StoragemagOnlineJan2012final2.pdf>
(pages 37-43)

### SSDs and the TPC-C top 10

-   <http://storagemojo.com/2012/01/19/ssds-and-the-tpc-c-top-10/>

If SSDs are so great, shouldn't we see the results in TPC-C benchmarks?
They are, and we do.

But there are some surprises.

### MWAC in Global Zone

-   <http://milek.blogspot.com/2012/01/immutable-global-zone.html>

Solaris 11 has a new cool feature called Immutable Zones. \[\...\]
Immutable Zones basically allow for read-only or partially read-only
Zones to be deployed. You can even combine it with ZFS encryption - see
Darren\'s blog entry for more details. The underlying technology to
immutable zones is called Mandatory Write Access Control (MWAC) and is
implemented in kernel. So for each open, unlink, etc. syscall a VFS
layer checks if MWAC is enabled for a given filesystem and a zone and if
it is it will check white and black lists associated with a zone and
potentially deny write access to a file (generating EROFS).

### IBM Slashes Some Power7 Processor Prices

-   <http://www.itjungle.com/tfh/tfh012312-story01.html>

The new year is well under way and IBM, as we report elsewhere in this
issue of The Four Hundred, has closed out last year and is facing
whatever new challenges it has. The big one is that new Opteron 6200
processors from Advanced Micro Devices and Sparc T4 processors from
Oracle are out, and the even bigger problem is that the Xeon E5
processors from Intel are shipping under NDA to selected customers and
are expected to launch this quarter.

With Power7+ machines expected this year, intense competition from X86
and Sparc iron, and a bunch of Power7 machines probably sitting in the
barn at IBM\'s resellers, there may never be a better time to get a
discount on Power7 processors.

### How Dell Migrated from SUSE Linux to Oracle Linux

*How Dell planned and implemented the migration, including key
conversion issues and an overview of their transition process.*

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/dell-linux-1498654.html>

In June of 2010, Dell made the decision to migrate 1,700 systems from
SUSE Linux to Oracle Linux, while leaving the hardware and application
layers unchanged. Standardization across the Linux platforms helped make
this large-scale conversion possible. The majority of the site-specific
operating system and application configuration could simply be backed up
and restored directly on the new operating system. Configuration changes
were minimal and most could be automated, easing the administration
effort required and helping achieve a reliable and consistent transition
procedure.

### ZFS Storage Appliance Calculator

-   <http://www.oracle.com/us/media/calculator/zfs/index.html>

### Hardware and Systems Upgrade

-   <http://www.oracle.com/oms/hardwareupgrade/index.html>

Learn how to upgrade and why refreshing your data center makes good
business sense.

### Oracle Solaris and Oracle SPARC T4 Servers---Engineered Together for Enterprise Cloud Deployments

-   <http://www.oracle.com/us/products/servers-storage/solaris/solaris-and-sparc-t4-497273.pdf>

Over the last 25 years, the Oracle Solaris has been developed
hand-in-hand with systems built around the SPARC processor. Oracle
Solaris is tightly integrated with the many system level capabilities of
the SPARC T4 processor, providing scalable, high-performance compute
capability coupled with integrated high-speed networking and
cryptographic acceleration.

Today, with Oracle Solaris 10 and SPARC T4 systems, existing
applications can receive an immediate performance boost and at the same
time companies can begin extending their operations into the cloud with
Oracle Solaris 11.

### Use the OPN Fast Track to move your Application to Oracle Solaris 11

-   <http://blogs.oracle.com/partnertech/entry/use_the_opn_fast_track>

The first building block is the Oracle Solaris binary guarantee. It
warrants that Oracle Solaris 10 binaries can be executed on Oracle
Solaris 11 without recompilation.

Even binary compatible applications rely on all the frame works which
have been provided with Oracle Solaris 10. Applications who need this
fine grained support of the older Oracle Solaris 10 infrastructure are
likely to work smoothly on Oracle Solaris 11systems using an Oracle
Solaris 10 branded zone. This will work as long as the applicaton has
been supported to run in a Oracle Solaris 10 zone before.

### Oracle and the Solaris Brand

-   <http://www.mis-asia.com/tech/operating-systems/oracle-and-the-solaris-brand/>

When Oracle Corp. acquired to save some brands of Sun Microsystems April
2009 through January 2010, much was made about the enterprise software
giant's entry to the hardware business. Now, two years on, things are
looking up for one of the industry's better known platforms for
performance and stability through the last couple decades---Solaris.
Just last month, Markus Flierl, Vice President of Software Development,
Oracle, told MIS Asia what Oracle has been doing to breathe new life
into Solaris and where he expects the new platform to add value to
industry users.
