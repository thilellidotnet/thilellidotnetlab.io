---
date: "2007-04-27T18:03:40Z"
tags:
- man
title: The alt.sysadmin.recovery Man Page Collection
---

When bitching on
[alt.sysadmin.recovery](news:alt.sysadmin.recovery "ASR Usenet"), some
of the contributors have authored the man pages they really wish were
included in `Unix` (with their associated commands). They (*sysadmins*)
truly are a twisted lot, and these contributions help to prove the
point. The ASR man page collection is a comprehensive reference to many
of the things sysadmins have to deal with in the profession\... as i do!
;-)

Please find the source distribution, `troff` formatted pages, below:

**Section 1 - User Commands**

-   [c](/blog/asr/c.1 "genericised soft drink generator (ie coffee, coke etc)")
-   [lart](/blog/asr/lart.1m "Luser Attitude Readjustment Tool")
-   [rtfm](/blog/asr/rtfm.1 "read the fscking manual")
-   [slave](/blog/asr/slave.1 "a semi-interactive interactive command for the dirty work")
-   [sysadmin](/blog/asr/sysadmin.1 "responsible for everything imaginable...")
-   [think](/blog/asr/think.1 "you don't have to think, the computer can think for you")
-   [whack](/blog/asr/whack.1 "mangle requests to a printer or damage a printer")

**Section 2 - System Calls**

-   [people](/blog/asr/people.2 "fetch a structure containing all ttys,...")

**Section 3 - C Library Functions**

-   [chastise](/blog/asr/chastise.3 "library function to punish users")

**Section 5 - File Formats**

-   [normality](/blog/asr/normality.5 "definition of what types of normalities...")

**Section 8 - Maintenance Commands**

-   [bosskill](/blog/asr/bosskill.8 "send a signal to your boss, or terminate your boss")
-   [ctluser](/blog/asr/ctluser.8 "control lusers")
-   [guru](/blog/asr/guru.8 "System administration")
-   [knife](/blog/asr/knife.8 "tools to improve network performance via SNIP")
-   [luser](/blog/asr/luser.8 "process to control the clueless individuals who (mis)use...")
-   [nuke](/blog/asr/nuke.8 "launch nuclear weapons at known sites")
-   [pmsd](/blog/asr/pmsd.8 "Periodically Manic System Daemon")

The online browsable version is also
[accessible](http://www.infonet.ee/~sbernard/manpage/ "alt.sysadmin.recovery man page distribution").

The whole lot can be obtained as the [ASR distribution archive
set](http://www.infonet.ee/~sbernard/manpage/asr_pages.tar.gz "ASR distribution archive").
