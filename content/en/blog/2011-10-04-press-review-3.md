---
date: "2011-10-04T15:55:42Z"
tags:
- Press
title: 'Press Review #3'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### Sun ZFS 7000 Storage Appliance 2010.Q3.4.0 Release

-   <http://blogs.oracle.com/fishworks/entry/sun_zfs_7000_storage_appliance>

A new minor update to the 2010.Q3 software has been posted. Note that
the release has over 80 bug fixes and includes a Disk Shelf SIM firmware
upgrade, ZFS resilvering performance improvements and Update
Healthchecks.

### What\'s New in Oracle Solaris 11: Oracle University session at Oracle Open World for System administrators and Developers New

-   <http://www.oracle.com/openworld/learn/other/oracle-university/index.html#solaris>

Course Description: This 1-day seminar provides a detailed look at the
newest key features of Oracle Solaris 11 and how to use these features
within your deployments. You will learn how to implement the new
packaging system, how using the default file system, ZFS, will improve
your data management capabilities, how to deliver fully virtualized
networking, and how to use the advanced user, application, and device
security.

### Developing Enterprise Applications for Oracle Solaris

-   <http://www.oracle.com/technetwork/server-storage/solaris11/technologies/developer-isv-422892.html>

Oracle Solaris delivers a highly robust, scalable and secure platform
for developing and delivering mission-critical enterprise and ISV
applications:\
. Run Oracle Solaris 10 applications unmodified in Oracle Solaris 10
Zones on Oracle Solaris 11\
. Protect your investment with the industry\'s first and most extensive
binary compatibility and source code guarantees\
. Leverage Oracle Solaris 11 cloud-ready application deployment
technologies such as IPS, SMF and Zones\
. Maximize application performance, increase application observability
and enhance developer productivity with Oracle Solaris Studio\
. \[\...\]

### IBM POWER Roadmap\... 7+ now late and only an almost 3 years projection for 8?

-   <http://netmgt.blogspot.com/2011/08/ibm-power-roadmap-now-late-and-almost-3.html>

Thank you for the IBM August 2011 POWER Roadmap tha the public
marketplace has been begging for\... did we miss the POWER7+ release???
A POWER 7 February 2010 launch would have POWER 7+ August 2011 launch
(and today is August 31, so unless there is a launch in the next 23
hours, it looks late to me.) Sketchy details on something possibly 3
years out??? No commitment beyond (almost) 3 years for POWER???

### Solaris 10 Update 10 Eminent and Imminent

-   <http://netmgt.blogspot.com/2011/08/solaris-10-update-10-eminent.html>

Oracle\'s Solaris and SPARC public road map is pretty clear - Solaris 10
Update 10 release is expected 2H 2011 with Solaris 10 Update 11
scheduled for 2H 2012.

In a little more than 1 month away, Oracle OpenWorld 2011 is scheduled
(October 2-6, 2011) to occur, which means significant announcements!

### Disable Large Segment Offload (LSO) in Solaris 10

-   <http://nilesh-joshi.blogspot.com/2011/08/disable-large-segment-offload-lso-in.html>

LSO saves valuable CPU cycles by allowing the network protocol stack to
handle large segments instead of the traditional model of MSS (TCP
Maximum Segment Size) sized segments. In the traditional network stack,
the TCP layer segments the outgoing data into the MSS sized segments and
passes them down to the driver. This becomes computationally expensive
with 10 GigE networking because of the large number of kernel functional
calls required for every MSS segment. With LSO, a large segment is
passed by TCP to the driver, and the driver or NIC hardware does the job
of TCP segmentation (LSO offload the segmentation job on Layer 4 to the
NIC driver). An LSO segment may be as large as 64 KByte. The larger the
LSO segment, better the CPU efficiency since the network stack has to
work with smaller number of segments for the same throughput.

So in simple words, use LSO for better network performance while
reducing processor (CPU) utilization.

### 100% of Solaris users use RBAC

-   <http://blogs.oracle.com/darren/entry/100_of_solaris_users_use>

Some of us in the Solaris Security Engineering group been asked a few
times recently questions like \"so how many customers actually use
Solaris RBAC ?\" The answer we give is usually variant of \"For Solaris
10 onwards 100% of users use RBAC\". Surely that is wrong and we can\'t
guarantee 100% of users of Solaris 10 and Solaris 11 are or will be
using RBAC can we ? We don\'t have data to back that up because we
don\'t even know who all the users of Solaris actually are. It actually
is correct we don\'t need data on usage to back it up. The reason being
you can\'t turn RBAC off in Solaris 10 onwards it is always in use in
parts of the system that 100% of users of Solaris always use.

The kernel always checks Solaris\'s fine grained privileges (82 distinct
privileges in Solaris 11 Express), even if the process is running \"as
root\". So 100% of Solaris systems make RBAC privilege checks.

### Oracle Unveils Oracle VM 3.0

-   <http://www.oracle.com/us/corporate/press/459406>

News Facts:\
. Today at an event for customers, partners and industry experts, Oracle
announced Oracle VM 3.0, the latest release of Oracle's server
virtualization and management solution.\
. Oracle VM 3.0 is suitable for all datacenter workloads and features
new policy-based management capabilities, advanced storage management
via the Oracle VM Storage Connect plug-in API; centralized network
configuration management, improved ease-of-use and Open Virtualization
Format (OVF) support.\
. With the centralization of storage management alongside of logical
network configuration and management, Oracle VM 3.0 allows
administrators to streamline and automate end-to-end virtual machine
provisioning for a significant reduction in time and overhead,
simplifying IT processes and helping to reduce costs.\
. Oracle VM 3.0 helps customers deploy enterprise software in a rapid,
repeatable and error-free manner with immediate availability of over 90
Oracle VM Templates for Oracle applications, middleware and databases.\
. Oracle VM 3.0 is four times more scalable than the latest VMware
offering, supporting up to 128 virtual CPUs per virtual machine, at a
fraction of the cost. Oracle VM 3.0 demonstrated support for up to 160
physical CPUs and 2TB memory using Oracle's Sun Fire X4800 M2 servers.\
. When compared to VMware vSphere5 running Red Hat Enterprise Linux
guest VMs, Oracle VM 3.0 running Oracle Linux guest VMs is four times
less expensive.\
Oracle VM is free to download, has zero license cost, and affordable,
enterprise-quality support is offered through a simple subscription
model per server. Terms, conditions and restrictions apply.\
\[\...\]

### Using smpatch to apply Solaris Cluster patches and other enhancements

-   <http://blogs.oracle.com/patch/entry/using_smpatch_to_apply_solaris>

It is now possible again to use the in-built Solaris 10 patch automation
utility, \'smpatch\' / Update Manager, to download patches for products
such as Oracle Solaris Cluster and Oracle Solaris Studio, as well as
Oracle Solaris Operating System patches. It is now also possible again
to use \'smpatch\' / Update Manager on 3rd party hardware.

These steps effectively switch \'smpatch\' / Update Manager from using
hardware serial number based access entitlement to User based access
entitlement, similar to the access entitlement mechanism used when
downloading patches via \'wget\' or manually via My Oracle Support
(MOS).

### Oracle Solaris 11 Express Available on Oracle Exadata Database Machines

-   <http://www.oracle.com/us/corporate/press/454114>

News Facts:\
. Oracle Solaris 11 Express is available on Oracle Exadata Database
Machines X2-2 and X2-8, Oracle today announced.\
. Customers can take advantage of the mission-critical reliability,
scalability, and security of Oracle Solaris to run their online
transaction processing (OLTP), data warehousing and consolidated
workloads on the x86-based Oracle Exadata systems.\
. With Oracle Exadata, Oracle Solaris customers can rapidly deploy an
engineered system to manage the largest and most critical database
applications, enabling them to run up to 10x faster with the rock solid
stability that Oracle Solaris consistently delivers.

### Oracle\'s SPARC T4 chip: Will you pay Larry\'s premium?

-   <http://www.theregister.co.uk/2011/08/22/oracle_sparc_t4_hot_chips/>

The SPARC T4 chips are presumably timed to hit the market with the
impending Solaris 11, which has been in the making for more than six
years and which presumably has been tuned to take every advantage of the
SPARC T4 chips. The original Sun roadmap had a eight-core,
eight-threaded SPARC T series chip coming out in the second half of 2011
for machines with one to four sockets implemented in a 40 nanometer
process from Taiwan Semiconductor Manufacturing Corp. This Yosemite
Falls processor was supposed to run at 2.5GHz and be based on a new
SPARC T core code-named \"VT,\" presumably short for \"Virtual Threads\"
but neither Sun or Oracle have said what VT is short for (probably not
Vermont).

Oracle has been hinting that this new VT core, which is now being called
S3 we learn from the Hot Chips presentation, has a feature called the
critical thread API. This feature allows a high priority application to
grab one thread on a core and hog all of the resources on that core to
significantly boost performance of that single thread; the other seven
threads on the chip get told to sit tight. In the prior S1 and S2 cores,
used in the prior SPARC T1, T2, T2+, and T3 processors, the threads were
hard coded and their sharing algorithms were set in stone\--etched in
silicon, to be more precise.

### Nyt om næste generation SPARC T-series

-   <http://blogs.oracle.com/hwpartner/entry/nyt_om_n%C3%A6ste_generations_sparc>

Excerpts from the article:\
\"The new T4 processor, running at 3GHz or more, has features that will
also allow T4-based systems to take on some workloads that today are
going to Intel Xeon processors, which today perform faster on
single-thread workloads than do the T3- series of SPARC processors.\"

\"At the Hot Chips 2011 conference, an IEEE technical conference held at
Stanford University from August 17-19, 2011, Oracle systems engineers
described the top features of the new T4 processors, including a
16-stage integer instruction pipeline and enhanced cryptographic
performance. Among the business benefits associated with the new design
will be: double the amount of per-thread throughput performance,
compared to T3 -- and a range of 2 to 7 times more single-thread
performance for business workloads than T3 processors. Given the
binary-compatibility of T3 and T4, this means that the same Oracle
Solaris applications that have been running on T3 will see considerable
speedup on T4, without recompilation.\"

### Why and How to Use Cluster Check in Oracle Solaris Cluster 3.3 5/11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/o11-068-cluster-check-solaris-485278.html>

The latest set of enhancements to cluster check is focused on validating
clusters during installation and initial configuration. The intent of
these enhancements is to enable administrators to perform the most
important of the Enterprise Installation Services (EIS) checks
themselves. If EIS personnel are involved, they, too, will benefit from
using cluster check, as described here. As a matter of fact, the EIS
team as well as the Oracle Support Services team played a vital part in
defining and implementing these enhancements.

In addition to a new focus on installation-time and configuration-time
checking, a seemingly small but quite important change was made to the
checks themselves: the way the results are titled. I always recommend
that cluster check be run with the -v (verbose) flag to turn on verbose
progress reporting. In the past, the checks were titled with \"problem
statements\" that described a problem. Many people would miss the fact
that most or all the checks were passing, so they were alarmed by the
titles and thought lots of problems were being discovered on their
cluster. Now, all existing checks are titled with \"check titles\"
instead of \"problem statements.\" Most of the titles are actually a
question and, typically, a \"yes\" answer means the cluster passed the
check.

Now for the big stuff: There are over forty new checks, many of which
apply both before clustering is installed (recall that scinstall(1M)
runs cluster check before configuring a node) and right after the
initial configuration of services. And even though the focus is on
initial installation and configuration, these checks are still useful
over the entire life of the cluster.

### Core Factor for T4 published

-   <http://www.oracle.com/us/corporate/contracts/processor-core-factor-table-070634.pdf>

Oracle has published an update to the Processor Core Factor Table that
lists the (yet to be released) T4 CPU with a factor of 0.5. This leaves
the license cost per socket the same compared to T3 and puts T4 in the
same league as SPARC64 VII+ and all current x86 CPUs. We will have to
wait for the announcement of the CPU until we can actually speak about
performance. But this core factor (which is by no means a measure of CPU
performance!) seems to confirm what the few other available bits of
information seem to be hinting at: T4 will deliver on Oracle\'s
performance claims.

### Installing Oracle Solaris 10 Using JumpStart on an Oracle Solaris 11 Express Server

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/installjumpstartons11ex-219229.html>

If you are familiar with using JumpStart to install the Oracle Solaris
10 operating system on networked SPARC and x86 platforms, then you
probably know that JumpStart can be used to install only the Oracle
Solaris 10 OS, not the Oracle Solaris 11 Express OS. However, the
JumpStart install server can be an Oracle Solaris 11 Express system.

Your Oracle Solaris 11 Express server can do two different jobs:\
. Serve Oracle Solaris 11 Express OS installations using Automated
Installer. For more information, see Oracle Solaris 11 Express Automated
Installer Guide.\
. Serve Oracle Solaris 10 OS installations using JumpStart. This article
describes how to set up a JumpStart install server on an Oracle Solaris
11 Express system.
