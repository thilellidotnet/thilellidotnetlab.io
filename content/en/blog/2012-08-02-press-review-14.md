---
date: "2012-08-02T13:52:41Z"
tags:
- Press
title: 'Press Review #14'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### SPARC Servers: An Effective Choice for Efficiency in the Datacenter

-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/idc-dce-2012-1612359.pdf>

Agility can also be added to their IT operations through more rapid
application rollout and an easy means to move services around in a
highly available, secure, and scalable environment. A new generation of
SPARC servers, based on SPARC T-Series processors, provides a wider
portfolio of hardware and software features that can be leveraged to
dramatically improve efficiency and agility.

### Migrate to Oracle's SPARC Systems

-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/migration/index.html>

Keeping up with changing market conditions can be a challenge.
Enterprises are looking for ways to cut costs and streamline their
business operations. So when it\'s time to migrate off your aging
servers, consider how Oracle\'s SPARC systems, software, and solutions
can protect your legacy application investment, improve IT efficiency
and services.

### It\'s good when it goes wrong and I am on holiday (nmon question peaks)

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/it_s_good_when_it_goes_wrong_and_i_am_on_holiday_nmon_question_peaks291>

I have never really understood why I get peaks and troughs in nmon
questions and urgent situations but July seems to be a peak and
fortunately I was not at work for a large part of it. Performance and
nmon Questions trend to come in three flavours: really dumb, genuine and
mega-urgent critical lets blame the hardware. Let me give you a flavour
of each type of question from the last month. Perhaps, reading these
will let you avoid the same problem or at least let you learn \"you are
not alone\".

### Enabling 2 GB Large Pages on Solaris 10

-   <https://blogs.oracle.com/mandalika/entry/enabling_2_gb_large_pages>

Giri Mandalika wrote an article how to use the 2 gigabyte page size
offered by T4 systems on Solaris 10: \"Enabling 2 GB Large Pages on
Solaris 10\". This can be really useful if you have an application
allocating really large amounts of memory. The more memory a single page
covers, the smaller the the translation tables from virtual to physical
memory gets, the higher the hit rate of the translation lookaside
buffer. However check out your application if it behaves well with such
large pages.

### CIS Solaris 11 Benchmark v1.0.0

-   <http://benchmarks.cisecurity.org/en-us/?route=downloads.show.single.solaris11.100>
-   <https://benchmarks.cisecurity.org/tools2/solaris/CIS_Solaris_11_Benchmark_v1.0.0.pdf>

This document is intended to address the recommended security settings
for the Solaris 11 operating system (Solaris 11 OS) running on x86 or
SPARC platforms.

### Coming from RHEL to Oracle Solaris? Need help?

-   <https://blogs.oracle.com/gman/entry/coming_from_rhel_to_oracle>

With Oracle Solaris 11 development, one of the primary goals was to
greatly modernize the operating system and make it easier to use, remove
some of the uneccessary differences between the two operating systems,
and remove some of the frustrations that people have had. I believe
we\'ve done exactly that with the introduction of Oracle Solaris ZFS as
the default root file system, the Image Packaging System (IPS) and much
more familiar installation experiences with the LiveCD and interactive
text installer. It\'s now even easier to approach Oracle Solaris,
install it into a virtual machine and give it a spin!

### Best Practices for Securely Deploying the SPARC SuperCluster T4-4

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/supercluster-security-1723872.html>

Oracle\'s SPARC SuperCluster T4-4 is a high performance, multipurpose
engineered system designed, tested, and integrated to run a wide array
of enterprise applications. It is well suited to many different tasks
including database and application consolidation, running multitier
enterprise applications, and multitenant application delivery. To
realize secure architectures such as these, the SPARC SuperCluster
platform offers a level of security synergy not often found in today\'s
IT architectures. Its engineering innovation and high degree of
integration provide a security potential that is truly greater than the
sum of its individual components.

### Oracle Solaris 11 Developer Webinar Series

-   <http://www.oracle.com/technetwork/server-storage/solaris11/overview/webinar-series-1563626.html>

Who should attend? Application developers and administrators who want to
learn how they can deploy Oracle Solaris 11 in their application
environment.

### Introducing the Basics of Service Management Facility (SMF) on Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/intro-smf-basics-s11-1729181.html>

The Service Management Facility (SMF), first introduced in Oracle
Solaris 10, is a feature of the operating system for managing system and
application services, replacing the legacy init scripting start-up
mechanism common to prior releases of Oracle Solaris and other UNIX
operating systems. SMF improves the availability of a system by ensuring
that essential system and application services run continuously even in
the event of hardware or software failures. SMF is one of the components
of the wider Oracle Solaris Predictive Self Healing capability.

### Advanced Administration with the Service Management Facility (SMF) on Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/adv-smf-admin-s11-1729196.html>

This article covers some more-advanced administrative tasks with SMF,
including an introduction to service manifests, understanding layering
within the SMF configuration repository, and how best to apply
configuration to a system. To learn more about SMF, check out a variety
of content at the SMF technology page on Oracle Technology Network.

### Are you entitled to the latest AIX 5.3 Service Pack?

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/AIXDownUnder/entry/are_you_entitled_to_the_aix_5_3_service_pack10>

There are a lot of organisations out there that are still running AIX
5.3. As you must know, AIX 5.3 is out of support. That doesn\'t stop you
updating to the latest Technology Level (TL 12). But you may not be
entitled to the latest Service Pack (SP 6). If you\'re staying with AIX
5.3, then you have a choice of being supported; unsupported (but legal),
or unsupported and illegal!

### How I Use the Advanced Capabilities of Btrfs

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/advanced-btrfs-1734952.html>

This article continues an exploration of Btrfs, looking into the more
interesting---and sometimes less obvious---features of Btrfs, such as
redundant configurations, data integrity options, compression,
snapshots, and performance enhancements.

### nmon Analyser Version 3.4 = Just Released, Get Your Copy Today

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/nmon_analyser_version_3_4_just_released_get_your_copy_today11>

Stephen Atkins (the Guru behind the nmon Analyser) has released a new
version - two days ago. This includes loads of improvements and some new
features. Best of all - less problems running on newer Excel releases
(it works around inconsistencies with the Microsoft API).

### How to Create Oracle Solaris 11 Zones with Oracle Enterprise Manager Ops Center

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-create-zones-ops-center-1737990.html>

Oracle Enterprise Manager Ops Center 12c manages Oracle hardware,
virtualization technologies, and operating systems that are deployed in
traditional, virtualized, and cloud environments. This article describes
how you can use it to create and configure Oracle Solaris 11 zones.

### Upcoming SPARC CPUs

-   <http://sparcv9.blogspot.fr/2012/08/upcoming-sparc-cpus.html>

The upcoming Hot Chips symposiums \"Big iron\" session will feature two
future SPARC processors\...

### The Role of Oracle VM Server for SPARC In a Virtualization Strategy

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/ovm-sparc-virtualization-fit-1835325.html>

Overview of hardware and software virtualization basics, and the role
that Oracle VM Server for SPARC plays in a virtualization strategy.
