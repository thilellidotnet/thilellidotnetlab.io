---
date: "2007-04-27T17:33:28Z"
tags:
- PR
- Nanoblogger
title: First PR Against the NanoBlogger's Port
---

Here is the
[first](http://www.freebsd.org/cgi/query-pr.cgi?pr=96985 "Problem Report ports/96985 : nanoblogger port 'bug'")
Problem Report against the NanoBlogger FreeBSD\'s
[port](http://portsmon.freebsd.org/portoverview.py?category=www&portname=nanoblogger "FreeBSD port overview").
In fact, this was not really a bug nor a big issue, just a misread at
the installation time it seems. Read the bug\'s follow-up for more
information\... really nothing more to say here.
