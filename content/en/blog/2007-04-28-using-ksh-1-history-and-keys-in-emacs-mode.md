---
date: "2007-04-28T09:50:25Z"
tags:
- Utility
title: Using ksh(1) History and Keys in Emacs Mode
---

In order to map the arrow keys to use history commands under the emacs
mode with `ksh(1)`, please put the following lines in the
`${HOME}/.kshrc`:

    # Arrow keys to use history commands under the emacs command edition mode.
    alias __A=`echo "\020"`   # mapping arrow key 'up'
    alias __B=`echo "\016"`   # mapping arrow key 'down'
    alias __C=`echo "\006"`   # mapping arrow key 'right'
    alias __D=`echo "\002"`   # mapping arrow key 'left'
    alias __H=`echo "\001"`   # mapping arrow key 'home'

*There are two underline characters in alias commands.*

To be able to get the PageUp and PageDown keys to work in `vi(1)`, edit
the `${HOME}/.exrc` file and insert the following lines:

    " Mapping PageUp & PageDown keys.
    map  ^[[5~   ^B
    map  ^[[6~   ^F

*Note: comment begin with a \" in `.exrc`, not a \#.*

How to enter the \^\[\[5\~ character? You have to type Control+V, then
press the PageUp key.

How to enter the \^B character? You have to type Control+Vn then press
the Control+B keys.

As a last tip, here is how to change the prompt using working directory
information. Using `export PS1='${PWD} $ '`, the behavior is as follow:

    /home/jpeg $ cd
    /home/jpeg $ cd /var/log
    /var/log $

Using `export PS1='${PWD##*/} $ '`, the behavior is as follow:

    jpeg $ cd
    jpeg $ cd /var/log
    log $
