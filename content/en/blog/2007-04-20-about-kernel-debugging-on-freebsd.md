---
date: "2007-04-20T20:34:17Z"
tags:
- Kernel
- Debug
title: About Kernel Debugging on FreeBSD
---

After
[posting](/post/2007/01/06/Kernel-Debugger-in-Solaris-10 "Kernel debugger in Solaris 10")
about some very interesting entries on how to debug kernel problems in
Solaris 10, here are some keywords which may be needed while looking for
help on FreeBSD mailing lists. This is just a quick memo, so please read
the excellent [Kernel
Debugging](http://www.freebsd.org/doc/en_US.ISO8859-1/books/developers-handbook/kerneldebug.html "Kernel Debugging")
chapter of the FreeBSD Developers\'s Handbook.

At the debugger prompt, using DDB at the system console:

    db> where
    db> show pcpu
    db> show alllocks
    db> ps
    db> show lockedvnods
    db> show lockedbufs

Good luck!
