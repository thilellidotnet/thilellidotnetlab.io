---
date: "2007-04-27T21:18:28Z"
tags:
- VIOS
- ODM
- SAN
title: Export and Import a Volume Group... When Things Goes the Wrong Way
---

1.  *nordika* is the hostname of the LPAR\... which is a VIOC too
2.  *nordikavg* is the name of volume group which resides on the SAN
    disks impacted by the export/reimport

Assuming that we want to migrate, on a VIOC, one or more currently SAN
disks attached on a local fibre channel adapter to the same one or more
SAN disks but now presented as SCSI storage media, seen \-- this time
\-- through a VIOS.

Here is the logical steps to follow\... when all things doesn\'t work as
expected (real life example)!

Get the list of the physical and logical volumes corresponding to the
volume group:

    # lsvg -p nordikavg
    nordikavg:
    PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
    hdisk2            active            269         0           00..00..00..00..00
    hdisk3            active            269         0           00..00..00..00..00
    hdisk4            active            269         0           00..00..00..00..00
    hdisk5            active            269         115         07..00..00..54..54
    hdisk6            active            269         269         54..54..53..54..54
    #
    # lsvg -l nordikavg       
    nordikavg:
    LV NAME             TYPE       LPs   PPs   PVs  LV STATE      MOUNT POINT
    loglv01             jfs2log    1     1     1    open/syncd    N/A
    fslv01              jfs2       480   480   2    open/syncd    /files/tables_oragl
    fslv02              jfs2       80    80    2    open/syncd    /files/oracle
    fslv03              jfs2       40    40    1    open/syncd    /files/tempo_oragl
    fslv05              jfs2       40    40    1    open/syncd    /files/redologs_oragl
    fslv06              jfs2       40    40    1    open/syncd    /files/system_oragl
    fslv07              jfs2       280   280   2    open/syncd    /files/tmpcdinst

Unmount the already mounted file systems:

    # umount /files/tables_oragl
    # umount /files/oracle
    # umount /files/tempo_oragl
    # umount /files/redologs_oragl
    # umount /files/system_oragl
    # umount /files/tmpcdinst

Deactivate a volume group and export the definition of a volume group
from a set of physical volumes:

    # varyoffvg nordikavg
    # exportvg nordikavg

Having verified that there is no physical volumes in the desired volume
group using `lspv`, remove them from the devices list with the
corresponding adapter:

    # rmdev -l hdisk2 -Rd
    # rmdev -l hdisk3 -Rd
    # rmdev -l hdisk4 -Rd
    # rmdev -l hdisk5 -Rd
    # rmdev -l hdisk6 -Rd
    #
    # lsslot -c slot
    # rmdev -l pci2 -Rd

**We assume that the fibre channel adapter is now seen through a VIOS:
it is not shown here how to dynamically move it from the LPAR to the
VIOS and allocate the PVs to a particular VIOC, i.e. `nordika` in our
case.**

Make the *new* disks available to the OS and verify that the presented
LUNs are the right ones:

    # cfgmgr
    # lscfg -l hdisk2
      hdisk2           U9113.550.65E3A0C-V5-C5-T1-L830000000000  Virtual SCSI Disk Drive
    # lscfg -l hdisk3
      hdisk3           U9113.550.65E3A0C-V5-C5-T1-L840000000000  Virtual SCSI Disk Drive
    # lscfg -l hdisk4
      hdisk4           U9113.550.65E3A0C-V5-C5-T1-L850000000000  Virtual SCSI Disk Drive
    # lscfg -l hdisk5
      hdisk5           U9113.550.65E3A0C-V5-C5-T1-L860000000000  Virtual SCSI Disk Drive
    # lscfg -l hdisk6
      hdisk6           U9113.550.65E3A0C-V5-C5-T1-L870000000000  Virtual SCSI Disk Drive

Generally, we just have to import the `nordikavg` volume group, activate
it, mount the file systems on it and\... enjoy. Since we encountered a
problem during the import (the information between the VM and the ODM
seems not synchronized accordingly), here are the steps we follow to
recover the situation.

Reimport the volume group, redefine the set of PVs of the given VG in
the device configuration database and activate it:

    # importvg nordikavg               /* Ooops... something goes wrong here! */
    # redefinevg -d hdisk2 nordikavg   /* One disk is sufficient to get the volume group information back. */
    # varyonvg nordikavg

Ok, the PVs are back in the configuration but not the type of the LVs,
according to:

    # lsvg -l nordikavg
    nordikavg:
    LV NAME             TYPE       LPs   PPs   PVs  LV STATE      MOUNT POINT
    loglv01             ???        1     1     1    open/syncd    N/A
    fslv01              ???        480   480   2    open/syncd    /files/tables_oragl
    fslv02              ???        80    80    2    open/syncd    /files/oracle
    fslv03              ???        40    40    1    open/syncd    /files/tempo_oragl
    fslv05              ???        40    40    1    open/syncd    /files/redologs_oragl
    fslv06              ???        40    40    1    open/syncd    /files/system_oragl
    fslv07              ???        280   280   2    open/syncd    /files/tmpcdinst

Synchronize or rebuild the logical volume control block, the device
configuration database and the volume group descriptor areas on the PVs:

    # synclvodm -v -P nordikavg
    synclvodm: Physical volume data updated.
    synclvodm: Logical volume loglv01 updated.
    synclvodm: Logical volume fslv01 updated.
    synclvodm: Logical volume fslv02 updated.
    synclvodm: Logical volume fslv03 updated.
    synclvodm: Logical volume fslv05 updated.
    synclvodm: Logical volume fslv06 updated.
    synclvodm: Logical volume fslv07 updated.
    #
    # lsvg -l nordikavg
    nordikavg:
    LV NAME             TYPE       LPs   PPs   PVs  LV STATE      MOUNT POINT
    loglv01             jfs2log    1     1     1    open/syncd    N/A
    fslv01              jfs2       480   480   2    open/syncd    /files/tables_oragl
    fslv02              jfs2       80    80    2    open/syncd    /files/oracle
    fslv03              jfs2       40    40    1    open/syncd    /files/tempo_oragl
    fslv05              jfs2       40    40    1    open/syncd    /files/redologs_oragl
    fslv06              jfs2       40    40    1    open/syncd    /files/system_oragl
    fslv07              jfs2       280   280   2    open/syncd    /files/tmpcdinst

Create complete boot image and device (in order to keep the type of LVs
persistent across reboot):

    # bosboot -a

    bosboot: Boot image is 23377 512 byte blocks.

Mount the file systems and\... enjoy :)

    # mount /files/tables_oragl
    # mount /files/oracle
    # mount /files/tempo_oragl
    # mount /files/redologs_oragl
    # mount /files/system_oragl
    # mount /files/tmpcdinst
