---
date: "2007-05-08T11:54:11Z"
tags:
- System
title: Best Practices in Installation Locations and Filesystem Hierarchies
---

As it is repeatedly discussed at work these days, I think it may worth
mentioning there exists official locations to install bundled and
unbundled packages, and well defined paths with a recommended purpose
for most major players in UNIX and UNIX-like platforms such as Solaris,
GNU/Linux and FreeBSD for example.

For Solaris (and OpenSolaris derived distributions), you can consult the
[Recommended Installation Locations for Solaris-compatible Software
Components](http://www.opensolaris.org/os/community/arc/policies/install-locations/)
document from the *Architecture Process and Tools* community, along with
the
[filesystem(5)](http://docs.sun.com/app/docs/doc/816-5175/filesystem-5?a=view)
manual page online, or on an installed system.

For a GNU/Linux environment, you can read both the [Linux Standard
Base](http://www.linux-foundation.org/en/LSB) and the [Filesystem
Hierarchy Standard](http://www.pathname.com/fhs/) recommendations.

Although the BSD are not so well-known as GNU/Linux, there are some very
good and alive projects. More, the documentation available for each
distributions are well written, and frequently updated. For these
systems, and most notably FreeBSD and HP-UX systems, you can read the
[hier(7)](http://www.freebsd.org/cgi/man.cgi?query=hier) manual page
online, or on a live system.

As a side note, it is interesting to note that [Ian
Murdock](http://ianmurdock.com/) (founder of the famous
[Debian](http://www.debian.org/) GNU/Linux distribution, and just
joining Sun to head up operating system platform strategy) is also the
chair of the Linux Standard Base---while Sun is a member of the Linux
Foundation (which host the LSB project). Maybe can we expect more
standardization between these two major players in the future?
