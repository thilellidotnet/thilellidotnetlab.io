---
date: "2007-09-04T18:43:59Z"
tags: []
title: Solaris 10 8/07 Release
---

The long awaited Solaris 10 8/07 release, a.k.a. Update 4 is now
available, and ready [to
download](http://www.sun.com/software/solaris/get.jsp). This release was
postponed many times due to some concerns about better testing and
validating advanced features such as the new [Deferred-Activation
Patching](http://www.sun.com/bigadmin/sundocs/articles/patch-wn.jsp), or
the support for the upcoming AMD quad-core next generation processor.
Interestingly, this release re-introduce the Documentation DVD.

As always, you *must* consult the excellent [What\'s New in the Solaris
10 8/07 Release](http://docs.sun.com/app/docs/doc/817-0547/getjd?a=view)
web pages. The following points are those I prefer:

-   [ZFS and Solaris iSCSI
    improvements](http://docs.sun.com/app/docs/doc/817-0547/getjj?a=view)
-   [x86: Fault Management For Next Generation AMD Opteron
    Processors](http://docs.sun.com/app/docs/doc/817-0547/getjj?a=view)
-   [Solaris Live Upgrade: Upgrading the Solaris OS When Non-Global
    Zones Are
    Installed](http://docs.sun.com/app/docs/doc/817-0547/getku?a=view)
-   [Deferred-Activation
    Patching](http://docs.sun.com/app/docs/doc/817-0547/getku?a=view)
-   [Single Hosts
    File](http://docs.sun.com/app/docs/doc/817-0547/getlg?a=view)
-   [Support for iSCSI Target
    Devices](http://docs.sun.com/app/docs/doc/817-0547/getkl?a=view)
-   [Multi-level CMT Scheduling
    Optimizations](http://docs.sun.com/app/docs/doc/817-0547/getjw?a=view)

Last, it is interesting to mention that the release number corresponds
to the build date of the release, not the general availability (GA)date.
It is the case for the 8/07 release, made public at the beginning of
September 2007. And it was the case in the past with the 11/06 release
which was made publicly available at the beginning of December, although
it was build during mid-November 2006:

    # cat /etc/release
                           Solaris 10 11/06 s10s_u3wos_10 SPARC
               Copyright 2006 Sun Microsystems, Inc.  All Rights Reserved.
                            Use is subject to license terms.
                               Assembled 14 November 2006

    # cat /etc/release
                           Solaris 10 8/07 s10x_u4wos_12b X86
               Copyright 2007 Sun Microsystems, Inc.  All Rights Reserved.
                            Use is subject to license terms.
                               Assembled 16 August 2007

*Update \#1: 2007-09-10*

I just forgot to update the preferred list with this particular point:
the [lx Branded Zones: Solaris Containers for Linux
Applications](http://docs.sun.com/app/docs/doc/817-0547/gevib?a=view).
Let me explain why in a [next blog
entry](/post/2007/09/12/The-BrandZ-Framework-Enhancements).

*Update \#2: 2007-09-11*

Sadly, the impressive work currently being done in the OpenSolaris
community (and already available with the Solaris Express Community
Edition) on the [performance on NFS with
ZFS](http://developers.sun.com/solaris/articles/nfs_zfs.html)
problem\--particularly the boot time problem\--is not yet back-ported to
the official release tree, including the use of evolution from
`sharemgr(1m)` plus in-kernel `sharetab(4)` facility. I hope for the
next Update.
