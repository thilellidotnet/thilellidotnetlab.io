---
date: "2007-04-28T09:43:59Z"
tags:
- Resource Management
title: 'Role, Pam and Su: Not So Well-Known Tip!'
---

Because `pam.conf(4)` is aware about role (see `rbac(5)` for more
information), we need to adapt the extended user attributes database
`user_attr(4)` in order to be able to substitute user using `su(1)` as
follow:

    # grep "^patrolusr" /etc/user_attr
    patrolusr::::type=normal;roles=oracle

*Substitute user from* patrolusr *to* oracle*.*
