---
date: "2007-04-26T20:31:25Z"
tags:
- JDS
title: Simply Can't Wait for Nevada Build 53, Desktop Experience
---

I am really excited by the upcoming Nevada build 53, particularly in the
desktop user space. Just read these two posts from [Alan
Coopersmith](http://blogs.sun.com/alanc/entry/my_favorite_new_feature_in "My favorite new feature in Nevada build 53...")
and [Stephen
Hahn](http://blogs.sun.com/sch/entry/53_looks_good "53 looks good") to
understand why! ;)
