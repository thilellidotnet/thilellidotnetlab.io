---
date: "2011-10-04T15:46:16Z"
tags:
- Press
title: 'Press Review #1'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### SPARC64 VIIIfx: New HPC \#1

-   <http://www.top500.org/lists/2011/06/press-release>
-   <http://www.opensparc.net/>

The 37th edition of the closely watched list was released Monday, June
20, at the 2011 International Supercomputing Conference in Hamburg. The
ranking of all systems is based on how fast they run Linpack, a
benchmark application developed to solve a dense system of linear
equations.

For the first time, all of the top 10 systems achieved petaflop/s
performance -- and those are also the only petaflop/s systems on the
list. The U.S. is tops in petaflop/s with five systems performing at
that level; Japan and China have two each, and France has one.

The K Computer, built by Fujitsu, currently combines 68544 SPARC64
VIIIfx CPUs, each with eight cores, for a total of 548,352
cores---almost twice as many as any other system in the TOP500. The K
Computer is also more powerful than the next five systems on the list
combined.

### Oracle Solaris Cluster 3.3 5/11 announced

-   <http://blogs.oracle.com/SC/en_US/entry/oracle_solaris_cluster_3_31>

Oracle announces Oracle Solaris Cluster 3.3 5/11, delivering unrivaled
High Availability (HA) on the Oracle Solaris OS for the largest
selection of enterprise applications and databases. Oracle Solaris
Cluster unique HA solution for Oracle Solaris\' virtual environments
lowers the inherent risk of consolidated infrastructures by leveraging
redundancy to protect from outages. Oracle Solaris Cluster Disaster
Recovery (DR) option extends these benefits to multi-site, multi-cluster
architectures to protect against planned and unplanned downtime, in
physical and virtual environments.

### Oracle Solaris Virtual Machine (VM) Templates

-   <http://www.oracle.com/technetwork/server-storage/solaris/solaris-vm-405695.html>

This page offers a variety of links to Oracle Solaris Virtual Machine
Templates. These Oracle VM Templates speed the creation of Proof of
Concept environments and other evaluation/development tasks by
dramatically simplifying the installation process. Oracle Solaris VM
Templates are available for both SPARC and x86-based systems.

### Pre-Built Developer VMs (for Oracle VM VirtualBox)

-   <http://www.oracle.com/technetwork/community/developer-vm/index.html>

Learning your way around a new software stack is challenging enough
without having to spend multiple cycles on the install process. Instead,
we have packaged such stacks into pre-built Oracle VM VirtualBox
appliances that you can download, install, and experience as a single
unit. Just assemble the downloaded files (if needed), import into
VirtualBox (available for free), import, and go!

### A Solaris Recommended Patchset to bind them all

-   <http://blogs.oracle.com/patch/entry/a_solaris_recommended_patchset_to>

A collaborative effort between the Software Patch Services, Enterprise
Installation Standards (EIS), Sun Risk Analysis System (SRAS) - now
renamed Oracle Risk Analysis Services (ORAS) - and the Explominer team
in the Oracle Solaris Technical Center (TSC), has achieved this goal
with the creation of the Recommended Patchset for Solaris.

Up until now, while the Solaris OS Recommended Patch Cluster was the
core basis for Solaris patch recommendations, various teams tended to
recommend their own favorite patches on top of this core set. This
wasn\'t just by whim. Each team was looking at patching from a slightly
different angle - for example various angles of proactive patching
(issue prevention) versus reactive patching (issue correction).

The Recommended Patchset for Solaris is the result of the combined
wisdom of the various teams. It is designed for proactive patching
(issue prevention). The contents are generic and should be suitable for
most customer configurations.

### Accelerating SPARC Solaris Servers

-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/public-sparc-roadmap-421264.pdf>
-   <http://digitaldaily.allthingsd.com/files/2009/09/oraclead.jpg>

### Integrated Load Balancer

-   <http://blogs.oracle.com/observatory/en_US/entry/integrated_load_balancer>

I\'m not sure how well known it is that Solaris 11 contains a load
balancer. The official documentation, starting with the Integrated Load
Balancer Overview, does a great job of explaining this feature. In this
blog entry my goal is to provide an implementation example.

For starters, I will be using the HALF-NAT operation mode. Basically,
HALF-NAT means that the client\'s IP address is not mapped so that the
servers know the real client address. This is usually preferred for
server logging (see ILB Operation Modes for more).

### Effective Resource Management Using Oracle Solaris Resource Manager

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/o11-055-solaris-rm-419384.pdf>

Oracle Solaris Resource Manager provides specific software components
and utilities that are used to manage hardware resources. It is
integrated into the operating system, and it is available on SPARC and
x86/x64 platforms running Solaris 9 or later.

Oracle Solaris Resource Manager is a key enabler for server and workload
consolidation and increased resource utilization. It provides the
ability to allocate and control major system resources, such as CPU,
virtual memory, physical memory, I/O bandwidth, and number of processes.
It also implements administrative policies that govern which resources
different users can access and, more specifically, how much of a
particular resource each user is permitted to use. Based on the
implemented policies, all users can receive resources commensurate with
their service levels and the relative importance of their work.
