---
date: "2010-08-14T18:45:53Z"
tags:
- SAN
title: Looking For WWN On HP-UX System
---

Just as a complement to more older entries about [SAN
storage](/post/2007/12/01/Nifty-Tool-For-Querying-Heterogeneous-SCSI-Devices)
and how to get specific [SAN
information](/post/2007/06/09/Getting-Emulex-HBA-Information-on-a-GNU/Linux-System),
here is the way I generally use when looking for World Wide Node Name,
and World Wide Port Name (WWNN, and WWPN respectively) on a HP-UX
system.

First, get the driver and instance name of all the HBA installed on the
system:

    # ioscan -knfC fc
    Class     I  H/W Path        Driver   S/W State   H/W Type     Description
    ===========================================================================
    fc        0  0/0/6/0/0/4/0   fcd      CLAIMED     INTERFACE    HP A9784-60002 PCI/PCI-X Fibre Channel FC/GigE Combo Adapter (FC Port 1)
                                /dev/fcd0
    fc        1  0/0/10/0/0/4/0  fcd      CLAIMED     INTERFACE    HP A9784-60002 PCI/PCI-X Fibre Channel FC/GigE Combo Adapter (FC Port 1)
                                /dev/fcd1
    fc        2  0/0/12/0/0/4/0  fcd      CLAIMED     INTERFACE    HP A9784-60002 PCI/PCI-X Fibre Channel FC/GigE Combo Adapter (FC Port 1)
                                /dev/fcd2
    fc        3  0/0/14/0/0/4/0  fcd      CLAIMED     INTERFACE    HP A9784-60002 PCI/PCI-X Fibre Channel FC/GigE Combo Adapter (FC Port 1)
                                /dev/fcd3

Then, get the disk or tape hardware paths to be crossed with the FC
information above:

    # ioscan -knfC tape
    Class     I  H/W Path        Driver   S/W State   H/W Type     Description
    ===========================================================================
    tape      0  0/0/10/0/0/4/0.11.16.255.0.0.1  stape    NO_HW       DEVICE       HP      Ultrium 3-SCSI
                                /dev/rmt/0m             /dev/rmt/0mn            /dev/rmt/c40t0d1BEST    /dev/rmt/c40t0d1BESTn
                                /dev/rmt/0mb            /dev/rmt/0mnb           /dev/rmt/c40t0d1BESTb   /dev/rmt/c40t0d1BESTnb
    tape      1  0/0/10/0/0/4/0.11.16.255.0.0.2  stape    NO_HW       DEVICE       HP      Ultrium 3-SCSI
                                /dev/rmt/1m             /dev/rmt/1mn            /dev/rmt/c40t0d2BEST    /dev/rmt/c40t0d2BESTn
                                /dev/rmt/1mb            /dev/rmt/1mnb           /dev/rmt/c40t0d2BESTb   /dev/rmt/c40t0d2BESTnb

Last, you just have to use the `fcmsutil` on the proper FC device:

    # fcmsutil /dev/fcd1

                               Vendor ID is = 0x001077
                               Device ID is = 0x002312
                PCI Sub-system Vendor ID is = 0x00103c
                       PCI Sub-system ID is = 0x0012c7
                                   PCI Mode = PCI-X 133 MHz
                           ISP Code version = 3.3.166
                           ISP Chip version = 3
                                   Topology = PTTOPT_FABRIC
                                 Link Speed = 2Gb
                         Local N_Port_id is = 0x0b0e00
                      Previous N_Port_id is = None
                N_Port Node World Wide Name = 0x50060b00005de769
                N_Port Port World Wide Name = 0x50060b00005de768
                Switch Port World Wide Name = 0x200e00051e5c1c17
                Switch Node World Wide Name = 0x100000051e5c1c17
                               Driver state = ONLINE
                           Hardware Path is = 0/0/10/0/0/4/0
                         Maximum Frame Size = 2048
             Driver-Firmware Dump Available = NO
             Driver-Firmware Dump Timestamp = N/A
                             Driver Version = @(#) libfcd.a HP Fibre Channel ISP 23xx & 24xx Driver B.11.11.10 /ux/kern/kisu/FCD/src/common/wsio/fcd_init.c:Sep 14 2006,17:45:41

That\'s it.
