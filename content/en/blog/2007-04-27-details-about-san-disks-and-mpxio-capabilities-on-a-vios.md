---
date: "2007-04-27T20:49:59Z"
tags:
- SAN
- MPxIO
title: Details About SAN Disks and MPxIO Capabilities on a VIOS
---

Obtaining these sorts of particular and specific informations (such as
MultiPath I/O status) from a Virtual I/O Server can be very easily
achieved using the following one (long) line shell script, helped by the
`lsdev(1)`, `lscfg(1)` and `lspath` commands:

    # for disk in `lsdev | grep hdisk | egrep  -v "SCSI Disk Drive|Raid1" | awk '{print $1}'`
    > do
    > lscfg -v -l ${disk} | egrep "${disk}|Manufacturer|Machine Type|ROS Level and ID|Serial Number|Part Number"
    > echo "`lspath -H -l ${disk} | grep ${disk} | awk '{print\"\tMultiPath I/O (MPIO) status: \"$1\" on parent \"$3}'`"
    > echo ""
    > done

      hdisk3           U787B.001.DNW3897-P1-C3-T1-W5006048448930A41-L9000000000000  EMC Symmetrix FCP MPIO RaidS
            Manufacturer................EMC     
            Machine Type and Model......SYMMETRIX       
            ROS Level and ID............5670
            Serial Number...............9312A020
            Part Number.................000000000000510001000287
            MultiPath I/O (MPIO) status: Enabled on parent fscsi0
            MultiPath I/O (MPIO) status: Enabled on parent fscsi1

      hdisk4           U787B.001.DNW3897-P1-C3-T1-W5006048448930A41-LA000000000000  EMC Symmetrix FCP MPIO RaidS
            Manufacturer................EMC     
            Machine Type and Model......SYMMETRIX       
            ROS Level and ID............5670
            Serial Number...............9312E020
            Part Number.................000000000000510001000287
            MultiPath I/O (MPIO) status: Enabled on parent fscsi0
            MultiPath I/O (MPIO) status: Enabled on parent fscsi1
    [...]

Pattern **SCSI Disk Drive** is excluded since it represents local SCSI
disks, as well as pattern **Raid1** because it is a *view* corresponding
to parity disks (which are logical disks only used by SAN
administrators).
