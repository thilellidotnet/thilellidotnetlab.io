---
title: "Blog"
weight: 20
---

_Blog entries older than Oct 13, 2013 are archives which dates back in
times when `blog o'thnet` articles were hosted on [gandi.net](https://www.gandi.net/)
running [dotclear](https://dotclear.org/)._

