---
date: "2007-04-27T18:19:59Z"
tags: []
title: New Infrastructure Server for the Hosting Project
---

As some of you may have seen in the past few weeks, web services
provided by the Thilelli.NET Project encountered some downtime.

In fact, the main reason behind this is that the second fan for the
power supply died suddenly, resulting to a very, very hot temperature
inside the server. After four days *as is*, we ended to reboot the
server after a Security Advisory from the FreeBSD [Security
Officer](http://www.freebsd.org/security/ "FreeBSD Security Information")\...
but the server never restarted. Sad news.

So, to make a relatively long story short, the project came back after
more than a week of continuous downtime: really sorry about that. It is
an all new hardware and upgraded software, as sum\' up here:

**Hardware side:**

-   Processor: AMD Athlon64X2 3800+, dual core 2GHz (socket 939) 64-bit
-   Motherboard: Asus A8N-SLI Deluxe
-   Memory: 2xCorsair PC4200 512MB Value DDR2 (1GB RAM)
-   Server Case: Antec Titan550 + 2xTriCool 92mm fans (front side panel)
-   Storage: 2xMaxtor 300GB 7200 RPM SATA300 (Maxline III), 16MB cache
-   Storage: 1xLaCie Hard Drive USB2 250GB design by Porsche
-   Storage: 1xLaCie Mobile Drive 2.5\" 40GB USB2.0

**Software side:**

-   Update from FreeBSD-5 to FreeBSD-6 Unix-like operating system
-   Switch from the
    [i386](http://www.freebsd.org/platforms/i386.html "FreeBSD/i386 Project")
    to the
    [amd64](http://www.freebsd.org/platforms/amd64.html "FreeBSD/amd64 Project")
    platform
-   All the software were upgraded and recompiled on the new platform
    architecture using the latest ports sources and available products

All of the services are now recovered, up and functional.

*Note:* Some old statistics were not put back (as for MRTG graphs),
although others were restored and updated (as for awstats, minus the
more than a week problem).

Last, sorry for this very hard but unexpected problem. All is OK by now.
