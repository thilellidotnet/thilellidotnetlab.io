#!/usr/sbin/dtrace -s
#pragma D option quiet

dtrace:::BEGIN
{
  printf("%s %s %s", "Sampling for", $$2, "... Please wait.");
}

syscall::*read:entry
/execname == $$1/
{
  self->thr = 1;
  self->fd = arg0;
}

syscall::*read:return
/self->thr/
{
  /*
   * About resolving the file name (self->vpath):
   * Shamefully stolen from the `rwsnoop' dtrace script (found
   * in the DTraceToolkit), by Brendan Gregg.  Please refer to
   * http://www.brendangregg.com/dtrace.html for more information
   * on DTrace tools and good examples.
   */
  this->filistp = curthread->t_procp->p_user.u_finfo.fi_list;
  this->ufentryp = (uf_entry_t *)((uint64_t)this->filistp +
   (uint64_t)self->fd * (uint64_t)sizeof(uf_entry_t));
  this->filep = this->ufentryp->uf_file;
  this->vnodep = this->filep != 0 ? this->filep->f_vnode : 0;
  self->vpath = this->vnodep ? (this->vnodep->v_path != 0 ?
   cleanpath(this->vnodep->v_path) : "<unknown>") : "<unknown>";

  @rnum[self->vpath] = count();
  self->thr = 0;
  self->fd = 0;
  self->vpath = 0;
}

profile:::tick-$2
{
  printf("%-80s %7s", "FILE NAME", "COUNT");
  printa("%-80s %@7d", @rnum);
  exit(0);
}
