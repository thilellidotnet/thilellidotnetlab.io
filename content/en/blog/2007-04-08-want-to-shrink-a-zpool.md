---
date: "2007-04-08T19:38:57Z"
tags:
- ZFS
title: Want to Shrink a zpool?
---

If so, be patient. In fact it **is** a high-wanted feature, and the ZFS
team is working hard on it right now. You can learn more about this
feature following the [Shrinking a
zpool?](http://www.opensolaris.org/jive/thread.jspa?threadID=8125 "Thread: Shrinking a zpool?")
thread on the `zfs-discuss` forum on opensolaris.org. Here are some
chosen excerpts.

From Matthew A. Ahrens \#1:

> Regardless of where you want or don\'t want to use shrink, we are
> actively working on this, targeting delivery in s10u5.

From Matthew A. Ahrens \#2:

> Yeah, the implementation is nontrivial. Of course, this won\'t have
> any impact on snapshots, clones, etc. and will happen on-line. Any
> other solution would be unacceptable.

Howdy\... I **really** like these kind of short and concise answers!
