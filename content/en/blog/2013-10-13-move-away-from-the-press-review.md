---
date: "2013-10-13T15:19:53Z"
tags:
- Press
title: Move Away From The Press Review
---

Well, a little bit of history about the press review. Initially, the
press review was born as a request from one of my customer, a little
more than four years ago. A this time, this review was oriented towards
its locally used technology only, and was commented and discussed to sum
things up appropriately according to its own needs.

Since then, this review was finally regularly published on this
blog\--some months after I leaved this customer in fact\--, but it now
takes me some time to collect and format, and was not really *discussed*
anymore: it\'s just a bunch of (I hope ;-)) interesting links, but
without much of value as it stands now. More, that information which are
asynchronous by nature are not well served when published once a month,
when interesting news came at the beginning of a month and is only
released four weeks later through the press review.

So, I decided to push these links more synchronously through my [Twitter
account](https://twitter.com/thilellidotnet), which feed this need more
appropriately I think. This way the information will flow more
regularly, and no need to wait for a bunch of them late each month.
Please follow [\@thilellidotnet](https://twitter.com/thilellidotnet) to
*subscribe* to the new form of the press review :)

Last, I just want to say I will try my best to resurrect the old purpose
of this blog which is more about writing technical contents, better
served using this kind of media.

I hope these changes will fit your needs. Please let me know if you see
any problem with this move. Thank you.
