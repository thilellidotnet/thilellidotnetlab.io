---
date: "2008-12-26T13:19:28Z"
tags:
- Network
title: Quick Reference Guides For Network Technologies
---

I am always looking for some good IT *cheat sheets*, and for a lots of
thing. Here is one of [my favorite web site](http://packetlife.net/)
around network technologies. Interestingly, this site propose some
regularly updated quick reference guides for the network, and are
classified under the following categories (at the time of this writing):

-   Protocols
-   Applications
-   Reference
-   Syntax
-   Technologies
-   Miscellaneous

You can find them all at the [Cheat Sheets
Library](http://packetlife.net/library/cheat-sheets/) section.

As the author wrote on his blog, "if you notice an error or would like
to see a new cheat sheet on a specific topic", don\'t hesitate and [drop
him a line](http://packetlife.net/contact/).
