---
date: "2013-04-02T07:30:32Z"
tags:
- Press
title: 'Press Review #22'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### Permissive and Restricted Policies

-   <https://blogs.oracle.com/gfaden/entry/permissive_and_restricted_policies>

Recently I posted two entries about the new Extended Policy
functionality in Oracle Solaris 11.1. One demonstrated how to create
application sandboxes, and the other how to confine services, like
MySQL. Both of these are examples of restrictive policies, whereas
privileges have traditionally been used to implement permissive
policies, hence the term privilege escalation. The basic functionality
and terminology first appeared in Trusted Solaris in 1991.
Unfortunately, a draft of the POSIX 1.e Security Specification
(withdrawn), used the term capabilities for this functionality, which
was subsequently used by Linux developers. Oracle Solaris privileges do
share some common terminology with Linux, including the permitted,
effective, and inheritable privilege sets. But almost everything else is
different.

![](https://blogs.oracle.com/gfaden/resource/RBAC_flow1.png)

### Midrange Power7+ Servers: The More Oomph You Want, The More It Costs

-   <http://www.itjungle.com/tfh/tfh040113-story01.html>

In last week\'s issue of The Four Hundred, I walked you though the
pricing for various sized configurations of the new Power 750+ and Power
760+ midrange servers using IBM\'s dual-chip module (DCM) variant of the
Power7+ processor. That just looked at the system hardware costs. This
week, as I have done in the past, I want to walk you through how the raw
computing, operating system, and Software Maintenance costs compare
across each processor SKU and how those costs stack up with prior Power
750 machines.

### Ksplice Inspector

-   <https://blogs.oracle.com/ksplice/entry/ksplice_inspector>

With so many kernel updates released, it can be difficult to keep track.
At Oracle, we monitor kernels on a daily basis and provide bug and
security updates administrators can apply without a system reboot. To
help out, the Ksplice team at Oracle has produced the Ksplice Inspector,
a web tool to show you the updates Ksplice can apply to your kernel with
zero downtime.

### Oracle Solaris and SPARC Performance, Part 1

-   <https://blogs.oracle.com/solaris/entry/oracle_solaris_and_sparc_performance>

To start with: we\'ve just released an update to Oracle Solaris Studio,
with compiler optimizations specifically designed to get the most
performance out of applications on Oracle T5, Oracle M5, and Fujitsu M10
systems.

### And no \... zfs scrub isn\'t a hidden fsck

-   <http://www.c0t0d0s0.org/archives/7570-And-no-...-zfs-scrub-isnt-a-hidden-fsck.html>

I\'ve got quite a number of tweets and mails with the question \"But is
zfs scrub\" not something like fsck. And the answer is \"Well \... no\".

### For Big Data Customers, Top Performance Means High Speed And Low Cost

-   <http://www.forbes.com/sites/oracle/2013/04/02/big-data-performance-speed-cost/>

I hate to quibble with the general manager of IBM's Power systems
division, but his argument that systems speed and power are no longer
top priorities for businesses is, to use his own words, "not at all in
tune with the market today."\
\[\...\]\
As a result, IBM is trying to shift the terms of the debate in order to
buy itself some time so that it can try to come up with some sort of
story that might blunt the simple facts that Oracle's new T5 servers
have not only posted benchmark results several times faster than
comparable IBM products, but are much less expensive as well.

### SPARC T5/M5-32 VAR Training Replay NOW AVAILABLE!

-   <https://enablement20.webex.com/enablement20/ldr.php?AT=pb&SP=MC&rID=74262692&rKey=ec671a779516a233>

Promote to your partners who were unable to attend the SPARC T5/M5-32
VAR Training session on March 28th or if you just want to review some of
the content again.

### 7 big reasons you should be running Solaris on Oracle x86 servers

-   <http://www.oracle.com/us/products/servers-storage/servers/x86/best-x86-platforms-solaris-wp-1907496.pdf>

Oracle's x86 Systems: The Best x86 Platforms for Oracle Solaris.

### Deploying an application in Solaris 11

-   <https://blogs.oracle.com/unixman/entry/deploying_applications_in_solaris_11>

Glynn takes his time to provide a thorough explanation to anyone who
might be entering the world of Solaris 11 and is interested in seeing
how things can be put together. Glynn goes over IPS components such as
actuators, facets, variants, mediators, a family of pkg\* commands,
reviews SMF elements such as manifests, profiles, and svcbundle, as well
as ties everything into building a zone and performing basic resource
management on how the zone consumes network bandwidth.

### Oracle Solaris and SPARC Performance, Part 2

-   <https://blogs.oracle.com/solaris/entry/oracle_solaris_and_sparc_performance1>

This is a perfect example of what we can do by co-engineering the
processor, the system, the OS, and the database.

### Oracle VM Templates now available on SPARC platforms

-   <https://blogs.oracle.com/virtualization/entry/oracle_vm_templates_now_available>

Oracle VM Templates provide an innovative approach to deploying a fully
configured software stack by offering pre-installed and pre-configured
software images. Use of Oracle VM Templates eliminates the installation
and configuration costs, and reduces the ongoing maintenance costs
helping organizations achieve faster time to market and lower cost of
operations. Customers have been enjoying the benefits of accelerating
software deployment with Oracle VM Templates on x86 platforms. Now we
have made Oracle VM Templates available when deployed with Oracle VM
Server for SPARC.

![](https://blogs.oracle.com/virtualization/resource/oraclevm/ovm32-sparcvm-running.jpg)

### Massive Solaris Scalability for the T5-8 and M5-32, Part 1

-   <https://blogs.oracle.com/sistare/entry/massive_solaris_scalability_for_the>

How do you scale a general purpose operating system to handle a single
system image with 1000\'s of CPUs and 10\'s of terabytes of memory? You
start with the scalable Solaris foundation. You use superior tools such
as Dtrace to expose issues, quantify them, and extrapolate to the
future. You pay careful attention to computer science, data structures,
and algorithms, when designing fixes. You implement fixes that
automatically scale with system size, so that once exposed, an issue
never recurs in future systems, and the set of issues you must fix in
each larger generation steadily shrinks.

### Shared Storage Pools (SSP3) and Disaster Recovery in 30 seconds

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/shared_storage_pools_ssp3_and_disaster_recovery_in_30_seconds>

Shared Storage Pools - just got even more interesting!

### Massive Solaris Scalability for the T5-8 and M5-32, Part 2

-   <https://blogs.oracle.com/sistare/entry/massive_solaris_scalability_for_the1>

Last time, I outlined the general issues that must be addressed to
achieve operating system scalability. Next I will provide more detail on
what we modified in Solaris to reach the M5-32 scalability level. We
worked in most of the major areas of Solaris, including Virtual Memory,
Resources, Scheduler, Devices, Tools, and Reboot. Today I cover VM and
resources.

### Oracle Solaris and SPARC (and x86) Performance, Part 4

-   <https://blogs.oracle.com/solaris/entry/oracle_solaris_and_sparc_and>

Simply speaking, east/west traffic is the traffic that rather than going
in and out of the data center (which in the world of this metaphor is
called north/south traffic), goes between servers in the same data
center \-- or even in the same physical server.

![](https://blogs.oracle.com/solaris/resource/2013/east-west.jpg)

### Oracle takes apparent step toward standardizing on a single chip design

-   <http://www.computerworld.com/s/article/9238181/Oracle_takes_apparent_step_toward_standardizing_on_a_single_chip_design>

Analysts say the unveiling of midrange and high-end servers running
Oracle-built Sparc chips that share the same architecture is a step
toward fulfilling the company\'s post-Sun plans.

### Sizing with rPerf but Don\'t Forget the Assumptions

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/size_with_rperf_if_you_must_but_don_t_forget_the_assumptions98>

POWER Relative Performance (rPerf) is often used as a way to approximate
the expected difference in performance between two Power Systems
servers. Although rPerf is a useful tool, it is important to understand
the limitations of using rPerf to provide an estimate the performance of
your specific workloads in your particular environment with a new
server.

### Systems Strategy Update Hartford CT April 2013

-   <http://fr.slideshare.net/OracleHardware/systems-strategy-update-hartford-ct-april-2013>

Slides from Systems Strategy Update Hartford CT April 2013 on the SPARC
Server Announcement and Oracle Virtual Networking (OVN) also known as
Xsigo.

### Clonez vos bases avec Oracle Snap Management Utility sur baies Sun ZFS

-   <http://www.digora.com/blog/clonez-vos-bases-avec-oracle-snap-management-utility-sur-baies-sun-zfs/>

Voulez-vous utiliser la technique des Snapshots avec vos bases Oracle ?
La solution Sun ZFS Storage Appliance peut vous simplifier la vie\...

![](http://www.digora.com/wp-content/uploads/2013/04/smu-01.jpg)

### Massive Solaris Scalability for the T5-8 and M5-32, Part 3

-   <https://blogs.oracle.com/sistare/entry/massive_solaris_scalability_for_the2>

Today I conclude this series on M5-32 scalability \[ Part1 , Part2 \]
with enhancements we made in the Scheduler, Devices, Tools, and Reboot
areas of Solaris.

### DoD customer receives authority to operate SparcSupercluster

-   <https://blogs.oracle.com/jimlaurent/entry/dod_customer_receives_authority_to>

Recently, one of our good U.S. DoD customers purchased a SPARC
SuperCluster system and received their \"Interim Authority to Operate\"
on the DoD network. Why is this a big deal? First, allow me provide an
overview of the SPARC SuperCluster system.

### Oracle Launches T5 and M5 Servers: A New Generation of Oracle\'s SPARC/Solaris Servers

-   <http://www.oracle.com/us/corporate/analystreports/idc-t5-launch-2013-1933475.pdf>

For longtime Sun customers, the technology refresh will be welcome,
leading to improved SPARC server sales right away. But Oracle can be
expected to look far beyond its own installed base, planning to grow its
total available market (TAM) by taking share from its longtime Unix
competitors, IBM and HP. Now that the new T5 and M5 technology is ready,
IDC will watch with great interest in coming quarters to see whether the
T5 and M5 announcements will lead to near-term market share gain and to
a long-awaited turnaround in Oracle\'s hardware business.

### Solaris 11 SRU naming convention change

-   <https://blogs.oracle.com/Solaris11Life/entry/solaris_11_sru_naming_convention>

We\'re tweaking the naming convention used by Oracle Solaris SRUs
(Support Repository Updates) to use a 5-digit version taxonomy.

For example, Oracle Solaris 11.1.6.4.0. The digits represent
Release.Update.SRU.Build.Respin. For the above example, Oracle Solaris
11.1 SRU 6.4.

### Understanding I/O: Random vs Sequential

-   <http://flashdba.com/2013/04/15/understanding-io-random-vs-sequential/>

I have another article planned for later in this series which describes
the inescapable mechanics of disk. For now though, I'll outline the
basics: every time you need to access a block on a disk drive, the disk
actuator arm has to move the head to the correct track (the seek time),
then the disk platter has to rotate to locate the correct sector (the
rotational latency). This mechanical action takes time, just like the
sushi travelling around the conveyor belt.

### A File System All Its Own

-   <http://queue.acm.org/detail.cfm?id=2463636>

In the past five years, flash memory has progressed from a promising
accelerator, whose place in the data center was still uncertain, to an
established enterprise component for storing performance-critical data.
It\'s rise to prominence followed its proliferation in the consumer
world and the volume economics that followed. With SSDs (solid-state
devices), flash arrived in a form optimized for compatibility---just
replace a hard drive with an SSD for radically better performance. But
the properties of the NAND flash memory used by SSDs differ
significantly from those of the magnetic media in the hard drives they
often displace. While SSDs have become more pervasive in a variety of
uses, the industry has only just started to design storage systems that
embrace the nuances of flash memory. As it escapes the confines of
compatibility, significant improvements in performance, reliability, and
cost are possible.

### What is the best platform to run your Oracle Database on?

-   <https://blogs.oracle.com/orasysat/entry/what_is_the_best_platform>

As a Systems Consultant I am often faced with the following question:
What does Oracle recommend as the best platform to run the Oracle DB in
large enterprise environments on? Is the Exadata DB Machine the
recommended platform? Are SPARC/Solaris servers the way to go? Or should
customers consolidate on SPARC SuperCluster setups?

### ZFS Analytics

-   <http://sparcv9.blogspot.com/2013/04/zfs-analytics.html>

While working with ZFS performance I created a dashboard to get a good
overview with lots of different statistics. It\'s powered by Dtrace,
python and graphite. There is a high level of detail but still easy to
correlate different statistics.

![](http://3.bp.blogspot.com/-0Y0BINhujoE/UXY1H6R_2_I/AAAAAAAAAHQ/rCM6SOwvfP8/s1600/Screen+Shot+2013-04-22+at+10.24.44+PM.png)

### Oracle Solaris 11 and PCI DSS Compliance

-   <http://www.oracle.com/us/products/servers-storage/solaris/solaris11/solaris11-pci-dss-wp-1937938.pdf>

This paper provides guidance to IT professionals who are implementing
Oracle Solaris 11 within their Cardholder Data Environment (CDE) and to
the Qualified Security Assessor (QSA) assessing those environments. The
Payment Card Industry Data Security Standard (PCI DSS) applies to all
organizations that store, process, or transmit cardholder data. This
includes entities such as merchants, service providers, payment
gateways, data centers, and outsourced service providers.
