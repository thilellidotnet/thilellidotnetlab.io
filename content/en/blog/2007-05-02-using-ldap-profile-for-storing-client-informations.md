---
date: "2007-05-02T20:00:54Z"
tags:
- LDAP
title: Using LDAP Profile for Storing Client Informations
---

One very interesting feature available in the LDAP client implementation
found on the Solaris platform is the capacity to define most of the
characteristic of a client specification in a profile, which can itself
be stored in an LDAP directory.

This way, you can easily change, deploy and modify common informations
for lots of clients. You can even change the duration of these
informations, using a Time To Live parameter (such as the TTL found in
DNS systems). This can be handy in the case of the push of new or
updated settings.

For example, you can easily define a Solaris system as a LDAP client
with all of relevant characteristics in the *workstation* profile:

    # ldapclient init \
     -a profileName=workstation \
     -a proxyDN=cn=proxyagent,ou=profile,dc=example,dc=com \
     -a proxyPassword=proxypassword \
     -a domainName=dc=example,dc=com \
     xx.xx.xx.xx
