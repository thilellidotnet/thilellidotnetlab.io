---
date: "2007-04-26T20:19:52Z"
tags:
- TSM
- Service
- Scheduler
title: How to Add a NewTSM Scheduler Daemon Service on RHEL4
---

-   `dsmc sched` represents the TSM *scheduler* daemon

Create the *TSM* service file:

    # cat /etc/rc.d/init.d/tsm
    #!/usr/bin/env sh

    # chkconfig: 345 56 24
    # description: TSM scheduler daemon
    # processname: /usr/bin/dsmc

    # Source functions library.
    . /etc/rc.d/init.d/functions

    LC_ALL=en_US; export LC_ALL
    dsmc="/usr/bin/dsmc"
    options="sched"
    rc=0

    start() {
      echo -n $"Starting `basename ${dsmc}`:"
      # Doesn't behave as expected because of the launching command
      # of the `tsm' scheduler :-(
      #initlog -c "${dsmc} ${options}  "    success || failure
      # So...
      [ -x ${dsmc} ]    (${dsmc} ${options}  ) > /dev/null 2> 1
      rc=$?
      if [ ${rc} -eq 0 ]; then
        success $"Starting `basename ${dsmc}`"
      else
        failure $"Starting `basename ${dsmc}`"
      fi
      echo
    }

    stop() {
      echo -n $"Stopping `basename ${dsmc}`:"
      if [ -n "`pidofproc ${dsmc}`" ]; then
        killproc ${dsmc} -TERM
      else
        failure $"Stopping `basename ${dsmc}`"
      fi
      rc=$?
      echo
    }

    case "$1" in
    start)
      start
      ;;
    stop)
      stop
      ;;
    restart)
      stop
      start
      ;;
    status)
      status ${dsmc}
      rc=$?
      ;;
    *)
      echo "Usage: `basename $0` {start|stop|restart|status}"
      rc=1
      ;;
    esac

    exit ${rc}

Add and configure the *TSM* service:

    # chmod 744 /etc/rc.d/init.d/tsm
    # chkconfig --add tsm
    # chkconfig --level 0126 tsm off
    # chkconfig --level 345 tsm on

Start, or restart, the *TSM* service, and monitor it:

    # service tsm restart
    # service tsm status
