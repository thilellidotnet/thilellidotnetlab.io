---
date: "2008-04-06T17:13:13Z"
tags:
- DST
title: Change The DST Time Zone Definition For Test Purpose
---

Because we continue to encounter some specialized software vendor which
can\'t tell if there is a problem with the Daylight Saving Time change
for their application, the need to test the time adjustment beforehand
arise sometimes. In this case, the first thing which comes in mind is to
change the system clock, without modifying the timezone. Although this
can do the job, it doesn\'t test the DST adjustment properly, and affect
the overall operating environment, system-wide.

To do things cleaner, we can try to modify directly the timezone in use.
This will test the real DST automatic adjustment, while not changing the
system clock (and not impacting other software, or services). Say we
want to change DST for `Europe/Paris` time zone one day before the
official date for summer 2008. First, obtain the current setting:

    # zdump -v `rtc` | \
       awk '$6 ~ /'"`date '+%Y'`"'/ && $12 !~ /'"`date '+%H:%M:%S'`"'/ {print $0}'
    Europe/Paris  Sun Mar 30 00:59:59 2008 UTC = Sun Mar 30 01:59:59 2008 CET isdst=0
    Europe/Paris  Sun Mar 30 01:00:00 2008 UTC = Sun Mar 30 03:00:00 2008 CEST isdst=1
    Europe/Paris  Sun Oct 26 00:59:59 2008 UTC = Sun Oct 26 02:59:59 2008 CEST isdst=1
    Europe/Paris  Sun Oct 26 01:00:00 2008 UTC = Sun Oct 26 02:00:00 2008 CET isdst=0

Then, get the time zone source file for the `Europe/Paris` geographic
definition and adapt it writing an exception rule for the current year.
Because the current definition for `Europe/Paris` is based on the `EU`
rule, our modification will be based on this rule name.

    # mkdir -p /tmp/zoneinfo/src
    # cp /usr/share/lib/zoneinfo/src/europe /tmp/zoneinfo/src
    # diff /usr/share/lib/zoneinfo/src/europe /tmp/zoneinfo/src/europe
    1073a1074,1076
    > # Test the DST adjustment one day in advance, for March 2008 only.
    > # Rule        NAME    FROM    TO      TYPE    IN      ON      AT      SAVE    LETTER/S
    > Rule          EU      2008    only    -       Mar     29       2:00   1:00    S

Last, compile the updated time zone definition, put it in place, and
verify the new DST date.

    # zic -d /tmp/zoneinfo /tmp/zoneinfo/src/europe
    # cp /tmp/zoneinfo/Europe/Paris /usr/share/lib/zoneinfo/Europe
    # zdump -v `rtc` | \
       awk '$6 ~ /'"`date '+%Y'`"'/ && $12 !~ /'"`date '+%H:%M:%S'`"'/ {print $0}'
    Europe/Paris  Sat Mar 29 00:59:59 2008 UTC = Sat Mar 29 01:59:59 2008 CET isdst=0
    Europe/Paris  Sat Mar 29 01:00:00 2008 UTC = Sat Mar 29 03:00:00 2008 CEST isdst=1
    Europe/Paris  Sun Oct 26 00:59:59 2008 UTC = Sun Oct 26 02:59:59 2008 CEST isdst=1
    Europe/Paris  Sun Oct 26 01:00:00 2008 UTC = Sun Oct 26 02:00:00 2008 CET isdst=0

Once the fake DST adjustment is validated at the software level, clean
things up a little.

    # zic /usr/share/lib/zoneinfo/src/europe
    # rm -r /tmp/zoneinfo
