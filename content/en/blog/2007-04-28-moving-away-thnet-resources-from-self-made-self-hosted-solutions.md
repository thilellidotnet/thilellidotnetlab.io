---
date: "2007-04-28T12:16:29Z"
tags: []
title: Moving away thnet resources from self-made, self-hosted solutions
---

After more than five years hosting my own services exclusively based on
Open Source products and executing on rock solid UNIX-like based
environments\--known as the *Thilelli.NET Project*, I decided (for
personal convenience only) to move to external hosting providers. The
new resources is now based on a mixed of [Gandi](http://www.gandi.net/)
and [Joyent](http://www.joyent.com/) solutions.

Gandi and Joyent are well known for doing a very great job, in a
pleasant manner. I urge you to learn about their work, and why Joyent is
the biggest player in the OpenSolaris community (using SXCE in
production), hiring nice and skilled guys in their team: [Ben
Rockwood](http://www.cuddletech.com/), [Mark
Mayo](http://www.vmunix.com/mark/blog/), and [Derek
Crudgington](http://hell.jedicoder.net/), just to name a few.

It means that I will have more internal resources to try and build new
technologies, avoiding strong constraints such as maintaining public
view and external services without the resources and financial support
such mentioned providers can have. It will increase email and web
serviceability as well.

So, I hope you will have great time reading my blog, and don\'t forget
to update your bookmarks and entries feed to:

-   [blog\'o thnet](/)
-   [Subscribe (RSS)](/feed/rss2)
-   [Subscribe (Atom)](/feed/atom)
