---
date: "2007-09-13T09:48:32Z"
tags:
- ISA
title: 64-bit System (Kernel), But 32-bit Binaries?
---

Once again, well known and great developer [Casper
Dik](http://blogs.sun.com/casper/) from Sun Microsystems give us an
interesting answer on the fact that there is not 32-bit emulation in the
Solaris OS when running a 64-bit kernel, and why most installed binaries
seems to be compiled as 32-bit executables, even if you boot on a 64-bit
platform, as in the following example:

    # isainfo -kv
    64-bit amd64 kernel modules
    # file /usr/bin/ls
    /usr/bin/ls:    ELF 32-bit LSB executable 80386 Version 1 [FPU], dynamically linked, \
     not stripped, no debugging information available

Here is the explanation, shown as a little Q&A:

Q: *Once I boot it in 64bit mode, i\'d have to run emulation libraries
to run 32bit bins right?*

A: No; you run the exact same binaries and libraries under 32 and 64
bit. It\'s not emulation; it\'s basically two syscall entry tables one
for 32 bit and one for 64 bit, mostly sharing the same code except where
pointer sizes matter.

Q: *Since it will be a very light server (only bare binaries to run my
req.s) they probably end up most likely all being 64bit?*

A: No, most binaries are 32 bit only those that need to be 64 bit are 64
bit.

Q: *so you are saying that i can run both 32bit and 64bit code
simultaneously, natively with the solaris kernel? That\'s pretty damn
cool*

A: Correct. There are several reasons for an OS which generally comes in
binary distributions to do so:

-   maintain complete binary compatibility with old applications (and
    yes, closed source does matter, even to a lot of Linux customers).
-   allows a single distribution to work on both 32 and 64 bit systems
    of the same architecture.

As a last note, it is interesting to see that a full install of SXCE
`snv_70` provide 1188 32-bit files and only 82 64-bit files in the
following paths `/bin`, `/usr/bin`, `/usr/openwin/bin`, `/usr/ucb`,
`/usr/sfw/bin`, and `/opt/SUNWspro/bin`. The 64-bit files are mostly
stored in a specific architecture subdirectory; i.e. `amd64` in this
case, while some of them are provided as both 32-bit and 64-bit
incarnations:

    # file /usr/bin/pfiles /usr/bin/amd64/pfiles
    /usr/bin/pfiles:        ELF 32-bit LSB executable 80386 Version 1 [FPU], \
     dynamically linked, not stripped, no debugging information available
    /usr/bin/amd64/pfiles:  ELF 64-bit LSB executable AMD64 Version 1 [SSE FXSR FPU], \
     dynamically linked, not stripped, no debugging information available

*Update \#1: 2008-04-07*

Be sure to consult the excellent [64-bit
FAQ](http://www.sun.com/software/solaris/faqs/64bit.xml) for Solaris.
