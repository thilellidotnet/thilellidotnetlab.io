---
date: "2007-04-26T20:24:26Z"
tags: []
title: Installation of the AsciiDoc Utility on Solaris
---
--- install.sh.orig  Sat Oct 21 09:21:11 2006
+++ install.sh       Sat Nov 11 14:02:21 2006
@@ -1,36 +1,61 @@
-#! /bin/sh
-#
+#!/usr/bin/env sh
+
 # AsciiDoc canonical installer/uninstaller.
 # Definative packager's guide.
 # Documented in INSTALL file.
-#

-BINDIR=/usr/local/bin
-MANDIR=/usr/local/man
-CONFDIR=/etc/asciidoc
+case `uname -s` in
+  SunOS)
+    install=/usr/ucb/install
+    sfwdir=/opt/sfw
+    bindir=${sfwdir}/bin
+    mandir=${sfwdir}/man
+    cnfdir=${sfwdir}/etc/asciidoc
+  ;;
+  *)
+    install=/bin/install
+    sfwdir=/usr/local
+    bindir=${sfwdir}/bin
+    mandir=${sfwdir}/man
+    cnfdir=/etc/asciidoc
+  ;;
+esac

-if [ `basename $0` == uninstall.sh ]; then
-    rm $BINDIR/asciidoc
-    rm $BINDIR/a2x
-    rm $MANDIR/man1/asciidoc.1
-    rm $MANDIR/man1/a2x.1
-    rm -rf $CONFDIR
+if [ &quot;`basename $0`&quot; = &quot;uninstall.sh&quot; ]; then
+  rm ${bindir}/asciidoc 2&gt; /dev/null
+  rm ${bindir}/a2x 2&gt; /dev/null
+  rmdir ${bindir} 2&gt; /dev/null
+  rm ${mandir}/man1/asciidoc.1 2&gt; /dev/null
+  rm ${mandir}/man1/a2x.1 2&gt; /dev/null
+  rmdir ${mandir}/man1 2&gt; /dev/null
+  rmdir ${mandir} 2&gt; /dev/null
+  rm -rf ${cnfdir}
 else
-    install asciidoc.py $BINDIR/asciidoc
-    install a2x $BINDIR/a2x
-    install -d $MANDIR/man1
-    install doc/*.1 $MANDIR/man1
-    install -d $CONFDIR/filters $CONFDIR/docbook-xsl \
-        $CONFDIR/stylesheets $CONFDIR/javascripts \
-        $CONFDIR/images/icons/callouts
-    install -m 644 *.conf $CONFDIR
-    install filters/*.py $CONFDIR/filters
-    install -m 644 filters/*.conf $CONFDIR/filters
-    install -m 644 docbook-xsl/*.xsl $CONFDIR/docbook-xsl
-    install -m 644 stylesheets/*.css $CONFDIR/stylesheets
-    install -m 644 javascripts/*.js $CONFDIR/javascripts
-    install -m 644 images/icons/callouts/* $CONFDIR/images/icons/callouts
-    install -m 644 images/icons/README images/icons/*.png $CONFDIR/images/icons
+  cp a2x a2x.$$
+  sed -e 's,#!/bin/bash,#!/usr/bin/env bash,' \
+   -e &quot;s,/etc/asciidoc,${cnfdir},&quot; a2x.$$ &gt; a2x
+  cp asciidoc.py asciidoc.py.$$
+  sed -e &quot;s,/etc/asciidoc,${cnfdir},&quot; asciidoc.py.$$ &gt; asciidoc.py
+  ${install} -d ${bindir}
+  ${install} asciidoc.py ${bindir}/asciidoc
+  ${install} a2x ${bindir}/a2x
+  ${install} -d ${mandir}/man1
+  ${install} doc/*.1 ${mandir}/man1
+  for dir in ${cnfdir}/filters ${cnfdir}/docbook-xsl ${cnfdir}/stylesheets \
+   ${cnfdir}/javascripts ${cnfdir}/images/icons/callouts; do
+    ${install} -d ${dir}
+  done
+  ${install} -m 644 *.conf ${cnfdir}
+  ${install} filters/*.py ${cnfdir}/filters
+  ${install} -m 644 filters/*.conf ${cnfdir}/filters
+  ${install} -m 644 docbook-xsl/*.xsl ${cnfdir}/docbook-xsl
+  ${install} -m 644 stylesheets/*.css ${cnfdir}/stylesheets
+  ${install} -m 644 javascripts/*.js ${cnfdir}/javascripts
+  ${install} -m 644 images/icons/callouts/* ${cnfdir}/images/icons/callouts
+  ${install} -m 644 images/icons/README images/icons/*.png ${cnfdir}/images/icons
+
+  mv a2x.$$ a2x
+  mv asciidoc.py.$$ asciidoc.py
 fi

-# vim: set et ts=4 sw=4 sts=4:
+exit 0
</pre>
<p>This tool is one of the utilities mentioned on the excellent OpenSolaris
<a title="Documentation Tools on OpenSolaris" href="http://www.opensolaris.org/os/community/documentation/doc_collab/tools/" lang="en">Documentation Tools</a> web page.</p>

Because it must always be easy to install, and uninstall, some new tools
to try, here is a very quick and little update against the last
installation script (`(un)install.sh`), in order to work out of the box
on
[Solaris](http://www.sun.com/software/solaris/ "Solaris Enterprise System")
and [OpenSolaris](http://www.opensolaris.org/os/ "OpenSolaris Project")
distributions. It try to preserve the very same behavior for (the
original-targeted) Linux platform, although not tested on it.

Here is the diff against the
[AsciiDoc](http://www.methods.co.nz/asciidoc/ "Text based document generation")
8.1.0 release:


