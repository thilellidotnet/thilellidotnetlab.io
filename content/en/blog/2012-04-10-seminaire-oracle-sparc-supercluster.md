---
date: "2012-04-10T09:10:54Z"
tags:
- Oracle
- Press
title: Séminaire Oracle SPARC  SuperCluster
---

I had the great opportunity to assist to the french event "Séminaire
Oracle SPARC SuperCluster: Le premier système intégré Oracle
multi-fonctions", which took place in Paris last Tuesday, April 5, 2012.

Follow the slides corresponding to this event.

### Title: Introduction Harry Zarrouk

*Speaker: Harry Zarrouk, Managing Director, Oracle Systems France*

URL:
<http://www.ndclients.com/oracle/downloads/00_intro_ssc_harry_zarrouk.pdf>
(fr) (1.4 MB)

### Title: Présentation Oracle SPARC SuperCluster

*Speaker: Jean-Yves Migeon, Business Development Manager*

URL: <http://www.ndclients.com/oracle/downloads/01_ssc_overview.pdf>
(fr) (1.5 MB)

### Title: Solutions optimisées Oracle sur SPARC SuperCluster

*Speaker: Eric Bezille, CTO Oracle Hardware France*

URL:
<http://www.ndclients.com/oracle/downloads/02_rapid_appli_reduced_risk_oos.pdf>
(fr) (1.4 MB)

### Title: Illustration avec Oracle PeopleSoft HCM/Scenarii d\'intégration

*Speaker: Nathalie Sabatte, Principal Sales Consultant Applications*

*Speaker: Eric Bezille, CTO Oracle Hardware France*

URL:
<http://www.ndclients.com/oracle/downloads/03_peoplesoft_on_ssc.pdf>
(fr) (1.2 MB)

### Title: SPARC SuperCluster, un système \"Production Ready\"

*Speaker: Pascal Guy, Solution Architect, EMEA Expert Server Group*

URL:
<http://www.ndclients.com/oracle/downloads/04_ssc_production_ready.pdf>
(fr) (2.4 MB)

### Title: SPARC SuperCluster: retour d\'expérience clients

*Speaker: Dario Wiser, Sr.Manager, SuperCluster Business Development*

URL:
<http://www.ndclients.com/oracle/downloads/05_supercluster_ref_usecases.pdf>
(fr) (0.4 MB)
