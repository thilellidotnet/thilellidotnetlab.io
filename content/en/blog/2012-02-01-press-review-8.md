---
date: "2012-02-01T12:56:47Z"
tags:
- Press
title: 'Press Review #8'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### How I Used CGroups to Manage System Resources In Oracle Linux 6

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/resource-controllers-linux-1506602.html>

Having worked with resource controls in Oracle Solaris, I was anxious to
learn how to do the same thing in Oracle Linux 6 with the Unbreakable
Enterprise Kernel (UEK). Resource management using control groups
(cgroups) is just one of many new features in Oracle Linux 6. Cgroups
give administrators fine-grained control over resource allocations,
helping to make sure that applications get what they need to deliver
consistent response times and adequate performance.

### ZFS: Apple Enters Storage Arena

-   <http://netmgt.blogspot.com/2012/02/zfs-apple-enters-storage-arena.html>

With the creation of ZFS, Apple MacOSX has finally made it into the
realm of being a very viable platform for server applications. No longer
will people need to use MacOSX as a client and buy a SPARC or Intel
Solaris platform as a server to gain the benefits of ZFS. Common
designers, video publishers, and media collectors can now just add the
occasional multi-terabyte hard drive and just keep on building their
data collection with limited concern for failure - it will all be
protected with parity and old deletions can be easily rolled back.

### Intimate Shared Memory (ISM) et Solaris x86

-   <http://www.gloumps.org/article-intimate-shared-memory-ism-et-solaris-x86-1er-partie-96734001.html>
-   <http://www.gloumps.org/article-intimate-shared-memory-ism-et-solaris-x86-2eme-partie-98254778.html>

Suite à une migration d\'une base de données Oracle d\'une architecture
Solaris Sparc à une architecture Solaris x86, l\'équipe DBA a décidé
d\'utiliser pleinement la mémoire disponible sur cette nouvelle
infrastructure. Disposant d\'un serveur avec 512 Go de mémoire, la SGA
de la base Oracle a été positionnée à 290 Go (afin de diminuer les
lectures physiques et d\'éviter les locks R/W). L\'augmentation de cette
SGA a eu un bénéfice important sur les opérations de lectures (plus
d\'activité sur les disques SAN concernant les lectures) par contre le
système Solaris saturait\...

### Standard Locations (why?)

-   <http://blogs.oracle.com/SolarisSMF/entry/standard_locations_why>

The manifest-import service manages importing of manifests that are
delivered as part of a package for an application. This instantiates the
service and its instances on the system. The manifest-import service
will then manage re-importing those manifests if they are
modified/upgraded in some way.

### Changes to svccfg import and delete

-   <http://blogs.oracle.com/SolarisSMF/entry/changes_to_svccfg_import_and>

The behavior of svccfg import and svccfg delete fmri has changed in S11
if the manifests are in SMF\'s standard locations.

### Oracle's SPARC T4 Server Momentum Expands Demand for SPARC Systems

-   <https://emeapressoffice.oracle.com/Press-Releases/Oracle-s-SPARC-T4-Server-Momentum-Expands-Demand-for-SPARC-Systems-2806.aspx>

SPARC T4 Servers Adopted By Customers Across All Industries, Regions

### Oracle Beats NetApp and EMC in Storage Magazine Quality Awards for NAS

-   <http://www.oracle.com/us/corporate/press/1505421>

Oracle's Sun ZFS Storage Appliances Earn Highest Ratings for Enterprise
and Midrange NAS Systems

### Solaris and SPARC virtualization management features of Oracle Enterprise Manager Ops Center including \"Live Migration\"

-   <http://blogs.oracle.com/oem/entry/virtualization_features_of_oem_ops>

Virtualization is not a new concept; however, there have been numerous
advances in recent years that are helping businesses to be more
effective at managing their virtualized environments. The easier it is
to manage assets reliably, with reduced risk of downtime, the better the
ability to focus on optimizing asset utilization in balance with
required Service Level Objectives.

### Fujian Mobile Replacing its Existing Teradata System with Oracle Exadata

-   <http://blogs.oracle.com/databaseinsider/entry/fujian_mobile_replacing_its_existing>

China Mobile (Fujian) (Fujian Mobile) is a rapidly expanding subsidiary
of China Mobile (HK) Group. Fujian Mobile's customer base has expanded
rapidly with subscribers growing from 2 million to more than 22 million
in the past few years. To meet this increasing demand, Fujian Mobile is
replacing its current Teradata system with a full-rack Oracle Exadata
system for its next-generation high performance BASS.

### SPARC: Road Map Updated!

-   <http://netmgt.blogspot.com/2012/02/sparc-road-map-updated.html>

The SPARC Road Map has been experiencing updates at a tremendous pace
over the past few years, with new SPARC releases either happening early,
with higher performance, or with a combination of the two. It is quite
exciting to see SPARC back in the processor game again!

### Less known Solaris 11 features: Shadow Migration

-   <http://www.c0t0d0s0.org/archives/7432-Less-known-Solaris-11-features-Shadow-Migration.html>

In the ZFS Storage Appliance we have little nice feature enabling you to
do migrations of data in the background. It\'s called Shadow Migration.
It\'s a really useful feature. Imagine you have a RAIDZ. After a time
you recognize that RAIDZ wasn\'t a good decision for your workload and
RAID10 would be much better choice. But how to transform it into a
RAID10 and how to do it with minimal interruption? You can do this with
the Shadow Migration feature. With the Shadow Migration feature, you can
migrate the data from one local or remote filesystem to another, while
you are already accessing the new one to get the data on the old ZFS
filesystem. This feature is available in Solaris 11 as well.

### Installing ZFS on a CentOS 6 Linux server

-   <http://prefetch.net/blog/index.php/2012/02/13/installing-zfs-on-a-centos-6-linux-server/>

As most of my long term readers know I am a huge Solaris fan. How can't
you love an Operating System that comes with ZFS, DTrace, Zones, FMA and
Network Virtualization amongst other things? I use Linux during my day
job, and I've been hoping for quite some time that Oracle would port one
or more of these technologies to Linux. Well the first salvo has been
fired, though it wasn't from Oracle. It comes by way of the ZFS on Linux
project, which is an in-kernel implementation of ZFS (this project is
different from the FUSE ZFS port).

### Increasing Application Availability by Using the Oracle VM Server for SPARC Live Migration Feature: An Oracle Database Example

-   <http://www.oracle.com/technetwork/server-storage/vm/ovm-sparc-livemigration-1522412.pdf>

One of the most significant business challenges is to create and
preserve value in a highly competitive environment, while keeping
business applications available and reducing costs. It is important to
maximize the business application availability during planned or
unplanned outages. This document provides information about increasing
application availability by using the Oracle VM Server for SPARC
software (previously called Sun Logical Domains).

By using an example and presenting various scenarios, this paper
describes how to take advantage of the Live Migration capability in the
Oracle VM Server for SPARC 2.1 software to increase the availability of
an Oracle Database 11g Release 2 single-instance database.

### Oracle Security Evaluations

-   <http://www.oracle.com/technetwork/topics/security/security-evaluations-099357.html>

Oracle Security Evaluations are an integral part of the Oracle Software
Security Assurance program. Go to Security Evaluations for more
information on the evaluations and validations that Oracle undertake.

### CIFS Sharing on Solaris 11

-   <https://blogs.oracle.com/paulie/entry/cifs_sharing_on_solaris_11>

Things have changed since Solaris 10 (and Solaris 11 Express too!) on
how to properly set up a CIFS server on your Solaris 11 machine so that
Windows clients can access files. There\'s some documentation on the
changes here, but let me share the full instructions from beginning to
end.

### The Difference Between a Standard and a Preferred Vendor

-   <http://constantin.glez.de/blog/2012/02/difference-between-standard-and-preferred-vendor>

Recently, I attended a customer workshop where the customer declared
that they standardized on x86, VMware and Linux.

That got me and my colleague thinking about what standardization really
means and whether that actually makes sense.

The workshop was actually about defining a PaaS platform for the
customer, and early in the process they just said: Fine, but it\'s gonna
be x86, VMware and Linux, because that\'s our standard. WTF?

### Three Enterprise Architecture Principles for Building Clouds

-   <http://constantin.glez.de/blog/2012/02/three-enterprise-architecture-principles-building-clouds>

One of the first things TOGAF recommends architects do when establishing
an Enterprise Architecture practice within a company is to formulate
Architecture Principles that guide the development of solutions. During
the last few workshops and during some discussions with other
architects, three principles in particular struck me as being key to
successfully developing a Cloud solution.

### Oracle Solaris Studio 12.3 has now been packaged for IPS

-   <http://pkg-register.oracle.com/>

Go to http://pkg-register.oracle.com/ for details of adding your cert,
key and publisher info!
