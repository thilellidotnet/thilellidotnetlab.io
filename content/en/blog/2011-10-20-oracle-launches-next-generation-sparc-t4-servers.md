---
date: "2011-10-20T15:05:37Z"
tags:
- Oracle
- Press
title: Oracle Launches Next Generation SPARC T4 Servers
---

Here is a little press review around Oracle technologies:

### New SPARC T4 Servers Deliver World Record Performance, Trump the Competition on Multiple Business-Critical Workloads

-   <http://www.oracle.com/us/corporate/press/497230>
-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/t-series/sparc-t4-4-faq-496527.pdf>
-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/t-series/sparc-t4-4-ds-486944.pdf>

#### Overview

The SPARC T4-4 is a high performing two or four-socket server based on
the SPARC T4 processor and optimized for data-intensive and enterprise
workloads. The SPARC T4-4 is the most powerful server in the T-Series
product family delivering unsurpassed single and multi-thread throughput
performance. With several world record benchmarks, the SPARC T4-4 has
set yet another milestone for the SPARC based industry leading server
platforms. The SPARC T4-4 server boasts speed, security, and unmatched
availability to data in a modular and compact 5 RU design. It is an
optimal server platform for Oracle database with enterprise reliability,
availability and security along with outstanding single thread
performance. SPARC T4-4 server nodes are the high performance system
building blocks for fault tolerant SPARC Supercluster servers supporting
business critical and performance sensitive workloads on Oracle Solaris.

#### News Facts

Oracle today announced its new SPARC T4 server line, delivering the
biggest generational performance increase in the history of Oracle's
SPARC processors. Oracle's SPARC T4 servers with Oracle Solaris deliver
unparalleled performance with impressive economics and are designed for
every tier in the enterprise. Oracle's new SPARC systems excel on
mission-critical single threaded and highly concurrent workloads, and
enable customers to consolidate multiple application tiers onto a single
server, reducing system complexity and improving utilization. Oracle's
SPARC T4 servers are engineered to provide both Oracle and third- party
applications with high performance, security, availability and
scalability, and are the foundation for Oracle's SPARC SuperCluster
T4-4, also announced today. Additional information on Oracle's SPARC T4
servers will be available during Oracle OpenWorld 2011. Oracle's SPARC
T4 Servers Offer Built-In Virtualization, Security and Dynamic Threads.

#### Key features in Oracle's SPARC T4 servers include

Built-in Virtualization with Live Migration -- With both Oracle VM for
SPARC and Solaris Zones, Oracle's SPARC T4 servers provide the
industry's most robust framework for virtualizing both instances of
Oracle Solaris, as well as lightweight virtualization for applications.
The servers provision in seconds, and now come with live, secure
migration. On-chip Cryptographic Acceleration -- New crypto units
support over a dozen industry standard ciphers, enabling security
conscious organizations in industries including telecommunications,
healthcare, financial services and the public sector to keep their data
safe with up to 44 percent faster secure queries than the latest
generation of x86 systems when encrypted with Oracle\'s Advanced
Security Products(4), 3x faster Oracle Solaris ZFS file system
encryption than the latest generation of x86 systems(5), and 4x faster
single-thread OpenSSL security than IBM POWER7(6).

Dynamic Threads -- The SPARC T4 processor includes automatic continuous
adjustment of core resources to balance between per thread and many
thread workloads. Integrated with Oracle Database 11g, Oracle WebLogic
Server 11g, and Java, the SPARC T4 processor presents no performance
compromise against any customer workload, in real time.

### SPARC T4 Deep Dive With Rick Hetherington

-   <http://www.oracle.com/us/corporate/innovation/deep-dive-hetherington-489126.html>

Rick Hetherington, Oracle's vice president of hardware development,
manages a team of architects and performance analysts who design
Oracle's M- and T-series processors. In this interview, Hetherington
describes the technical details of the new SPARC T4 processor and
explains why he thinks it is going to be an eye-opener for the industry.

### Oracle\'s SPARC T4-1, SPARC T4-2, SPARC T4-4, and SPARC T4-1B Server Architecture

-   <http://www.oracle.com/technetwork/server-storage/sun-sparc-enterprise/documentation/o11-090-sparc-t4-arch-496245.pdf>

### Oracle Solaris and Oracle SPARC T4 Servers---Engineered Together for Enterprise Cloud Deployments

-   <http://www.oracle.com/us/dm/h2fy11/s11-t4-final-497414.pdf>
