---
date: "2007-04-27T19:52:23Z"
tags:
- Bug
title: FreeBSD's Upcoming 6.0 Release
---

After playing with RELENG\_6 for a while, i can now safely says that the
new
[upcoming](http://www.freebsd.org/where.html#helptest "Help With The Next FreeBSD Release")
FreeBSD release will be a very good release(TM)! Altogether, the
operating system is very stable and have a good responsiveness.

But\... my main problem encountered with my laptop still remain: a long
standing problem with the GigE Ethernet driver
[re(4)](http://www.freebsd.org/cgi/man.cgi?query=re&sektion=4 "re -- RealTek 8139C+/8169/8169S/8110S PCI Ethernet adapter driver").
See Problem Report
[kern/80005](http://www.freebsd.org/cgi/query-pr.cgi?pr=80005 "PR kern/80005")
for more information on that subject. Just note that the problem was not
solved (nor changed) trying the new
[polling(4)](http://www.freebsd.org/cgi/man.cgi?query=polling&sektion=4 "polling -- device polling support")
operational mode, nor trying the development branch of FreeBSD:
7-CURRENT, as somebody had advised it to me in the past\... Too bad.
