---
date: "2010-12-19T17:50:59Z"
tags:
- X11
- SMF
- Bug
title: 'Solaris 11 Express: Problem #5'
---

*In this series, I will report the bugs or problems I find when running
the Oracle Solaris 11 Express distribution. I hope this will give more
visibility on those PR to Oracle to correct them before the release of
Solaris 11 next year.*

Some builds before Oracle decided not to provide a binary distribution
of Solaris Next anymore (build snv\_124 if I recall correctly), virtual
consoles were introduced. This a well-known feature for the Linux and
BSD people, but Solaris 11 Express is the first supported release of
Solaris where we can run multiple virtual terminals on the console.

This long-time missing feature is not enable by default though. Here are
the steps to do so:

    $ pfexec svccfg -s vtdaemon setprop options/secure=false
    $ pfexec svccfg -s vtdaemon setprop options/hotkeys=true
    $ pfexec svcadm enable vtdaemon
    $ pfexec svcadm enable console-login:vt2
    $ pfexec svcadm enable console-login:vt3
    $ pfexec svcadm enable console-login:vt4
    $ pfexec svcadm enable console-login:vt5
    $ pfexec svcadm enable console-login:vt6

Using Control-Alt-F1 to Control-Alt-F6, one can now switch to virtual
consoles. And using Control-Alt-F7, one can switch back to the X server.
Note: not setting the `options/secure` property to false will
automatically lock the X server screen.

Although switching to virtual consoles works as expected, getting back
to the X server is not really easy. From my experience, with the
`options/secure` property set to true, using Control-Alt-F7 get me to a
new login prompt, bypassing my (always?) logged-in session. With the
`options/secure` property set to false, using Control-Alt-F7 leave me
with a black screen and a blinking \_ cursor\... and nothing what I can
do without a remote access.

FYI, this problem is covered by the Bug ID number 7001741. Note that you
can add yourself to the interest list at the bottom of the bug report
page:

-   <http://bugs.opensolaris.org/bugdatabase/view_bug.do?bug_id=7001741>
