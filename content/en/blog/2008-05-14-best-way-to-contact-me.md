---
date: "2008-05-14T17:49:00Z"
tags: []
title: Best Way To Contact Me
---

If you care, the **best way** to contact me is certainly by email.
Although this an *asynchronous* and *informal* method, i really tend to
prefer this way since it best corresponds to my needs (at least for a
first contact). Generally speaking, i am very busy. I am busy with real
life, and i am busy working as an IT systems engineer. I rarely answer
GSM mobile phone calls directly, especially with an unknown caller phone
number.

Take into account that i am attentive to the format of email i receive,
too. Please consider reading carefully [Using Internet
mail](http://www.lemis.com/grog/email/email.php "Using Internet mail"),
written by Greg Lehey, to have an idea of what i mean here. Although a
little outdated, it is generally always applicable. Last, and as can be
found on his web site:

> As a measure against spam, my mail servers reject mail from servers
> which do not have reverse DNS lookup, or which claim to be a different
> name from the values returned by the reverse lookup. \[\...\] If your
> ISP is one of these, you will not be able to send me mail.

If you are always interested to contact me, you can [email
me](mailto:julien%20dot%20gabel%20at%20thilelli%20dot%20net) right now.
