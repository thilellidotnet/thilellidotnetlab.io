---
date: "2007-04-26T20:53:35Z"
tags:
- UPS
title: Configuring an APC Back-UPS RS 800VA
---
--- /usr/local/etc/apcupsd/apcupsd.conf.sample  Thu Jun 29 20:42:09 2006
+++ /usr/local/etc/apcupsd/apcupsd.conf Tue Jul  4 21:58:32 2006
@@ -26,7 +26,8 @@
 #     940-1524C, 940-0024G, 940-0095A, 940-0095B,
 #     940-0095C, M-04-02-2000
 #
-UPSCABLE smart
+#UPSCABLE smart
+UPSCABLE usb

 # To get apcupsd to work, in addition to defining the cable
 # above, you must also define a UPSTYPE, which corresponds to
@@ -63,8 +64,9 @@
 # dumb      /dev/tty**       Old serial character device for use
 #                            with simple-signaling UPSes.
 #
-UPSTYPE apcsmart
-DEVICE /dev/usv
+#UPSTYPE apcsmart
+UPSTYPE usb
+#DEVICE /dev/usv


 # LOCKFILE &lt;path to lockfile&gt;
</pre>
<p>Then, launch the daemon and get status information on the current UPS system
state:</p>
<pre>
# grep apcupsd /etc/rc.conf
apcupsd_enable=&quot;YES&quot;
# /usr/local/etc/rc.d/apcupsd start
# apcaccess status
APC      : 001,040,1003
DATE     : Fri Jul 07 20:48:14 CEST 2006
HOSTNAME : bento.thilelli.net
RELEASE  : 3.12.3
VERSION  : 3.12.3 (26 April 2006) freebsd
UPSNAME  : bento.thilelli.net
CABLE    : USB Cable
MODEL    : Back-UPS BR  800
UPSMODE  : Stand Alone
STARTTIME: Wed Jul 05 07:19:04 CEST 2006
STATUS   : ONLINE
LINEV    : 228.0 Volts
LOADPCT  :  39.0 Percent Load Capacity
BCHARGE  : 100.0 Percent
TIMELEFT :  20.6 Minutes
MBATTCHG : 5 Percent
MINTIMEL : 3 Minutes
MAXTIME  : 0 Seconds
OUTPUTV  : 230.0 Volts
DWAKE    : 000 Seconds
DSHUTD   : 000 Seconds
LOTRANS  : 194.0 Volts
HITRANS  : 264.0 Volts
RETPCT   : 000.0 Percent
ITEMP    : 29.2 C Internal
ALARMDEL : Always
BATTV    : 27.4 Volts
LINEFREQ : 50.0 Hz
LASTXFER : Low line voltage
NUMXFERS : 0
TONBATT  : 0 seconds
CUMONBATT: 0 seconds
XOFFBATT : N/A
SELFTEST : NO
STATFLAG : 0x07000008 Status Flag
SERIALNO : JB0604031535
BATTDATE : 2001-09-25
NOMBATTV :  24.0
FIRMWARE : .o2 .I USB FW:o2
APCMODEL : Back-UPS BR  800
END APC  : Fri Jul 07 20:48:15 CEST 2006
</pre>
<p>It is always a good thing to test the behavior of your UPS equipment and the
correct interaction with your server, so follow carefully the well
documentation provided on that <a title="Testing Apcupsd" href="http://www.apcupsd.com/manual/manual.html#testing-apcupsd" lang="en">subject</a> by the <code>apcupsd</code> team, particularly the
<em>Simulated Power Fail Test</em>, <em>System Shutdown Test</em> and <em>Full
Power Down Test</em> suggestions.</p>
<p>Here are some logs obtained after encountering some real power problems:</p>
<pre>
# cat /var/log/apcupsd.events
[...]   /* During a complete managed reboot after loosing power supply. */
Tue Jul 04 21:37:44 CEST 2006  Power failure.
Tue Jul 04 21:37:50 CEST 2006  Running on UPS batteries.
Tue Jul 04 21:38:21 CEST 2006  Reached run time limit on batteries.
Tue Jul 04 21:38:21 CEST 2006  Initiating system shutdown!
Tue Jul 04 21:38:21 CEST 2006  User logins prohibited
Tue Jul 04 21:38:21 CEST 2006  Attempting to kill the UPS power!
Tue Jul 04 21:38:31 CEST 2006  apcupsd exiting, signal 15
Tue Jul 04 21:38:31 CEST 2006  apcupsd shutdown succeeded
[...]   /* The next day... what a bad night for power supply, but a very
           good luck for all the servers! */
Tue Jul 04 23:57:58 CEST 2006  Power failure.
Tue Jul 04 23:58:01 CEST 2006  Power is back. UPS running on mains.
Wed Jul 05 00:16:07 CEST 2006  Power failure.
Wed Jul 05 00:16:09 CEST 2006  Power is back. UPS running on mains.
Wed Jul 05 00:18:58 CEST 2006  Power failure.
Wed Jul 05 00:19:01 CEST 2006  Power is back. UPS running on mains.
Wed Jul 05 00:27:10 CEST 2006  Power failure.
Wed Jul 05 00:27:13 CEST 2006  Power is back. UPS running on mains.
Wed Jul 05 02:45:08 CEST 2006  Power failure.
Wed Jul 05 02:45:11 CEST 2006  Power is back. UPS running on mains.
Wed Jul 05 03:29:29 CEST 2006  Power failure.
Wed Jul 05 03:29:32 CEST 2006  Power is back. UPS running on mains.
</pre>
<p>It may be worth mentioning that when the daemon encounter an event, an email
is sent to the <code>root</code> account. The subject says what it appended
(such as <em>Power has returned</em> or <em>Power Failure !!!</em>) and the
body is simply the ouput of the <code>apcaccess</code> program.</p>
<p>As a side note, the <code>apctest</code> sef-test program seems to fail with
my APC. Curiously, reading the result separately says... the opposite. Which
one to trust here, anyone?</p>
<pre>
# /usr/local/etc/rc.d/apcupsd stop
# apctest
2006-07-09 13:43:31 apctest 3.12.3 (26 April 2006) freebsd
Checking configuration ...
Attached to driver: usb
sharenet.type = DISABLE
cable.type = USB_CABLE

You are using a USB cable type, so I'm entering USB test mode
mode.type = USB_UPS
Setting up the port ...
Hello, this is the apcupsd Cable Test program.
This part of apctest is for testing USB UPSes.

Getting UPS capabilities...SUCCESS

Please select the function you want to perform.

1) Test kill UPS power
2) Perform self-test
3) Read last self-test result
4) Change battery date
5) View battery date
6) View manufacturing date
7) Set alarm behavior
8) Quit

Select function number: 2

This test instructs the UPS to perform a self-test
operation and reports the result when the test completes.

Clearing previous self test result...CLEARED
Initiating self test...INITIATED
Waiting for test to complete...TEST DID NOT COMPLETE

1) Test kill UPS power
2) Perform self-test
3) Read last self-test result
4) Change battery date
5) View battery date
6) View manufacturing date
7) Set alarm behavior
8) Quit

Select function number: 3

Result of last self test: PASSED
[...]
# /usr/local/etc/rc.d/apcupsd start
</pre>
<p>So, not too bad after all... at least, it seems.</p>
<p>For more information on that subject, go to the excellent Dan Langille's
<a title="The FreeBSD Diary" href="http://www.freebsddiary.org/" lang="en">FreeBSD Diary</a> which has a very well documented entry on his
<a title="apcupsd - Configuring a UPS daemon" href="http://www.freebsddiary.org/apcupsd.php" lang="en">settings</a>.</p>

I recently installed and configured an APC [Back-UPS RS
800VA](http://apc.com/resource/include/techspec_index.cfm?base_sku=BR800-FR "APC Back-UPS RS 800VA230V France"),
which is an UPS aimed to be a high performance battery backup and
protection for business computers. As i run some machines at home
(servers and workstations), this kind of protection became more than
necessary.

The main point is to properly install and configure `apcupsd`. Using the
FreeBSD\'s ports collection and a recent FreeBSD RELENG\_6 system, all
will works out of box.

So, after plug in the USB cable for the very first time, you can see
this in your `/var/log/messages` file:

    ugen0: American Power Conversion Back-UPS BR  800 FW:9.o2 .I USB FW:o2, rev 1.10/1.06, addr 2

Good. Just configure `apcupsd` to learn it about `UPSCABLE` and
`UPSTYPE` value:

    # diff -u /usr/local/etc/apcupsd/apcupsd.conf.sample /usr/local/etc/apcupsd/apcupsd.conf
