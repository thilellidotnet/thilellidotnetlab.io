---
date: "2007-04-20T20:40:11Z"
tags:
- Drupal
title: Thilelli.NET Project Website Design Update
---

In order to follow security fixes along with the evolving FreeBSD
Drupal\'s Port, the Thilelli.NET website has just been updated to the
Drupal [4.7.5 release](http://drupal.org/drupal-4.7.5 "Drupal 4.7.5").

The update process was not as smooth as expected, but all seems ok by
now. In the same time, reset the `thnet` theme to the more classical
`pushbutton`.
