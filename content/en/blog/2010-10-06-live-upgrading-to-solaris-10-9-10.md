---
date: "2010-10-06T14:51:14Z"
tags:
- Live Upgrade
- Upgrade
title: Live Upgrading To Solaris 10 9/10
---

If you try to update to the latest Solaris 10 Update (U9), one new step
is now required in order to be able to successfully `luupgrade` to the
desired Update. As mentioned in the [Oracle Solaris 10 9/10 Release
Notes](http://docs.sun.com/app/docs/doc/821-1839/installbugs-113?l=en&a=view),
a new Auto Registration mecanism has been added to this release to
facilitate registering the system using your Oracle support credentials.

So, if you try the classical `luupgrade` following incantation, it will
fail with the reported message:

    # luupgrade -u -n s10u9 -s /mnt -j /var/tmp/profile
    System has findroot enabled GRUB
    No entry for BE  in GRUB menu
    Copying failsafe kernel from media.
    61364 blocks
    miniroot filesystem is 
    Mounting miniroot at 
    ERROR: The auto registration file <> does not exist or incomplete.
           The auto registration file is mandatory for this upgrade.
           Use -k  argument along with luupgrade command.

So, you now need to set the Auto Registration choice as a mandatory
parameter. Here is how it resembles right now:

    # echo "auto_reg=disable" > /var/tmp/sysidcfg
    # luupgrade -u -n s10u9 -s /mnt -j /var/tmp/profile -k /var/tmp/sysidcfg
    System has findroot enabled GRUB
    No entry for BE  in GRUB menu
    Copying failsafe kernel from media.
    61364 blocks
    miniroot filesystem is 
    Mounting miniroot at 
    #######################################################################
     NOTE: To improve products and services, Oracle Solaris communicates
     configuration data to Oracle after rebooting.

     You can register your version of Oracle Solaris to capture this data
     for your use, or the data is sent anonymously.

     For information about what configuration data is communicated and how
     to control this facility, see the Release Notes or
     www.oracle.com/goto/solarisautoreg.

     INFORMATION: After activated and booted into new BE ,
     Auto Registration happens automatically with the following Information

    autoreg=disable
    #######################################################################
    Validating the contents of the media .
    The media is a standard Solaris media.
    The media contains an operating system upgrade image.
    The media contains  version <10>.
    Constructing upgrade profile to use.
    Locating the operating system upgrade program.
    Checking for existence of previously scheduled Live Upgrade requests.
    Creating upgrade profile for BE .
    Checking for GRUB menu on ABE .
    Saving GRUB menu on ABE .
    Checking for x86 boot partition on ABE.
    Determining packages to install or upgrade for BE .
    Performing the operating system upgrade of the BE .
    CAUTION: Interrupting this process may leave the boot environment unstable
    or unbootable.
    Upgrading Solaris: 100% completed
    Installation of the packages from this media is complete.
    Restoring GRUB menu on ABE .
    Updating package information on boot environment .
    Package information successfully updated on boot environment .
    Adding operating system patches to the BE .
    The operating system patch installation is complete.
    ABE boot partition backing deleted.
    PBE GRUB has no capability information.
    PBE GRUB has no versioning information.
    ABE GRUB is newer than PBE GRUB. Updating GRUB.
    GRUB update was successfull.
    INFORMATION: The file  on boot
    environment  contains a log of the upgrade operation.
    INFORMATION: The file  on boot
    environment  contains a log of cleanup operations required.
    INFORMATION: Review the files listed above. Remember that all of the files
    are located on boot environment . Before you activate boot
    environment , determine if any additional system maintenance is
    required or if additional media of the software distribution must be
    installed.
    The Solaris upgrade of the boot environment  is complete.
    Creating miniroot device
    Configuring failsafe for system.
    Failsafe configuration is complete.
    Installing failsafe
    Failsafe install is complete.

Not sure this will "ease" the upgrade path to this Update, even if there
is nothing really wrong with this. It may just have been less intrusive
I think.
