---
date: "2007-04-26T20:16:26Z"
tags:
- Control-M
title: How to Add a New "ctmag" Boot Script on AIX 5L (Control-M Agent v6)
---

Launch the *Control-M* agent at run-level \#2:

    # cat << EOF > /local/etc/init.d/ctmag
    #!/usr/bin/env sh
    ########################################################################
    # name: {K|S}ctmag
    # purpose: script that will start or stop the Control-M agent processes.
    ########################################################################

    ctmag_user="ctmag620"
    #ctmag_home=`getent passwd ${ctmag_user} | awk -F\: '{print $6}'`
    ctmag_home=`ypcat passwd | awk -F\: '$1 ~/'"${ctmag_user}"'/ {print $6}'`
    ctmag_opts="-u ${ctmag_user} -p ALL"
    rc=0

    case "$1" in
    start)
      ${ctmag_home}/ctm/scripts/start-ag ${ctmag_opts}
      ;;
    stop)
      ${ctmag_home}/ctm/scripts/shut-ag ${ctmag_opts}
      ;;
    * )
      echo "Usage: `basename $0` {start|stop}"
      rc=1
      ;;
    esac

    exit ${rc}
    EOF
    #
    # chmod 744 /local/etc/init.d/ctmag
    # ln /local/etc/init.d/ctmag /local/etc/rc2.d/Sctmag
    # ln /local/etc/init.d/ctmag /local/etc/rcS.d/Kctmag
    # ln /local/etc/init.d/ctmag /local/etc/rcshutdown.d/Kctmag

Start the new boot script:

    # /local/etc/init.d/ctmag start
    Control-M/Agent Listener started. pid: 237696
    Control-M/Agent Tracker started. pid: 442610

    Control-M/Agent started successfully.
