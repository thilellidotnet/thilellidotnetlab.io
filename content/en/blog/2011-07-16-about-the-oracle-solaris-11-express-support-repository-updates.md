---
date: "2011-07-16T11:21:53Z"
tags:
- Upgrade
title: About The Oracle Solaris 11 Express Support Repository Updates
---

With the last update of the Oracle Solaris 11 Express 2010.11
[SRU](https://support.oracle.com/CSP/main/article?type=NOT&id=1275533.1)
released last week (5 July 2011), Oracle introduced the number of the
repository update in the output of the `uname` command.

For Solaris up to version 10, the `uname` command just displayed the
kernel revision number which is nothing but explicit, unless you are a
[intimate with the kernel
PatchID](http://blogs.oracle.com/patch/entry/solaris_10_kernel_patchid_progression).

If you wanted to be confident about the Update of the OS you are
running, a better way was to look at the `/etc/release` file which is
more accurate in term of operating system baseline information, because
it was updated by a system update or by applying the Oracle Solaris
Patch Update Bundle for a given Update.

It seems that things are now evolving, certainly because of the way IPS
works. The `uname` now shows the exact update of the SRU, reflecting
very precisely the update the system is running, but the `/etc/release`
file is currently stuck at the build of the Solaris release, say
snv\_151a in the case of Oracle Solaris 11 Express 2010.11:

    $ pkg search -p entire
    PACKAGE                        PUBLISHER
    pkg:/entire@0.5.11-0.151.0.1.8 solaris
    $ uname -v
    151.0.1.8
    $ cat /etc/release
                          Oracle Solaris 11 Express snv_151a X86
         Copyright (c) 2010, Oracle and/or its affiliates.  All rights reserved.
                               Assembled 04 November 2010
