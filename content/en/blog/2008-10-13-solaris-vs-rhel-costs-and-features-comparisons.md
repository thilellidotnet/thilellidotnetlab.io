---
date: "2008-10-13T15:04:06Z"
tags:
- Comparison
title: Solaris vs. RHEL Costs And Features Comparisons
---

Clearly, the costs involved in running Solaris and RHEL platforms are
not well understood, and generally favors GNU/Linux environments. This
is (most of the time) untrue, since this tend to be based on personal
user experience, which is in fact far different from running lots of
systems in high demand production data centers.

Here some interesting readings on these subjects\--costs and features
analysis\--from:

-   Jim Laurent, OS Ambassador within Sun and evangelist for the Solaris
    OS\
    [Solaris 10 costs less than Red Hat Enterprise Linux (and does
    more)](http://blogs.sun.com/jimlaurent/entry/solaris_10_costs_less_than)
-   Paul Murphy, an IT consultant specializing in Unix and related
    technologies\
    [Cost comparison: Solaris/SPARC vs
    Linux/x86](http://blogs.zdnet.com/Murphy/?p=1020)
-   Marc Andreessen, creator of the Mosaic web browser, and co-founder
    of Netscape Communications\
    [Solaris is a better Linux than
    Linux](http://www.sun.com/software/solaris/ning.jsp)
-   Bill Vass, President and Chief Operating Officer of Sun Federal\
    [UPDATED Solaris vs. RHEL Comparison - to include Microsoft
    Windows](http://blogs.sun.com/BVass/entry/updated_solaris_vs_rhel_comparison1)

YMMV for sure, but I personally think that Solaris costs are
overestimated, and its features are mostly unknown, or at least
underused\... but this is a very large and hot topic nowadays, I know.

*Update \#1 (2008-11-14):* Go to read interesting comment update from
Jim Laurent.

*Update \#2 (2008-12-02):* Go to read Joerg Moellenkamp\'s entry about
[similar
points](http://www.c0t0d0s0.org/archives/5070-About-migrations.html).

*Update \#3 (2008-12-03):* Go to read this article appearing in
[Computerworld.com](http://www.computerworld.com/action/article.do?command=viewArticleBasic&articleId=9121382).
