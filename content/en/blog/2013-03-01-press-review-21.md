---
date: "2013-03-01T11:57:43Z"
tags:
- Press
title: 'Press Review #21'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### Implementing Root Domains with Oracle VM Server for SPARC

-   <http://www.oracle.com/technetwork/server-storage/vm/ovm-sparc-rootdomains-wp-1914481.pdf>

This paper describes how to implement an increasingly useful type of
virtualization feature known as root domains. It also describes an
operational model where root domains can be used in conjunction with
Solaris Zones for maximum performance and a high degree of flexibility.

### Oracle Magazine: SPARC at 25

-   <http://netmgt.blogspot.com/2013/03/oracle-magazine-sparc-at-25.html>

The SPARC architecture is perhaps the first and longest lasting open and
mainstram computing architecture in human history. In Ocrober 2012,
Network Management published a reminder for people to attend the \"SPARC
at 25\" event at the Computer History Museum. In November of 2012,
Network Management published an short article pointing to the replay of
the historic events: SPARC at 25: Past, Present, and Future. Diana
Reichardt published an article \"SPARC at 25\" in the bi-monthly printed
Oracle Magazine, covering the event.

### New Tab: OpenSXCE New Distribution!

-   <http://netmgt.blogspot.com/2013/03/new-tab-opensxce-new-distribution.html>

OpenSolaris grew from an Open Source repository to Open Solaris
Distribution (for both Intel and SPARC.) Solaris Express Community
Edition (Solaris SXCE) was the Intel/SPARC forerunner of Oracle Solaris
11, which abandoned UltraSPARC processors. OpenSXCE, based upon the work
of MarTUX, brings OpenIndiana and Illumos back to SPARC as a full
distribution, based upon standards such as SVR4 packaging.

### Encapsulating Oracle Databases with Oracle Solaris 11 Zones

-   <http://www.oracle.com/technetwork/database/database-cloud/private/dbcloud-s11-zones-wp-1911914.pdf>

Implementing higher degrees of isolation can be accomplished by
encapsulating each database environment. Encapsulation can be
accomplished with physical or logical isolation techniques. Oracle
recently certified 11gR2 RAC in Solaris 11 Zones, which is an important
capability for database clouds, because it enables strong isolation
between databases consolidated together on a shared hardware and O/S
infrastructure.

### How to Build a Secure Cluster

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-build-secure-cluster-1908780.html>

This article discusses how to enable Trusted Extensions on an Oracle
Solaris Cluster 4.1 cluster and configure a labeled-branded Exclusive-IP
type of zone cluster.

### How to Install Oracle Linux 6.3 with a Btrfs Root File System

-   <http://www.oracle.com/technetwork/articles/linux/howto-install-linux6-3-btrfs-1911478.html>

This article is a step-by-step guide for installing Oracle Linux 6.3
with a Btrfs root file system on an Oracle VM guest.

### Improve Your Hardware Support Experience

-   <https://blogs.oracle.com/GetProactive/entry/improve_your_hardware_support_experience>

Oracle Hardware customers who are running Oracle Solaris or Oracle
Solaris x86, can improve their support experience. Oracle Support
recommends these three Hardware Essentials to all Oracle Hardware
customers:

1.  Download and install the Services Tools Bundle.
2.  Set up Oracle Explorer.
3.  Enable Automatic Service Request (ASR).

### Console logging in Oracle VM Server for SPARC

-   <https://blogs.oracle.com/jsavit/entry/console_logging_in_oracle_vm>

A new feature in Oracle VM Server for SPARC provides logging for guest
domain consoles. Console I/O - all text on the serial console - is
logged for all or selected domains, including text that appears before
the domain boots Solaris. Logs are stored in files under the directory
/var/log/vntsd/domain. These directories are root-owned to protect them
from unauthorized access.

### ZFS Write Performance (Impact of fragmentation)

-   <http://blog.delphix.com/uday/2013/02/19/78/>

In the past few months, George Wilson and Adam Leventhal made
significant improvements to how writes are handled in ZFS. In this multi
part blog post, I will talk about a benchmark we created to measure
improvements in ZFS write performance as we make changes to the OS. In
this post, I will talk about the benchmark setup and run. I will show
some of the results from this benchmark on Delphix OS. In part two, I
will present data and some analysis on the bottlenecks discovered and
how they are going to be addressed.

### Performance

-   <http://www.c0t0d0s0.org/archives/7529-Performance.html>

Brendan Gregg has written an interesting piece about finding performance
problems: \"The USE method addresses shortcomings in other commonly used
methodologies\".

### Announcing Oracle Solaris Cluster 3.3 3/13

-   <https://blogs.oracle.com/solaris/entry/announcing_oracle_solaris_cluster_31>

Today we released Oracle Solaris Cluster 3.3 3/13 (that\'s a lot of
threes!). This update is specifically for use with Oracle Solaris 10,
delivering even more high availability and disaster recovery
capabilities for mission-critical application deployments.

### What is DSSD building?

-   <http://storagemojo.com/2013/03/13/what-is-dssd-building/>

The brains behind the ZFS filesystem -- including Jeff Bonwick and Bill
Moore -- have been hard at work for several years at start up DSSD. What
are they doing with Andy Bechtolsheim's money?

### Welcome to the mainline Linux kernel blog

-   <https://blogs.oracle.com/linuxkernel/entry/welcome>

I\'d like to welcome everyone to this new blog, where I\'ll be
discussing what\'s happening with the Oracle mainline Linux kernel
development team. I\'m James Morris, manager of the team. I report to
Wim Coekaerts. I\'m also the maintainer of the Linux kernel security
subsystem, which I blog about separately here.

### OpenAFS on Solaris 11 x86 - Robert Milkowski

-   <http://www.ukoug.org/what-we-offer/library/openafs-on-solaris-11-x86-robert-milkowski/>

The March Solaris SIG event is about running OpenAFS on Solaris 11 x86.
This talk will explore what makes Solaris unique in large OpenAFS
deployments with petabytes of storage - how it can save millions of
dollars and make debugging and performance analysis much easier and
quicker compared to other platforms. Some of the unique features of
OpenAFS will be described and how we used them to migrate one of the
largest OpenAFS deployments in the world - completely on-line and
transparently to AFS clients. Real world examples of how DTrace was used
to improve OpenAFS performance and scalability will be discussed.

### Dear StorageMojo: should I go all SSD?

-   <http://storagemojo.com/2013/03/19/dear-storagemojo-should-i-go-all-ssd/>

This came in this morning's email from a reader I'll call Perplexed. How
would you advise Perplexed?

### 10Gbit Ethernet, bad assumption and Best Practice - Part 1

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/10gbit_ethernet_bad_assumption_and_best_practice_part_137>

We find many people are over optimistic and making assumptions - which
can catch them out - we learnt the hard way too.

### PowerVM Virtual Ethernet Speed is often confused with VIOS, SEA IVE/HEA speed

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/powervm_virtual_ethernet_speed_is_often_confused_with_vios_sea_ive_hea_speed>

I have had a couple of Power systems administrators make assumptions
about the virtual Ethernet speed improvements when they install a 10 Gb
IVE/HEA in a VIOS which are simply not true. I guess that if three teams
have made this mistake then others are about too. So I intend here to
put the record straight.

### a few words about EtherChannel

-   <http://www.wmduszyk.com/?p=9780>

Originally, this technology was meant to protect a host against a
failure of its network adapter and/or switch (network switch).
Additionally, some unscrupulous salesmen claimed a fantastic increase in
throughput aka two adapters tied together will double, three adapters
tied together will triple throughput of the associated with them
EtherChannel adapter -- yes, in a salesman pure land of fantasy.

### Less known Solaris Features: Highly available loadbalancing

-   <http://www.c0t0d0s0.org/archives/7549-Less-known-Solaris-Features-Highly-available-loadbalancing..html>

As you may know, Solaris contains an integrated load balancer. It\'s
really easy to configure. Not that well known is the point, that you can
make it higly-available in an easy way as well. The following tutorial
will give you an overview on the configuration of this feature.

### Less known Solaris Features - Data Link Multipathing

-   <http://www.c0t0d0s0.org/archives/7553-Less-known-Solaris-Features-Data-Link-Multipathing.html>

I used this feature in the HA-loadbalancing tutorial already. However
the future too useful to stay just a \"by-word\" in a different article.
It is DLMP. Or by its full name:\"Data Link Multipathing\".

### SVM hang due to error disk (analyze)

-   <http://www.gloumps.org/article-svm-hang-due-to-error-disk-analyze-116565678.html>

Last weekend, I found the origin of the SVM bug using the mdb tool. Good
reading !!

After restarting the server, I wanted to mount a filesystem (metaset
object), but the following command was not responding\...

### Less known, but frequently used Solaris feature: Address space layout randomisation

-   <http://www.c0t0d0s0.org/archives/7562-Less-known,-but-frequently-used-Solaris-feature-Address-space-layout-randomisation.html>

One of the features introduced with 11.1 is the Address Space Layout
Randomization (ASLR) . And when you work with 11.1 you are already using
it. So it\'s a less known, but frequently used feature: less known in
the point that it exists, less known in the point of the methods to
control it, frequently used as it\'s activated per default for selected
binaries (and many were selected).

### How to Use an Existing Oracle Solaris 10 JumpStart Server to Provision Oracle Solaris 11 11/11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-s10jumpstart-s11-1918535.html>

This article illustrates how to take an existing JumpStart server,
install the Oracle Solaris 11 Provisioning Assistant for Oracle Solaris
10 on it, create and configure an installation service, and then
provision a client system using that installation service.

### Q&A About the Latest Oracle Linux Releases

-   <https://blogs.oracle.com/linux/entry/q_a_about_everything_oracle>

We sat down with Monica Kumar, Senior Director, Oracle Linux, Oracle
Virtualization, and MySQL Product Marketing, to discuss the latest
update on Oracle Linux.

### Exemple: Link Aggregations and VLAN Configurations for your consolidation (Solaris 11 and Solaris Zone)

-   <http://www.gloumps.org/article-exemple-link-aggregations-and-vlan-configurations-for-your-consolidation-solaris-11-and-solaris-zo-116675436.html>

Everyone knows that one of the major problem for consolidating Solaris
10 is network. if each Solaris Zones use a different network (vlan), the
configuration of the Global Zone becomes a real headache.

### No, ZFS still doesn\'t need a fsck. Really!

-   <http://www.c0t0d0s0.org/permalink/No,-ZFS-still-really-doesnt-need-a-fsck.html>

Friday was a day that i called once 10k day. More 10.000 visitors to my
blog in one day. Saturday was similar. This surge was create by an link
on news.ycombinator.com article i wrote roughly four years ago about
ZFS: No, ZFS really doesn\'t need a fsck.

Just wanted to express that four years later and a lot more experience
with ZFS later, 12 years after ZFS saw the light of the word, i\'m more
of the opinion that ZFS doesn\'t need a fsck than ever.
