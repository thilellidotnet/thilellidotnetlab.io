---
date: "2007-04-08T19:35:07Z"
tags:
- NFS
- File
title: Stale NFS File Handle
---

How to decode this cryptic NFS write error a Solaris client system,
found in the `/var/adm/messages` log file?

    Feb 26 11:54:26 clssunp120 nfs: [ID 626546 kern.notice] NFS write error on \
     host nfs1_prd: Stale NFS file handle.
    Feb 26 11:54:26 clssunp120 nfs: [ID 702911 kern.notice] (file handle: 40260001 \
     ffffffff a0000 1dc24 d5 a0000 2 0)

The problem is that the file handle specification is used and
interpreted internally by the NFS client subsystem, and evolved with
each release of the operating system. Luckily, we need only two fields
(on the eight printed in the record file). The first field is the file
system id (or device id) and is generally the first number of the file
handle. The second field of interest is the inode number, which is found
at the four, or five reference in the file handle (please note that the
inode is reported in hexadecimal format).

So, consider the following file handle:

    40260001 ffffffff a0000 1dc24 d5 a0000 2 0

We can now know which file system is the culprit:

    # grep 40260001 /etc/mnttab
    nfssrv:/t/tools/SunOS/isa /tools/isa nfs rw,intr,soft,dev=40260001 1172569690

Translate the inode number to decimal, to be used in conjunction with
the `find(1)` utility:

    # echo "ibase=16; `echo 1dc24 | tr [:lower:] [:upper:]`" | bc
    121892

Last, the file name a process is currently trying to reference can be
found easily with:

    # find /tools/isa -mount -inum 121892 -print 2> /dev/null

Search for the info doc
[73152](http://sunsolve.sun.com/search/document.do?assetkey=1-25-73152-1 "How to interpret the file handle in NFS...")
on the SunSolve web site for more information in this subject (you must
have a registered Sun support customer account to be able to view this
document).
