---
date: "2007-04-27T18:42:39Z"
tags:
- PR
- Nanoblogger
title: Maintainer of The www/nanoblogger FreeBSD's Port
---

Just to say that i am the new maintainer of the
[www/nanoblogger](http://www.freebsd.org/cgi/url.cgi?ports/www/nanoblogger/pkg-descr "Port description for www/nanoblogger")
port for the FreeBSD project (for [some
weeks](http://www.freebsd.org/cgi/cvsweb.cgi/ports/www/nanoblogger/ "ports/www/nanoblogger/")
now). See Problem Report
[ports/84817](http://www.freebsd.org/cgi/query-pr.cgi?pr=84817 "PR ports/84817")
for complete patch and update history.

Here is an excerpt of the PR description:

-   Make `portlint(1)` happy (`portlint -A`)
-   Add the files `pkg-plist` and `pkg-message`
-   The port conforms a little more to the [Porter\'s
    Handbook](http://www.freebsd.org/doc/en_US.ISO8859-1/books/porters-handbook/ "FreeBSD Porter's Handbook")
-   Validate a complete recommended test ordering from
    `porting-testing.html`

As a side note, the additional contributors
[list](http://www.freebsd.org/doc/en_US.ISO8859-1/articles/contributors/contrib-additional.html "Additional FreeBSD Contributors")
was not updated to reflect the change of maintainer ship\... Not so very
important, i think ;-)
