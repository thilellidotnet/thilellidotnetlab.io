---
date: "2011-10-20T15:06:05Z"
tags:
- Oracle
- Press
title: Oracle Unveils The World’s Fastest General Purpose Engineered System, The SPARC
  SuperCluster T4-4
---

Here is a little press review around Oracle technologies:

### The world's fastest general-purpose engineered system

-   <http://www.oracle.com/us/corporate/press/497229>
-   <http://www.oracle.com/us/corporate/features/sparc-supercluster-t4-4-489157.html>
-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/t-series/sparc-supercluster-faq-496617.pdf>
-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/t-series/sparc-supercluster-ds-496616.pdf>

#### Overview

The Oracle SPARC SuperCluster T4-4 is a general purpose engineered
solution for running a wide range of enterprise applications with the
highest levels of performance and mission critical reliability. The
SPARC SuperCluster T4-4 utilizes high performance software from Oracle
Exadata and Oracle Exalogic combined with new SPARC T4-4 servers, Oracle
Exadata Storage Servers, ZFS Storage Appliance, and InfiniBand
technology, and Oracle Solaris 11. With the addition of the SPARC
SuperCluster, Oracle continues to set the standard for engineered
systems: maximizing customer value with leading performance in a
complete and tested package.

#### News Facts

In a preview of Oracle OpenWorld 2011, Oracle announced the SPARC
SuperCluster T4-4, the first general purpose engineered system that
combines the computing power of the new SPARC T4 processor, the
performance and scalability of Oracle Solaris 11, the optimized database
performance of Oracle Exadata storage, and the accelerated middleware
processing of the Oracle Exalogic Elastic Cloud.

SPARC SuperCluster T4-4 server is an integrated apps-to-disk solution
that delivers the highest performance, security, and manageability with
the lowest TCO. Based on Oracle\'s next-generation SPARC T4 servers,
these systems can deploy multiple databases and applications, and
multiple tiers of applications while providing lightning-fast
improvements on data compression, queries, OLTP response times, and Java
middleware performance. Applications can run on a mix of Oracle Solaris
10 and Oracle Solaris 11 via Oracle VM Server for SPARC and Oracle
Solaris Containers.

Oracle\'s SPARC SuperCluster is ideal for the consolidation of
mission-critical enterprise applications or for the deployment of Oracle
Optimized Solutions, which provide fully documented best practices and
ongoing full-stack and patch testing. Today Oracle announced two new
Oracle Optimized Solutions for the SPARC SuperCluster T4-4 that will
support PeopleSoft Human Capital Management and Oracle WebCenter
Content. Delivered tested and ready to run, these systems can be
deployed in days, not months. Oracle continues to set the standard for
engineered systems that deliver record-breaking performance in a
complete and tested package.

### Oracle rises for Unix server push

*SPARC T4 systems: Same skins, new brains*

-   <http://www.theregister.co.uk/2011/09/27/oracle_sparc_t4_chip_servers/>

Oracle is taking the fight to Unix market leader IBM with its eight-core
SPARC T4 processor and systems with rack, blade, and clustered systems
-- a full data center press.

The SPARC T4 processors, with an S3 core, were developed under the
code-name \"Yosemite Falls\" and offer better performance than Oracle
expected. They will be included in standalone rack and blade servers as
well as in SPARC SuperCluster configurations that mimic the Exadata
parallel database and Exalogic parallel application serving system,
built on Intel x86 processors and running Linux.

The clock speeds of the processors were not divulged, but Oracle has
been able to rev them up to 2.85GHz and 3GHz in the SPARC T4 systems, 73
and 82 per cent faster respectively than the previous 16-core SPARC T3
processors, which ran at a much slower 1.65GHz.

While those SPARC T3 chips had eight threads per core (using the older
S2 cores) and did a reasonable amount of work on some applications (like
database, Java, and application serving), single-threaded code did not
perform particularly well. So with the S3 cores, Oracle\'s chipheads
added dynamic threading (in contrast to the static threading in the S1
and S2 cores) and also added something called the critical thread API.
This allows applications to hog all the resources on an S3 core to boost
the performance of a single-threaded application. The SPARC T4 can
switch between the thread-hog and normal modes on the fly.

### Ellison rides SPARC T4 SuperCluster into data centers

*Four star general purpose, sir!*

-   <http://www.theregister.co.uk/2011/09/27/oracle_sparc_supercluster_t4/>

Oracle resells Fujitsu\'s SPARC64-VII+ machines, badged the SPARC
Enterprise M machines, for customers who have big jobs that require a
shared memory system. But over the past two decades, Oracle co-founder
and CEO Larry Ellison has made no secret of the fact that he believes
that computing in future will be parallel, spreading data and database
crunching across multiple compute nodes, instead of trying to create
ever-larger shared memory systems to hold databases.

Ellison lectured considerably on the benefits of parallelism and data
compression for database processing, and talked quite a bit about the
Exadata machines, of which Oracle has sold 1,000 machines thus far --
Oracle\'s \"most successful product ever,\" he claimed -- and plans to
sell an additional 3,000 machines before the end of the year. (It is not
clear if Oracle meant calendar or fiscal year there.)

\"We\'re a lot faster than IBM\'s biggest pSeries machine,\" Ellison
proclaimed, comparing a cluster of x86 servers running the 11g database
and the Exadata storage software on an InfiniBand backbone to a wonking
256-core Power 795 SMP server. Here\'s how he stacked the two machines
up, fully loaded:

![exadata.extreme.png](/blog/exadata.extreme.jpg "exadata.extreme.png, Oct 2011")\
*Eight Exadata X2-2 racks versus one IBM Power 795 and four DS8700
arrays*

This is the \"engineered systems\" game that Oracle will be playing.
Ellison said that two racks of Exadata could do queries anywhere from 10
to 50 times faster than the Power 795/DS8700 combo, with 4 to 10 times
the OLTP throughput and with 10 times the amount of storage (with
compression turned on) -- and do so for a cost of \$3.3m, compared to
\$18.86m for the IBM hardware. \"The Exadata system costs way less than
a memory upgrade on the IBM pSeries, and you have to be willing to run a
lot faster,\" Ellison quipped. \"The P795 is one big, expensive single
point of failure,\" he added, pointing out that Oracle RAC was
inherently fault-tolerant.

### IBM opens Power8 kimono (a little bit more)

*Wafer baked in 22 nanometers*

-   <http://www.theregister.co.uk/2011/08/31/ibm_power_chip_roadmap_update/>

Data was a little thin, and intentionally so on the part of Big Blue.
But with Oracle kicking up a big fuss over Intel\'s Itanium processor
roadmap - which the software giant says is a dead end - it looks like
IBM has decided it was time to be more specific.

Only a little bit more specific, mind you. Server makers and chip makers
don\'t like to make promises because business conditions change and
issues crop up in reality that can cause a processor or server design
and its schedule to diverge from the roadmap.

A case in point is one of the earlier schedules for the Power processor
lineup, which had Power6 coming out in 2006, Power6+ in 2007, Power7 in
2008, and Power7+ in 2009. That was a two-year cadence for a new
processor design and a two-year cadence for a chip manufacturing process
shrink interwoven.

For reasons that IBM never explained, and which no doubt had to do with
its wafer baking plant in East Fishkill, New York, and maybe its 65
nanometer processes as well as reduced competition from Intel and Sun
Microsystems (now part of Oracle) in the high-end server racket, Big
Blue lengthened the cadence by 50 per cent ahead of the Power6 launch.
Also the Power6+ was not whatever it was supposed to be.
