---
date: "2010-02-28T13:51:20Z"
tags:
- Oracle
title: Oracle On The Future Of OpenSolaris, Finally
---

After the official and long-awaited [public
information](http://www.oracle.com/us/sun/index.html) from Oracle on the
merge with Sun Microsystems, some great news came on both hardware and
software portfolios, in particular the x86 ans SPARC ecosystems, and
around the Solaris operating system.

The main unknown was about the OpenSolaris community, and distribution
model. And until very recently, some important voices around the
community stayed without answer, in particular from [Ben
Rockwood](http://www.cuddletech.com/blog/pivot/entry.php?id=1108), or
[Peter
Tribble](http://ptribble.blogspot.com/2010/02/opensolaris-oracle-where-art-thou.html).

Well, until recently. In fact, the OpenSolaris Annual Meeting (held on
IRC through the \#opensolaris-meeting canal last 26 February) brought
some answers very shortly, which currently begin to
[spread](http://ptribble.blogspot.com/2010/02/opening-up-some-details-of-opensolaris.html)
[through](http://www.h-online.com/open/news/item/OpenSolaris-future-assured-by-Oracle-942161.html)
the
[community](http://www.c0t0d0s0.org/archives/6393-About-the-future-of-Opensolaris.html).
I hope this will quiet some recent misunderstanding on the support model
of the OpenSolaris distribution.
