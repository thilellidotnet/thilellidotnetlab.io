---
date: "2011-10-20T15:53:10Z"
tags:
- Oracle
- Press
title: Focus On The SPARC Architecture
---

Here is a brief overview on a Oracle OpenWorld 2011presentation on the
SPARC architecture:

### Next Generation SPARC Processor

*An In-Depth Technical Review*

-   <https://oracleus.wingateweb.com/published/oracleus2011/sessions/15360/15360_Cho133116.pdf>

Some slides about the SPARC T4 performance:

![sparc.1.png](/blog/sparc.1.jpg "sparc.1.png, Oct 2011")\
![sparc.2.png](/blog/sparc.2.jpg "sparc.2.png, Oct 2011")\
![sparc.3.png](/blog/sparc.3.jpg "sparc.")\
![sparc.4.png](/blog/sparc.4.jpg "sparc.4.png, Oct 2011")\
![sparc.5.png](/blog/sparc.5.jpg "sparc.5.png, Oct 2011")

### SPARC Strategy

-   <https://oracleus.wingateweb.com/published/oracleus2011/sessions/15326/S15326_2650100.pdf>

Public roadmap updated:

![sparc.6.png](/blog/sparc.6.jpg "sparc.6.png, Oct 2011")

About the new SPARC SuperCluster:

![sparc.7.png](/blog/sparc.7.jpg "sparc.7.png, Oct 2011")\
![sparc.8.png](/blog/sparc.8.jpg "sparc.8.png, Oct 2011")\
![sparc.9.png](/blog/sparc.9.jpg "sparc.9.png, Oct 2011")

Some slides about the T-Series and SuperCluster performance:

![sparc.10.png](/blog/sparc.10.jpg "sparc.10.png, Oct 2011")\
![sparc.11.png](/blog/sparc.11.jpg "sparc.11.png, Oct 2011")\
![sparc.12.png](/blog/sparc.12.jpg "sparc.12.png, Oct 2011")\
![sparc.13.png](/blog/sparc.13.jpg "sparc.13.png, Oct 2011")
