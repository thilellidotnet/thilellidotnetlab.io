---
date: "2007-04-22T17:34:10Z"
tags:
- ZFS
title: IBM TSM and Sun ZFS File System
---

Because
[ZFS](http://www.sun.com/software/solaris/faq.jsp#q_3 "Data Management and Solaris ZFS")
is a relatively new file system, not all the giant corporate and well
known backup/restore tools are able to support it. It is the case by now
using EMC NetWorker and Veritas NetBackup. Not before the first half of
the next year for TSM, IBM support said.

So. In order to backup ZFS file system with this tool, we need to write
a little script which will launch the TSM CLI utility, thus backup\'ing
the wanted ZFS file systems. (This *hack* does not support the new ZFS
ACLs, though.)

Here it is:

    # grep postschedulecmd /opt/tivoli/tsm/client/ba/bin/dsm.sys
    postschedulecmd "/root/bin/tsm.do.post.sh >> \
     /opt/tivoli/tsm/client/ba/bin/dsmsched.log 2>&1"
    #
    # cat /root/bin/tsm.do.post.sh
    #!/usr/bin/env sh

    PATH=/usr/sbin:${PATH}
    export PATH

    for mount in `zfs mount | awk '$0 ~ /.*\/.*\/.*/ {print $2}'`; do
      dsmc i ${mount} -subdir=yes
    done

    exit 0

Just relaunch the TSM scheduler, and watch your `dsmsched.log` log file
with care.
