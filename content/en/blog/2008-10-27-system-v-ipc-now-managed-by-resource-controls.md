---
date: "2008-10-27T19:02:06Z"
tags:
- Resource Management
title: System V IPC Now Managed By Resource Controls
---

When it comes to Solaris 10, [all IPC
facilities](http://docs.sun.com/app/docs/doc/817-0404/chapter1-33?a=view)
are either automatically configured or can be controlled by resource
controls. In the same time, they get new default values, when
applicable.

As an example, we will assume that we need to change the limit on number
of shared memory segments that can be created, and that the new default
(128) is not enough either. Before Solaris 10, you\'ve had to set the
`shmsys:shminfo_shmmni` tunable parameter in the `/etc/system` kernel
configuration file, which is a *system wide* limit, and required a
*reboot*. This parameter is now marked as [Obsolete or Have Been
Removed](http://docs.sun.com/app/docs/doc/817-0404/appendixa-6?a=view),
and its use is clearly deprecated.

To increase the corresponding limit up to 256 shared memory segments, we
now have to deal with the `project.max-shm-ids` resource control which
is controlled at the [project
level](http://docs.sun.com/app/docs/doc/817-1592/resource?a=view). The
idea is to set the appropriate resource control to a project, then
execute a program in the context of this project. One method to achieve
this is to create a project at one side (using the
[project(4)](http://docs.sun.com/app/docs/doc/816-5174/project-4?a=view)
database), and to populate the extended user attributes to do the
association between this project and a user account (using the
[user\_attr(4)](http://docs.sun.com/app/docs/doc/816-5174/user-attr-4?a=view)
database) in order to put the new project as the default project for the
user. Or it is possible not to create an extended user attribute with
this project at all, but use its characteristics explicitly through the
[newtask(1)](http://docs.sun.com/app/docs/doc/816-5165/newtask-1?a=view)
command (and the
[login(1)](http://docs.sun.com/app/docs/doc/816-5165/login-1?a=view),
[cron(1M)](http://docs.sun.com/app/docs/doc/816-5166/cron-1m?a=view),
and [su(1M)](http://docs.sun.com/app/docs/doc/816-5166/su-1m?a=view)
programs, or the
[setproject(3PROJECT)](http://docs.sun.com/app/docs/doc/816-5172/setproject-3project?a=view)
function). But the simplest method, and the less intrusive one, is
certainly to directly put the project as the default one for a user
account. Here is how to do so.

By default, no error message is logged against the `syslog` daemon for
resource controls. To be able to see an appropriate message in the
`messages` log file, you must first enable globally the `syslog` action
for the wanted resource control (the default level is *notice*).

    # rctladm -e syslog project.max-shm-ids
    # rctladm -l project.max-shm-ids
    project.max-shm-ids   syslog=notice   [ no-basic deny count ]

When the limit on the number of shared memory segments is reached, one
message similar to the following is write to the log file:

    # grep rctl /var/adm/messages
    /var/adm/messages:Oct 21 16:47:29 hostname genunix: [ID 883052 kern.notice] privileged rctl project.max-shm-ids (value 128) exceeded by project 3

Here is the definition of the new project, and its configuration.

    # getent project user.username
    user.username:1000:Project To Increase The Limit Of SHM Segments:::project.max-shm-ids=(priv,256,deny)
    #
    # projects -l user.username
    user.username
          projid : 1000
          comment: "Project To Increase The Limit Of SHM Segments"
          users  : (none)
          groups : (none)
          attribs: project.max-shm-ids=(priv,256,deny)

When a project name begin with the pattern `user.`, the project will
automatically be set as the default one for the corresponding user,
without the need to populate the extended user attributes database.
Check that the project is set as the default project for the account
`username`.

    # id -p username
    uid=100(username) gid=100(groupname) projid=1000(user.username)
    #
    # projects -d username
    user.username

After a login phase using the `username` identity, the programs
`progname` is launched. We can confirm the use of shared memory segments
under the context of the project `user.username`, and we can consult the
programs statistics report.

    # ipcs -mJ
    IPC status from  as of Wed Oct 29 11:39:59 CET 2008
    T         ID KEY        MODE    OWNER     GROUP       PROJECT
    Shared Memory:
    m 1409286255   0 --rw-rw-rw- username groupname user.username
    m  469762152   0 --rw-rw-rw- username groupname user.username
    m         56   0 --rw-rw-rw- username groupname user.username
    #
    # prstat -n5 -cJ
       PID USERNAME  SIZE   RSS STATE PRI NICE    TIME  CPU PROCESS/NLWP
      3704 username  373M  284M cpu24   2   10 0:07:37 2.1% progname/26
      6785 username  285M  196M sleep  29   10 0:04:13 1.1% progname/26
      4480 username  785M  697M sleep  29   10 0:11:40 1.1% progname/26
      5836 username  293M  204M sleep  29   10 0:06:31 1.0% progname/26
      7635 username  277M  188M sleep  29   10 0:01:00 0.9% progname/26
    PROJID    NPROC  SWAP   RSS MEMORY      TIME  CPU PROJECT
      1000       26 6472M 6333M    26%   3:57:24  23% user.username
         1       17   41M   87M   0.4%   2:39:58 0.0% user.root
         0       43  184M  267M   1.1%   4:07:25 0.0% system
         3        4 5856K   11M   0.0%   0:00:00 0.0% default
    Total: 90 processes, 916 lwps, load averages: 4.41, 2.36, 1.04

Last, we can verify the new setting for one `progname` instance. For
example for PID 3704:

    # prctl -n project.max-shm-ids 3704
    process: 3704: bin/progname 54 80 -Xmx192m
    NAME    PRIVILEGE       VALUE    FLAG   ACTION      RECIPIENT
    project.max-shm-ids
            privileged        256       -   deny                -
            system          16.8M     max   deny                -

The resource management facility can do much more than just tuning IPC
settings, such as managing [CPU usage, and physical memory
control](http://www.c0t0d0s0.org/archives/4211-Less-known-Solaris-Features-Resource-Management.html).
It is a more fine-grained facility than what is in place before Solaris
10, and did not required a reboot anymore.

As a last word, we can note that there are command line tools to help
creating and managing projects and extended user attributes for locally
stored databases: respectively
[projadd(1M)](http://docs.sun.com/app/docs/doc/816-5166/projadd-1m?a=view),
[projmod(1M)](http://docs.sun.com/app/docs/doc/816-5166/projmod-1m?a=view),
and
[useradd(1M)](http://docs.sun.com/app/docs/doc/816-5166/useradd-1m?a=view),
[usermod(1M)](http://docs.sun.com/app/docs/doc/816-5166/usermod-1m?a=view).
But since the information sources was hosted in NIS and LDAP network
directories, we did not use them for this test case though.
