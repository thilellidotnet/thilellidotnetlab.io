---
date: "2009-11-28T14:33:48Z"
tags:
- Oracle
title: Oracle Commitment To Sun Technologies
---

Here are more information about [Oracle commitment to Sun
business](http://www.oracle.com/us/sun/index.htm), and the Solaris
operating system in particular:

-   There is the Oracle Metalink \#742060.1 on the new position of the
    Solaris 10 x86 platform which is raised up at a higher level than
    before.
-   The licensing mode for the UltraSPARC T2+ is [more favorable than
    for other processor
    chips](http://www.oracle.com/us/corporate/contracts/processor-core-factor-table-070634.pdf),
    such as IBM POWER5+ and POWER6 notably.
-   The Database Smart Flash Cache technology from the [Exadata V2
    Database
    Machine](http://www.oracle.com/us/products/database/exadata/index.html)
    can only be used with Oracle Database 11g Release 2 on [Solaris or
    Oracle Enterprise
    Linux](http://download.oracle.com/docs/cd/E11882_01/server.112/e10595/memory005.htm#BABCBAFG)
    operating systems only.

Lastly, [Oracle and Sun Overview and
FAQ](http://www.oracle.com/us/038563.pdf) for customers and partners is
a must read for all persons interested in the future and Oracle\'s
investment in all the current technologies from Sun Microsystems.
