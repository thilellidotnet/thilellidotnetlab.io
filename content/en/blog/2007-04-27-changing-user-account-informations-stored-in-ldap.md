---
date: "2007-04-27T18:51:35Z"
tags:
- LDAP
title: Changing user account informations stored in LDAP
---

This little real-life example will be based on changing the user\'s
login shell for a given account.

-   *grills* represents the LDAP server (master)

Verify that the user exists in the directory server:

    # ldaplist passwd jpeg
    dn: uid=jpeg,ou=people,dc=thilelli,dc=net

Get detailed parameters for this account:

    # ldaplist -l passwd jpeg
    dn: uid=jpeg,ou=people,dc=thilelli,dc=net
            cn: jpeg
            uidNumber: 1000
            gecos: Gabel, Julien
            homeDirectory: /u/jpeg
            gidNumber: 1000
            objectClass: posixAccount
            objectClass: shadowAccount
            objectClass: account
            objectClass: top
            uid: ut0tar
            userPassword: XXXXXXXX
            shadowLastChange: 6445
            shadowFlag: 0
            loginShell: /usr/local/bin/bash

Create the desired modifications using a ldif LDAP file:

    # ldaplist -l passwd jpeg > ldapmodifiy.jpeg.ldif
    /* Edit and save the ldif file. */
    #
    # cat ldapmodifiy.jpeg.ldif
    dn: uid=jpeg,ou=people,dc=thilelli,dc=net
    changetype: modify
    replace: loginShell
    loginShell: /usr/local/bin/ksh

Inject this modification(s) into the LDAP server:

    # ldapmodify -h grills -D "cn=Directory Manager" -f ldapmodifiy.jpeg.ldif
    Enter bind password: 
    modifying entry uid=jpeg,ou=people,dc=thilelli,dc=net

Verify that the changes are done as expected:

    # ldaplist -l passwd jpeg
    dn: uid=jpeg,ou=people,dc=thilelli,dc=net
            cn: jpeg
            uidNumber: 1000
            gecos: Gabel, Julien
            homeDirectory: /u/jpeg
            gidNumber: 1000
            objectClass: posixAccount
            objectClass: shadowAccount
            objectClass: account
            objectClass: top
            uid: ut0tar
            userPassword: XXXXXXXX
            shadowLastChange: 6445
            shadowFlag: 0
            loginShell: /usr/local/bin/ksh

**The example shown below use the `ldaplist(1)` and `ldapmodify(1)`
commands line, which use and options may vary between OS versions and/or
family. Be warned.**
