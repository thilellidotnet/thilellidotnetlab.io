---
date: "2007-04-27T21:01:04Z"
tags:
- File
title: Reversing a File Line-by-Line
---

Reversing a file line-by-line seems not very interesting, but may be in
fact very helpful in a lot of cases, especially under Unix-like systems.

Interestingly, just after reading a very good
[article](http://www.samag.com/documents/s=9766/sam0506g/0506g.htm "Eight Ways to Reverse a File")
on that particular subject from the printed edition of the Sys Admin
Magazine, great known
[NetBSD](http://www.netbsd.org/ "Of course it runs NetBSD") developer
[Hubert
Feyrer](http://www.feyrer.de/hubert_feyrer_english.html "Hubert Feyrer")
blogged about this
[issue](http://www.feyrer.de/NetBSD/blog.html#20050729_1425 "Hubertf's NetBSD Blog")\...
growing the proposed solution with a *new* method not yet given,
although very simple.

All the spirit of UNIX can be found here: combined different powerful
general tools to achieve a very specific task\... sounds great to me.
