---
date: "2010-12-19T17:05:35Z"
tags:
- ZFS
- Zone
- Bug
title: 'Solaris 11 Express: Problem #2'
---

*In this series, I will report the bugs or problems I find when running
the Oracle Solaris 11 Express distribution. I hope this will give more
visibility on those PR to Oracle to correct them before the release of
Solaris 11 next year.*

For some customers, I had the habit to clone a non-global zone using a
template zone. But in order to save some space, I generally use the
capability to use a ZFS snapshot as input for the clone, avoiding
creating a snapshot each time a new clone is created.

It seems that this capability is not usable anymore on Solaris 11
Express at this time:

    # zoneadm -z zone3 clone -s dpool/store/zone/zone1/ROOT/zbe@zone2_snap zone1
    /usr/lib/brand/ipkg/clone: -s: unknown option

Nevertheless, this functionality is always described in the manual page:

> brand-specific usage: clone {sourcezone} usage: clone \[-m method\]
> \[-s \] \[brand-specific args\] zonename Clone the installation of
> another zone. The -m option can be used to specify \'copy\' which
> forces a copy of the source zone. The -s option can be used to specify
> the name of a ZFS snapshot that was taken from a previous clone
> command. The snapshot will be used as the source instead of creating a
> new ZFS snapshot. All other arguments are passed to the brand clone
> function; see brands(5) for more information.

No luck here. Even if the space consideration may be minimized by the
deduplication feature of ZFS in Solaris 11 Express, it is not always
appropriate nor usable: on small size server for example.

FYI, this problem is covered by the Bug ID number 6383119. Note that you
can add yourself to the interest list at the bottom of the bug report
page:

-   <http://bugs.opensolaris.org/bugdatabase/view_bug.do?bug_id=6383119>
