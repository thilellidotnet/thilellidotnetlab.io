---
date: "2007-04-28T10:09:23Z"
tags:
- SVM
title: Encapsulation of the Data's Disk using SVM
---

*Examples of a raw device and a file system using soft partition.*

1.  `c0t10d0s2` represents the first data disk
2.  `c0t11d0s2` represents the second disk (mirror)

Duplicate the label\'s content from the data disk to the mirror disk.
There are two slices on it:

-   `s4` for the replicas
-   `s7` for all the data using soft partitions

<!-- -->

    # prtvtoc /dev/rdsk/c0t0d0s2 | fmthard -s - /dev/rdsk/c0t1d0s2

Create replicas of the metadevice state database:

    # metadb -a -c3 c0t10d0s4
    # metadb

Creation of metadevices:

    # metainit d17 1 1 c0t10d0s7
    # metainit d27 1 1 c0t11d0s7

Create the first part of the mirror:

    # metainit d7 -m d17

Attach the second part of the mirror:

    # metattach d7 d27

Verify all:

    # metastat -p
    # metastat | grep \%

Example \#1: create a raw device for Sybase\...

    # metainit d30 -p d7 2g
    # chown sybase:sybase /dev/md/rdsk/d30

Example \#2: create a new file system for Sybase\...

    # mkdir -p /files/sybase
    # metainit d31 -p d7 1g
    # newfs /dev/md/rdsk/d31
    # grep d31 /etc/vfstab
    /dev/md/dsk/d31 /dev/md/rdsk/d31 /files/sybase ufs 2 yes logging
    # mount /files/sybase
    # chown sybase:sybase /files/sybase
    # ln -s /files/sybase /opt/sybase

Example \#3: Grow an existing file system for Sybase\...

    # metattach d31 +1g
    # growfs -M /files/sybase /dev/md/rdsk/d31
