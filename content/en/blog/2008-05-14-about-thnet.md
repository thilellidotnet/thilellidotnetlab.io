---
date: "2008-05-14T17:42:01Z"
tags: []
title: About thnet
---

### Why Choose this Particular Domain Name?

As for a lot of things in this world, there is no *real* reason behind
it. It must be unique, quickly recognizable and\... have a hidden
meaning (like all Good Names(TM)).

So, after days trying to figure out which may be the better name for
this project, my choice went on a very uncommon one:
[thilelli](http://www.google.fr/search?q=thilelli+first+name "thilelli on Google")
. This name, also sometimes spelled *tilelli*, is in fact a (female)
first name with Berber\'s origin, which means *Freedom*. What a hidden
meaning for an open source based project, isn\'t it?

### Who Own It?

The short answer is basically\... me.

A more longer answer is that it was registered at
[Gandi.net](http://www.gandi.net/ "Gandi is an ICANN accredited registrar").
More information related to this domain name may be consulted at
whatever
[WhoIs](https://www.gandi.net/whois/details?search=thilelli.net "Whois at Gandi.net")
you prefer.

### Why is this Site in English?

Because English is so common in Information Technology (the main purpose
of this blog), i thought it may be interesting to write in this language
in order to be more accessible through a lot of readers. However, since
i am a French guy and not a native English speaker, any spelling/grammar
mistakes are not to be excluded.

Feel free to proof-read my pages and [email
me](mailto:julien%20dot%20gabel%20at%20thilelli%20dot%20net), if any.
