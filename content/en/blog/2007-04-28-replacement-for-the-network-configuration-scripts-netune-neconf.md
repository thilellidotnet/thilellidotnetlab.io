---
date: "2007-04-28T10:00:06Z"
tags:
- Network
title: Replacement for the Network Configuration Scripts (netune, neconf)
---

This tool aims to replace the old `netune` and `neconf` scripts,
collecting and enhancing in one tool the best of both world. The typical
layout for this tool is as follow:

-   `/etc/init.d/ifinit` represents the general and only global script
    which contains the all different drivers configuration capabilities
    and the implementation of the available actions
-   `/etc/ifinit.conf` this is the local configuration file to be
    sourced and lists which interface(s) to set and how (mode, speed,
    etc.)
-   `/etc/rc2.d/S68ifinit` -\> `/etc/init.d/ifinit` at which run-level
    must start this script

Here are little samples of usage:

    # sh /tmp/ifinit -f /tmp/ifinit.conf 
    Usage: ifinit [-f cf_file] {start|status|diag}
    #
    # sh /tmp/ifinit -f /tmp/ifinit.conf start
    Setting interface hme instance 0: done
    Setting network stack parameters: done
    #
    # sh /tmp/ifinit -f /tmp/ifinit.conf status
    Local link information for interface hme instance 0:
      transceiver_inuse: 0 (0=internal_rj45_connector,1=external_mii_transceiver)
      link_status: 1 (0=down,1=up)
      link_speed: 1 (0=10Mbytes,1=100Mbytes)
      link_mode: 1 (0=half-duplex,1=full-duplex)
      carrier_errors: 0 (number_of)
      collisions: 0 (number_of)
      retry_error: 0 (number_of)
    #
    # sh /tmp/ifinit -f /tmp/ifinit.conf diag
    Local link information for interface hme instance 0:
      transceiver_inuse: 0 (0=internal_rj45_connector,1=external_mii_transceiver)
      link_status: 1 (0=down,1=up)
      link_speed: 1 (0=10Mbytes,1=100Mbytes)
      link_mode: 1 (0=half-duplex,1=full-duplex)
      carrier_errors: 0 (number_of)
      collisions: 0 (number_of)
      retry_error: 0 (number_of)

    Configuration of interface hme instance 0:
      adv_100fdx_cap: 1
      adv_100hdx_cap: 1
      adv_10fdx_cap: 1
      adv_10hdx_cap: 1
      adv_100T4_cap: 0
      adv_autoneg_cap: 1

    Configuration of the network stack tunables:
      ip_forward_directed_broadcasts: 0
      ip_forward_src_routed: 0
      ip_ignore_redirect: 0
      ip_respond_to_address_mask_broadcast: 0
      ip_respond_to_echo_broadcast: 1
      ip_respond_to_timestamp: 0
      ip_respond_to_timestamp_broadcast: 0
      ip_send_redirects: 1
      ip_strict_dst_multihoming: 0
      ip_ire_arp_interval: 1200000
      tcp_xmit_hiwat: 49152
      tcp_recv_hiwat: 49152
      tcp_cwnd_max: 1048576
      tcp_rexmit_interval_max: 60000
      tcp_rexmit_interval_min: 400
      tcp_rexmit_interval_initial: 3000
      tcp_slow_start_initial: 4
      tcp_conn_req_max_q: 128
      tcp_conn_req_max_q0: 1024
      tcp_smallest_anon_port: 32768
      tcp_largest_anon_port: 65535
      tcp_smallest_nonpriv_port: 1024
      tcp_extra_priv_ports: 2049 4045 9010
      tcp_time_wait_interval: 60000
      tcp_rev_src_routes: 0
      udp_recv_hiwat: 57344
      udp_xmit_hiwat: 57344
      udp_smallest_anon_port: 32768
      udp_largest_anon_port: 65535
      udp_smallest_nonpriv_port: 1024
      udp_extra_priv_ports: 2049 4045
      arp_cleanup_interval: 300000

*Please replace `/tmp` with `/etc/init.d` and `/etc`, accordingly.*
