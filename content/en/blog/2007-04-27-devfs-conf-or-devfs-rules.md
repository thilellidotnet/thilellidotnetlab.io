---
date: "2007-04-27T19:50:42Z"
tags:
- USB
title: devfs.conf, or... devfs.rules
---

If you are using a USB thumbdrive, it may be interesting to be able to
mount it as a normal user, i.e. without the need for a privileged
account. The question is how to set these rights for devices not
available at boot time?\... Just use `devfs.rules(5)`, it serves the
same purpose than `/etc/devfs.conf`, but is specifically well suited for
removable devices.

Here is an example of setting the mode and group (in which must resides
the desired user(s)) for a USB removable key, seen as an SCSI device of
the direct access class:

    $ cat /etc/devfs.rules
    [devfsrules_allow_usb=10]
    add path 'da*' mode 0660 group usb

    # Devices usually not available at boot time.
    [devfsrules_allow=100]
    add include $devfsrules_allow_usb

Many thanks to Roland Smith for its particular interesting
[notes](http://www.xs4all.nl/~rsmith/freebsd/ "Roland's FreeBSD page")
on that subject and for contributing the `devfs.conf(5)` and
`devfs.rules(5)` manual pages to the FreeBSD project.
