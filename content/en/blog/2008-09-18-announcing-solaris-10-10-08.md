---
date: "2008-09-18T17:16:53Z"
tags:
- ZFS
title: Announcing Solaris 10 10/08
---

Although this seems a little bit confident, the long-awaited Update 6 to
the Solaris 10 operating system release is just behind the door. This
release (scheduled to be available in mid-October) will "*includes
virtualization enhancements including the ability for a Solaris
Container to automatically update its environment when moved from one
system to another, Logical Domains support for dynamically
reconfigurable disk and network I/O, and paravirtualization support when
Solaris 10 is used as a guest OS in Xen-based environments such as Sun
xVM Server. Solaris 10 10/08 also includes support for the latest
systems from Sun and other vendors, such as those based on the Intel
Xeon Processor 7400 Series*".

This release will be the very first Solaris release to be able to boot
from [ZFS](http://www.opensolaris.org/os/community/zfs/) and use it as
their root file system, such as what can be found on
[OpenSolaris](http://www.opensolaris.com/) or [Solaris Express Community
Release](http://www.opensolaris.org/os/downloads/) today.

Check the [What's
New](http://www.sun.com/software/solaris/whats_new.jsp) web page for
Solaris, and consult the [Solaris Media
Gallery](http://www.sun.com/software/solaris/media/k5_media.jsp#solaris1)
videos for more information.

*Update \#1 (2008-10-14):* Don\'t forget to consult the **must read**
[What\'s New in Solaris 10
10/08?](http://www.opensolaris.org/os/project/alamo-osug/events/Whats_New_in_Solaris_10u6.pdf)
from the San Antonio OpenSolaris User Group.

*Update \#2 (2008-10-31):* [Get
yours](http://www.sun.com/software/solaris/get.jsp), and go reading the
official [What\'s New in the Solaris 10 10/08
Release](http://docs.sun.com/app/docs/doc/817-0547/ghgdx?a=view) page.
