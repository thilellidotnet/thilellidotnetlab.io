---
date: "2011-10-20T16:36:30Z"
tags:
- Oracle
- Press
title: Focus On The Solaris 11 OS
---

Here is a brief overview on a Oracle OpenWorld 2011presentation on the
Solaris 11 operating system:

### SPARC Strategy

-   <https://oracleus.wingateweb.com/published/oracleus2011/sessions/15326/S15326_2650100.pdf>

![solaris11.1.png](/blog/solaris11.1.jpg "solaris11.1.png, Oct 2011")\
![solaris11.2.png](/blog/solaris11.2.jpg "solaris11.2.png, Oct 2011")\
![solaris11.3.png](/blog/solaris11.3.jpg "solaris11.3.png, Oct 2011")\
![solaris11.4.png](/blog/solaris11.4.jpg "solaris11.4.png, Oct 2011")
