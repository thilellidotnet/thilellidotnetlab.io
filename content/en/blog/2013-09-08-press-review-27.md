---
date: "2013-09-08T13:43:21Z"
tags:
- Press
title: 'Press Review #27'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### How to Use Hardware Fault Management in Oracle Linux

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/fault-management-linux-2005816.html>

Understanding, installing, enabling, and using IPMI and MCE---two
hardware fault management tools in Oracle Linux.

### Preparing for \#OOW: DB12c, M6, In-memory, Clouds, Big Data\... and IoT

-   <https://blogs.oracle.com/EricBezille/entry/preparing_for_oow_db12c_m6>

It's always difficult to fit the upcoming Oracle Open World topics, and
all its sessions in one title, even if \"Simplifying IT. Enabling
Business Transformation.\" makes it clear on what Oracle is focusing on,
I wanted to be more specific on the \"How\". At least for those of you
who attended Hot Chips conference, some of the acronyms will be familiar
to you, some may not (I will come back to \"IoT\" later). For those of
you attending, or those of you who will get the sessions presentations
once available online, here are few things that you don\'t want to miss
which will give you not only what Oracle R&D has done for you since last
year, but also what customers -like you- have implemented thanks to the
red-stack and its partners, being ISVs or SIs.

### Misys Kondor+ runs best on SPARC T5

-   <https://blogs.oracle.com/openomics/entry/kondor_runs_best_on_sparc>

Misys is a leading financial software vendor, providing the broadest
portfolio of banking, treasury, trading and risk solutions available on
the market. At ISV Engineering, we have a long-standing collaboration
with the Kondor+ product line, that came from the Turaz acquisition
(formerly part of Thomson Reuters) and now part of the Misys Treasury &
Capital Markets (TCM) portfolio. Jean-Marc Jacquot, Senior Technical
Consultant at Misys TCM, was recently interviewed by ITplace.TV (in
French language) about a recent IT redesign of the Kondor+ installed
base at a major French financial institution.

### FAQ2: Analyzing Large Volumes of nmon Data

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/faq2_analyzing_large_volumes_of_nmon_data>

I regularly get asked a question like: I have 4 months of data from 25
machines and have to develop a Capacity Planning model to size these
LPARs on to new machines but I am having problems with having so much
data. What can you recommend?

### FAQ3: How can I Monitoring Shared Processor Pools longer term?

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/faq3_how_can_i_monitoring_shared_processor_pools_longer_term>

\[\...\] The CPU Pool information is gathered (if switched on) at the
HMC via the service processor but extracting it and then using it to
generate graphs or spreadsheet data is extremely complex particularly if
you have a complex set of up to 64 CPU Pools!

### Happy 10th Birthday, DTrace!

-   <http://dtrace.org/blogs/bmc/2013/09/03/happy-10th-birthday-dtrace/>

Ten years ago this morning --- at 10:27a local time, to be exact ---
DTrace was integrated. On the occasion of DTrace's fifth birthday a
half-decade ago, I reflected on much of the drama of the final DTrace
splashdown, but much has happend in the DTrace community in the last
five years; some highlights\...

### You won\'t find this in your phone: A 4GHz 12-core Power8 for badass boxes

-   <http://www.theregister.co.uk/2013/08/27/ibm_power8_server_chip/>

Hot Chips Big iron sales are still generating \$6bn to \$7bn a year for
IBM - which is enough to justify designing its own Power processors and
building its own wafer baker.

At the Hot Chips conference at Stanford University on Monday, some of
the chief architects behind the Power8 electronics were on hand to show
off the feeds and speeds of the next-generation motor for the company\'s
Power Systems lineup.

![](http://regmedia.co.uk/2013/08/27/ibm_power_chip_comparison.jpg){width="498"
height="263"}

### Advanced Network Monitoring Using Oracle Solaris 11 Tools

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/sol-adv-network-monitoring-2008573.html>

\
In this article, we will examine three use cases in order to demonstrate
the capability of Oracle Solaris 11 tools. The first use case will show
how to measure the network bandwidth between two systems. The second use
case will demonstrate how to observe network statistics on a specific
TCP port, and the last use case will demonstrate, by example, how to
collect and display historical statistics of network resource usage for
a given date and time range.

### Announcing: ZS3 - Oracle\'s Next Generation Application Engineered Storage

-   <https://blogs.oracle.com/eSTEP/entry/announcing_zs3_oracle_s_next>

\
On 10th of September, Oracle announced its latest ZFS Storage
Appliances, the ZS3 Series, that enables customers to accelerate time to
insight, while extensive Oracle co-engineering improves operational
efficiencies and reduces data center costs through dynamic automation
through the Oracle stack. As IT skills converge, the ZS3 provides
enterprises with the ability to respond to application needs dynamically
while reducing time-to-business benefits.

### Solaris 10 Patches Now On Monthly Release Cadence

-   <https://blogs.oracle.com/patch/entry/solaris_10_patches_now_on>

We\'ve recently moved to a monthly release cadence for Solaris 10 OS
patches.\
\
New Solaris 10 OS patches are now available from MOS by the Monday
closest to 17th of each month.

### Solaris Random Number Generation

-   [https://blogs.oracle.com/darren/entry/solaris\_random\_number\_generation](%20https://blogs.oracle.com/darren/entry/solaris_random_number_generation)

    The following was originally written to assist some of our new hires
    in learning about how our random number generators work and also to
    provide context for some questions that were asked as part of the
    ongoing (at the time of writing) FIPS 140-2 evaluation of the
    Solaris 11 Cryptographic Framework.

### How to determine why your AIX oslevel is downlevel (and a script to help!)

-   <https://www.ibm.com/developerworks/community/blogs/brian/entry/how_to_determine_why_your_aix_oslevel_is_downlevel_and_a_script_to_help>

To determine the oslevel on AIX, you can run the \"oslevel -s\"
command.   However, what \"oslevel -s\" reports doesn\'t always show the
entire picture.   The OS level reported will be the lowest level of any
installed AIX fileset on your server.

### Amazon, Avere and Oracle Offer Up the Most Transformational Technologies at VMworld 2013

-   <http://www.dcig.com/2013/08/amazon-avere-and-oracle-offer-up-transformation-vmworld.html>

The good news is that what Oracle had to share with me about its
innovations in storage went well beyond anything I could have possibly
imagined and it was certainly better than anything I was hearing on the
VMworld show floor. In short, what Oracle has done has brought together
the best of what ZFS has to offer and tied that together with the best
of what of its Oracle databases have to offer to deliver Oracle
application performance that far outshines what anyone else can even
remotely hope to offer anytime in the near future.

### Top Tips for Updating Solaris 11 Systems

-   <https://blogs.oracle.com/Solaris11Life/entry/top_tips_for_updating_solaris>

We now have quite a bit of experience of IPS and Repositories under our
belt.\
\
Feedback from customers has been extremely positive.  I recently met a
customer with 1000+ Solaris servers who told me that with Solaris 10 it
took them 2 months to roll out a new patchset across their enterprise. 
With Solaris 11, it takes 10 days.  That really helps lower TCO.

### Oracle VM virtualization on T4-4 : architecture and service catalog

-   <https://blogs.oracle.com/EricBezille/entry/oracle_vm_virtualization_on_t4>

Last Tuesday, during the SPARC Showcase event, Jean-François
Charpentier, François Napoleoni and Sébastien Flourac delivered very
interesting uses cases of deployment of latest Oracle Systems at work :
Oracle VM virtualization project on T4-4, intensive financial trading on
T5-2 and a complete Information System migration to Oracle SuperCluster.

### GRTgaz new Information System on Oracle SuperCluster

-   <https://blogs.oracle.com/EricBezille/entry/grtgaz_new_si_on_oracle>

This testimony from Mr Sébastien Flourac, Head of Strategy for GRTgaz
IT, concluded the last week SPARC Showcase event. Mr Flourac highlighted
why he selected an Oracle SuperCluster, Engineered Systems, over a more
traditional build it yourself approach, that he also studied.

### \#OOW2013: All your Database in-memory for All your existing applications\... on Big Memory Machines

-   <https://blogs.oracle.com/EricBezille/entry/oow2013_all_your_database_in>

Many announcements have been made today by Larry Ellison, during his
opening of Oracle OpenWorld.  \[\...\] On OpenWorld side, it was also
very impressive. More people this year are attending the event : 60 000
! And in terms of big numbers, we saw very impressive results of the new
features and products that have been announced today by Larry: Database
12c in-memory option, M6-32 Big Memory Machine, M6-32 SuperCluster and
Oracle Database Backup, Logging, Recovery Appliance (yes, I am not
joking, that\'s its real product name !).

### PureSystems Anyone?

-   <http://www.itjungle.com/tfh/tfh092313-story02.html>

IBM introduced the PureSystems in April 2012. It was a big deal and it
still is. IBM is certain this is the system to converge all systems,
with infrastructure in a box\--PureFlex\--as the solution to the huge
business knot caused by IT redundancies. Instead of each platform
requiring its own infrastructure, this is one infrastructure for all and
all for one. In the past year and a half, the successes have mostly
piled up on the X86 side as a server consolidation play, but IBM i shops
are in this game, too.

### Oracle\'s SPARC M6

-   <http://www.oracle.com/us/corporate/features/sparc-m6/>

Companies are awash in data. In fact, Science Daily reported that 90% of
the world\'s data was generated over the last two years. Capturing,
integrating, and analyzing massive amounts of data from both social
media interactions and traditional in-house systems demands a shift to
in-memory processing.\
\
With Oracle\'s new SPARC M6-32 systems, customers can now run entire
applications in memory. Based on the SPARC M6 12-core processor, these
systems have a staggering 32 terabytes of system memory and up to 384
processor cores with 8-threads per core. The SPARC M6-32 provides twice
the memory capacity of IBM Power servers and delivers extreme in-memory
compute performance for large-scale, mission-critical workloads and
databases. And it offers the highest levels of system availability for
core business applications.

### Alternative flash architectural implementation options for AIX with detailed instructions on implementing LVM mirroring with preferred read from flash

-   <http://www-03.ibm.com/support/techdocs/atsmastr.nsf/WebIndex/TD106159>

This document discusses the benefits and positioning of using AIX LVM
mirroring with preferred read from Flash/SSD and provides detailed
implementation steps to set it up.

### Solaris General Session at Oracle Open World 2013

-   <https://blogs.oracle.com/markusflierl/entry/solaris_general_session_at_oracle>

\[\...\]\
Here are some of the highlights of what\'s coming in the next release:\
\
kernel zones: With kernel zones customers will have to option to run
different kernel patch levels across different zones while maintaining
the simplicity of zones management. We\'ll also be able to do live
migration of kernel zones. All of that across HW platforms, i.e. kernel
zones will be available on both SPARC as well as x86. Key benefits of
kernel zones:

1.  Low overhead (Lots of optimizations because we run Solaris on
    Solaris)
2.  Unique security features: Ability to make them immutable by locking
    down the root file system
3.  Integration with the Solaris resource management capabilities: CPU,
    memory, I/O, networking
4.  Fully compatible with OVM SPARC as well as native and S10 branded
    zones
5.  Comprehensive SDN capabilities: Distributed Virtual Switch and VxLAN
    capabilities

\[\...\]
