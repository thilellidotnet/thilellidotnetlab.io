---
date: "2007-04-26T20:14:15Z"
tags:
- Control-M
- Service
title: How to Add a New "ctmag" Service on RHEL4 (Control-M Agent v6)
---

Create the *Control-M* service file:

    # cat /etc/rc.d/init.d/ctmag
    #!/usr/bin/env sh

    # chkconfig: 345 57 23
    # description: Control-M agent daemons

    # Source functions library.
    . /etc/rc.d/init.d/functions

    LC_ALL=en_US; export LC_ALL
    ctmag_user="ctmag620"
    ctmag_home=`getent passwd ${ctmag_user} | awk -F\: '{print $6}'`
    ctmag_opts="-u ${ctmag_user} -p ALL"
    rc=0

    start() {
      echo -n $"Starting `basename $0`:"
      initlog -c "${ctmag_home}/ctm/scripts/start-ag ${ctmag_opts}" > /dev/null \
       && success || failure
      echo
    }

    stop() {
      echo -n $"Stopping `basename $0`:"
      ${ctmag_home}/ctm/scripts/shut-ag ${ctmag_opts} > /dev/null
      rc=$?
      if [ ${rc} -eq 0 ]; then
        success $"Stopping `basename $0`"
      else
        failure $"Stopping `basename $0`"
      fi
      echo
    }

    case "$1" in
    start)
      start
      ;;
    stop)
      stop
      ;;
    restart)
      stop
      start
      ;;
    *)
      echo "Usage: `basename $0` {start|stop|restart}"
      rc=1
      ;;
    esac

    exit ${rc}

Add and configure the *Control-M* service:

    # chmod 755 /etc/rc.d/init.d/ctmag
    # chkconfig --add ctmag
    # chkconfig --level 0126 ctmag off
    # chkconfig --level 345 ctmag on

Start, or restart, the *Control-M* service:

    # service ctmag restart
