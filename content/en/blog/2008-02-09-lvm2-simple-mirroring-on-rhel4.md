---
date: "2008-02-09T11:32:52Z"
tags:
- RAID
- LVM
title: LVM2 Simple Mirroring On RHEL4
---

When the need to evacuate all persistent SAN storage from EMC DMX1K to
HP XP12K (HDS), three main solutions were envisaged. The first one was
brute data copy (`tar`, `cpio`, etc.) but was not very practical with
the size of the data (multi-terabytes) and the time involved in copying
them. The two others were based on LVM technologies: mirroring, or
moving.

Although the choice has been to use the online and transparent moving
data technology (see `pvmove` for more information), it was interesting
to note that Red Hat has backported support for the [creation and
manipulation of simple
mirrors](https://rhn.redhat.com/errata/RHBA-2006-0504.html) to their
RHEL4 distribution. These functionalities were introduced with the
`RHBA-2006:0504-15` advisory issued on 2006-08-10, i.e. between RHEL4
Update 4 and RHEL4 Update 5 (and so available via RHN at this time). It
is just too bad that the online help for LVM commands are not properly
synchronized nor fully documented by the corresponding manual page:
clearly, this doesn\'t help to use them in the best conditions (no,
Google isn\'t always the better option when using these kinds of
functionalities in big companies).
