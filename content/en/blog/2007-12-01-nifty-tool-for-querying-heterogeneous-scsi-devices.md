---
date: "2007-12-01T16:22:54Z"
tags:
- EMC
- SAN
title: Nifty Tool For Querying Heterogeneous SCSI Devices
---
---------------------------------------------------------------------------------------
DEVICE                            :VEND     :PROD             :REV  :SERNUM  :CAP(kb)
---------------------------------------------------------------------------------------
/dev/rdsk/c0t0d0s2                :TSSTcorp :DVD-ROM TS-H352C :SI00 :        :  -----
/dev/rdsk/c1t0d0s2                :FUJITSU  :MAX3147FCSUN146G :1103:0639G02A :143369664
/dev/rdsk/c1t3d0s2                :FUJITSU  :MAX3147FCSUN146G :1103:0638G02A :143369664
/dev/rdsk/c2t50060E8004F2F520d0s2 :HP       :OPEN-V*2         :5007 :500F2F5 :103683840
/dev/rdsk/c3t50060E8004F2F510d6s2 :HP       :OPEN-V*7         :5007 :500F2F5 :362893440
/dev/vx/rdmp/XP12K_SQC_0s2        :HP       :OPEN-V*3         :5007 :500F2F5 :155525760
/dev/vx/rdmp/XP12K_SQC_6s2        :HP       :OPEN-V*7         :5007 :500F2F5 :362893440
/dev/vx/rdmp/c1t0d0s2             :FUJITSU  :MAX3147FCSUN146G :1103:0639G02A :143369664
/dev/vx/rdmp/c1t3d0s2             :FUJITSU  :MAX3147FCSUN146G :1103:0638G02A :143369664
# 
# ./inq.sol64 -hba
Inquiry utility, Version V7.3-845 (Rev 2.0)      (SIL Version V6.4.2.0
(Edit Level 845)
Copyright (C) by EMC Corporation, all rights reserved.
For help type inq -h.
---------------------------------------------------
---------------------------------------------------
HBA name:           Sun Microsystems, Inc.-LP10000-S-1
host WWN:           20000000C957A8E8
vendor name:        Sun Microsystems, Inc.
model:              LP10000-S
firmware version:   1.91a5
driver version:     1.11i (2006.07.11.10.53)
serial number:      0999BG0-0635000219
vendor code:        0xc9
HBA type:           Fibre Channel
port count:         1

port number:       1
    port WWN:           10000000C957A8E8
    Port OS name:       /dev/cfg/c2
    port type:          NPORT
    port speed:         2GBIT
    supported speed:    2GBIT
    port state:         ONLINE
    port FCID:          0x10900
[...]
</pre>
<p>... and a HP DL585G2 AMD64 system running RHEL4U5. Both are connected to a
remote SAN served by a HP XP12K (HDS refurbished) storage system:</p>
<pre>
# ./inq.LinuxAMD64 -f_powerpath -f_hds
Inquiry utility, Version V7.3-845 (Rev 2.0)      (SIL Version V6.4.2.0
(Edit Level 845)
Copyright (C) by EMC Corporation, all rights reserved.
For help type inq -h.
----------------------------------------------------------------------
----------------------------------------------------------------------
/dev/emcpowere :HP      :OPEN-V*6        :5007  :50 0F2F5   :311051520
/dev/emcpowerf :HP      :OPEN-V*6        :5007  :50 0F2F5   :311051520
/dev/emcpowerg :HP      :OPEN-V*10       :5007  :50 0F2F5   :307785600
/dev/emcpowerh :HP      :OPEN-V*10       :5007  :50 0F2F5   :307785600
/dev/emcpowerj :HP      :OPEN-V*6        :5007  :50 0F2F5   :307203840
/dev/emcpowerk :HP      :OPEN-V*4        :5007  :50 0F2F5   :206085120
</pre>
<p>Although this tool works great with an OpenSolaris distribution (say, the
Solaris Express family), there appear not to have a x86 declination which is a
pity knowing the growing Solaris/OpenSolaris community in the marketplace
today. Well, maybe for a next <code>inq</code> release, at least I hope.</p>

[Lasse Østerild](http://www.oesterild.dk/) remind us about the EMC `inq`
tool, which is able to query SCSI buses to find a large range of
devices, of many sort. This great utility support non-EMC targets, and
is [freely available](ftp://ftp.emc.com/pub/symm3000/inquiry/) (just be
aware that the *latest* link seems not to be updated frequently, so
check the latest version yourself in the list).

Here are two examples taken respectively from a Sun Fire V490 UltraSPARC
system running Solaris 9\...

    # ./inq.sol64
    Inquiry utility, Version V7.3-845 (Rev 2.0)      (SIL Version V6.4.2.0
    (Edit Level 845)
    Copyright (C) by EMC Corporation, all rights reserved.
    For help type inq -h.
    HBA name:           QLogic Corp.-2200-0
    host WWN:           200000144F415386
    vendor name:        QLogic Corp.
    model:              2200
    firmware version:   2.1.144
    driver version:     20060630-2.16
    serial number:      Unknown
    vendor code:        0x144f
    HBA type:           Fibre Channel
    port count:         1

    port number:       1
        port WWN:           210000144F415386
        Port OS name:       /dev/cfg/c1
        port type:          LPORT
        port speed:         1GBIT
        supported speed:    1GBIT
        port state:         ONLINE
        port FCID:          0x1
    DEVICE         :VEND    :PROD            :REV   :SER NUM    :CAP(kb)
