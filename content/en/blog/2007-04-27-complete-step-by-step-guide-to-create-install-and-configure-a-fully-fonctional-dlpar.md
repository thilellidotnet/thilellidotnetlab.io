---
date: "2007-04-27T18:36:47Z"
tags:
- POWER
- VIOS
- SAN
- Install
title: Complete Step-by-Step Guide to Create, Install and Configure a Fully Fonctional
  DLPAR
---
--------------- -------------------------------------------- ------------------
vhost3          U9113.550.65E3A0C-V3-C8                      0x00000006

VTD                   vhdbromonts00
LUN                   0x8100000000000000
Backing device        hdbromonts00
Physloc

VTD                   vhdbromonts01
LUN                   0x8200000000000000
Backing device        hdbromonts01
Physloc
$
$ rmdev -dev vhost3 -recursive
vhdbromonts00 deleted
vhdbromonts01 deleted
vhost3 deleted
</pre>
<p>Verify that the virtual adapter doesn't exists anymore:</p>
<pre>
$ lsmap -vadapter vhost3
Device &quot;vhost3&quot; is not a Server Virtual SCSI Adapter (SVSA).
</pre>
<p>If necessary, remove the virtual devices: in this case the virtual devices
are some logical volumes but they may be some real storage devices, as with SAN
disks. For example:</p>
<pre>
# rmlv hdbromonts00
Warning, all data contained on logical volume hdbromonts00 will be destroyed.
rmlv: Do you wish to continue? y(es) n(o)? y
rmlv: Logical volume hdbromonts00 is removed.
#
# rmlv hdbromonts01
Warning, all data contained on logical volume hdbromonts01 will be destroyed.
rmlv: Do you wish to continue? y(es) n(o)? y
rmlv: Logical volume hdbromonts01 is removed.
</pre>
<p><strong>Don't forget to delete and recreate the corresponding virtual
adapter on the VIOS via the HMC console, otherwise this will cause problem
during the installation phase since the VIOS server will not properly give
access to the underlying storage disk(s) or device(s)!</strong></p>
<p>Recreate the device tree on the VIOS:</p>
<pre>
$ cfgdev
$
$ lsdev -virtual | grep vhost3
vhost3          Available  Virtual SCSI Server Adapter
</pre>
<h3>Case #2: configure a newly created virtual adapter</h3>
<p>On the other side, if the virtual adapter is freshly created -- as in the
previous example -- it is needed to inform the VIOS operating system about
it:</p>
<pre>
$ cfgdev
$
$ lsdev -virtual | grep vhost6
vhost6          Available  Virtual SCSI Server Adapter
$
$ lsmap -vadapter vhost6
SVSA            Physloc                                      Client Partition ID
--------------- -------------------------------------------- ------------------
--------------- -------------------------------------------- ------------------
vhost6          U9113.550.65E3A0C-V3-C11                     0x00000000

VTD                   vtscsi16
LUN                   0x8100000000000000
Backing device        beastielv
Physloc

VTD                   vtscsi17
LUN                   0x8200000000000000
Backing device        hdisk15
Physloc               U787B.001.DNW3897-P1-C4-T1-W5006048448930A41-L2000000000000
</pre>
<h2>Create and configure the Network Install Manager</h2>
<p>Log in to the NIM server and set the standalone machine configuration for
<code>beastie</code>:</p>
<pre>
# TERM=vt220 smitty nim
/*
 * Perform NIM Administration Tasks
 *  Manage Machines
 *   Define a Machine
 *    Host Name of Machine                                    [beastie]
 * Perform NIM Software Installation and Maintenance Tasks
 *  Install and Update Software
 *   Install the Base Operating System on Standalone Clients
 *    (Choose: &quot;beastie        machines       standalone&quot;)
 *    (Choose: &quot;spot - Install a copy of a SPOT resource&quot;)
 *    (Chosse: &quot;spot530     resources       spot&quot;)
 *    (Select: &quot;LPP_SOURCE                                    [lpp_source530]&quot;)
 *    (Select: &quot;ACCEPT new license agreements?                [yes]&quot;)
 *    (Select: &quot;Initiate reboot and installation now?         [no]&quot;)
 *    (Select: &quot;ACCEPT new license agreements?                [yes]&quot;)
 */
</pre>
<p><em>The only required prerequisite is to have a fully working name
resolution.</em></p>
<h2>Boot and automatically network install the DLPAR</h2>
<p>Start the DLPAR console via the HMC administration station: activate it and
boot it using the SMS mode.</p>
<p>Then, simply configure the network stack of the Initial Program Load to be
able to netboot the partition and remotely install it:</p>
<pre>
/*
 *  Steps:
 *   2.  Setup Remote IPL (Initial Program Load)
 *    2.   Interpartition Logical LAN      U9113.550.65E3A0C-V8-C3-T1  72ee80008003
 *     1.  IP Parameters
 *      1.  Client IP Address                    [10.126.213.196]
 *      2.  Server IP Address                    [10.126.213.193]
 *      3.  Gateway IP Address                   [000.000.000.000]
 *      4.  Subnet Mask                          [255.255.255.000]
 *     2.  Adapter Configuration
 *      2.  Spanning Tree Enabled
 *       2.  No  &lt;===
 *     3.  Ping Test
 *      1.  Execute Ping Test
 *   5.  Select Boot Options
 *    1.  Select Install/Boot Device
 *     7.  List all Devices
 *      2.        -      Virtual Ethernet
 *                       ( loc=U9113.550.65E3A0C-V2-C3-T1 )
 *      2.  Normal Mode Boot
 *      1.  Yes
 */
</pre>
<p>Notes:</p>
<ol>
<li>10.126.213.191 represents the IP address of the DLPAR</li>
<li>10.126.213.193 represents the IP address of the NIM server
(<code>bootp</code> and <code>tftp</code> servers)</li>
</ol>
<p>Then select the default console device, the installation language (English)
and follow the illustrated guide:</p>
<ol>
<li>In the proposed screen, choose to modify the default installation
settings...<br />
<a title="" href="/blog/consoleshot01.jpeg" lang="en"><img style="width: 300px; height: 236px;" src="/blog/consoleshot01.jpeg" alt="consoleshot #01" /></a></li>
<li>... in the optional choice...<br />
<a title="" href="/blog/consoleshot02.jpeg" lang="en"><img style="width: 300px; height: 236px;" src="/blog/consoleshot02.jpeg" alt="consoleshot #02" /></a></li>
<li>... and for additional software:<br />
<a title="" href="/blog/consoleshot03.jpeg" lang="en"><img style="width: 300px; height: 236px;" src="/blog/consoleshot03.jpeg" alt="consoleshot #03" /></a></li>
<li>Select the <em>Server</em> package:<br />
<a title="" href="/blog/consoleshot04.jpeg" lang="en"><img style="width: 300px; height: 236px;" src="/blog/consoleshot04.jpeg" alt="consoleshot #04" /></a></li>
<li>Choose &quot;Install with the settings listed above.&quot;.</li>
<li>Verify the overall selection...<br />
<a title="" href="/blog/consoleshot06.jpeg" lang="en"><img style="width: 300px; height: 236px;" src="/blog/consoleshot06.jpeg" alt="consoleshot #06" /></a></li>
<li>... and happily wait for the installation to proceed:<br />
<a title="" href="/blog/consoleshot07.jpeg" lang="en"><img style="width: 300px; height: 236px;" src="/blog/consoleshot07.jpeg" alt="consoleshot #07" /></a></li>
</ol>
<h2>Configuration steps on the DLPAR</h2>
<p>Well. The DLPAR <code>beastie</code> is now installed and basically
configured but lacks a lot of tools and none of the network infrastructure is
currently available from it (DNS, NIS, NFS, etc.).</p>
<p>Here is what can be done to render this <em>machine</em> (a little) more
usable.</p>
<h3>Internal IP address (high inter-partition bandwidth)</h3>
<p>As with the Inter-Domain Network (IDN) found on an E10K from Sun
Microsystems, it may be interesting to use an internal LAN to communicate
between LPARs using a high-bandwidth network. So, just pick up an unused IP
address from the chosen private range and apply the settings to the correct
network interface (e.g. configured with the <em>right</em> network ID):</p>
<pre>
# chdev -l en0 -a netaddr='193.168.10.5' -a netmask='255.255.255.0' -a state=up
</pre>
<h3>Allocate <strong>data</strong> storage space</h3>
<p>Applications and users data are kept outside the OS space and reside in
their own volume group. All the data are hosted on SAN disk as provided via the
VIOS:</p>
<pre>
# lsdev | grep hdisk
hdisk0     Available       Virtual SCSI Disk Drive
hdisk1     Available       Virtual SCSI Disk Drive
#
# lsvg
rootvg
#
# lsvg -p rootvg
rootvg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk0            active            639         468         127..85..00..128..128
#
# mkvg -y beastievg hdisk1
beastievg
#
# lsvg -p beastievg
beastievg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk1            active            539         539         108..108..107..108..108
</pre>
<p>Seems ready to put real stuff here right now. Later, if new disk will be
added through the VIOS, simply add the new virtual SCSI disk to the volume
group:</p>
<pre>
# cfgmgr
#
# lsdev | grep hdisk   
hdisk0     Available       Virtual SCSI Disk Drive
hdisk1     Available       Virtual SCSI Disk Drive
hdisk2     Available       Virtual SCSI Disk Drive
#
# extendvg beastievg hdisk2
0516-1254 extendvg: Changing the PVID in the ODM.
#
# lsvg -p beastievg
beastievg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk1            active            539         539         108..108..107..108..108
hdisk2            active            539         539         108..108..107..108..108
</pre>
<h3>Update the operating system to the latest Maintenance Level (e.g. 5.3ML3 at
the time of this writing)</h3>
<p>Log in to the NIM server and set the standalone machine configuration for
<code>beastie</code>:</p>
<pre>
# TERM=vt220 smitty nim
/*
 * Perform NIM Software Installation and Maintenance Tasks
 *  Install and Update Software
 *   Update Installed Software to Latest Level (Update All)
 *    (Choose: &quot;beastie        machines       standalone&quot;)
 *    (Choose: &quot;53ML3          resources      lpp_source&quot;)
 */
</pre>
<p>Most of the time, these kind of software changes require the system to be
rebooted in order for the changes to be made effective:</p>
<pre>
# shutdown -r now
</pre>
<h3>Attaching the partition to the general network infrastructure (LDAP, NIS,
NFS, DNS, etc.)</h3>
<p>Please consult the following online documentation: <a href="/post/2005/06/18/Use-the-NIS-and-NFS-Infrastructure-on-AIX-5X">Use the NIS and
NFS Infrastructure on AIX 5L</a>.</p>
<h3>Modify the maximum number of processes allowed per user</h3>
<p>Check for default setting:</p>
<pre>
# lsattr -D -l sys0 | grep maxuproc
maxuproc        128     Maximum number of PROCESSES allowed per user      True
</pre>
<p>Then change it accordingly, for example <code>default x 4</code>:</p>
<pre>
# chdev -l sys0 -a maxuproc=512
sys0 changed
#
# lsattr -El sys0 | grep maxuproc
maxuproc        512                Maximum number of PROCESSES allowed per user      True
</pre>
<h3>Get network time</h3>
<p>Assuming that the clock is given by the default gateway network device, we
can set and configure the NTP client this way.</p>
<p>Get the default gateway IP address...</p>
<pre>
# netstat -rn | grep default
default          10.126.213.1      UG        1     2738  en1     -   -      -
</pre>
<p>... and hard-set the date using <code>ntpdate(8)</code>:</p>
<pre>
# ntpdate 10.126.213.1
30 Sep 18:04:48 ntpdate[282628]: step time server 10.126.213.1 offset -25063.236234 sec
</pre>
<p>Then, configure the NTP deamon (<code>xntpd</code>) and start the
service:</p>
<pre>
# cp /etc/ntp.conf /etc/ntp.conf.orig
#
# diff -c /etc/ntp.conf.orig /etc/ntp.conf
*** /etc/ntp.conf.orig  Fri Sep 30 18:05:17 2005
--- /etc/ntp.conf       Fri Sep 30 18:05:43 2005
--- 36,42 ----
  #
  #   Broadcast client, no authentication.
  #
! #broadcastclient
! server 10.126.213.1
  driftfile /etc/ntp.drift
  tracefile /etc/ntp.trace
#
# chrctcp -S -a xntpd
0513-059 The xntpd Subsystem has been started. Subsystem PID is 336032.
</pre>
<p>After some time, be sure that the system get its time from the network
device (verify the <strong>*</strong> in front of the remote node):</p>
<pre>
# ntpq -pn
     remote           refid      st t when poll reach   delay   offset    disp
==============================================================================
*10.126.213.1    10.126.192.132   3 u    9   64   37     0.85   13.601  877.12
</pre>
<h3>Install and configure OpenSSH and the administrative tool
<code>sshd_adm</code></h3>
<p>The aim of this part is to have a tuned configuration of OpenSSH for all
clients, and have a specialized configuration for <code>sshd_adm</code>, a
second OpenSSH installation which purpose is to be dedicated to administrators
and administrative tasks.</p>
<p>Install the OpenSSL RPM package provided by the Linux toolbox for AIX:</p>
<pre>
# mount -n nim -v nfs /export/lpp_source /mnt
# rpm -ivh /mnt/cd_roms/AIX_Toolbox_for_Linux_Applications_for_Power_Systems_11.2004/RPMS/ppc/openssl-0.9.7d-1.aix5.1.ppc.rpm
openssl                     ##################################################
</pre>
<p>Then, install the OpenSSH RPM packages, found on the IBM web site (e.g.
<code>openssh-3.8.1p1_53.tar.Z</code> for AIX 5.3):</p>
<pre>
# mkdir /tmp/_sm_inst.$$   /* Put the downloaded package here. */
# zcat /tmp/_sm_inst.$$/openssh-3.8.1p1_`uname -v``uname -r`.tar.Z |  (cd /tmp/_sm_inst.$$ &amp;&amp; tar xf -)
# /usr/lib/instl/sm_inst installp_cmd -a -Q -d /tmp/_sm_inst.$$ -f openssh.* -c -N -g -X -G -Y
geninstall -I &quot;a -cgNQqwXY -J&quot;  -Z   -d /tmp/_sm_inst.176290 -f File 2&gt;&amp;1

File:
    I:openssh.base.client          3.8.0.5302
    I:openssh.base.server          3.8.0.5302
    I:openssh.license              3.8.0.5302
    I:openssh.man.en_US            3.8.0.5302
    I:openssh.msg.HU_HU            3.8.0.5302
    I:openssh.msg.RU_RU            3.8.0.5302
    I:openssh.msg.hu_HU            3.8.0.5302
    I:openssh.msg.ru_RU            3.8.0.5302
[...]
</pre>
<p>Don't forget to clean things up...</p>
<pre>
# rm -r /tmp/_sm_inst.$$
# umount /mnt
</pre>
<p>For the configuration steps of the two deamons, please consult the following
online documentation: <a href="/post/2005/06/14/How-to-Add-a-New-sshd_adm-Service-on-AIX-5X">How to Add a New
&quot;sshd_adm&quot; Service on AIX 5L</a>.</p>
<h3>Install some useful tools commonly used nowadays</h3>
<h4>The <code>tcsh</code> shell</h4>
<p>Log in to the NIM server and set the standalone machine configuration for
<code>beastie</code>:</p>
<pre>
# TERM=vt220 smitty nim
/*
 * Perform NIM Software Installation and Maintenance Tasks
 *  Install and Update Software
 *   Install Software
 *    (Choose: &quot;beastie        machines       standalone&quot;)
 *    (Choose: &quot;AIX_Toolbox_for_Linux     resources       lpp_source&quot;)
 *     (Select: &quot;coreutils-5.0                            ALL&quot;)
 *     (Select: &quot;grep-2.5.1                               ALL&quot;)
 *     (Select: &quot;tcsh-6.11                                ALL&quot;)
 */
</pre>
<h4>The <code>zip</code> and <code>unzip</code> utilities</h4>
<p>Log in to the NIM server and set the standalone machine configuration for
<code>beastie</code>:</p>
<pre>
# TERM=vt220 smitty nim
/*
 * Perform NIM Software Installation and Maintenance Tasks
 *  Install and Update Software
 *   Install Software
 *    (Choose: &quot;beastie        machines       standalone&quot;)
 *    (Choose: &quot;AIX_Toolbox_for_Linux     resources       lpp_source&quot;)
 *     (Select: &quot;zip-2.3                                  ALL&quot;)
 *     (Select: &quot;unzip-5.51                               ALL&quot;)
 */
</pre>
<h4>The <code>j2se</code> toolkit</h4>
<p>Install the Java 1.4.2 packages in mode 32-bit and 64-bit, found on the IBM
web site:</p>
<pre>
# mkdir /tmp/_sm_inst.$$   /* Put the downloaded packages here. */
# /usr/lib/instl/sm_inst installp_cmd -a -Q -d /tmp/_sm_inst.$$ -f _all_latest -g -X -G -Y
geninstall -I &quot;agQqwXY -J&quot;  -Z   -d /tmp/_sm_inst.299180 -f File 2&gt;&amp;1

File:
    I:Java14.license               1.4.2.0
    I:Java14.sdk                   1.4.2.4
    I:Java14_64.license            1.4.2.0
    I:Java14_64.sdk                1.4.2.3
[...]
</pre>
<p>Don't forget to clean things up...</p>
<pre>
# rm -r /tmp/_sm_inst.$$
</pre>
<h4>The <code>sudo</code> security program</h4>
<p>Log in to the NIM server and set the standalone machine configuration for
<code>beastie</code>:</p>
<pre>
# TERM=vt220 smitty nim
/*
 * Perform NIM Software Installation and Maintenance Tasks
 *  Install and Update Software
 *   Install Software
 *    (Choose: &quot;beastie        machines       standalone&quot;)
 *    (Choose: &quot;AIX_Toolbox_for_Linux     resources       lpp_source&quot;)
 *     (Select: &quot;sudo-1.6.7p5                             ALL&quot;)
 */
</pre>
<p>Don't forget to add the hostname <em>beastie</em> to the
<strong>correct</strong> list in the <code>sudoers</code> global site
configuration file.</p>
<h4>The <code>perl</code> parsing tool</h4>
<p>The <code>perl</code> program was compiled from sources, and is available as
a shared NFS resource for AIX at:</p>
<pre>
# df -k /Apps/perl
Filesystem    1024-blocks      Free %Used    Iused %Iused Mounted on
XX.XX.XX.XX:/export/nfssrv/apps/AIX/powerpc/64/5.3/perl    35349504  32800724    8%     7885     1% /Apps/perl
#
# /Apps/perl/5.8.7/bin/perl -v

This is perl, v5.8.7 built for aix
</pre>
<h4>The Unix administrative tool <code>lsof</code> (LiSt Open Files)</h4>
<p>Log in to the NIM server and set the standalone machine configuration for
<code>beastie</code>:</p>
<pre>
# TERM=vt220 smitty nim
/*
 * Perform NIM Software Installation and Maintenance Tasks
 *  Install and Update Software
 *   Install Software
 *    (Choose: &quot;beastie        machines       standalone&quot;)
 *    (Choose: &quot;AIX_Toolbox_for_Linux     resources       lpp_source&quot;)
 *     (Select: &quot;lsof-4.61                                ALL&quot;)
 */
</pre>
<h3>Configure the OS to boot via a SAN disk (thanks to the VIOS)</h3>
<p>So, the operating system is now at the required ML (e.g. 5.3ML3) to support
a SAN boot disk via the VIOS. Assuming that all the steps to make available
this disk to the VIOC are already done, here is how to fully put this partition
onto the SAN.</p>
<p>Information about the corresponding SAN disk provided through the VIOS:</p>
<pre>
# lscfg -v -l hdisk3
  hdisk3           U9113.550.65E3A0C-V8-C11-T1-L840000000000  Virtual SCSI Disk Drive
</pre>
<p>Try to put it in the <code>rootvg</code> system volume group:</p>
<pre>
# extendvg rootvg hdisk3
0516-1254 extendvg: Changing the PVID in the ODM.
0516-1162 extendvg: The Physical Partition Size of 16 requires the creation of
        1078 partitions for hdisk3.  The limitation for volume group rootvg is
        1016 physical partitions per physical volume.  Use chvg command with -t
        option to attempt to change the maximum Physical Partitions per Physical
        volume for this volume group.
0516-792 extendvg: Unable to extend volume group.
</pre>
<p>As shown, a limitation was reached, especially because of the current
settings of the <code>rootvg</code> system volume group. In fact, it is due to
the original size of the virtual disk hosting the system file systems... and so
the size of the corresponding logical volume on the VIOS,
<code>beastielv</code>. Change the <code>rootvg</code> charateristic as
proposed and extend the volume group one more time:</p>
<pre>
# chvg -t 2 rootvg
0516-1193 chvg: WARNING, once this operation is completed, volume group rootvg
        cannot be imported into AIX 430 or lower versions. Continue (y/n) ?
y
0516-1164 chvg: Volume group rootvg changed.  With given characteristics rootvg
        can include upto 16 physical volumes with 2032 physical partitions each.
#
# extendvg rootvg hdisk3
</pre>
<p>The next step is to <em>evacuate</em> the content of <code>hdisk0</code>
(logical volume via a virtual SCSI disk) to <code>hdisk3</code> (dedicated SAN
disk via a virtual SCSI disk):</p>
<pre>
# migratepv hdisk0 hdisk3
0516-1011 migratepv: Logical volume hd5 is labeled as a boot logical volume.
0516-1246 migratepv: If hd5 is the boot logical volume, please run 'chpv -c hdisk0'
        as root user to clear the boot record and avoid a potential boot
        off an old boot image that may reside on the disk from which this
        logical volume is moved/removed.
migratepv: boot logical volume hd5 migrated. Please remember to run
        bosboot, specifying /dev/hdisk3 as the target physical boot device.
        Also, run bootlist command to modify bootlist to include /dev/hdisk3.
</pre>
<p>As explain by the command <code>migratepv</code>, modifiy some boot settings
is <strong>necessary</strong> here and can't be avoided. Clear the boot record
on the `old' system disk:</p>
<pre>
# chpv -c hdisk0
</pre>
<p>Verify and create a boot image on the newly system disk:</p>
<pre>
# bosboot -vd hdisk3 &amp;&amp; bosboot -ad hdisk3

bosboot: Boot image is 23320 512 byte blocks.
</pre>
<p>Alter the device boot list to include (new) and remove (old) disks:</p>
<pre>
# bootlist -m normal -o
hdisk0
# bootlist -m normal hdisk3
# bootlist -m normal -o
hdisk3
#
# ls -ilF /dev/ipldevice /dev/rhdisk3
 1727 crw-------   2 root     system       17,  3 Oct  4 13:01 /dev/ipldevice
 1727 crw-------   2 root     system       17,  3 Oct  4 13:01 /dev/rhdisk3
</pre>
<p>We are done with this by now. Just clean <code>rootvg</code>, remove
<code>hdisk0</code> and restart the partition:</p>
<pre>
# reducevg rootvg hdisk0
#
# rmdev -l hdisk0 -Rd
hdisk0 deleted
#
# shutdown -r now
</pre>
<p>Last, clean the VIOS virtual adapter configuratrion for the partition...</p>
<pre>
$ rmdev -dev vtscsi16
vtscsi16 deleted
$
$ lsmap -vadapter vhost6
SVSA            Physloc                                      Client Partition ID
--------------- -------------------------------------------- ------------------

Build the DLPAR on the Hardware Management Console
--------------------------------------------------

Log in to the HMC using the Java WSM (Web-based System Manager) client.
Then, follow the illustrated guide:

1.  Set the logical name of the partition:\
    [![screenshot
    \#01](/blog/screenshot01.jpeg)](/blog/screenshot01.jpeg)
2.  Because a load manager is not needed, say so to the creation
    wizard:\
    [![screenshot
    \#02](/blog/screenshot02.jpeg)](/blog/screenshot02.jpeg)
3.  Provide a name for the default profile:\
    [![screenshot
    \#03](/blog/screenshot03.jpeg)](/blog/screenshot03.jpeg)
4.  Time to set the memory allocation sizes:\
    [![screenshot
    \#04](/blog/screenshot04.jpeg)](/blog/screenshot04.jpeg)
5.  Select the kind of logical partition, which is *shared* in this
    case:\
    [![screenshot
    \#05](/blog/screenshot05.jpeg)](/blog/screenshot05.jpeg)
6.  Choose the desired entitlement capacity settings,\...\
    [![screenshot
    \#06a](/blog/screenshot06a.jpeg)](/blog/screenshot06a.jpeg)\
    \... the shared mode and the use of virtual CPU (or not):\
    [![screenshot
    \#06b](/blog/screenshot06b.jpeg)](/blog/screenshot06b.jpeg)
7.  Select the I/O unit(s) and its(their) attribution(s) from the
    available hardware on the p550:\
    [![screenshot
    \#07](/blog/screenshot07.jpeg)](/blog/screenshot07.jpeg)
8.  Don\'t choose I/O pools (its purpose is for Mainframe installation
    only).
9.  Answer that virtual I/O cards are required and create two Ethernet
    I/O modules on the two local virtual networks port\'s ID, as for
    this one:\
    [![screenshot
    \#09a](/blog/screenshot09a.jpeg)](/blog/screenshot09a.jpeg)\
    Do the same thing for a SCSI I/O module, using the same port\'s ID
    for the local and remote card (on the Virtual I/O Server):\
    [![screenshot
    \#09b](/blog/screenshot09b.jpeg)](/blog/screenshot09b.jpeg)\
    Be sure to set them to *required* since the virtual SCSI adapter
    will host the boot disk and update the number of virtual cards
    slots, if necessary:\
    [![screenshot
    \#09c](/blog/screenshot09c.jpeg)](/blog/screenshot09c.jpeg)
10. The use of a power management partition is not needed here.
11. Last, select the optional settings for the profil, e.g. activate the
    surveillance of the connections:\
    [![screenshot
    \#11](/blog/screenshot11.jpeg)](/blog/screenshot11.jpeg)

**Important note:**Don\'t forget to add a new virtual SCSI I/O module
(of *server type*) on the VIOS in order to connect to the just created
VIOC. If you made this modification dynamically on the running VIOS,
please report the changes back to the corresponding profile or you will
be in big trouble at the next VIOS reboot!

Manage and allocate the Virtual I/O Server resources
----------------------------------------------------

The new DLPAR will use one sort of VIOS resource: storage device(s).

### Case \#1: reuse an already existing virtual adapter

If we want to reuse the virtual adapter provided to the DLPAR, we need
to clean things up a little. So, delete the corresponding virtual
adapter and its virtual devices attached:

    $ lsmap -vadapter vhost3
    SVSA            Physloc                                      Client Partition ID
    vhost6          U9113.550.65E3A0C-V3-C11                     0x00000000

    VTD                   NO VIRTUAL TARGET DEVICE FOUND

Assuming we will use one local SCSI disk space to hold the OS and one
SAN disk to host the application data, here are the required steps to
configure the VIOS.

Create a logical volume for the OS and be sure to have an unused SAN
disk:

    $ mklv -lv beastielv rootvg 80
    beastielv
    $
    $ lsdev | grep hdisk15
    hdisk15    Available EMC Symmetrix FCP MPIO RaidS

Insert the wanted storage resources in the virtual adapter:

    $  mkvdev -vdev beastielv -vadapter vhost6
    vtscsi16 Available
    $
    $  mkvdev  -vdev hdisk15 -vadapter vhost6
    vtscsi17 Available
    $
    $ lsmap -vadapter vhost6
    SVSA            Physloc                                      Client Partition ID
    ***************
    *** 36,41 ****
      #
      #   Broadcast client, no authentication.
      #
    ! broadcastclient
      driftfile /etc/ntp.drift
      tracefile /etc/ntp.trace
    vhost6          U9113.550.65E3A0C-V3-C11                     0x00000008

    VTD                   vtscsi17
    LUN                   0x8200000000000000
    Backing device        hdisk15
    Physloc               U787B.001.DNW3897-P1-C4-T1-W5006048448930A41-L2000000000000

    VTD                   vtscsi18
    LUN                   0x8300000000000000
    Backing device        hdisk16
    Physloc               U787B.001.DNW3897-P1-C4-T1-W5006048448930A41-L3000000000000

    VTD                   vtscsi19
    LUN                   0x8400000000000000
    Backing device        hdisk23
    Physloc               U787B.001.DNW3897-P1-C4-T1-W5006048448930A41-L12000000000000

    VTD                   vtscsi20
    LUN                   0x8500000000000000
    Backing device        hdisk24
    Physloc               U787B.001.DNW3897-P1-C4-T1-W5006048448930A41-L13000000000000

\... and remove the corresponding logical volume:

    $ rmlv beastielv
    Warning, all data contained on logical volume beastielv will be destroyed.
    rmlv: Do you wish to continue? y(es) n(o)? y
    rmlv: Logical volume beastielv is removed.

### Resize the `/tmp` file system and the swap space logical volume

Because the operating system was installed from scratch and the file
system sizes were automatically adapted to the underlying SCSI virtual
disk (in fact a logical volume provided by the VIOS on a locally
attached SCSI disk), some default values may not be relevant for day to
day use, especially on machine of server type.

In that sense, the size of the `/tmp` file system may be changed to a
more sensible setting:

    # df -k /tmp
    Filesystem    1024-blocks      Free %Used    Iused %Iused Mounted on
    /dev/hd3            32768     32388    2%       10     1% /tmp
    #
    # chfs -a size=1024M /tmp
    Filesystem size changed to 2097152
    #
    # df -k /tmp
    Filesystem    1024-blocks      Free %Used    Iused %Iused Mounted on
    /dev/hd3          1048576   1048044    1%       10     1% /tmp

In the same time, the swap space may be enlarge to grow the available VM
on the system:

    # lsps -al
    hd6
    #
    # lsps -as
    Total Paging Space   Percent Used
          512MB               1%
    #
    # lsvg rootvg | grep "PP SIZE"
    VG STATE:           active                   PP SIZE:        16 megabyte(s)
    #
    # echo "(2048-512)/16" | bc
    96
    #
    # chps -s 96 -a y hd6
    #
    # lsps -as
    Total Paging Space   Percent Used
          2048MB               1%

### Install and configure the backup software tool (TSM)

Please consult the following documentation: [How to Add a New
\"tsmsched\" Service on AIX
5L](/post/2005/06/16/How-to-Add-a-New-tsmsched-Service-on-AIX-5X).

### Put this machine under the `ldm` management tool

Don\'t forget to add the new *machine* in the `ldm` local site utility
in order to be able to manage it and obtain some useful information get
on a daily basis.

    # ldmadd beastie

    TESTING beastie REMOTE ACCESS ...
    sysadm: access denied on beastie ...
    Configuring sysadm's ssh authorized_keys file ... beastie done.

    Checking some prerequisites on beastie ... 
    ok

    Updating ldm agent files on beastie ...
    [...]

### Interesting additional reading

-   [Dynamic logical partitioning for Linux on
    POWER](http://www-128.ibm.com/developerworks/systems/library/es-dynamic/ "DLPAR for Linux on POWER from developerworks")
