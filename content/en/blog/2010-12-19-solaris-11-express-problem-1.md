---
date: "2010-12-19T16:42:29Z"
tags:
- SSH
- Bug
title: 'Solaris 11 Express: Problem #1'
---

*In this series, I will report the bugs or problems I find when running
the Oracle Solaris 11 Express distribution. I hope this will give more
visibility on those PR to Oracle to correct them before the release of
Solaris 11 next year.*

After a fresh installation of Solaris 11 Express from the LiveUSB media,
and with the default set of packages provided by this media (i.e. the
`slim_install` profile), I just fell on the following error message from
a secure-shell login connection:

    invalid UTF-8 sequence: Cannot convert UTF-8 strings to the local codeset

In fact, this problem is not new and was caused by the integration of
the locale fix number 6740240 which added a dependency of the `libssh`
against the locale framework. This generate error messages on systems
where the `iconv` data for UTF-8 is not available. So, this cause no
problem for the proper execution of SSH per-se, but I find this a little
bit annoying on a fresh installed system. So, my point here is that the
dependency is not properly managed from the installation of Solaris 11
Express.

FYI, this problem is covered by the Bug ID number 6872504. Note that you
can add yourself to the interest list at the bottom of the bug report
page:

-   <http://bugs.opensolaris.org/bugdatabase/view_bug.do?bug_id=6872504>
