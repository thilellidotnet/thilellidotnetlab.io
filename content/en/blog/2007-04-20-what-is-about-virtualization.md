---
date: "2007-04-20T20:39:03Z"
tags:
- Resource Management
- System
title: What is about... Virtualization
---

As we hear more and more things about server or data center
virtualization nowadays, i found those those two articles to be very
clear and interesting in explaining what is really involve behind this
general term (virtualization).

The first one came from the
[developerWorks](http://www-128.ibm.com/developerworks/library/l-linuxvirt/index.html "Virtual Linux"),
whereas the second one is hosted on
[InformationWeek](http://www.informationweek.com/news/showArticle.jhtml?articleID=196700116 "How Virtualization Is Revolutionizing Business Data Centers").
Worth reading, really.
