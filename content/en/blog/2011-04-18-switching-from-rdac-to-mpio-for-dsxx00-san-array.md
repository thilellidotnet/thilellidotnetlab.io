---
date: "2011-04-18T13:38:32Z"
tags:
- management
- SAN
- MPxIO
title: Switching From RDAC To MPIO For DSXX00 SAN Array
---
---dar0---

User array name = 'CUSTOMERSOFT'
dac0 ACTIVE dac5 ACTIVE

Disk     DAC   LUN Logical Drive
utm            127
hdisk7   dac5    6 beastie1_oracle
hdisk14  dac5   13 beastie2_datavg
hdisk15  dac0   14 beastie3_datavg
hdisk2   dac0    1 beastie3_rootvg
hdisk3   dac0    2 beastie4_rootvg
hdisk4   dac5    3 beastie5_rootvg
hdisk5   dac5    4 beastie2_rootvg
hdisk6   dac0    5 bakup
hdisk8   dac0    7 customer1
hdisk9   dac0    8 customer3
hdisk10  dac5    9 customer6
hdisk11  dac0   10 customer14
hdisk12  dac5   11 beastie2_db2
hdisk13  dac0   12 beastie3_scheduler
hdisk16  dac0   15 customer9
hdisk17  dac0   16 customer8
</pre>
<p>Listing of the disks as seen from the operating system:</p>
<pre>
# lsdev -Cc disk | grep DS
hdisk2  Available 00-08-02 1814     DS4700 Disk Array Device
hdisk3  Available 00-08-02 1814     DS4700 Disk Array Device
hdisk4  Available 02-00-02 1814     DS4700 Disk Array Device
hdisk5  Available 02-00-02 1814     DS4700 Disk Array Device
hdisk6  Available 00-08-02 1814     DS4700 Disk Array Device
hdisk7  Available 02-00-02 1814     DS4700 Disk Array Device
hdisk8  Available 00-08-02 1814     DS4700 Disk Array Device
hdisk9  Available 00-08-02 1814     DS4700 Disk Array Device
hdisk10 Available 02-00-02 1814     DS4700 Disk Array Device
hdisk11 Available 00-08-02 1814     DS4700 Disk Array Device
hdisk12 Available 02-00-02 1814     DS4700 Disk Array Device
hdisk13 Available 00-08-02 1814     DS4700 Disk Array Device
hdisk14 Available 02-00-02 1814     DS4700 Disk Array Device
hdisk15 Available 00-08-02 1814     DS4700 Disk Array Device
hdisk16 Available 00-08-02 1814     DS4700 Disk Array Device
hdisk17 Available 00-08-02 1814     DS4700 Disk Array Device
</pre>
<p>Switch to a multipath management for the SAN volumes presented from the
DS4700/DS4200 arrays:</p>
<pre>
# manage_disk_drivers -c 4
DS4700/DS4200 currently RDAC/fcparray
Change to alternate driver? [Y/N] Y
DS4700/DS4200 now managed by MPIO

It is necessary to perform a bosboot before rebooting the system in
order to incorporate this change into the boot image.

In order to change to the new driver, either a reboot or a full
unconfigure and reconfigure of all devices of the type changed
must be performed.
</pre>
<p>Reboot the system:</p>
<pre>
# bosboot –a
bosboot: Boot image is 39636 512 byte blocks.

# shutdown –Fr
[...]
</pre>
<p>Verification of the new multipath configuration:</p>
<pre>
# manage_disk_drivers
1: DS4100: currently RDAC; supported: RDAC/fcparray, MPIO
2: DS4300: currently RDAC; supported: RDAC/fcparray, MPIO
3: DS4500: currently RDAC; supported: RDAC/fcparray, MPIO
4: DS4700/DS4200: currently MPIO; supported: RDAC/fcparray, MPIO
5: DS4800: currently RDAC; supported: RDAC/fcparray, MPIO
</pre>
<p>Listing of the disks from the array:</p>
<pre>
# mpio_get_config -vA
Frame id 0:
    Storage Subsystem worldwide name: 60ab80016253400009786efca
    Controller count: 2
    Partition count: 1
    Partition 0:
    Storage Subsystem Name = 'CUSTOMERSOFT'
        hdisk      LUN #   Ownership          User Label
        hdisk0         1   A (preferred)      beastie3_rootvg
        hdisk1         2   A (preferred)      beastie4_rootvg
        hdisk2         3   B (preferred)      beastie5_rootvg
        hdisk3         4   B (preferred)      beastie2_rootvg
        hdisk4         5   A (preferred)      bakup
        hdisk5         6   B (preferred)      beastie1_oracle
        hdisk6        16   A (preferred)      customer8
        hdisk7         7   A (preferred)      customer1
        hdisk8         8   A (preferred)      customer3
        hdisk9         9   B (preferred)      customer6
        hdisk10       10   A (preferred)      customer14
        hdisk11       11   B (preferred)      beastie2_db2
        hdisk12       12   A (preferred)      beastie3_scheduler
        hdisk13       13   B (preferred)      beastie2_datavg
        hdisk14       14   A (preferred)      beastie3_datavg
        hdisk15       15   A (preferred)      customer9
</pre>
<p>Listing of the disks as seen from the operating system:</p>
<pre>
# lsdev -Cc disk | grep DS
hdisk0  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk1  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk2  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk3  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk4  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk5  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk6  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk7  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk8  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk9  Available 06-08-02 MPIO Other DS4K Array Disk
hdisk10 Available 06-08-02 MPIO Other DS4K Array Disk
hdisk11 Available 06-08-02 MPIO Other DS4K Array Disk
hdisk12 Available 06-08-02 MPIO Other DS4K Array Disk
hdisk13 Available 06-08-02 MPIO Other DS4K Array Disk
hdisk14 Available 06-08-02 MPIO Other DS4K Array Disk
hdisk15 Available 06-08-02 MPIO Other DS4K Array Disk
hdisk16 Available 06-08-02 MPIO Other DS4K Array Disk
</pre>
<p>That's it.</p>

Here is a simple procedure switching from a RDAC/fcparray management
mode to a MPIO multipath mode for SAN disks presented from an IBM DSXX00
array.

Verification of the current monopath configuration:

    # manage_disk_drivers
    1: DS4100: currently RDAC; supported: RDAC/fcparray, MPIO
    2: DS4300: currently RDAC; supported: RDAC/fcparray, MPIO
    3: DS4500: currently RDAC; supported: RDAC/fcparray, MPIO
    4: DS4700/DS4200: currently RDAC; supported: RDAC/fcparray, MPIO
    5: DS4800: currently RDAC; supported: RDAC/fcparray, MPIO

Listing of the disks from the array:

    # fget_config -vA
