---
date: "2010-09-24T15:51:19Z"
tags:
- Virtualization
title: Deactivate The VMware HGFS File System
---

I recently faced a problem when our backup administrator was unable to
browse remotely the root (/) file system on Solaris when the system was
installed as a guest in a VMware ESX hypervisor. After digging around
the system, I find that the Host-Guest File System made the HP
DataProtector agent unable to stat the `/hgfs` pseudo-file system as can
be seen in the `/var/opt/omni/log/debug.log` debug log file:

    [...]
    09/23/10 17:06:36  FSBRDA.11618.0 ["da/bda/solaris.c /main/blr_dp61/10":1324] A.06.11 b243
    SolStatObj: /hgfs lstat failed! errno 5

Although it is not a bug per itself, installing the VMware Tools just
enable the HGFS module independently of the virtualization stack: VMware
ESX doesn\'t provide access to the Shared Folders facility, although the
VMware Workstation does. So in my case, I can just disable it without
loosing any useful functionality.

Since it may be advisable to have a configuration option at the VMware
Tools level, I didn\'t find one. Some may argue that write a little
script at the boot to unmount the `/hgfs` file system is good enough, I
find painful and not very elegant. In fact, I prefer to disable at the
kernel module level using the module\'s configuration file:

    # cp -p /kernel/drv/vmhgfs.conf /kernel/drv/vmhgfs.conf.orig
    /* Edit, and comment the vmhgfs line. */
    # cat /kernel/drv/vmhgfs.conf
    # name= parent="pseudo" instance=0;
    #name="vmhgfs" parent="pseudo" instance=0;

Last, be sure to recreate the GRUB boot archive before rebooting the
system, and all backup stuff went well again.

    # bootadm update-archive
    # shutdown -y -i 6 -g 0
