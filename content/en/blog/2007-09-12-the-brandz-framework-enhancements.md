---
date: "2007-09-12T10:43:03Z"
tags:
- Zone
title: The BrandZ Framework Enhancements
---

Although not very versed in BrandZ technologies, I tried the `lx` brand
in the past, with success. But the fact is, the just supported release
of the `lx` brand in the Solaris 8/07 release will not really help us,
since it can *only* provide a 2.4 Linux kernel working environment which
is now a bit outdated (yes, I know the most recent Linux distributions
will be supported via the [Xen open-source
hypervisor](http://www.opensolaris.org/os/community/xen/) ).
Interestingly, two recent news will certainly (hopefully?) change this
in the future.

The first one is the availability of an [experimental Linux 2.6
support](http://mail.opensolaris.org/pipermail/brandz-discuss/2007-September/002436.html)
in OpenSolaris. The work has been done by a summer intern (!) in the
Solaris kernel group, Evan Hoke. What a hard work done in so little
time, really impressive. I just hope that the community will follow and
will enhance the proposed experimental branded zone extension.

The second news is really exciting too. This is the upcoming
availability of a SPARC-only brand designed to [emulate the Solaris 8
kernel](http://mail.opensolaris.org/pipermail/brandz-discuss/2007-September/002432.html).
Although not yet ready for use, this news is really, really amazing
since this enable a more *soft-upgrade* to Solaris 10 by enabling the
use of all the great features provided to us with this version (DTrace,
ZFS, FMA, SMF, etc.) on the global zone, while running a Solaris 8
branded zone\... on top of the most recent hardware (say Sun Fire V215,
T1000, T2000, etc.). Wow. I just can\'t wait to try this out, in
particular since it will be available for the last supported Solaris
8/07 release.
