---
date: "2007-04-08T19:47:37Z"
tags:
- System
title: Bad Online Reports on Windows Vista
---

Generally speaking, i am not really a big supporter of the Windows
operating system. And most of the time, i wait to see and try something
to get an opinion on it. But after seeing these three reports against
recent Microsoft products (two on Windows Vista, one about Office 2007),
i will certainly wait a little before even trying them, at least until
the already planned (scheduled?) Service Pack 1 will be out (at least
for Windows Vista).

I will not say much about it right now. Go reading the reports to get an
idea of what *frightened* me in the first place, if you care:

-   [Vista first look: Bugs and
    confusion](http://www.theregister.com/2007/02/14/pricey_beta_bugger/print.html "Vista at The Register")
-   [Dim
    Vista](http://www.forbes.com/forbes/2007/0226/050_print.html "Vista at Forbes")
-   [Officious
    Office](http://www.forbes.com/forbes/2007/0226/050a_print.html "Office 2007 at Forbes")
