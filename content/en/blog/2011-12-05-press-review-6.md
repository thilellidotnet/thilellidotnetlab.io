---
date: "2011-12-05T18:44:10Z"
tags:
- Press
title: 'Press Review #6'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### Oracle Solaris Crash Analysis Tool 5.3 now available

-   <http://blogs.oracle.com/solariscat/entry/oracle_solaris_crash_analysis_tool>

The Oracle Solaris Crash Analysis Tool Team is happy to announce the
availability of release 5.3. This release addresses bugs discovered
since the release of 5.2 plus enhancements to support Oracle Solaris 11
and updates to Oracle Solaris versions 7 through 10.

### Hard Partitioning!

-   <http://blogs.oracle.com/cmt/entry/hard_partitioning1>

Since December 2, LDoms count as \"Hard Partitioning\". This makes it
possible to license only those cores of a server with Oracle software
that you really need.

### Announcing Release of Oracle Solaris Cluster 4.0!

-   <http://blogs.oracle.com/SC/en_US/entry/announcing_release_of_oracle_solaris>
-   <http://www.oracle.com/us/products/servers-storage/solaris/oracle-solaris-cluster-ds-070228.pdf>
-   <http://www.oracle.com/technetwork/server-storage/solaris-cluster/overview/oracle-solaris-cluster-whatsnew-170557.pdf>

Oracle Solaris Cluster 4.0 offers the best availability for enterprise
applications with instant system failure detection for fastest service
recovery. It includes out-of the box support for Oracle database and
applications such as Oracle WebLogic Server and is pre-tested with
Oracle Sun servers, storage and networking components. It is optimized
to leverage the SPARC SuperCluster redundancy and reliability features
and delivers the high availability infrastructure for the Oracle
Optimized Solutions.

![OCSvsVCS.png](/blog/OCSvsVCS.jpg "OCSvsVCS.png, Dec 2011")

### Oracle E-Business Suite Release 12.1 Certified on Solaris 11

-   <http://blogs.oracle.com/stevenChan/entry/oracle_e_business_suite_release1>

Oracle Solaris 11 was announced last week, and I\'m pleased to also
announce that Oracle E-Business Suite Release 12.1 is now certified on
Oracle Solaris on SPARC (64-bit).

### LDoms networking in Solaris 11

-   <http://blogs.oracle.com/raghuram/entry/ldoms_networking_in_solaris_11>

The network stack for Oracle Solaris 11 has been substantially
re-architected in an effort known as the project Crossbow. One of the
main goals of Crossbow is to virtualize the hard NICs into Virtual NICs
(VNICs) to provide more effective sharing of networking resources. The
VNIC feature allows dividing a physical NIC into multiple virtual
interfaces to provide independent network stacks for applications.

### How low can we go ? (Minimised install of Solaris 11)

-   <http://blogs.oracle.com/darren/entry/how_low_can_we_go>

I wondered how little we can actually install as a starting point for
building a minimised system. The new IPS package system makes this much
easier and makes it work in a supportable way without all the pit falls
of patches and packages we had previously.

### Oracle Solaris Studio 12.3 Launched!

-   <http://blogs.oracle.com/solaris/entry/oracle_solaris_studio_12_3>

Oracle Solaris Studio 12.3, Oracle\'s advanced C, C++ and Fortran
development tool suite, accelerates application performance up to 300%
on Oracle Systems, provides extreme application observability and
enhances developer productivity. Oracle Solaris Studio 12.3 is optimized
for Oracle Solaris, Oracle Linux, and Red Hat Enterprise Linux operating
systems.

### Disgruntled employee? Oracle doesn\'t seem to care about Solaris 11 code leak

-   <http://arstechnica.com/business/news/2011/12/disgruntled-employee-oracle-doesnt-seem-to-care-about-solaris-11-code-leak.ars>

The source code for Oracle\'s Solaris 11 operating system is now out in
the open for anyone to peruse and compile, thanks to a furtive posting
of a compressed archive that has been mirrored across scores of
bitstreams and filesharing sites. But so far, Oracle hasn\'t moved to do
anything about it, and the question remains whether the code was leaked
by a disgruntled Oracle employee, or if this is the strangest
open-source code-drop in history.

### The Rise of Engineered Systems

-   <http://constantin.glez.de/blog/2011/12/rise-engineered-systems>

The point is:\
Building IT systems is complicated, time-consuming, error-prone,
unpredictable, resource-intensive, expensive and risky.

Or, more shortly:\
The way we build IT today is broken.

That\'s what Oracle's Engineered Systems are about.

### How to Install and Configure a Two-Node Cluster

*Using Oracle Solaris Cluster 4.0 on Oracle Solaris 11*

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/o11-147-install-2node-cluster-1395587.html>

How to quickly and easily install and configure Oracle Solaris Cluster
software for two nodes, including configuring a quorum device.

### The case of the un-unmountable tmpfs

-   <http://dtrace.org/blogs/ahl/2011/12/12/the-case-of-the-unmountable-tmpfs/>

Every once in a rare while our development machines encounter an fatal
error during boot because we couldn't unmount tmpfs. This weekend I
cracked the case, so I thought I'd share my uses of boot-time DTrace,
and the musty corners of the operating systems that I encountered along
the way. First I should explain a little bit about what happens during
boot and why we were unmounting a tmpfs filesystem.

### 2000x performance win

-   <http://dtrace.org/blogs/brendan/2011/12/08/2000x-performance-win/>

I recently helped analyze a performance issue in an unexpected but
common place, where the fix improved performance of a task by around
2000x (two thousand times faster). As this is short, interesting and
useful, I've reproduced it here in a lab environment to share details
and screenshots.

### Flame Graphs

-   <http://dtrace.org/blogs/brendan/2011/12/16/flame-graphs/>

Determining why CPUs are busy is a routine task for performance
analysis, which often involves profiling stack traces. Profiling by
sampling at a fixed rate is a coarse but effective way to see which
code-paths are hot (busy on-CPU). It usually works by creating a timed
interrupt that collects the current program counter, function address,
or entire stack back trace, and translates these to something human
readable when printing a summary report.

Profiling data can be thousands of lines long, and difficult to
comprehend. Flame graphs are a visualization for sampled stack traces,
which allows hot code-paths to be identified quickly and accurately.

### Visualizing Device Utilization

-   <http://dtrace.org/blogs/brendan/2011/12/18/visualizing-device-utilization/>

Device utilization is a key metric for performance analysis and capacity
planning. In this post, I'll illustrate different ways to visualize
device utilization across multiple devices, and how that utilization is
changing over time.

As a system to study, I'll examine a production cloud environment that
contains over 5,000 virtual CPUs (over 600 physical processors).

### Coming Soon: My Oracle Support Next-Generation User Interface

-   <http://www.oracle.com/us/dm/100035303-wwsu11104830mpp001-oem-1430133.html>
-   <https://support.oracle.com/CSP/main/article?type=NOT&id=1385682.1>
    (MOS access required)

My Oracle Support will receive a new user interface built using Oracle
Application Development Framework (Oracle ADF). The new interface is
designed to deliver faster, more streamlined access to support
information and services. The upgrade will bring immediate benefits and
also establish a new, state-of-the-art platform for service innovation
over time.

### More thoughts on ZFS compression and crash dumps

-   <http://blogs.oracle.com/cwb/entry/more_thoughts_on_zfs_compression>

Thanks to Darren Moffat for poking holes in my previous post, or more
explicitly pointing out that I could add more useful and interesting
data. Darren commented that it was a shame I hadn\'t included the time
to take a crash dump along side the size, and space usage. The reason
for this is that one reason for using vmdump format compression from
savecore is to minimize the time required to get the crash dump off the
dump device and on to the file system.

### Oracle Solaris 11, Aimed at Cloud Deployments, Enhances Network Virtualization

*December 07, 2011 - IDC Link*

-   <http://www.oracle.com/us/corporate/analystreports/idc-solaris-11-1406314.pdf>

Although customer updates have been shipping to customer sites for many
years, this was the first major release of Solaris in seven years ---
and the first major release since Oracle acquired Sun Microsystems in
January 2010. Following a beta program that began in 2010, there were
more than 750 customers with Solaris 11 in production at launch.

Oracle Solaris runs on the Oracle SPARC hardware systems and on x86
server systems (systems based on Intel or AMD x86 microprocessors).
Oracle sees this dual-platform approach as a differentiator from the two
other major Unix operating systems, IBM AIX and HP-UX 11 v3, which run
on POWER and Itanium systems, respectively, but not on x86 architecture.
\[...\] This dual-platform support, with SPARC and x86, gave Solaris a
bigger footprint in datacenter through the early 2000s and helped
sustain the full portfolio of 11,000 Solaris ISV applications.

Oracle has expanded the functionality of Solaris with Oracle Solaris 11
--- adding new features related to virtualization and cloud computing.
There is a very short list of vendors that show this kind of continued
investment in operating systems --- including Microsoft (Windows); Red
Hat (Linux); and the two leading Unix competitors, IBM and HP.

### SPARC T4 Server eBook

-   <http://www.oracle-downloads.com/sparc_t4_server_ebook/>

### The Oracle ACE Program Newsletter\| December 2011

-   <http://www.oracle.com/technetwork/community/oracle-ace/ace-newsletter-nov2011-1391303.pdf>
