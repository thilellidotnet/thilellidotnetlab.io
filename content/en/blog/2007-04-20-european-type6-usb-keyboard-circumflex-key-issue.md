---
date: "2007-04-20T20:36:08Z"
tags:
- Bug
- Keyboard
title: European Type6 USB Keyboard Circumflex Key Issue
---

Although my first bug report against snv\_38 on a issue related to the
use of a European Type6 USB keyboard with circumflex key seems to be
fixed somewhere between snv\_39 and snv\_54 (at least the status
mentioned \"Closed: Not Reproducible\"), it is not. Filled a
[new](http://bugs.opensolaris.org/view_bug.do?bug_id=6515644 "Bug ID 6515644")
bug report linking to the
[first](http://bugs.opensolaris.org/view_bug.do?bug_id=6420852 "Bug ID 6420852")
one, against snv\_55b this time.

Well, i think i will continue to copy and paste the appropriate word
using Google searches for a while\...
