---
date: "2007-04-27T20:36:50Z"
tags:
- USB
- DHCP
- Network
title: Update the Notebook to FreeBSD 6.0-BETA2
---

I decided to take advantage of this three days week-end (at least in
France) to upgrade the Notebook to FreeBSD
[6.0-BETA2](http://lists.freebsd.org/pipermail/freebsd-stable/2005-August/017586.html "FreeBSD 6.0-BETA2 Available")
and follow the RELENG\_6 branch testing the upcoming
[FreeBSD](http://www.freebsd.org/ "The Power To Serve") release.

In fact, i hope this one may help me in two areas\... more precisely:
fix a very annoying bug resulting in a USB interrupt storm and help on
the wifi side, based on the work done to incorporate the
`wpa_supplicant` code from `ports(7)`.

The installation went smooth and well, although there is no package yet
(it is a beta release, not a release candidate) and all third party need
to be compile from sources. So, i installed the minimum required. In the
same time, update (one more time) the installation notes, notably
regarding the [DHCP
part](http://www.freebsd.org/releases/6.0R/todo.html#docs "FreeBSD 6.0 Open Issues: Documentation items")
(this branch switched from the ISC DHCP client v3.x to the OpenBSD DHCP
client which was based on ISC DHCP v2.x). The biggest problem was to
figure how to set it up in order to be able to update its DNS records
obtained from the DHCP server. The solution is very simple: don\'t let
the client do this setting (as in RELENG\_5 with ISC DHCP) but do it on
the server side level. It has the other advantage not to care how to do
this on multiple heterogeneous clients (UNIX, Unix-like, Windows, etc.).

On the other side, there is **one** problem though. Because this release
needs testing, the default kernel comes with debugging options enable
(INVARIANTS and WITNESS(4) in particular) which have the side effect of
slowing down the machine, really. These options
[help](http://lists.freebsd.org/pipermail/freebsd-current/2003-December/016617.html "Lock order reversals - what do they mean?")
debugging and lightning some problems that may appeared in the
development process. One of this is called a
[LOR](http://www.freebsd.org/doc/faq/troubleshoot.html#LOCK-ORDER-REVERSAL "What is a lock order reversal?")
and i encountered two of them. They are already
[known](http://sources.zabbadoz.net/freebsd/lor.html "FreeBSD LOR(lock order reversal)s and comments")
to the developers, but we don\'t know yet the real impact of these:
harmless or not. Here is the
[thread](http://lists.freebsd.org/pipermail/freebsd-current/2005-August/054265.html "Fresh install of BETA2: console error about a LOR...")
about this on current@.

Go testing. I will try to reproduce the panic i encountered before to
know the code path where this happens.
