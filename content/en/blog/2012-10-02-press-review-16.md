---
date: "2012-10-02T14:38:16Z"
tags:
- Press
title: 'Press Review #16'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### What\'s up with LDoms: Part 4 - Virtual Networking Explained

-   <https://blogs.oracle.com/cmt/entry/what_s_up_with_ldoms4>

In this article, we\'ll have a closer look at virtual networking. Basic
connectivity as we\'ve seen it in the first, simple example, is easy
enough. But there are numerous options for the virtual switches and
virtual network ports, which we will discuss in more detail now. In this
section, we will concentrate on virtual networking - the capabilities of
virtual switches and virtual network ports - only. Other options
involving hardware assignment or redundancy will be covered in separate
sections later on.

### Best Practices - which domain types should be used to run applications

-   <https://blogs.oracle.com/jsavit/entry/best_practices_which_domain_types>

One question that frequently comes up is \"which types of domain should
I use to run applications?\" There used to be a simple answer in most
cases: \"only run applications in guest domains\", but enhancements to
T-series servers, Oracle VM Server for SPARC and the advent of SPARC
SuperCluster have made this question more interesting and worth
qualifying differently. This article reviews the relevant concepts and
provides suggestions on where to deploy applications in a logical
domains environment.

### Power Champions! POWER7+ is here!

-   <http://www.rootvg.net/content/view/521/114/>

Finally, I got the invite to join the Power Champions. Three days of
news and information before today\'s announcements . It has been quite
an experience - finally hearing the news directly from the developers,
project managers, etc. rather than only reading several stack of slides.

### Oracle Announces Oracle Solaris 11.1 at Oracle OpenWorld

-   <http://www.oracle.com/us/corporate/press/1859224>

Adds Unique Oracle Database Support, Extends Cloud Infrastructure
Capabilities and Delivers New Security and Compliance Features for
Highly Available Enterprise Application Deployments.

### Announcing Oracle Solaris 11.1

-   <http://www.oracle.com/technetwork/server-storage/solaris11/overview/solaris11-1-1845817.html>
-   <http://www.oracle.com/technetwork/server-storage/solaris11/documentation/solaris11-1-whatsnew-1732377.pdf>

This document highlights some of the many changes that have occurred
since the introduction of Oracle Solaris 11 11/11. It should be read in
conjunction with the release notes and documentation for the product.

### IPS changes in Solaris 11.1, brought to you by the letter 'P'

-   <http://timsfoster.wordpress.com/2012/10/04/ips-changes-in-solaris-11-1-brought-to-you-by-the-letter-p/>

I thought it might be a good idea to put together a post about some of
the IPS changes that appear in Solaris 11.1. To make it more of a
challenge, everything I'm going to talk about here, begins with the
letter 'P'.

### Measurement artifact

-   <http://www.c0t0d0s0.org/archives/7494-Measurement-artifact.html>

The application was throughoutly instrumented by means of something i
will call \"middleware\" and so the customer thought he was able to see
how long a job on the system took and how much CPU was used.

However the numbers were strange: High throughput, short time to
complete the job, however the CPU time used for a job looked strange.
Something in there was something wrong with the CPU times. Just a guess.
A feeling. Not really knowing.

### Doubled memory capacity on T4-4

-   <http://www.c0t0d0s0.org/archives/7496-Doubled-memory-capacity-on-T4-4.html>

There is only one thing that\'s better than a lot of memory in a system:
More memory! So it\'s quite nice that there is a new memory options with
the T4-4 system. You can now plug 32 GB DIMMS into the systems. The T4-4
can now hold 2 TB of main memory.

### SPARC Processor Roadmap Updated

-   <http://www.oracle.com/us/products/servers-storage/servers/sparc-enterprise/public-sparc-roadmap-421264.pdf>

The updated SPARC Processor Roadmap is now available here. The roadmap
provides data on the M-series,l the T-series and the T-4 series from
2011 through 2016. During that period the M-series is scheduled to add
up to 10x throughput; and the T-series an additional 4.5x throughput.
Thread strength is also set to increase in both series through that
period. Dates are also outlined for expectd updates to Solaris 10 OS and
Solaris 11 OS.

### Disks lie. And the controllers that run them are partners in crime.

-   <http://queue.acm.org/detail.cfm?id=2367378>

Most applications do not deal with disks directly, instead storing their
data in files in a file system, which protects us from those scoundrel
disks. After all, a key task of the file system is to ensure that the
file system can always be recovered to a consistent state after an
unplanned system crash (for example, a power failure). While a good file
system will be able to beat the disks into submission, the required
effort can be great and the reduced performance annoying. This article
examines the shortcuts that disks take and the hoops that file systems
must jump through to get the desired reliability.

### Solaris 11.1 announced

-   <http://sparcv9.blogspot.fr/2012/10/solaris-111-announced.html>

Solaris 11.1 have been announced and will be released later this month.
It is the first update of Solaris 11 since it release november last
year. It contains a few interesting features, I\'ve only list a few,
over 200 projects have integrated into this release.

### SPARC T5, M4 and SPARC64-X

-   <http://sparcv9.blogspot.fr/2012/10/sparc-t5-m4-and-sparc64-x.html>

Short summary of SPARC processor information that was disclosed at
Oracle world, in the near future Oracle will release two different SPARC
processors and Fujitsu will release a new SPARC64 processor with support
for LDOM.

### nicstat update - version 1.92

-   <https://blogs.oracle.com/timc/entry/nicstat_update_version_1_92>

Another minor nicstat release is now available.

### Solaris 11 SRU / Update relationship explained, and blackout period on delivery of new bug fixes eliminated

-   <https://blogs.oracle.com/Solaris11Life/entry/solaris_11_process_enhancement_no>

<!-- -->

-   Relationship between SRUs and Update releases
-   Changes to SRU and Update Naming Conventions
-   No Blackout Periods on Bug Fix Releases

### Active Benchmarking

-   <http://dtrace.org/blogs/brendan/2012/10/23/active-benchmarking/>

Benchmarking is often done badly: tools are run ad-hoc, without
understanding what they are testing or checking that the results are
valid. This can lead to poor architectural choices that haunt you later
on. I previously summarized this situation as: casual benchmarking: *you
benchmark A, but actually measure B, and conclude you've measured C*.

### How to Update to Oracle Solaris 11.1 Using the Image Packaging System

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-update-11dot1-ips-1866781.html>

This article details the steps required to update your Oracle Solaris 11
11/11 systems to Oracle Solaris 11.1 using the Image Packaging System
(IPS).

### Morgan Stanley chooses Solaris 11 to run cloud file services

-   <https://blogs.oracle.com/openomics/entry/morgan_stanley_openafs_solaris_11>
-   <https://blogs.oracle.com/zoneszone/entry/linux_to_solaris_morgan_stanley>

I came across this blog entry and the accompanying presentation by
Robert Milkowski about his experience switching from Linux to Oracle
Solaris 11 for a distributed OpenAFS file serving environment at Morgan
Stanley.

### IDC Recommends Oracle Solaris 11

-   <https://blogs.oracle.com/jimlaurent/entry/idc_recommends_oracle_solaris_11>

IDC published a research report this week on Oracle Solaris 11 and
described it as \"Delivering unique value.\" The report emphasizes the
ability of Oracle Solaris to scale up and provide a mission critical
platform for a wide variety of computing. Solaris built-in server and
network virtualization helps to lower costs and enable consolidation
while reducing administration costs and risks.

### How to Migrate Your Data to Oracle Solaris 11 Using Shadow Migration

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-migrate-s11-data-shadow-1866521.html>

Data migration is a fundamental building block of an Oracle Solaris 11
adoption strategy. This article describes shadow migration, which is a
technology for moving large amounts of data from one file system to
another while the data remains online and accessible to users.

### Oracle rolls up and rolls out Solaris 11.1 update

*Tweaked Solaris Cluster 4.1 system lasher tags along*

-   <http://www.theregister.co.uk/2012/10/25/oracle_solaris_11_update_1/>

Solaris 11, which debuted a year ago, was the first major release of the
Unix operating system that spans Sparc, Sparc64, and x86 iron to come
out since the former Sun Microsystems launched Solaris 10 in January
2005. Oracle had previewed some of the features in Solaris 11.1 and
today it put the code into the field and gave some more details on the
nips and tuck and tweaks in the update.

### Using svcbundle to Create SMF Manifests and Profiles in Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/howto-svcbundle-manifest-profile-1866525.html>

This articles covers a new utility introduced in Oracle Solaris 11.1,
svcbundle, and shows how developers and administrators can use it to
integrate their applications with SMF more quickly.

### How to Migrate Oracle Database from Oracle Solaris 8 to Oracle Solaris 11

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/migrate-s8db-to-s11-1867397.html>

How to use the Oracle Solaris 8 P2V (physical to virtual) Archiver tool,
which comes with Oracle Solaris Legacy Containers, to migrate a physical
Oracle Solaris 8 system with Oracle Database 10.2 and an Oracle
Automatic Storage Management file system into an Oracle Solaris 8
branded zone inside an Oracle Solaris 10 guest domain on top of an
Oracle Solaris 11 control domain.

### Announcing Release of Oracle Solaris Cluster 4.1!

-   <https://blogs.oracle.com/SC/entry/announcing_release_of_oracle_solaris1>

We are very happy to announce the release of Oracle Solaris Cluster 4.1,
providing High Availability (HA) and Disaster Recovery (DR) capabilities
for Oracle Solaris 11.1. This is yet another proof of Oracle\'s
continued investment in Oracle Solaris technologies such as Oracle
Solaris Cluster.

### Solaris 11.1: Encrypted Immutable Zones on (ZFS) Shared Storage

-   <https://blogs.oracle.com/darren/entry/encrypted_immutable_zones_on_shared>

Solaris 11 brought both ZFS encryption and the Immutable Zones feature
and I\'ve talked about the combination in the past. Solaris 11.1 adds a
fully supported method of storing zones in their own ZFS using shared
storage so lets update things a little and put all three parts together.
