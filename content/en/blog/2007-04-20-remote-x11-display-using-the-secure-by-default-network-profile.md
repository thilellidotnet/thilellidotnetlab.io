---
date: "2007-04-20T20:44:11Z"
tags:
- SMF
- X11
title: Remote X11 Display Using the Secure By Default Network Profile
---

Although already present for months in the Sun\'s OpenSolaris
distribution, the new Secure By Default Network Profile feature is now
available with the 11/06
[update](http://docs.sun.com/app/docs/doc/817-0547/6mgbdbsmb?a=view "What's New in the Solaris 10 11/06 Release")
of the official Solaris release.

If you didn\'t get it through a fresh install, you can turn it on using
the provided shell script (as `/usr/sbin/netservices`). It all works
well, except in our case (at work) we want to be able to do remote
display on X11. So, we use this *quickly-written* script in order to be
able to do so easily:

    # cat /tmp/svc.netservices
    #!/usr/bin/env sh

    echo "Setting the limited SMF configuration..."
    netservices limited

    echo "Setting the open form of x11-server..."
    svccfg -s x11-server setprop options/tcp_listen = boolean: true

    echo "Setting the open form of cde-login..."
    svccfg -s cde-login setprop dtlogin/args = "\"\""
    echo "Refresh the cde-login SMF service..."
    svcadm refresh cde-login

    echo "Restarting the SMF services..."
    svcadm restart cde-login

    exit 0

Hope this help\... at least, it will be an online-memo for us!
