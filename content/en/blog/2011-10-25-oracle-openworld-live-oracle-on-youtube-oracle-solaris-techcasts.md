---
date: "2011-10-25T15:01:21Z"
tags:
- Press
title: Oracle OpenWorld Live / Oracle on Youtube / Oracle Solaris TechCasts
---

Here are some multimedia links on Oracle OpenWorld Live, Oracle on
Youtube, and Oracle Solaris TechCasts:

### Watch Oracle OpenWorld Live

*Watch Oracle OpenWorld Live, including Keynotes and TechCast Live*

-   <http://www.oracle.com/openworld/live/index.html>

### Oracle Channel On YouTube

-   <http://www.youtube.com/oracle>

### Oracle Media Network

-   <http://medianetwork.oracle.com/media/>

Follow some highlighted TechCasts:

### TechCast: Oracle Solaris Virtualization

*Joost Pronk, CTO for Oracle Solaris Product Management, provides an
overview of the robust virtualization functionality built into the
Oracle Solaris OS.*

-   <http://medianetwork.oracle.com/video/player/89371481001>

### TechCast: Is Solaris Dead (Again)?

*Lynn Rohrer, director of Oracle Solaris product management, explains
the strategic importance of Solaris to Oracle, and why Oracle invests so
heavily in it.*

-   <http://medianetwork.oracle.com/video/player/1218920897001>

### TechCast: Oracle Systems Strategy Update: Oracle Solaris

*John Fowler, Oracle Executive Vice President, Server and Storage
Systems, details the strategy for Oracle Solaris.*

-   <http://medianetwork.oracle.com/video/player/591036338001>

### TechCast: What\'s Important about Oracle Solaris 11 Installation

*Improving the Installation Experience in Oracle Solaris 11.*

-   <http://medianetwork.oracle.com/video/player/1160018293001>

### TechCast: Oracle Optimized Solutions

*Marshall Choy, Director Optimized Solutions, explains why Oracle\'s
optimized applications-to-disk configurations free the sysadmin from
mundane and trivial tasks.*

-   <http://medianetwork.oracle.com/video/player/1113393796001>

### Techcast: Oracle Database chooses Oracle Solaris Studio

*Learn about what the Oracle Database likes most about the Oracle
Solaris platform and Oracle Solaris Studio development tools. Find out
what\'s new in Oracle Solaris Studio and how you can get early access to
the latest innovations.*

-   <http://medianetwork.oracle.com/video/player/1113393741001>

### TechCast: Oracle Solaris 11 Security Overview

*A high-level overview of Oracle Solaris 11 security capabilities.*

-   <http://medianetwork.oracle.com/video/player/1092539954001>

### TechCast: Changes in Oracle Solaris Cluster 3.3

*All about Oracle Solaris Cluster, including changes that have occurred
since it became a part of Oracle and planned future developments.*

-   <http://medianetwork.oracle.com/video/player/1069491134001>

### TechCast: Oracle Solaris Optimizations for x86 Hardware

*Chris Baker explains the optimizations for x86 hardware provided by
Oracle Solaris, and how developers and sysadmins can take advantage of
them.*

-   <http://medianetwork.oracle.com/video/player/941780492001>

### TechCast: Oracle Solaris 11 Express IPS

*Bart Smaalders, Solaris Core Engineering, explains how sysadmins will
install and manage updates and patches using the new-and-improved Image
Packaging System (IPS).*

-   <http://medianetwork.oracle.com/video/player/850059088001>

### TechCast: Oracle Solaris Studio and Solaris 11 Express

*Don Kretch and Vijay Tatkar discuss new features in Solaris Studio and
the capabilities of Solaris 11 Express, including optimizations for the
Oracle stack and both SPARC and x86 hardware.*

-   <http://medianetwork.oracle.com/video/player/677760187001>

### TechCast: What\'s Great in Solaris 11 Express for Developers

*George Drapeau, from Oracle ISV engineering, talks about the
capabilities of Oracle Solaris 11 Express that will interest application
developers, including the use of Solaris 10 branded zones and new DTrace
probes.*

-   <http://medianetwork.oracle.com/video/player/676329028001>

### TechCast: What\'s Great in Solaris 11 Express for Sysadmins

*Markus Flierl, Dan Price, and Lianne Praza, from Solaris Core
Engineering, describe how the new architecture of Solaris 11 Express
Provides an integrated system that simplifies administration.*

-   <http://medianetwork.oracle.com/video/player/676289533001>

### TechCast: DTrace for System Administrators, with Brendan Gregg

*Rick Ramsey, Solaris Community Leader, interviews Brendan Gregg.*

-   <http://medianetwork.oracle.com/video/player/610283520001>

### TechCast: Preparing for Solaris 11 Installation

*Dave Miner, architect for Solaris Installation, describes the changes
to the installation process and tools for Solaris 11.*

-   <http://medianetwork.oracle.com/video/player/610231229001>
