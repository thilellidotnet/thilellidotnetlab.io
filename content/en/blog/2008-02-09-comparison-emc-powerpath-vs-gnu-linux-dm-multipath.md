---
date: "2008-02-09T11:35:13Z"
tags:
- EMC
- MPxIO
title: 'Comparison: EMC PowerPath vs. GNU/Linux dm-multipath'
---

I will present some notes about the use of multipath solutions on Red
Hat systems: EMC PowerPath and GNU/Linux dm-multipath. Along those
notes, keep in mind that they were based on tests done when pressure was
very high to put new systems in production, so lack of time resulted in
less complete tests than expected. These tests were done more than a
year ago, and so before the release of RHEL4 Update 5 and some of RHBA
related to both LVM and dm-multipath technologies.

Keep in mind that without purchasing an appropriate EMC license,
PowerPath can only be used in failover mode (active-passive mode).
Multiple paths accesses are not supported in this case: no round-robin,
and no I/O load balancer for example.

### EMC PowerPath

#### Advantages

1.  Not specific to the SAN Host Bus Adapter (HBA).
2.  Support for multiple and heterogeneous SAN storage provider.
3.  Support for most UNIX and Unix-like platforms.
4.  Without a valid license, can only work in degraded mode (failover).
5.  Is not sensible to a change in the SCSI LUN renumbering. Adapt
    accordingly the corresponding multiple `sd` devices (different paths
    to a given device) with its multipath definition of the `emcpower`
    device.
6.  Provide easily the ID of the SAN storage.

#### Drawbacks

1.  Not integrated with the operating system (which generally has its
    own solution).
2.  The need to force a RPM re-installation in case of a kernel upgrade
    on RHEL systems (due to the fact that kernel modules are stored in a
    path containing the exact major and minor versions of the installed
    (booted) kernel.
3.  Non-automatic update procedure.

### GNU/Linux device-mapper-multipath

#### Advantages

1.  Not specific to the SAN Host Bus Adapter (HBA).
2.  Support for multiple and heterogeneous SAN storage provider.
3.  Well integrated with the operating system.
4.  Automatic update using RHN (you must be a licensed and registered
    user in this case).
5.  No *additional* license cost.

#### Drawbacks

1.  Only available on GNU/Linux systems.
2.  Configuration (files and keywords) very tedious and difficult.
3.  Without the use of LVM (Logical Volume Management), it has not the
    ability to follow SCSI LUN renumbering! Even in this case, be sure
    not to have blacklisted the newly discovered SCSI devices (`sd`).

Last, please find some interesting documentation on the subject below:

-   [How do I setup device-mapper multipathing in
    RHEL4?](http://kbase.redhat.com/faq/FAQ_51_7170.shtm)
-   [Setting Up Multipathing for
    Storage](https://secure.linux.ncsu.edu/moin/RealmLinuxServers/Multipathing)
-   [Enterprise Storage Quickstart on
    RHEL4](http://web.archive.org/web/20080730024024/http://people.redhat.com/nayfield/storage/RHEL4Storage.html)
