---
date: "2007-04-26T20:57:16Z"
tags:
- Nanoblogger
- Patch
title: FreeBSD Port Patch Preview for the Last NanoBlogger's 3.3 RC5
---

Here is the preview patch for the
[www/nanoblogger](http://www.freebsd.org/cgi/url.cgi?ports/www/nanoblogger/pkg-descr "Port description for www/nanoblogger")
port.

You can apply and install it using the following steps:

    # cd /usr/ports/www/nanoblogger && make deinstall
    # cd /usr/ports && patch < /tmp/nanoblogger.3.3-rc5.patch
    # cd /usr/ports/www/nanoblogger && make install clean clean-depends

Be aware that there are a lot of changes in NanoBlogger itself, so read
carefully the NanoBlogger User Manual [online
documentation](http://nanoblogger.sourceforge.net/docs/nanoblogger.html "NanoBlogger User Manual")
and the `pkg-message` post installation output. After the installation,
you can (re)read it using:

    # pkg_info -D /var/db/pkg/nanoblogger

Download:
[nanoblogger.3.3-rc5.patch](/blog/nanoblogger.3.3-rc5.patch)
