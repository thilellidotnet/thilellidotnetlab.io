---
date: "2008-03-12T17:32:18Z"
tags:
- Boot
- SVM
- GRUB
- Failsafe
- Crash
title: Update A Corrupted GRUB Boot Archive, Without SVM
---

Solaris 10 systems on x86 architecture use the GNU GRand Unified
Bootloader (GRUB) which is the boot loader responsible for loading a
boot archive into a system\'s memory. The boot archive is a collection
of critical files (kernel modules and configuration files) that are
required to boot the Solaris OS. As stated in the [Sun
documentation](http://docs.sun.com/app/docs/doc/817-1985/6mhm8o5ou?a=view):

> These files are needed during system startup before the root file
> system is mounted. Two boot archives are maintained on a system:
>
> -   The boot archive that is used to boot the Solaris OS on a system.
>     This boot archive is sometimes called the primary boot archive.
> -   The boot archive that is used for recovery when the primary boot
>     archive is damaged. This boot archive starts the system without
>     mounting the root file system. On the GRUB menu, this boot archive
>     is called failsafe. The archive\'s essential purpose is to
>     regenerate the primary boot archive, which is usually used to boot
>     the system.

The Solaris OS generally keeps the boot archive properly synchronized on
its own. Sometimes, the boot archive gets corrupted\--for example when
(bad) patches are applied, or the the operating system crashed. In these
cases, the boot archive must be regenerated. This is easily accomplished
following the Sun documentations [x86: How to Boot the Failsafe Archive
for Recovery
Purposes](http://docs.sun.com/app/docs/doc/817-1985/6mhm8o5pu?a=view),
and [x86: How to Boot the Failsafe Archive to Forcibly Update a Corrupt
Boot
Archive](http://docs.sun.com/app/docs/doc/817-1985/6mhm8o5q0?a=view).
The main drawback is when the system is encapsulated under a SVM mirror
(RAID-1) since the `md` driver is not managed under the failsafe mode.
Please refer to this [blog
entry](/post/2008/03/13/Update-A-Corrupted-GRUB-Boot-Archive-With-SVM)
on this subject, if needed.
