---
date: "2007-04-26T21:03:20Z"
tags:
- PHP
- Bug
title: Precompiled PHP RPMs and IBM IHS Web Server on AIX
---

Some developers wanted to use the IBM provided PHP packages on our AIX
pSeries servers to use with the IHS web server. So, we installed the
corresponding RPMs from the *AIX Toolbox for Linux Applications* CD.

But quickly, the following error messages show up when loading the PHP
module:

    Cannot load /opt/freeware/apache/libexec/libphp4.so into server:
    rtld: 0712-001 Symbol ap_child_terminate was referenced from module
    /opt/freeware/apache/libexec/libphp4.so(), but a runtime definition of the symbol
    was not found.
    rtld: 0712-001 Symbol ap_signal was referenced from module
    /opt/freeware/apache/libexec/libphp4.so
    (), but a runtime definition of the symbol was not found.
    rtld: 0712-001 Symbol ap_hard_timeout was referenced from module
    /opt/freeware/apache/libexec/libphp4.so(), but a runtime definition of the symbol
    was not found.
    rtld: 0712-001 Symbol ap_reset_timeout was referenced from module
    /opt/freeware/apache/libexec/libphp4.so(), but a runtime definition of the symbol
    was not found.
    rtld: 0712-001 Symbol ap_table_get was referenced from module
    /opt/freeware/apache/libexec/libphp4.so(), but a runtime definition of the symbol
    was not found.
    rtld: 0712-001 Symbol ap_pstrdup was referenced from module
    /opt/freeware/apache/libexec/libphp4.so
    (), but a runtime definition of the symbol was not found.
    rtld: 0712-001 Symbol ap_table_add was referenced from module
    /opt/freeware/apache/libexec/libphp4.so(), but a runtime definition of the symbol
    was not found.
    Additional errors occurred but are not reported.

However, we can see that:

1.  The dependencies (dynamic linked libraries) are all resolved:

        # ldd /opt/freeware/apache/libexec/libphp4.so
        /opt/freeware/apache/libexec/libphp4.so needs:
                 /usr/lib/libc.a(shr.o)
                 /usr/lib/libdl.a(shr.o)
                 /usr/lib/libbind.a(shr.o)
                 /usr/lib/libcrypt.a(shr.o)
                 /usr/lib/libz.a(libz.so.1)
                 /unix
           

2.  The needed symbols are not found anyway:

        # nm /opt/freeware/apache/libexec/libphp4.so | grep ap_hard_timeout
        .ap_hard_timeout     T      843164
        .ap_hard_timeout     t      843164          40
        ap_hard_timeout      U           -
        ap_hard_timeout      d      247100           4
           

    The `U` means *Undefined symbol* in this case.

In fact, the system calls which begin with `ap_` are generally Apache
specific. In fact, here is what we can found on a FreeBSD system with
the 2.2 release of the Apache web server installed:

    # find /usr/local -print | xargs grep ap_allow_options
    /usr/local/include/apache22/http_core.h:AP_DECLARE(int)
    ap_allow_options(request_rec *r);
    Binary file /usr/local/libexec/apache22/mod_include.so matches
    Binary file /usr/local/libexec/apache22/mod_autoindex.so matches
    Binary file /usr/local/libexec/apache22/mod_cgi.so matches
    Binary file /usr/local/libexec/apache22/mod_negotiation.so matches Binary file
    /usr/local/libexec/apache22/mod_rewrite.so matches
    /usr/local/libexec/apache22/httpd.exp:ap_allow_options
    Binary file /usr/local/sbin/httpd matches
    #
    # nm /usr/local/sbin/httpd | grep -i ap_allow_options
    0000000000429c50 T ap_allow_options

So, it seems that the RPMs provided for the PHP support is compiled for
Apache exclusively (since the function prototypes are declared in a
header file provided by the core Apache distribution), which seems very
reasonable at a second though.

Ended to compile it by hand; which isn\'t finally too bad for the last
PHP5 version.
