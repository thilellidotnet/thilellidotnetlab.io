---
date: "2009-10-07T18:00:33Z"
tags:
- Oracle
title: Upcoming Oracle RDBMS And Solaris News
---

Following the recent news about the future of Sun from [Larry Ellison
itself](http://www.cuddletech.com/blog/pivot/entry.php?id=1069), we now
can hope about more things to come on Solaris, both on SPARC and x86
platforms. In particular, this quote is particularly encouraging:

> We\'re a big supporter of Linux, but the fact is that Solaris just a
> much more mature OS, its just a fact. We became a big supporter of
> Linux years ago because it ran on smaller and cheaper X86 processors
> and Solaris did not, we had no choice. \[\...\] So we are a supporter
> of Linux, but Solaris is a more mature operating system designed for
> bigger systems. We support both.

In the very same vein, I just heard from two different sources these
upcoming changes from Oracle:

1.  The just release [Oracle Database 11g Release
    2](http://www.oracle.com/technology/products/database/oracle11g/index.html),
    currently available only for Linux (and released on 1 September
    2009), will be available soon\--one to two months\--for both Solaris
    SPARC and x86, at the **same time**.
2.  Secondly, Solaris x86 will be raised to *Tier 2* platform from *Tier
    3* currently.

Well, pretty good news in fact! Seems that Solaris will be a serious and
growing competitor in the (near) future!
