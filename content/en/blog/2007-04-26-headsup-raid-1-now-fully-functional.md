---
date: "2007-04-26T20:43:33Z"
tags:
- Gmirror
title: '[headsup] RAID-1 Now Fully Functional'
---

After three months without a second hard disk, a new fresh one came from
the manufacturer. Just scheduled a little intervention-time window; the
main server is back online in less than fifteen minutes.

I just hope that this one will have a little more longer *life* than the
past disk.

As a side note, i just have to remove the old `gmirror` configuration,
and insert the second disk in it, again:

    # gmirror forget gm0
    # gmirror insert gm0 ad10
    # gmirror status
          Name    Status  Components
    mirror/gm0  DEGRADED  ad8
                          ad10 (16%)
