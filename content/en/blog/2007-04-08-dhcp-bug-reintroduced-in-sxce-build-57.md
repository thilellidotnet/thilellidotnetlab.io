---
date: "2007-04-08T19:49:08Z"
tags:
- DHCP
- Bug
title: DHCP Bug Reintroduced in SXCE Build 57
---

After `live upgrade`\'ing to `snv_57`, i was unable to get network
informations, normally obtained using internal DHCP server. In the
system log file, i can see something like:

    Sat Feb 17 11:06:18 unic /sbin/dhcpagent[1494]: [ID 91213 daemon.warning]
     configure_v4_lease: incorrect broadcast address 255.255.255.255 specified
     for nge0; ignoring
    Sat Feb 17 11:06:18 unic /sbin/dhcpagent[1494]: [ID 94624 daemon.warning]
     checkaddr: expected peer address 192.168.1.255 on nge0, have 255.255.255.255

In fact, it seems an old bug was reintroduced while working on an other
(related) CR. See bug report
[6516629](http://bugs.opensolaris.org/view_bug.do?bug_id=6516629 "Bug ID 6516629")
for a detail description of the problem. Since it was already fixed in
`snv_58`, and because there are other known issues with this build being
unable to boot some systems i will stay with build 56, waiting for build
58.
