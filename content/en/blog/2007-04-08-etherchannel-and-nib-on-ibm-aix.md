---
date: "2007-04-08T19:42:00Z"
tags:
- Network
title: EtherChannel and NIB on IBM AIX
---

Recently, while adding availability features to some of our AIX DLPARs
(Dynamic Logical PARtitions), i went through
[this](http://inetsd01.boulder.ibm.com/pseries/sk_SK/aixbman/commadmn/tcp_etherchannel.htm "EtherChannel and IEEE 802.3ad Link Aggregation")
very interesting (and well written) reading which explain why and how to
use, deploy and configure properly some of the very nice network
features on the AIX operating system: EtherChannel and Network Interface
Backup. In short, *EtherChannel* represent a network port aggregation
technology that allow multiple Ethernet adapters to create a single
pseudo-Ethernet device. In its second form, known as *Network Interface
Backup*, it protects against network-SPOF by providing failure detection
and failover mechanism.
