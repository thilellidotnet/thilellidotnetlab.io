---
date: "2008-09-26T19:07:42Z"
tags: []
title: Sun's Free Proficiency Assessment System
---

"Free. Online. Right now." As Peter Tribble [mentioned in his
blog](http://ptribble.blogspot.com/2008/09/would-you-pass.html), it is
clearly not perfect and some questions (and answers) seems a little
obscure sometimes. Nonetheless, as I have never had any (yes, any)
formal training, I though interesting to try these tests, and here are
the results for pre-assessment for:

1.  UNIX Essentials Featuring the Solaris 10 OS, I scored 37 out of 42.
2.  Sun Certified System Administrator for the Solaris 10 OS (Part 1), I
    scored 45 out of 48.
3.  Certified System Administrator for the Solaris 10 OS (Part 2), I
    scored 45 out of 48.

Maybe, is it time to pass to official [Solaris Operating System
certifications](http://www.sun.com/training/certification/resources/paths.html)?
;)

-   Sun Certified Solaris Associate (SCSAS)
-   Sun Certified System Administrator (SCSA)
-   Sun Certified Network Administrator (SCNA)
-   Sun Certified Security Administrator (SCSECA)

Well\... if time permit!
