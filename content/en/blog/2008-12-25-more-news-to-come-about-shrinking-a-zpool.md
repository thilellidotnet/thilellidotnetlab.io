---
date: "2008-12-25T18:06:36Z"
tags:
- ZFS
title: More News To Come About Shrinking A zpool
---

As a little update to [an older
post](/post/2007/03/01/Want-to-Shrink-a-zpool) on this subject, and
although [this post from Matthew
Ahrens](http://blogs.sun.com/ahrens/entry/new_scrub_code) is about the
new scrub code recently introduced in OpenSolaris build 94\--and was in
fact a priority before the launch of the [Sun Storage 7000 Unified
Storage
Systems](http://www.sun.com/storage/disk_systems/unified_storage/)
(a.k.a. Amber Road)\--it is interesting to note that some of the new
code will be usable to remove a disk from a ZFS pool.

As Matthew wrote:

> This work lays a bunch of infrastructure that will be used by the
> upcoming device removal feature.
