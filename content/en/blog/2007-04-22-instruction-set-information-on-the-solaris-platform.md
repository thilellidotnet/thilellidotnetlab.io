---
date: "2007-04-22T17:31:27Z"
tags:
- ISA
- Kernel
title: Instruction Set Information on the Solaris Platform
---

Although i already known about the
[isainfo(1)](http://docs.sun.com/app/docs/doc/816-5165/6mbb0m9j7?a=view "describe instruction set architectures")
utility, i always welcome some very nice
[post](http://prefetch.net/blog/index.php/2006/12/10/displaying-cpu-capabilities-with-solaris/ "Displaying CPU capabilities with Solaris")
as seen on the Blog O\' Matty weblog which learn me about other ISA
command counterparts:
[isalist(1)](http://docs.sun.com/app/docs/doc/816-5165/6mbb0m9j8?a=view "display the native instruction sets executable on this platform"),
and
[optisa(1)](http://docs.sun.com/app/docs/doc/816-5165/6mbb0m9nl?a=view "determine which variant instruction set is optimal to use").

So, here are the corresponding outputs on a Sun Ultra 20 workstation:

    $ isainfo -v
    64-bit amd64 applications
            sse3 sse2 sse fxsr amd_3dnowx amd_3dnow amd_mmx mmx cmov amd_sysc cx8
            tsc fpu
    32-bit i386 applications
            sse3 sse2 sse fxsr amd_3dnowx amd_3dnow amd_mmx mmx cmov amd_sysc cx8
            tsc fpu
    $
    $ isalist
    amd64 pentium_pro+mmx pentium_pro pentium+mmx pentium i486 i386 i86
    $
    $ optisa `isalist`
    amd64

Here are the name of the instruction set(s) used by the operating system
kernel components, and those used by portable applications:

    $ cat /etc/release
                            Solaris 10 11/06 s10x_u3wos_10 X86
               Copyright 2006 Sun Microsystems, Inc.  All Rights Reserved.
                            Use is subject to license terms.
                               Assembled 14 November 2006
    $
    $ uname -a
    $
    SunOS unic 5.10 Generic_118855-33 i86pc i386 i86pc
    $ isainfo -kv
    64-bit amd64 kernel modules
    $
    $ isainfo -nv
    64-bit amd64 applications
            sse3 sse2 sse fxsr amd_3dnowx amd_3dnow amd_mmx mmx cmov amd_sysc cx8
            tsc fpu
