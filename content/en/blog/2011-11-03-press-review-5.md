---
date: "2011-11-03T10:16:55Z"
tags:
- Press
title: 'Press Review #5'
---
<li><a href="http://blogs.computerworlduk.com/infrastructure-and-operations/2011/10/unix---dead-or-alive/index.htm" hreflang="en">http://blogs.computerworlduk.com/infrastructure-and-operations/2011/10/unix---dead-or-alive/index.htm</a></li>
</ul>
<p>Looking to the future of UNIX, Fichera predicts vendors will offer improved
scalability in both hardware and software; and that there will be improvements
in oline maintenance and availability, along with improved partitioning and in
systems management tools as well. None of these developments, it hardly need be
said, could take place without sufficient marketplace interest in UNIX. Fichera
is confident that it exists and will continue. He also expects continued
interest in UNIX from Oracle, IBM and HP.</p>
<h3>The IPS System Repository</h3>
<ul>
<li><a href="http://timsfoster.wordpress.com/2011/11/09/the-ips-system-repository/" hreflang="en">http://timsfoster.wordpress.com/2011/11/09/the-ips-system-repository/</a></li>
</ul>
<p>Some packages in the zone always need to be kept in sync with those packages
in the global zone. For example, anything which delivers a kernel module and a
userland application that interfaces with it must be kept in sync between the
global zone and any non-global zones on the system.</p>
<p>Performing a pkg update from the global zone ensures that all zones are kept
in sync, and will update all zones automatically (though, as mentioned in the
Zones administration guide, pkg update will simply update the global zone, and
ensure that during that update only the packages that cross the kernel/userland
boundary are updated in each zone.)</p>
<h3>SPARC T4 OpenSSL Engine</h3>
<ul>
<li><a href="http://blogs.oracle.com/DanX/entry/sparc_t4_openssl_engine" hreflang="en">http://blogs.oracle.com/DanX/entry/sparc_t4_openssl_engine</a></li>
</ul>
<p>The SPARC T4 microprocessor has several new instructions available to
perform several cryptography functions in hardware. These instructions are used
in a new built-in OpenSSL 1.0 engine available in Solaris 11, the t4 engine.
These new crypto instructions are different from previous generations of SPARC
hardware, which has separate crypto processing units./p&gt;</p>
<h3>Solaris X86 AESNI OpenSSL Engine</h3>
<ul>
<li><a href="http://blogs.oracle.com/DanX/entry/solaris_x86_aesni_openssl_engine" hreflang="en">http://blogs.oracle.com/DanX/entry/solaris_x86_aesni_openssl_engine</a></li>
</ul>
<p>The Intel Westmere microprocessor has six new instructions to accelerate AES
encryption. They are called &quot;AESNI&quot; for &quot;AES New Instructions&quot;. These are
unprivileged instructions, so no &quot;root&quot;, other elevated access, or context
switch is required to execute these instructions. These instructions are used
in a new built-in OpenSSL 1.0 engine available in Solaris 11, the aesni
engine.</p>
<h3>My New Favorite Tool: Oracle VM VirtualBox</h3>
<ul>
<li><a href="http://www.oracle.com/technetwork/articles/servers-storage-admin/vmlove-1368887.html" hreflang="en">http://www.oracle.com/technetwork/articles/servers-storage-admin/vmlove-1368887.html</a></li>
</ul>
<p>This article explains how I used Oracle VM VirtualBox to save time when
testing database installation procedures. Oracle VM VirtualBox proved to be an
incredibly useful tool because I could easily create multiple OS installation
test cases as well as snapshot my progress at various points along the way.</p>
<h3>Virtually the fastest way to try Solaris 11 (and Solaris 10 zones)</h3>
<ul>
<li><a href="http://blogs.oracle.com/dminer/entry/virtually_the_fastest_way_to" hreflang="en">http://blogs.oracle.com/dminer/entry/virtually_the_fastest_way_to</a></li>
</ul>
<p>If you're looking to try out Solaris 11, there are the standard ISO and USB
image downloads on the main page. Those are great if you're looking to install
Solaris 11 on hardware, and we hope you will. But if you take the time to look
down the page, you'll find a link off to the Oracle Solaris 11 Virtual Machine
downloads. There are two downloads there:</p>
<ul>
<li>A pre-built Solaris 10 zone</li>
<li>A pre-built Solaris 11 VM for use with VirtualBox</li>
</ul>
<h3>Critical Threads Optimization</h3>
<ul>
<li><a href="http://blogs.oracle.com/observatory/en_US/entry/critical_threads_optimization" hreflang="en">http://blogs.oracle.com/observatory/en_US/entry/critical_threads_optimization</a></li>
</ul>
<p>The hardware is providing mechanisms to dynamically resource threads
according to their runtime behavior.</p>
<p>We're very aware of these challenges in Solaris, and have been working to
provide the best out of box performance while providing mechanisms to further
optimize applications when necessary. The Critical Threads Optimization was
introduced in Solaris 10 8/11 and Solaris 11 as one such mechanism that allows
customers to both address issues caused by contention over shared hardware
resources and explicitly take advantage of features such as T4's dynamic
threading.</p>
<h3>Solaris 11 : les nouveautés vues par les équipes de développement</h3>
<ul>
<li><a href="http://blogs.oracle.com/EricBezille/entry/solaris_11_les_nouveaut%C3%A9s_vues" hreflang="fr">http://blogs.oracle.com/EricBezille/entry/solaris_11_les_nouveautés_vues</a></li>
</ul>
<p>Pour ceux qui ne sont pas dans la liste de distribution de la communauté des
utilisateurs Solaris francophones, voici une petite compilation de liens sur
les blogs des développeurs de Solaris 11 et qui couvre en détails les
nouveautés dans de multiples domaines.</p>
<h3>Sparc M4 chips etched by Oracle, not Fujitsu</h3>
<ul>
<li><a href="http://www.theregister.co.uk/2011/11/18/oracle_sparc_m4_not_fujitsu/" hreflang="en">http://www.theregister.co.uk/2011/11/18/oracle_sparc_m4_not_fujitsu/</a></li>
</ul>
<p>In a briefing with El Reg to discuss the Solaris 11 launch, we pointed out
that while the logical domain (LDom) partitioning technology on the Sparc T
series was good and competitive with anything in the RISC/Unix and x86 server
spaces, the dynamic domain hardware partitions used in the Sparc Enterprise M
machines were a little bit rigid by comparison and that Oracle had to do
something to bring LDoms to the future M4 processors.</p>
<h3>Solaris 11 Customer Maintenance Lifecycle</h3>
<ul>
<li><a href="http://blogs.oracle.com/Solaris11Life/" hreflang="en">http://blogs.oracle.com/Solaris11Life/</a></li>
<li><a href="http://blogs.oracle.com/Solaris11Life/resource/Solaris_11_Customer_Maintenance_Lifecycle4blog.pdf" hreflang="en">http://blogs.oracle.com/Solaris11Life/resource/Solaris_11_Customer_Maintenance_Lifecycle4blog.pdf</a></li>
</ul>
<p>This new blog is all about the Customer Maintenance Lifecycle for Image
Packaging System (IPS) based Solaris releases, such as Solaris 11. It'll
include policies, best practices, clarifications, and lots of other stuff which
the writer hope you'll find useful as you get up to speed with Solaris 11 and
IPS.</p>
<p>Let's start with a version of its Solaris 11 Customer Maintenance Lifecycle
presentation which he gave at this year's Oracle Open World and at the recent
Deutsche Oracle Anwendergruppe (DOAG - German Oracle Users Group) conference in
Nürnberg.</p>

Here is a little press review around Oracle technologies, and Solaris in
particular:

### Oracle Delivers On SPARC Promises With New T4 Processors And Systems

-   <http://blogs.forrester.com/richard_fichera/11-09-26-oracle_delivers_on_sparc_promises_with_new_t4_processors_and_systems>

This is a major milestone for Oracle and its server community. The
virtues of the SuperCluster aside, it is the first tangible product of
their commitments to a renewed investment in SPARC processor technology,
and as such, it looks impressive. It retains the highly threaded
throughput-oriented architecture of the T-series, and makes major
improvements in single-threaded performance, which was a weakness in
previous generations of T-series technology. But most importantly, it is
early, laying to rest the ghosts of previous disasters at Sun and
Oracle, validating not only Oracle's intentions but their ability to
execute with this new stream of CPU architectures.

### CloudSigma invites Solaris to frolic on its cloud

-   <http://www.theregister.co.uk/2011/11/02/cloudsigma_solaris_infrastructure_cloud/>

CloudSigma, an infrastructure cloud operator based in Zurich, is letting
customers run Solaris and the ZFS file system on its cloud, giving it
full peer status with Linux and Windows operating systems.

Robert Jenkins, CloudSigma CTO, tells El Reg that the company is not
putting servers using either Sparc64 or Sparc T series processors into
its clouds. However, the company will let the x86 version of Solaris 10
run around its cloud and play alongside of myriad Linux and Windows
distributions.

### The System Developer\'s Edge, by Darryl Gove

*Selected Blog Posts and Articles*

-   <http://www.oracle.com/technetwork/server-storage/archive/r11-005-sys-edge-archive-495362.pdf>

The Developer\'s Edge was envisioned as an almanac for developers,
something that gathered together a set of useful resources that could be
dipped into, referred to, or read cover-to-cover. The book was never
intended as the only location where the information resided, however
some of the content is no longer available elsewhere, making it
fortuitous that it has been captured here.

While the main body of text has not been changed, the book has been
updated to the Oracle brand. The title has changed to include the word
\"systems\", to target the intended audience more clearly.

### Oracle Enterprise Manager Cloud Control 12c: Complete, Integrated and Business-Driven Cloud Management

-   <http://www.oracle.com/technetwork/oem/cloud-mgmt/cloudmgmt12c-wp-516612.pdf>

Everyone is now talking about cloud and most of the IT vendor has
latched on to the Cloud promise. Traditional systems management vendors
are no exceptions. However, in most cases, Cloud is treated as a
technology fashion, the newest buzzword in the ever changing landscape
of enterprise technology.

![EM12c.jpg](/blog/EM12c.jpg "EM12c.jpg, Nov 2011")

This whitepaper delves into what really makes an enterprise Cloud. It
covers the complete cloud life cycle and how Oracle Enterprise Manager
12c offers a complete, integrated and business-driven cloud management.

### Engineered and General Purpose Systems

-   <http://blogs.oracle.com/jsavit/entry/engineered_and_general_purpose_systems>

The virtues of Oracle\'s engineered systems, news about which came to
the fore at Oracle OpenWorld2011, are discussed by Jeff Savit\'s blog
post Engineered and General Purpose Systems, where he stresses the
economy to the user of Oracle\'s integrated approach in saving customers
time spent on the \"non-revenue generating efforts\" involved with
designing and configuring enterprise systems. Because Oracle\'s
engineered systems are designed to be optimal for a particular workload
class, validated and proven by Oracle to be reliable, simple to
purchase, configure and manage, and have dramatically superior
performance for their target purpose. Furthermore, Savit notes, these
systems are built on industry-standard components rather than rare or
exotic chips, in order to take advantage of price/performance advances.
So, while there will always be a niche at least for general purpose
systems, the advantages of the engineered system will prove compelling
in most instances, Savit predicts.

### Replacing the Application Packaging Developer's guide

-   <http://timsfoster.wordpress.com/2011/10/17/replacing-the-application-packaging-developers-guide/>
-   <http://wikis.sun.com/download/attachments/240520636/oracle-solaris-11-ips-dev-guide.pdf>

The guide is a lot shorter than the old book -- currently 56 pages, as
opposed to the 190 pages in the document it replaces. Some of this is
because of the fewer examples we have, but also we don't have to write
about patch creation, complex postinstall or class-action scripting or
relocatable packages. IPS is simpler than SVR4 in many ways, though
there is a learning curve, which this book aims to help with.

### The SPARC T4 servers are here!

-   <http://blogs.oracle.com/orasysat/entry/the_sparc_t4_servers_have>

The M-Series are designed with Mainframe-class RAS features
(Reliability, Availability, Serviceability). They are based on the
Sparc64-VII+ CPUs, excelling at single threaded performance.

The T-Series are the CoolThread servers, with the CMT
(chipmultithreading) design, they are designed to run heavily parallel
workloads, concentrating on throughput, running up to 512 threads
actively at the same time, if desired.

The latter category just got a brand new update, let\'s see, what makes
the T4 special.

### Completely disabling root logins on Solaris 11

-   <http://blogs.oracle.com/darren/entry/completely_disabling_root_logins_on>

### Password (PAM) caching for Solaris su - \"a la sudo\"

-   <http://blogs.oracle.com/darren/entry/password_caching_for_solaris_su>

### User home directory encryption with ZFS

-   <http://blogs.oracle.com/darren/entry/user_user_home_directory_encryption>

### Immutable Zones on Encrypted ZFS

-   <http://blogs.oracle.com/darren/entry/immutable_zones_on_encrypted_zfs>

### OpenSSL Versions in Solaris

-   <http://blogs.oracle.com/darren/entry/openssl_versions_in_solaris>

### HOWTO Turn off SPARC T4 or Intel AES-NI crypto acceleration

-   <http://blogs.oracle.com/darren/entry/howto_turn_off_sparc_t4>

Here are a few technical blog entries stacked up using new security
capabilities of Solaris 11:

1.  Completely disabling root logins on Solaris 11
2.  User home directory encryption with ZFS & PAM
3.  Password caching for Solaris su
4.  Immutable Zones on Encrypted ZFS
5.  OpenSSL Versions in Solaris
6.  HOWTO Turn off SPARC T4 or Intel AES-NI crypto acceleration

### Oracle VM vs. VMware vSphere Cost Calculator

-   <http://www.oracle.com/goto/ServerVirtualizationTCO>

This online calculator let you experiment with different scenarios and
see the total cost of ownership in real world dollars for various
solutions.

### Oracle Solaris 11 Engineered for Oracle VM Server Virtualization

-   <http://blogs.oracle.com/virtualization/entry/oracle_solaris_11_engineered_for>

Oracle Solaris 11 was announced today. Oracle Solaris 11 is engineered
for Oracle VM sever virtualization on both x86 and SPARC based systems,
providing deployment flexibility and secure live migration.

### Solaris 11 DTrace syscall Provider Changes

-   <http://dtrace.org/blogs/brendan/2011/11/09/solaris-11-dtrace-syscall-provider-changes/>

Oracle Solaris 11 dropped many commonly used probes from the DTrace
syscall provider, a disappointing side-effect of some code refactoring
in the system call trap table (PSARC 2010/441 "delete obsolete system
call traps"). This breaks a lot of scripts and one liners, including
many that are used to teach beginners DTrace. Functionality is still (I
think) possible, albeit by learning trap table mappings and tracing
those.

### What\'s new on the Solaris 11 Desktop?

-   <http://blogs.oracle.com/calum/entry/what_s_new_on_the>

Much has been written today about the enterprise and cloud features of
Oracle Solaris 11, which was launched today, but what\'s new for those
of us who just like to have the robustness and security of Solaris on
our desktop machines? Here are a few of the Solaris 11 desktop
highlights.

### UNIX - Dead or alive?

*The extinction of UNIX is not going to happen in our lifetimes*
