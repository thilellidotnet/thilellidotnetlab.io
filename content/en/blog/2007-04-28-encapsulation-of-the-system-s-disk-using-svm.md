---
date: "2007-04-28T10:07:47Z"
tags:
- SVM
- RAID
- System
title: Encapsulation of the System's Disk Using SVM
---

1.  `c0t0d0s2` represents the first system disk (boot)
2.  `c0t1d0s2` represents the second disk (mirror)

Duplicate the label\'s content from the boot disk to the mirror disk:

    # prtvtoc /dev/rdsk/c0t0d0s2 | fmthard -s - /dev/rdsk/c0t1d0s2

Create replicas of the metadevice state database:

    # metadb -a -c3 -f c0t0d0s4 c0t1d0s4
    # metadb

*Option `-f` is needed because it is the first invocation/creation of
`metadb(1m)`.*

Creation of metadevices:

    # metainit -f d10 1 1 c0t0d0s0
    # metainit -f d11 1 1 c0t0d0s1
    # metainit -f d13 1 1 c0t0d0s3
    # metainit -f d16 1 1 c0t0d0s6
    #
    # metainit d20 1 1 c0t1d0s0
    # metainit d21 1 1 c0t1d0s1
    # metainit d23 1 1 c0t1d0s3
    # metainit d26 1 1 c0t1d0s6

*Option `-f` is needed because the file systems created on the slice we
want to initialize a new metadevice are already mounted.*

Create the first part of the mirror:

    # metainit d0 -m d10
    # metainit d1 -m d11
    # metainit d3 -m d13
    # metainit d6 -m d16
    #
    # cp /etc/vfstab /etc/vfstab.beforesvm
    # metaroot d0

Don\'t forget to edit `/etc/vfstab` in order to reflect the other
metadevices:

-   `s@/dev/dsk/cXtYdZsN@/dev/md/dsk/dN@`
-   `s@/dev/rdsk/cXtYdZsN@/dev/md/rdsk/dN@`

Install the boot block code on the alternate boot disk and set it in the
OpenBoot Prom (OBP):

    # installboot /usr/platform/`uname -i`/lib/fs/ufs/bootblk /dev/rdsk/c0t1d0s0
    # eeprom boot-device="disk disk1 net"   /* Or just "disk disk1". */

Reboot on the new metadevices (the operating system will now boot
encapsulated):

    # shutdown -y -g 0 -i 6

Attach the second part of the mirror:

    # metattach d0 d20
    # metattach d1 d21
    # metattach d3 d23
    # metattach d6 d26

Verify all:

    # metastat -p
    # metastat | grep \%

Modify the system dump configuration:

    # mkdir /var/crash/`hostname`
    # chmod 700 /var/crash/`hostname`
    # dumpadm -s /var/crash/`hostname`
    # dumpadm -d /dev/md/dsk/d1
