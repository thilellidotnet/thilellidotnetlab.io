---
date: "2007-04-08T19:33:42Z"
tags:
- SMF
- Network
title: Adding a Specific Logical Interface with the Solaris 10
---

Under Solaris 10, there is no `/etc/init.d/inetsvc` network script
anymore. You must use the new SMF to manage your network services, even
special initialization ones. In order to plumb and up a new logical
interface, you can now use the service instance named
`svc:/network/physical:default`.

Here is quick example on how to use it:

    # ifconfig -au4
    lo0: flags=2001000849<UP,LOOPBACK,RUNNING,MULTICAST,IPv4,VIRTUAL> mtu 8232 index 1
            inet 127.0.0.1 netmask ff000000 
    nge0: flags=201004843<UP,BROADCAST,RUNNING,MULTICAST,DHCP,IPv4,CoS> mtu 1500 index 2
            inet 192.168.1.101 netmask ffffff00 broadcast 192.168.1.255
            ether 0:e0:81:58:88:ae 
    # 
    # cat /etc/hostname.nge0:1
    192.168.1.50
    # 
    # svcadm restart network/physical
    #
    # ifconfig -au4
    lo0: flags=2001000849<UP,LOOPBACK,RUNNING,MULTICAST,IPv4,VIRTUAL> mtu 8232 index 1
            inet 127.0.0.1 netmask ff000000 
    nge0: flags=201004843<UP,BROADCAST,RUNNING,MULTICAST,DHCP,IPv4,CoS> mtu 1500 index 2
            inet 192.168.1.101 netmask ffffff00 broadcast 192.168.1.255
            ether 0:e0:81:58:88:ae 
    nge0:1: flags=201000843<UP,BROADCAST,RUNNING,MULTICAST,IPv4,CoS> mtu 1500 index 2
            inet 192.168.1.50 netmask ffffff00 broadcast 192.168.1.255
    # 
    # ifconfig nge0:1 down unplumb
    #
    # ifconfig -a4
    lo0: flags=2001000849<UP,LOOPBACK,RUNNING,MULTICAST,IPv4,VIRTUAL> mtu 8232 index 1
            inet 127.0.0.1 netmask ff000000 
    nge0: flags=201004843<UP,BROADCAST,RUNNING,MULTICAST,DHCP,IPv4,CoS> mtu 1500 index 2
            inet 192.168.1.101 netmask ffffff00 broadcast 192.168.1.255
            ether 0:e0:81:58:88:ae
