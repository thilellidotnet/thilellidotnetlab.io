---
date: "2007-04-27T17:45:07Z"
tags:
- X11
- Bug
title: List of Bugs Fixed per Operating System/Networking (ON) Build
---

Already want to be able to easily find what is new in each build of the
ON
[consolidation](http://www.opensolaris.org/os/downloads/on/ "Operating System/Networking (ON) Download Center")
from OpenSolaris; including Solaris Express and Solaris Express:
Community Release?

Speaking about specifics *putback logs* (`HTML` and text based reports
are available):

1.  ON consolidation: [change logs and
    downloads](http://dlc.sun.com/osol/on/downloads/ "OpenSolaris Download Center")
2.  X consolidation: [change
    logs](http://opensolaris.org/os/community/x_win/changelogs/ "OpenSolaris X Consolidation Change Logs")

For more general overview of what happen at the SX release level, the
two best places are certainly:

1.  docs.sun.com: [What\'s New in Solaris
    Express](http://docs.sun.com/app/docs/doc/819-2183/6n4g726ua?l=en "Solaris Express Release and Installation Collection: What's New in Solaris Express")
2.  Dan Price\'s Weblog: [Solaris
    entries](http://blogs.sun.com/roller/page/dp?catname=/Solaris "The View from the Moon")
