---
date: "2013-06-04T11:50:36Z"
tags:
- Press
title: 'Press Review #24'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### Using ndisk64 to test new disk set-ups like Flash Systems

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/using_ndisk64_to_test_new_disk_set_ups_like_flash_systems>

This is a typical request from IBMers in services or direct from
customers wanting to confirm a good disk setup or confirm disks deliver
as promised. This time \"\... a test of SAN disk performance where
LUN\'s are carved from a IBM Flash System 820 with SVC. Primary
objective is to show the IBM Flash System 820 can do hundreds of
thousands IOPS with response time \<1ms with good throughput. This is
for an Oracle RDBMS using ASM\".

### A SPARC CPU Overview: SPARC64-VII+ (M3), T3, T4, T5, M5, M10 - how do they compare?

-   <https://blogs.oracle.com/orasysat/entry/sparc_t4_t5_m5_m10>

Do you know how many SPARC CPU types does Oracle deliver currently? Not
one, not two, but five. The SPARC64-VII+, the T4, T5, M5 and M10.

### Instant Automated Installer Zone for Oracle Solaris 11.1

-   <https://blogs.oracle.com/solaris/entry/instant_ai_zone>

We\'ve just introduced something new to make it easier to start working
with AI, so you can get past that \"different\" part and start reaping
the (major) benefits. The name\'s kinda long: Oracle Solaris Zone with
Automated Installer and Repository\... but that\'s because there\'s a
lot it\'s doing for you in one shot. As the name implies, it sets up a
new zone on your system, with a local Oracle Solaris 11.1 package
repository for both SPARC and x86 deployments, and a DHCP server which
you can optionally turn on, configured to act as an AI server. It\'s
pretty much \"install and go\" and you can immediately start to explore
the extensive feature set of AI.

### Linux Performance Analysis and Tools

-   <http://dtrace.org/blogs/brendan/2013/06/08/linux-performance-analysis-and-tools/>

At the Southern California Linux Expo earlier this year (SCaLE 11x) I
presented a talk on Linux Performance Analysis and Tools. It's a great
conference, and I was happy to be back.

My talk provided an overview of over twenty performance tools, and I
described the problems they solve. At the end of the talk, I summarized
some methodologies for using these tools, so that you know when to reach
for what.

### How many IOPS can a HDD, HHDD or SSD do?

-   <http://storageioblog.com/iops-hdd-hhdd-ssd-vmware/>

A common question I run across is how many IOPS (IO Operations Per
Second) can a storage device or system do or give.

The answer is or should be it depends.

![](http://i0.wp.com/storageio.com/images/HardDiskDriveStorage.png)

### A closer look at the new T5 TPC-H result

-   <https://blogs.oracle.com/cmt/entry/a_closer_look_at_the>

You\'ve probably all seen the new TPC-H benchmark result for the SPARC
T5-4 submitted to TPC on June 7. Our benchmark guys over at \"BestPerf\"
have already pointed out the major takeaways from the result. However, I
believe there\'s more to make note of.

### Moving Oracle Solaris 11 Zones between physical servers

-   <https://blogs.oracle.com/openomics/entry/solaris_zone_migration>

As part of my job in the ISV Engineering team, I am often asked by
partners the following question : is it possible to easily move a
Solaris 11 Zone from a physical server to another? The short answer is :
YES !

![](https://blogs.oracle.com/openomics/resource/Zone_Migration.gif)

### Comparing Solaris 11 Zones to Solaris 10 Zones

-   <https://blogs.oracle.com/JeffV/entry/comparing_solaris_11_zones_to>

Many people have asked whether Oracle Solaris 11 uses sparse-root zones
or whole-root zones. I think the best answer is \"both and neither, and
more\" - but that\'s a wee bit confusing. This blog entry attempts to
explain that answer.

### Best Practices - Live Migration on Oracle VM Server for SPARC

-   <https://blogs.oracle.com/jsavit/entry/best_practices_live_migration>

Oracle VM Server for SPARC has supported live migration since 2011,
providing operational flexibility for customers who need to move a
running guest domain between servers. This can be extremely useful, but
there\'s confusion about when it is the right tool to use, when it
isn\'t, and how to best make use of it. This article will discuss some
best practices and \"do\'s and don\'ts\" for live migration.

### Frequency Trails

-   <http://dtrace.org/blogs/brendan/2013/06/19/frequency-trails/>

Frequency trails are a simple and intuitive visualization of the
distribution of sampled data. I developed them to study the finer
details of hundreds of measured latency distributions from production
servers, and in particular, to study latency outliers.

![](http://dtrace.org/blogs/brendan/files/2013/06/frequencytrail_waterfall_20ms_black.png)

### Linux Performance Analysis and Tools

-   <http://dtrace.org/blogs/brendan/2013/06/08/linux-performance-analysis-and-tools/>

At the Southern California Linux Expo earlier this year (SCaLE 11x) I
presented a talk on Linux Performance Analysis and Tools. It's a great
conference, and I was happy to be back.

### Systems Performance: Enterprise and the Cloud

-   <http://dtrace.org/blogs/brendan/2013/06/21/systems-performance-enterprise-and-the-cloud/>

Systems performance analysis is an important skill for all computer
users, whether you're trying to understand why your laptop is slow, or
optimizing the performance of a large-scale production environment. It
is the study of both operating system (kernel) and application
performance, but can also lead to more specialized performance topics,
for specific languages or applications. Systems performance is covered
in my upcoming book: Systems Performance: Enterprise and the Cloud, to
be published by Prentice Hall this year.

![](http://dtrace.org/blogs/brendan/files/2013/06/systemsperf_coverc.jpeg)

### nmon CPU graphs - Why are the PCPU\_ALL graphs lower?

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/nmon_cpu_graphs_why_do_the_pcpu_all_graphs_lower>

I have been looking at some nmon data from an IBMer looking into a
customers machine. The virtual machine (LPAR) is running the Oracle
RDBMS with 75 dedicated CPUs - on a POWER7 Power 795 at 4 GHz.

![](https://www.ibm.com/developerworks/community/blogs/aixpert/resource/BLOGS_UPLOADED_IMAGES/nmon_PCPU480.jpg)

### Facebook's advanced erasure codes

-   <http://storagemojo.com/2013/06/21/facebooks-advanced-erasure-codes/>

We want our data protected from device failures. When there is a failure
we want to get our data back quickly. And we want to pay as little as
possible for the protection and the restore. How?

### Hello, Manta: Bringing Unix to Big Data

-   <http://www.joyent.com/blog/hello-manta-bringing-unix-to-big-data>

Joyent Manta is a highly scalable, distributed object storage service
with integrated compute. Developers can store and process any amount of
data at any time where a simple web API call replaces the need for
spinning up instances. Joyent Manta Compute is a complete and high
performance compute environment including R, Python, node.js, Perl,
Ruby, Java, C/C++, ffmpeg, grep, awk and others. Metering is by the
second with zero provisioning, data movement or scheduling latency
costs.

### The death of disks

-   <http://storagemojo.com/2013/06/25/the-death-of-disks/>

A forecast says PC shipments with disk drives will drop by a third
between now and 2017. IBM is pushing the all-flash datacenter. SSD start
ups are claiming that flash is really as cheap as disk with much better
performance. Is it the beginning of the end for disk drives?

### Manta: Unix Meets Map Reduce

-   <http://dtrace.org/blogs/brendan/2013/06/25/manta-unix-meets-map-reduce/>

Today Joyent launched Manta: an object store built upon ZFS and Zones,
the SmartOS platform, and with a familiar Unix interface as the API.
This supports compute jobs such as map reduce, and provides high
performance by co-locating zones with the object storage. We also have
extensive DTrace instrumentation throughout the product, which we've
been using in development to help tune performance and respond to
performance issues.

### Linux-Containers --- Part 1: Overview

-   <https://blogs.oracle.com/OTNGarage/entry/linux_containers_part_1_overview>

Linux Containers (LXC) provide a means to isolate individual services or
applications as well as of a complete Linux operating system from other
services running on the same host. To accomplish this, each container
gets its own directory structure, network devices, IP addresses and
process table. The processes running in other containers or the host
system are not visible from inside a container. Additionally, Linux
Containers allow for fine granular control of resources like RAM, CPU or
disk I/O.

### Oracle 12c: First (and best!) on Solaris

-   <https://blogs.oracle.com/zoneszone/entry/oracle_12c_first_and_best>

Oracle 12c is now available for download. Notice that support for
Solaris SPARC and x86-64 are among the operating systems supported on
the first day of availability. \[\...\] As has been the case for some
time, Oracle databases are supported in zones.

### New White Paper about Upgrade to Oracle Database 12c

-   <http://www.oracle.com/technetwork/database/upgrade/upgrading-oracle-database-wp-12c-1896123.pdf>

This white paper outlines the methods available for you to upgrade and
migrate your database to Oracle Database 12c. Learn about different use
cases and key factors to consider when choosing the method that best
fits your requirements.

### Improving Manageability of Virtual Environments

-   <https://blogs.oracle.com/JeffV/entry/improving_manageability_of_virtual_environments>

Until recently, Solaris 10 Branded Zones on Solaris 11 suffered one
notable regression: Live Upgrade did not work. The individual packaging
and patching tools work correctly, but the ability to upgrade Solaris
while the production workload continued running did not exist. A recent
Solaris 11 SRU (Solaris 11.1 SRU 6.4) restored most of that
functionality, although with a slightly different concept, different
commands, and without all of the feature details. This new method gives
you the ability to create and manage multiple boot environments (BEs)
for a Solaris 10 Branded Zone, and modify the active or any inactive BE,
and to do so while the production workload continues to run.

### AIX and VIOS Minimum Levels for POWER7 and 7+ Hardware

-   <https://www.ibm.com/developerworks/community/blogs/aixpert/entry/aix_and_vios_minimum_levels_for_power7_and_7_hardware>

A very nice chart from Richard Milton of IBM on the minimum levels for
AIX and VIOS of Power7 and Power7+ hardware.

![](https://www.ibm.com/developerworks/community/blogs/aixpert/resource/BLOGS_UPLOADED_IMAGES/AIX_and_VIOS_Levels_for_POWER7_and_POWER7plus_2013_06_20.jpg)

### Ginormous\...the new Oracle T5-8 SPARC SuperCluster!

-   <https://blogs.oracle.com/drcloud/entry/ginormous_the_new_oracle_t5>

Ginormous\...no other way to describe it\...the new Oracle T5-8 SPARC
SuperCluster\...2000+ fast CPU threads, massive memory (DRAM & Flash),
1.2 M IOPS, HUGE storage and bandwidth\...WOW! Wanna build a SPARC
Cloud? This is it! Multiple virtualization technologies (VM Server for
SPARC, and Solaris zones) for elasticity and resource pooling along with
Oracle Enterprise Manager Ops Center 12c providing full cloud management
capability.

### Oracle cranks up SuperCluster with Sparc T5 engines

*Anything Big Blue can do, Big Red can do cheaper -- and better*

-   <http://www.theregister.co.uk/2013/06/27/oracle_sparc_supercluster_t5_8/>

Software giant and hardware playa Oracle has launched its high-end
SuperCluster T5-8 \"engineered system\" based on its sixteen-core Sparc
T5 processor.

The half-rack configuration of the Sparc SuperCluster populates each
node with four of its eight Sparc T5 chips plus 1TB of main memory,
eight 900GB disk drives spinning at 10K RPM, four 40Gb/sec InfiniBand
ports and four 10GB/sec Ethernet ports. The InfiniBand ports are used to
link the two server nodes to each other in an Oracle RAC cluster and to
the Exadata storage servers that are also in the rack. Across those two
nodes, you have 128 cores and 2TB of memory.

![](http://regmedia.co.uk/2013/06/27/oracle_sparc_supercluster_versus_ibm.jpg)

### Oracle Database 12c: Finally, a True Cloud Database

-   <http://www.cio.com/article/735481/Oracle_Database_12c_Finally_a_True_Cloud_Database>

In development for roughly four years, Oracle Database 12c introduces so
many important new capabilities in so many areas \-- database
consolidation, query optimization, performance tuning, high
availability, partitioning, backup and recovery \-- that even a lengthy
review has to cut corners. Nevertheless, in addition to covering the big
ticket items, I\'ll give a number of the lesser enhancements their due.
