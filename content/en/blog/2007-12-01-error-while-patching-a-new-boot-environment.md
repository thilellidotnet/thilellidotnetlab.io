---
date: "2007-12-01T17:04:29Z"
tags:
- Zone
- Live Upgrade
- Patch
title: Error While Patching A New Boot Environment
---

After creating a new boot environment (BE) named `beastie`, see below,
to upgrade a system running Solaris 8 to Solaris 10 11/06 (as I do many
times in the past without a hiccup), I encounter a problem when I tried
to apply the appropriate Recommended cluster patch to the new BE with
this message:

    # luupgrade -t -n beastie -s /var/tmp/10_Recommended

    Validating the contents of the media .
    The media contains 76 software patches that can be added.
    All 76 patches will be added because you did not specify any specific
    patches to add.
    Mounting the BE .
    ERROR: The boot environment  supports non-global
    zones.The current boot environment does not support non-global zones.
    Releases prior to Solaris 10 cannot be used to maintain Solaris 10 and
    later releases that include support for non-global zones. You may only
    execute the specified operation on a system with Solaris 10 (or later)
    installed.

I can\'t find any reference to a known bug or problem after looking for
this against SunSolve, Sun Support, and *Googling*. Has anyone already
seen this error, and solved it The Right Way? As for me, I needed to
boot from the BE and apply the cluster patch: this was a pain since this
bundle include the `-36` kernel patch which is known to be relatively
disruptive, since it need two reboot to apply the entire cluster patch
(it contains a new version for the kernel).

*Update \#1 (2009-03-22):* Seems to be explained in this excellent
[BigAdmin
article](http://www.sun.com/bigadmin/features/articles/live_upgrade_patch.jsp).
