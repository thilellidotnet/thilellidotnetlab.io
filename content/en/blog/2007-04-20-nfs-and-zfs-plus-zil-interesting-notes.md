---
date: "2007-04-20T20:37:49Z"
tags:
- ZFS
- NFS
- Performance
title: NFS and ZFS plus ZIL Interesting Notes
---

I recently learn about NFS on ZFS interaction problem reading the great
blog of [Ben
Rockwood](http://www.cuddletech.com/blog/pivot/entry.php?id=780 "The Season of Giving: Some Experiences from Joyent").
Although not directly related to what he encountered, this recent great
post about how NFS behaves with ZFS backend, particularly on the
performance comparison front, says a lot of things about why you might
see *poor* performance using this two technologies together.

To go deeper on this front, you can read more about the ZIL purpose on
[Eric
Kustarz](http://blogs.sun.com/roch/entry/nfs_and_zfs_a_fine "NFS and ZFS, a fine combination")\'s
weblog, and follow closely this ZFS
[thread](http://www.opensolaris.org/jive/thread.jspa?threadID=21431 "NFS and ZFS, a fine combination")
on the OpenSolaris website.
