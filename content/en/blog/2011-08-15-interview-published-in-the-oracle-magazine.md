---
date: "2011-08-15T16:04:10Z"
tags:
- Oracle
- Press
title: Interview Published In The Oracle Magazine
---

I had the great opportunity to be profiled for the *Peer-to-Peer* colum
of the [Oracle Magazine], which is an international publication
published by Oracle on a monthly basis.

Although the interview is now available [online], it has also been
truncated because of some space concerns, and other things related to
the format of the publication.  For those who are interested, here is
the complete interview.

### Basic information

#### Brief description of your responsibilities

Multi-platform UNIX systems administrator and consultant in mutualized
and virtualized environments.  Architecture and expertise building
strong UNIX/Linux experience in large enterprises such as banking and
financial services, IT services, Telecommunication and multimedia
companies.

#### Length of time using Oracle products

For over eleven years I worked with [Solaris] as a system administrator,
but it was [Sun Microsystems] at this time, not yet Oracle.

#### Published author?

I am not a published author, but I participated in writing an article in
GLMF, and blogging publicly on the Internet for many years.

#### If so, title/publication/year

- Title: [GNU Solaris: Introduction]
  * Publication: GNU/Linux Magazine France (GLMF)
  * Year: June 2008
  * Authors: [Yves Mettier], Julien Gabel
  * Participation in the writing of an article on OpenSolaris: brief
history, major distributions, use with a concrete case, and evolution
to come.

- I blogged for more than six years at [https://blog.thilelli.net/]

#### Which version and release of the Oracle database [or for Applications ACEs, which systems] do you currently use?

I work on Solaris systems for more than ten years, from Solaris 2.6 on
SPARC systems to Solaris 11 on both x86 and SPARC platforms.

### Interview questions

#### How did you get started in IT?

I had the chance to discover IT and UNIX system administration when I
was yet at school preparing a Master's degree in Engineering, but this
was very very late in fact, since I was twenty years old at this time!
Since then I always found myself passionate by sysadmin on UNIX
platforms without interruption, greatly helped by the always evolving
technologies, such as virtualization for example.

#### What is your favorite tool or technique on the job?

In general, I tend to encourage work on topics around virtualization, in
particular when based on [Zones] and [ZFS] which both completely
revolutionized and extends the possibilities of such configurations.

Secondly, I am interested by performance and pinpointing the root cause
of a performance penalty problem, or observation.  If [DTrace] is
definitely a invaluable new tool introduced in Solaris 10, better
understand how things works at the operating system level will help
better understand the metric reported by more classical (historical)
tools which can always gives us useful information if we are able to
interpret them properly.

#### If you are exploring the new features in Oracle Database or Oracle Fusion Middleware, please tell us which features you're finding valuable, and why. If you are exploring any new features in an Oracle application, please tell use which features you're finding valuable, and why.

Since Solaris 10, the integration of Zones, Solaris Resource Manager and
ZFS opened new opportunities with very light overhead, and interesting
capabilities for virtualization: dynamic resource capping for CPU and
memory at the non-global zone level, and very flexible use for data
management through the use of ZFS.

With the upcoming release of Solaris 11, [virtual networks] will greatly
extends the Zones experience, while some other new features will help us
at least just as much: for example the new [IPS packaging] technology in
conjunction with boot environments which will make old patching paths a
thing of the past, and automates some new capability such as instantly
cloning an environment when a package require a reboot (a sort of
intelligent automated Live Upgrade).

#### What advice do you have about how to get into Web, database, and/or application development or software architecture?

Well, some essential points came in mind here: be voluntary, be
passionate and interested by what you are doing, educate yourself
(helped by appropriate training, experienced coworkers, etc.), and
keep up-to-date as much as possible!

#### What would you like to see Oracle, as a company or as a technology, do differently, better, or more of?

Well, good question.  Two points here.

As a company, I really liked Oracle to better integrate and cooperate
with [OpenSolaris], Solaris and OpenSource projects and communities.  In
fact, I particularly regrets the fact that Oracle will not deliver more
regularly development releases of the operating system and its
ecosystems (such as Sun did in the past since early 2005) because it
gave the interested system administrators and specialists some insight
of what will arrive next, and is a good opportunity to test new
functionality before they arrived as a finalized product.  I miss this
opportunity where it was possible to test, to report and to follow next
system technologies beforehand, followed by a large community of
enthusiasts participating in the Solaris evolution. I am convinced this
will greatly help to improve Solaris as a target enterprise OS for
Oracle, and get a new and vibrant community as can be found in the Linux
 world.

As a technology, I think Solaris miss a tool such as `nmon` on IBM AIX.
This nifty tool has the ability to provide lots of interesting and
important information live on the systems from one single tool (such as
`sar` but with much more information) but, more interestingly, it has
the capability to collect these information to be used by different
tools to generate graphs afterward. We can always do this ourselves
(self made solutions) or let some expensive enterprise tools do this
job, but I really think this may be a very valuable add-on to Oracle
Solaris.

#### What's your favorite Oracle reference book and why?
*(Put another way, if you were going to the International Space Station
for six months and could only take one Oracle reference book, what would
it be?)*

Without hesitation, my answer is the second book of the latest edition
of Solaris Internals [Solaris Performance and Tools, DTrace and MDB
Techniques for Solaris 10 and OpenSolaris].  This book is a must have
since it is really complete, covering both tools and methodologies for
performance observability and debugging.  It provides a better
understanding and metrics interpretation of the output of some already
known tools up to the latest utilities found in Solaris 10 and Solaris
11.

#### What's your favorite thing to do that doesn't involve work?

Although I am involved in system administration and IT communities even
when I am not at work, I really like driving my [motorcycle]... at least
when the weather permit ;)

#### Do you have a favorite vacation spot?

Not a favorite vacation spot in itself, but I like winter sports,
especially snowboarding.


[Oracle Magazine]: https://blogs.oracle.com/oraclemagazine/
[online]: https://blogs.oracle.com/oraclemagazine/in-with-the-new
[Solaris]: https://www.oracle.com/solaris/
[Sun Microsystems]: https://en.wikipedia.org/wiki/Sun_Microsystems
[GNU Solaris: Introduction]: https://boutique.ed-diamond.com/numeros-deja-parus/97-lm106.html
[Yves Mettier]: http://ymettier.free.fr/
[Zones]: http://www.oracle.com/us/products/servers-storage/solaris/solaris-containers-ds-075585.pdf
[ZFS]: http://www.oracle.com/us/products/servers-storage/solaris/solaris-zfs-ds-067320.pdf
[DTrace]: http://www.oracle.com/us/products/servers-storage/solaris/solaris-dynamic-tracing-ds-067318.pdf
[virtual networks]: http://www.oracle.com/technetwork/server-storage/solaris11/technologies/networkvirtualization-312278.html
[IPS packaging]: http://www.oracle.com/technetwork/server-storage/solaris11/technologies/ips-323421.html
[OpenSolaris]: https://en.wikipedia.org/wiki/OpenSolaris
[Solaris Performance and Tools, DTrace and MDB Techniques for Solaris 10 and OpenSolaris]: http://www.brendangregg.com/books.html
[motorcycle]: https://www.triumphmotorcycles.com/motorcycles/roadsters/speed-triple

