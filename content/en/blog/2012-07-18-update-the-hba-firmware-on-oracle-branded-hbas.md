---
date: "2012-07-18T09:00:09Z"
tags:
- MPxIO
- SAN
- Kernel
- Patch
- Upgrade
title: Update the HBA firmware on Oracle-branded HBAs
---

Updating the `emlxs` driver will no longer automatically update the HBA
firmware on Oracle-branded HBAs. If an HBA firmware update is required
on an Oracle-branded HBA, a WARNING message will be placed in the
`/var/adm/messages` file, such as this one:

    # grep emlx /var/adm/messages
    [...]
    Jul 18 02:37:11 beastie emlxs: [ID 349649 kern.info] [ 1.0340]emlxs0:WARNING:1540: Firmware update required. (A manual HBA reset or link reset (using luxadm or fcadm) is required.)
    Jul 18 02:37:15 beastie emlxs: [ID 349649 kern.info] [ 1.0340]emlxs1:WARNING:1540: Firmware update required. (A manual HBA reset or link reset (using luxadm or fcadm) is required.)
    [...]

If found, this message is stating that the `emlxs` driver has determined
that the firmware kernel component needs to be updated. To perform this
update, execute `luxadm -e forcelip` on Solaris 10 (or a
`fcadm force-lip` on Solaris 11) against each `emlxs` instance that
reports the message. As stated in the documentation:

> This procedure, while disruptive, will ensure that both driver and
> firmware are current. The *force lip* will temporarily disrupt I/O on
> the port. The disruption and firmware upgrade takes approximately
> 30-60 seconds to complete as seen from the example messages below. The
> example shows an update is needed for `emlxs` instance 0 (`emlxs0`)
> and `emlxs` instance 1 (`emlxs1`), which happens to correlate to the
> `c1` and `c2` controllers in this case.

    # fcinfo hba-port
    HBA Port WWN: 10000000c9e43860
            OS Device Name: /dev/cfg/c1
            Manufacturer: Emulex
            Model: LPe12000-S
            Firmware Version: 1.00a12 (U3D1.00A12)
            FCode/BIOS Version: Boot:5.03a0 Fcode:3.01a1
            Serial Number: 0999BT0-1136000725
            Driver Name: emlxs
            Driver Version: 2.60k (2011.03.24.16.45)
            Type: N-port
            State: online
            Supported Speeds: 2Gb 4Gb 8Gb
            Current Speed: 8Gb
            Node WWN: 20000000c9e43860
    HBA Port WWN: 10000000c9e435fe
            OS Device Name: /dev/cfg/c2
            Manufacturer: Emulex
            Model: LPe12000-S
            Firmware Version: 1.00a12 (U3D1.00A12)
            FCode/BIOS Version: Boot:5.03a0 Fcode:3.01a1
            Serial Number: 0999BT0-1136000724
            Driver Name: emlxs
            Driver Version: 2.60k (2011.03.24.16.45)
            Type: N-port
            State: online
            Supported Speeds: 2Gb 4Gb 8Gb
            Current Speed: 8Gb
            Node WWN: 20000000c9e435fe

In order not to interrupt the service, and because MPxIO (native
multipathing I/O) is in use, each `emlxs` instance will be update one
after each other.

    # date
    Wed Jul 18 09:34:11 CEST 2012

    # luxadm -e forcelip /dev/cfg/c1

    # grep emlx /var/adm/messages
    [...]
    Jul 18 09:35:48 beastie emlxs: [ID 349649 kern.info] [ 5.0334]emlxs0: NOTICE: 710: Link down.
    Jul 18 09:35:53 beastie emlxs: [ID 349649 kern.info] [13.02C0]emlxs0: NOTICE: 200: Adapter initialization. (Firmware update needed. Updating. id=67 fw=6)
    Jul 18 09:35:53 beastie emlxs: [ID 349649 kern.info] [ 3.0ECB]emlxs0: NOTICE:1520: Firmware download. (AWC file: KERN: old=1.00a11  new=1.10a8  Update.)
    Jul 18 09:35:53 beastie emlxs: [ID 349649 kern.info] [ 3.0EEB]emlxs0: NOTICE:1520: Firmware download. (DWC file: TEST:             new=1.00a4  Update.)
    Jul 18 09:35:53 beastie emlxs: [ID 349649 kern.info] [ 3.0EFF]emlxs0: NOTICE:1520: Firmware download. (DWC file: STUB: old=1.00a12  new=2.00a3  Update.)
    Jul 18 09:35:53 beastie emlxs: [ID 349649 kern.info] [ 3.0F1D]emlxs0: NOTICE:1520: Firmware download. (DWC file: SLI2: old=1.00a12  new=2.00a3  Update.)
    Jul 18 09:35:53 beastie emlxs: [ID 349649 kern.info] [ 3.0F2C]emlxs0: NOTICE:1520: Firmware download. (DWC file: SLI3: old=1.00a12  new=2.00a3  Update.)
    Jul 18 09:36:01 beastie emlxs: [ID 349649 kern.info] [ 3.0143]emlxs0: NOTICE:1521: Firmware download complete. (Status good.)
    Jul 18 09:36:06 beastie emlxs: [ID 349649 kern.info] [ 5.055E]emlxs0: NOTICE: 720: Link up. (8Gb, fabric, initiator)

    # date
    Wed Jul 18 09:39:51 CEST 2012

    # luxadm -e forcelip /dev/cfg/c2

    # grep emlx /var/adm/messages
    [...]
    Jul 18 09:41:35 beastie emlxs: [ID 349649 kern.info] [ 5.0334]emlxs1: NOTICE: 710: Link down.
    Jul 18 09:41:40 beastie emlxs: [ID 349649 kern.info] [13.02C0]emlxs1: NOTICE: 200: Adapter initialization. (Firmware update needed. Updating. id=67 fw=6)
    Jul 18 09:41:40 beastie emlxs: [ID 349649 kern.info] [ 3.0ECB]emlxs1: NOTICE:1520: Firmware download. (AWC file: KERN: old=1.00a11  new=1.10a8  Update.)
    Jul 18 09:41:40 beastie emlxs: [ID 349649 kern.info] [ 3.0EEB]emlxs1: NOTICE:1520: Firmware download. (DWC file: TEST:             new=1.00a4  Update.)
    Jul 18 09:41:40 beastie emlxs: [ID 349649 kern.info] [ 3.0EFF]emlxs1: NOTICE:1520: Firmware download. (DWC file: STUB: old=1.00a12  new=2.00a3  Update.)
    Jul 18 09:41:40 beastie emlxs: [ID 349649 kern.info] [ 3.0F1D]emlxs1: NOTICE:1520: Firmware download. (DWC file: SLI2: old=1.00a12  new=2.00a3  Update.)
    Jul 18 09:41:40 beastie emlxs: [ID 349649 kern.info] [ 3.0F2C]emlxs1: NOTICE:1520: Firmware download. (DWC file: SLI3: old=1.00a12  new=2.00a3  Update.)
    Jul 18 09:41:48 beastie emlxs: [ID 349649 kern.info] [ 3.0143]emlxs1: NOTICE:1521: Firmware download complete. (Status good.)
    Jul 18 09:41:53 beastie emlxs: [ID 349649 kern.info] [ 5.055E]emlxs1: NOTICE: 720: Link up. (8Gb, fabric, initiator)

That\'s it. Lastly, the documentation says:

> At this point, the firmware upgrade is complete as indicated by the
> "Status good" message above. A reboot is not strictly necessary to
> begin using the new firmware. But the `fcinfo hba-port` command may
> still report the old firmware version. This is only a reporting defect
> that does not affect firmware operation and will be corrected in a
> later version of `fcinfo`. To correct the version shown by `fcinfo`, a
> second reboot is necessary. On systems capable of DR, you can perform
> dynamic reconfiguration on the HBA (via
> `cfgadm unconfigure/configure`) instead of rebooting.

For my part, I tried to unconfigure/configure each `emlxs` instance
using `cfgadm` without a reboot, but this didn\'t work as expected on
Solaris 10. The `fcinfo` utility still report the old firmware version,
seems until the next reboot.
