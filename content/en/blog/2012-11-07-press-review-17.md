---
date: "2012-11-07T15:00:32Z"
tags:
- Press
title: 'Press Review #17'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### OS Analytics - Deep Dive Into Your OS

-   <https://blogs.oracle.com/oem/entry/enterprise_manager_ops_center_using>

Oracle Enterprise Manager Ops Center provides a feature called \"OS
Analytics\". This feature allows you to get a better understanding of
how the Operating System is being utilized. You can research the
historical usage as well as real time data. This post will show how you
can benefit from OS Analytics and how it works behind the scenes.

### Solaris 11.1: Changes to included FOSS packages

-   <https://blogs.oracle.com/alanc/entry/solaris_11_1_changes_to>

1.  Solaris 11 can update more readily than Solaris 10
2.  Core OS Functionality
3.  Developer Stack
4.  Desktop Stack
5.  Detailed list of changes

### debugging IO calls in Solaris 11 with kstat

-   <http://portrix-systems.de/blog/brost/debugging-io-calls-in-solaris-11-with-kstat/>

Solaris 11 update 1 was released just a few days ago and I have been
curious to find out about new features. One of the things mentioned in
the What's-New guide that caught my attention were "File System
Statistics for Oracle Solaris Zones". Until now, there was no way to
tell which zone was responsible for how much IO. \[\...\] This is where
these new kstat statistics for each filesystem type in each zone come in
handy.

### My splitvg Went Splat (updated)

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/AIXDownUnder/entry/splitting_a_mirror_can_bring_bad_luck7>

A couple of years ago I wrote about the splitvg command. This takes a
volume group (VG) that has been mirrored using mirrorvg, and effectively
breaks off one copy of the mirror, turning it into a point-in-time
snapshot. The snapshot can then be used for reports, backups or for
building a new database. I\'m not sure how much this is used in the real
world these days, but if you are still doing your backups this way,
perhaps it\'s time to revisit the strategy.

### Can Oracle REALLY Increase Throughput by 6x?

-   <http://netmgt.blogspot.fr/2012/11/can-oracle-really-increase-throughput.html>

During some SPARC road map discussions, a particular anonymous IBM POWER
enthusiast inquires:

> \"How\... are 192 S3 cores going to provide x6 throughput of 128
> SPARC64-VII+ cores?\"

### Unexpected advantage of Engineered Systems

-   <https://blogs.oracle.com/Solaris11Life/entry/unexpected_advantage_of_engineered_systems>

It\'s not surprising that Engineered Systems accelerate the debugging
and resolution of customer issues. But what has surprised me is just how
much faster issue resolution is with Engineered Systems such as SPARC
SuperCluster. These are powerful, complex, systems used by customers
wanting extreme database performance, app performance, and cost saving
server consolidation.

### What\'s up with LDoms: Part 5 - A few Words about Consoles

-   <https://blogs.oracle.com/cmt/entry/what_s_up_with_ldoms5>

Back again to look at a detail of LDom configuration that is often
forgotten - the virtual console server.

### 25 Years of SPARC, 20 Years of Solaris

-   <http://www.youtube.com/watch?v=IKB9zV8TXuQ>
-   <http://www.youtube.com/watch?v=IpYkPI2REWo>

### Faster Memory Allocation Using vmtasks

-   <https://blogs.oracle.com/sistare/entry/faster_memory_allocation_using_vmtasks>

What is vmtasks, and why should you care? In a nutshell, vmtasks
accelerates creation, locking, and destruction of pages in shared memory
segments.

### Introducing RedPatch

-   <https://blogs.oracle.com/ksplice/entry/introducing_redpatch>

The Ksplice team is happy to announce the public availability of one of
our git repositories, RedPatch. RedPatch contains the source for all of
the changes Red Hat makes to their kernel, one commit per fix and we\'ve
published it on oss.oracle.com/git.

### Patching a miniroot image (astuce)

-   <http://www.gloumps.org/article-patching-a-miniroot-image-astuce-112419363.html>

Petite astuce pour la mise à jour kernel d\'un miniroot Solaris 10. Mais
avant de commencer, parlons un peu du contexte. Lors d\'une migration
p2v d\'un serveur Sparc vers une Ldom, j\'ai rencontré l\'erreur
suivante lors de l\'extraction de l\'archive flar.

### Oracle Solaris: Zones on Shared Storage

-   <https://blogs.oracle.com/JeffV/entry/oracle_solaris_zones_on_shared>

One of the significant new features, and the most significant new
feature related to Oracle Solaris Zones, is casually called \"Zones on
Shared Storage\" or simply ZOSS (rhymes with \"moss\"). ZOSS offers much
more flexibility because you can store Solaris Zones on shared storage
(surprise!) so that you can perform quick and easy migration of a zone
from one system to another. This blog entry describes and demonstrates
the use of ZOSS.

### IBM POWER 7+ Better Late than Never?

-   <http://netmgt.blogspot.fr/2012/11/ibm-power-roadmap-power-7-better-late.html>

Just announced, via TPM at The Register, is the ability to provision an
IBM p260 with new POWER 7+ processors!

### Automatic Storage Tiering

-   <http://netmgt.blogspot.fr/2012/11/automatic-storage-tiering.html>

Automatic Storage Tiering or Hierarchical Storage Management is the
process of placing the most data onto storage which is most cost
effective, while meeting basic accessibility and efficient requirements.
There has been much movement over the past half-decade in storage
management.

### Looking \"Under the Hood\" at Networking in Oracle VM Server for x86

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/networking-ovm-x86-1873548.html>

Normally, you configure Oracle VM Server for x86 networking with the
Oracle VM Manager GUI. The GUI simplifies administration, speeds
deployment, and reduces the chance of configuration errors. Hiding the
implementation details using the GUI has its benefits, but here on the
Oracle Technology Network, we like to expose the heart of the machine;
it\'s not only interesting, but it can help you troubleshoot problems
that might arise.

### Oh my god, it\'s full of threads \... and out of memory

-   <http://www.c0t0d0s0.org/archives/7515-Oh-my-god,-its-full-of-threads-...-and-out-of-memory.html>

This is for the people with the really large systems (however thread
wise a T4-2 or T4-1 can be really large systems). Imagine you have
dozens to hundreds of zones. All with thousands of threads. Or you have
an extreme number of ZFS pools \... with all their zpool processes and a
lot of zones with a lot of processes.

### Solaris x86 sur serveur Dell (gamme PowerEdge Rxx0)

-   <http://www.gloumps.org/article-solaris-x86-sur-serveur-dell-gamme-poweredge-rxx0-112522509.html>

Petit article sur la configuration du déport Console Série et des
interruptions NMI sur les serveurs Dell (gamme PowerEdge Rxx0).

### VIOS SEA Failover flapping on backup SEA

-   <http://unixadmin.free.fr/?p=389>

Why is the backup SEA adapter of my SEA failover flapping from Primary
to Backup repeatedly?

### Consommation CPU & Gestion mémoire (1er partie)

-   <http://www.gloumps.org/article-consommation-cpu-gestion-memoire-1er-partie-112661908.html>

Première partie d\'une analyse effectuée sur un système Solaris 10
exécutant plusieurs bases de données Oracle. Les symptômes remontés par
l\'équipe DBA sont les suivants : temps de réponse long sur plusieurs
bases.

### Logical Domains Manager (v 3.0.0.0.28)

-   <http://docs.oracle.com/cd/E26502_01/>

Oracle have release Oracle VM for SPARC 3.0 (LDOM).
