---
date: "2007-04-27T20:52:02Z"
tags:
- Cobol
title: Installation of Micro Focus Server Express 64-bit Edition
---

### Installation of the Server Express Development System product

#### sh ./setup.sh

Mount the installation media:

    # mkdir /cdrom
    # mount -o ro -v cdrfs /dev/cd0 /cdrom

Run the setup script:

    # cd /cdrom
    # sh ./setup.sh
    /* 
     * a) Answer y to the license agreement
     * b) Select 5 "IBM RS6000    running AIX 5.1/5.2/5.3                   32/64-bit"
     * c) Answer y to confirm this choice
     * d) Select 1 "Server Express Development System - 32/64-bit - PRN=RXCAK/AAL:9i.T4.40.02"
     * e) Answer y to confirm this choice
     * f) Select /opt/microfocus/cobol as the default directory name for this product
     * g) Answer y to create it
     * h) Answer y to continue
     * i) Answer return to list the environmental settings
     * j) Answer y to confirm this choice
     * k) Answer y to "Do you want to make use of COBOL and Java working together?"
     * l) Select 6 "1.4.2 64-bit"
     * m) Answer y to install Micro Focus LMF
     * n) Answer y to keep the default directory name for License Manager (/opt/microfocus/mflmf)
     * o) Answer y to confirm this choice
     * p) Answer y to "Do you want only superuser to be able to access the License Admin System?"
     * q) Answer y to "Do you want license manager to be automatically started at boot time?"
     * r) Answer 64 to "Please enter either 32 or 64 to set the system default mode"
     * s) Answer n to "Do you want to install the Enterprise Server Development System - J2EE, Web Services, etc.?"
     * t) Answer y to "Would you like to install any LMF licenses now?"
     * u) Select I to install the License Manager
     * v) Enter the Serial Number part of the License Key...
     * w) Enter the License Number part of the License Key...
     */

Unmount the installation media:

    # cd
    # umount /cdrom
    # rmdir /cdrom

### Post-installation phase

Ensure that the environment is set correctly to point the newly
installed product:

    COBDIR=/opt/microfocus/cobol
    PATH=${COBDIR}/bin:${PATH}
    LIBPATH=/usr/lib:${COBDIR}/lib:${LIBPATH}
    export COBDIR PATH LIBPATH

### Licensing

Note that the Server Express come with 5 run-time builtin licensing
facilities. If more licenses are required (or if a standalone
Application Server is necessary), you can set them as follow:

#### Server Express

    # cd /opt/microfocus/mflmf
    # stty -a | egrep "col|row"
    speed 38400 baud; 24 rows; 80 columns   /* A terminal of 80x25 or 80x24 is MANDATORY!  Be warned. */
    # TERM=vt220 ./mflmadm

#### Application Server

    # cd /opt/microfocus/cobol/aslmf
    # ./apptrack

#### License Manager

Verify that it is automatically loaded at boot time:

    # lsitab mF        
    mF:2345:wait:sh /etc/mflmrcscript > /dev/null 2>&1

*Note: It can be monitored using the `lmfgetpv` tool found under
`/opt/microfocus/mflmf`.*

Please refer to the Micro Focus
[website](http://supportline.microfocus.com/productreleaselevels/unix.asp "Current Micro Focus Product Release Levels")
for more information on these products.
