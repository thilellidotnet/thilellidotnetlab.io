---
date: "2007-04-27T20:29:01Z"
tags:
- NFS
- Network
title: How to Set Up the NFS Server, for the Use of the Automounter Capability
---
--- /var/yp/GNUmakefile.2005-08-25      Thu Aug 25 13:18:31 2005
+++ /var/yp/GNUmakefile Thu Aug 25 14:30:04 2005
@@ -76,6 +76,7 @@
 AUTONT=auto_nt
 AUTOALT=auto_alt
 AUTODIRECT=auto_direct
+AUTOAPPS=auto_apps
 else
 AUTOMASTER=auto.master
 AUTOHOME=auto.home
@@ -83,6 +84,7 @@
 AUTONT=auto.nt
 AUTOALT=auto.alt
 AUTODIRECT=auto.direct
+AUTOAPPS=auto.apps
 endif
 GNUGREP=/usr/local/bin/ggrep
 MALIASES=$(YPDBDIR)/$(DOM)/mail.aliases
@@ -96,7 +98,7 @@
 
 all:: passwd group hosts ethers networks rpc services protocols \
        netgroup bootparams aliases publickey netid netmasks c2secure \
-       timezone auto.master auto.home auto.soft auto.nt auto.alt auto.direct \
+       timezone auto.master auto.home auto.soft auto.nt auto.alt auto.apps auto.direct \
        printcap dmmo printers.conf virtualip
 
 diams: dmmo_applis dmmo_sites dmmo_users ypservers
@@ -424,6 +426,22 @@
                echo &quot;couldn't find $(DIR)/auto.alt&quot;; \
        fi
 
+auto.apps.time:  $(DIR)/$(AUTOAPPS)
+       -@if [ -f $(DIR)/$(AUTOAPPS) ]; then \
+               sed -e &quot;/^#/d&quot; -e s/#.*$$// $(DIR)/$(AUTOAPPS) \
+               | $(MAKEDBM) - $(YPDBDIR)/$(DOM)/auto.apps; \
+               touch auto.apps.time; \
+               echo &quot;updated auto.apps&quot;; \
+               if [ ! $(NOPUSH) ]; then \
+                       $(YPPUSH) auto.apps; \
+                       echo &quot;pushed auto.apps&quot;; \
+               else \
+               : ; \
+               fi \
+       else \
+               echo &quot;couldn't find $(DIR)/auto.apps&quot;; \
+       fi
+
 printcap.time: $(DIR)/ypprintcap
        -@if [ -f $(DIR)/yp$(@:.time=) ]; then \
                sed &lt; $(DIR)/yp$(@:.time=) \
@@ -686,6 +704,7 @@
 auto.soft: auto.soft.time
 auto.nt: auto.nt.time
 auto.alt: auto.alt.time
+auto.apps: auto.apps.time
 auto.direct: auto.direct.time
 printcap: printcap.time
 printers.conf: printers.conf.time
@@ -710,6 +729,7 @@
 $(DIR)/$(AUTOSOFT):
 $(DIR)/$(AUTONT):
 $(DIR)/$(AUTOALT):
+$(DIR)/$(AUTOAPPS):
 $(DIR)/$(AUTODIRECT):
 $(DIR)/ypprintcap:
 $(DIR)/printers.conf:
</pre>
<p>At this stage, the new map must be first manually transferred on the slave
NIS server.</p>
<p>Create the corresponding map files (<code>*.dir</code> and
<code>*.pag</code> ) only, using:</p>
<pre>
# cd /etc/ivyp
# gmake -C /var/yp NOPUSH=&quot;YES&quot;
</pre>
<p>Log in on the slave server (a Sun system), for example <code>kitchen</code>
:</p>
<pre>
# /usr/lib/netsvc/yp/ypxfr -h soul auto.apps
# ls -l /var/yp/`domainname`/auto.apps.*
-rw-r--r--   1 root     root           0 Aug 25 18:10 /var/yp/`domainname`/auto.apps.dir
-rw-r--r--   1 root     root        1024 Aug 25 18:10 /var/yp/`domainname`/auto.apps.pag
</pre>
<p><em>Note:</em> You can know which hostname is(are) currently set up as slave
server(s) using:</p>
<pre>
# cd /var/yp/`domainname`
# makedbm -u ypservers
YP_LAST_MODIFIED 1072719185
YP_MASTER_NAME soul
kitchen
timal
</pre>
<p>Then, on the master server, <strong>really</strong> push the map this
time:</p>
<pre>
# cd /etc/ivyp
# gmake -C /var/yp
</pre>
<p>Now, the new mount point and sub-tree must be ready and available to all
your <strong>properly</strong> configured clients, as found in the following
online documents:</p>
<ol>
<li><a href="/post/2005/06/18/Use-the-NIS-and-NFS-Infrastructure-on-AIX-5X">Use
the NIS and NFS Infrastructure on AIX 5L</a></li>
<li><a href="/post/2005/08/31/Use-the-NIS-and-NFS-Infrastructure-on-Red-Hat-Advanced-Server-21">
Use the NIS and NFS Infrastructure on Red Hat Advanced Server 2.1</a></li>
</ol>

1.  *soul* is the hostname of the master NIS server for a given domain
2.  *kitchen* is the hostname of the slave NIS server for a given domain
3.  *bento* is the hostname of the NFS server
4.  *nfs* is an alias (`CNAME`) for the NFS server

### NFS configuration, logged on *bento*

Just add the wanted share in the export list on server `bento`. On Sun
Solaris, it is done as follow:

    # cat << EOF >> /etc/dfs/dfstab
    share -F nfs -o rw -d "Applications repository, NFS shared" /export/bento/apps
    EOF

*The file system `/export/bento/apps` must already be available.*

Start the `nfsd` service (and associated daemons: `mountd`, `statd` and
`lockd`) if necessary:

    # /etc/init.d/nfs.server start

Or simply export the new share:

    # shareall

### NIS configuration, logged on *soul*

Because all of the automounted NFS clients (in particular Solaris, AIX
and GNU/Linux) know how to use `automount` maps shared via the NIS
protocol, the centralized `auto_apps` (`SYSV` style) map resides on the
master NIS server, `soul`.

Since this is a new map to add, there is a little more to do here.

Create the map in the `DIR` (in NIS parlance) directory:

    # touch /etc/ivyp/auto_apps
    # ci -i /etc/ivyp/auto_apps
    /*
     * Description message: "Automounter map for the applications repository,
     * NFS shared.".
     */
    # co -l /etc/yp/auto_apps
    # cat << EOF > /etc/ivyp/auto_apps
    * -rw,hard,bg,intr nfs:/export/bento/apps/${OSNAME}/${CPU}/${NATISA}/${OSREL}/?
    EOF
    # ci -u /etc/ivyp/auto_apps

*The `RCS` commands may (and must) be used through the `ivv.sh` wrapper
script.*

Teach the customized NIS infrastructure (based on GNU tools, not the
native ones) about the new map:

    # diff -u /var/yp/GNUmakefile.2005-08-25 /var/yp/GNUmakefile
