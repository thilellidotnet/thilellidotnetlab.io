---
date: "2007-04-27T21:20:42Z"
tags:
- SSH
- Service
title: How to Add a New "sshd_adm" Service on Red Hat Advanced Server 2.1
---
--- /etc/init.d/sshd    Thu Jun  9 16:54:18 2005
+++ /etc/init.d/sshd_adm        Thu Jun  9 14:03:24 2005
@@ -5,29 +5,30 @@
 # chkconfig: 2345 55 25
 # description: OpenSSH server daemon
 #
-# processname: sshd
+# processname: sshd_adm
 # config: /etc/ssh/ssh_host_key
 # config: /etc/ssh/ssh_host_key.pub
 # config: /etc/ssh/ssh_random_seed
-# config: /etc/ssh/sshd_config
-# pidfile: /var/run/sshd.pid
+# config: /etc/ssh/sshd_config_adm
+# pidfile: /var/run/sshd_adm.pid
 
 # source function library
 . /etc/rc.d/init.d/functions
 
 # pull in sysconfig settings
-[ -f /etc/sysconfig/sshd ] &amp;&amp; . /etc/sysconfig/sshd
+[ -f /etc/sysconfig/sshd_adm ] &amp;&amp; . /etc/sysconfig/sshd_adm
 
 RETVAL=0
-prog=&quot;sshd&quot;
+prog=&quot;sshd_adm&quot;
 
 # Some functions to make the below more readable
 KEYGEN=/usr/bin/ssh-keygen
-SSHD=/usr/sbin/sshd
+SSHD=/usr/sbin/sshd_adm
 RSA1_KEY=/etc/ssh/ssh_host_key
 RSA_KEY=/etc/ssh/ssh_host_rsa_key
 DSA_KEY=/etc/ssh/ssh_host_dsa_key
-PID_FILE=/var/run/sshd.pid
+PID_FILE=/var/run/sshd_adm.pid
+OPTIONS=&quot;-f /etc/ssh/sshd_config_adm -o \&quot;PidFile ${PID_FILE}\&quot; ${OPTIONS}&quot;
 
 do_rsa1_keygen() {
        if [ ! -s $RSA1_KEY ]; then
@@ -97,7 +98,7 @@
        echo -n $&quot;Starting $prog:&quot;
        initlog -c &quot;$SSHD $OPTIONS&quot; &amp;&amp; success || failure
        RETVAL=$?
-       [ &quot;$RETVAL&quot; = 0 ] &amp;&amp; touch /var/lock/subsys/sshd
+       [ &quot;$RETVAL&quot; = 0 ] &amp;&amp; touch /var/lock/subsys/sshd_adm
        echo
 }
 
@@ -106,7 +107,7 @@
        echo -n $&quot;Stopping $prog:&quot;
        killproc $SSHD -TERM
        RETVAL=$?
-       [ &quot;$RETVAL&quot; = 0 ] &amp;&amp; rm -f /var/lock/subsys/sshd
+       [ &quot;$RETVAL&quot; = 0 ] &amp;&amp; rm -f /var/lock/subsys/sshd_adm
        echo
 }
 
@@ -133,7 +134,7 @@
                reload
                ;;
        condrestart)
-               if [ -f /var/lock/subsys/sshd ] ; then
+               if [ -f /var/lock/subsys/sshd_adm ] ; then
                        do_restart_sanity_check
                        if [ &quot;$RETVAL&quot; = 0 ] ; then
                                stop
</pre>
<p><em>See the provided attached patch to get it right now:</em> <a title="sshd_adm.service.patch patch file" href="/blog/sshd_adm.service.patch" lang="en">sshd_adm.service.patch</a></p>
<p>Create (copy) the sshd_adm configuration file:</p>
<pre>
# cp /etc/ssh/sshd_config /etc/ssh/sshd_config_adm
</pre>
<p>Edit and modify the sshd <strong>and</strong> sshd_adm configuration files
according to:</p>
<pre>
# diff -u /etc/ssh/sshd_config /etc/ssh/sshd_config_adm
--- /etc/ssh/sshd_config        Thu Jun  9 15:44:42 2005

1.  sshd represents the *classical* SSH server (for users)
2.  sshd\_adm represents the *administrator* SSH server (for
    administrators)

Copy the original executable to be able to differentiate them later
using `ps(1)`:

    # cp /usr/sbin/sshd /usr/sbin/sshd_adm

Create (copy) the sshd\_adm service file:

    # cp /etc/init.d/sshd /etc/init.d/sshd_adm

Edit and modify the sshd\_adm service file according to:

    # diff -u /etc/init.d/sshd /etc/init.d/sshd_adm
    +++ /etc/ssh/sshd_config_adm    Thu Jun  9 15:45:08 2005
    @@ -11,6 +11,7 @@
     # default value.
     
     #Port 22
    +Port 77
     #Protocol 2,1
     #ListenAddress 0.0.0.0
     #ListenAddress ::
    @@ -35,13 +36,11 @@
     
     #LoginGraceTime 600
     #PermitRootLogin yes
    -PermitRootLogin no
     #StrictModes yes
     
     #RSAAuthentication yes
     #PubkeyAuthentication yes
     #AuthorizedKeysFile    .ssh/authorized_keys
    -AuthorizedKeysFile     /var/.ssh/%u/authorized_keys
     
     # rhosts authentication should not be used
     #RhostsAuthentication no
    @@ -79,6 +78,7 @@
     #PAMAuthenticationViaKbdInt yes
     
     #X11Forwarding no
    +X11Forwarding yes
     #X11DisplayOffset 10
     #X11UseLocalhost yes
     #PrintMotd yes

*See the provided attached patch to get it right now:*
[sshd\_adm.configuration.patch](/blog/sshd_adm.configuration.patch "sshd_adm.configuration.patch patch file")

Clone the `pam(8)` configuration for the sshd\_adm processus:

    # cp /etc/pam.d/sshd /etc/pam.d/sshd_adm

Add and/or update the wanted run-level informations for the sshd\_adm
service:

    # chkconfig --add sshd_adm
    # chkconfig --level 2345 sshd_adm on

Start, or restart, the corresponding services using the new
configurations:

    # service sshd restart
    # service sshd_adm start

Verify the running services:

    # service sshd status
    # service sshd_adm status
