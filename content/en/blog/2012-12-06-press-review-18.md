---
date: "2012-12-06T13:17:42Z"
tags:
- Press
title: 'Press Review #18'
---

Here is a little press review mostly around Oracle technologies and
Solaris in particular, and a little lot more:

### Best Practices for Building a Virtualized SPARC Computing Environment

-   <http://www.oracle.com/technetwork/oem/host-server-mgmt/hghlyavailldomserverpoolsemoc12cv09-1845483.pdf>

This best practices guide provides a solution for a SPARC virtualized
environment that hosts general computing workloads.

### The Role of Oracle Solaris Zones and Linux Containers in a Virtualization Strategy

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/zones-containers-virtualization-1880908.html>

In the previous two articles of this series, we covered Oracle VM Server
for SPARC and Oracle VM Server for x86, which provide hypervisor-based
hardware virtualization. Now, we will cover the operating system level
of virtualization, which is one type of software virtualization, by
looking at Oracle Solaris Zones and Linux Containers.

### Using rrdtool to graph vmstat output - a worked example

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/using_rrdtool_to_graph_vmstat_output_a_worked_example>

rrdtool is a fantastically brilliant command to have in your toolbox. Up
there with awk, grep, sed, Apache, ksh, and nmon (of course). It is used
to save data in a fixed size \"database\", does cascade summation of
older data to keep the data volume down, it can extract the data across
any period and then it can quickly generate impressive .gif file graphs
from the data - which are perfect for displaying on a webserver.

### Compiling for T4

-   <https://blogs.oracle.com/d/entry/compiling_for_t4>

I\'ve recently had quite a few queries about compiling for T4 based
systems. So it\'s probably a good time to review what I consider to be
the best practices.

### Solaris 11 pkg fix is my new friend

-   <https://blogs.oracle.com/bobn/entry/solaris11_pkg_fix_is_my>

While putting together some examples of the Solaris 11 Automated
Installer (AI), I managed to really mess up my system, to the point
where AI was completely unusable. This was my fault as a combination of
unfortunate incidents left some remnants that were causing problems, so
I tried to clean things up. Unsuccessfully. Perhaps that was a bad idea
(OK, it was a terrible idea), but this is Solaris 11 and there are a few
more tricks in the sysadmin toolbox.

### ZFS fundamentals: transaction groups

-   <http://dtrace.org/blogs/ahl/2012/12/13/zfs-fundamentals-transaction-groups/>

I've continued to explore ZFS as I try to understand performance
pathologies, and improve performance. A particular point of interest has
been the ZFS write throttle, the mechanism ZFS uses to avoid filling all
of system memory with modified data.

### Local, Near, Far part 12 - I have a 10 core POWER7 chip, eh!

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/local_near_far_part_12_i_have_a_10_core_power7_chip_eh68>

Previously I said the POWER Hypervisor decides where to put a Virtual
Machine (VM/LPAR) based on the Virtual Processor number (spreading
factor). Well, apart from it nearly being right \... I was actually
wrong! I got talking to one of these very impressive Hypervisor
developers in Germany and he put me right.

### USENIX LISA 2012: Performance Analysis Methodology

-   <http://dtrace.org/blogs/brendan/2012/12/13/usenix-lisa-2012-performance-analysis-methodology/>

At USENIX LISA 2012, I gave a talk titled Performance Analysis
Methodology. This covered ten performance analysis anti-methodologies
and methodologies, including the USE Method. I wrote about these in the
ACMQ article Thinking Methodically about Performance, which is worth
reading for more detail. I've also posted USE Method-derived checklists
for Solaris- and Linux-based systems.

### Shared Storage Pools 3 and Systems Director 6.3.2

-   <https://www.ibm.com/developerworks/mydeveloperworks/blogs/aixpert/entry/shared_storage_pools_3_and_systems_director_6_3_297>

The new VIOS Shared Storage Pools 3 (SSP3) arrived a month ago and I
have all my VIOS updated now to VIOS 2.2.2.1 plus fixes. This brings
with it the regular VIOS improvements but also new features for SSP3
including\...

### Oracle affiche de forts bénéfices et loue l\'apport de Sun Microsystems

-   <http://www.pcinpact.com/news/76196-oracle-affiche-forts-benefices-et-loue-apport-sun-microsystems.htm>

Concernant Sun Microsystems, racheté par Oracle il y a trois ans pour
7,4 milliards de dollars, Larry Ellison, le PDG d\'Oracle, estime que «
Sun s\'est avéré être l\'une des acquisitions les plus stratégiques et
rentables que nous n\'ayons jamais fait ». Une affirmation basée sur la
forte rentabilité d\'Oracle sur les systèmes d\'ingénierie et sur
l\'impact de Sun sur la croissance de ses activités matérielles, qui
perdent pourtant en chiffre d\'affaires.

### Ops Center Solaris 11 IPS Repository Management: Using ISO Images

-   <https://blogs.oracle.com/oem/entry/using_an_iso_image_to>

With Enterprise Manager Ops Center 12c, you can provision, patch,
monitor and manage Oracle Solaris 11 instances. To do this, Ops Center
creates and maintains a Solaris 11 Image Packaging System (IPS)
repository on the Enterprise Controller. During the Enterprise
Controller configuration, you can load repository content directly from
Oracle\'s Support Web site and subsequently synchronize the repository
as new content becomes available.

Of course, you can also use Solaris 11 ISO images to create and update
your Ops Center repository.

### Using Oracle Enterprise Manager Ops Center to Update Solaris via Live Upgrade

-   <https://blogs.oracle.com/oem/entry/using_ops_center_to_update>

This Oracle Enterprise Manager Ops Center blog entry provides tips for
using Ops Center to update Solaris using Live Upgrade on Solaris 10 and
Boot Environments on Solaris 11.

### Locating WWPNs on Linux servers

-   <http://prefetch.net/blog/index.php/2012/12/19/locating-wwpns-on-linux-servers/>

I do a lot of storage-related work, and often times need to grab WWPNs
to zone hosts and to mask storage. To gather the WWPNs I would often
times use the following script on my RHEL and CentOS servers\...

### The USE Method: SmartOS Performance Checklist

-   <http://dtrace.org/blogs/brendan/2012/12/19/the-use-method-smartos-performance-checklist/>

The USE Method provides a strategy for performing a complete check of
system health, identifying common bottlenecks and errors. For each
system resource, metrics for utilization, saturation and errors are
identified and checked. Any issues discovered are then investigated
using further strategies.

In this post, I'll provide an example of a USE-based metric list for use
within a SmartOS SmartMachine (Zone), such as those provided by the
Joyent Public Cloud.

### Resolving Duplicate disk/device entries in "vxdisk list" or vxdisksetup

-   <http://nilesh-joshi.blogspot.com/2012/09/resolving-duplicate-diskdevice-entries.html>

One fine morning I had a undertaking to replace the disk which was part
of VxVM. Easy enough -- just another routine stuff \[\...\] Everything
you do or don't do has an inherent risk !!!

### Weird issue with VERITAS after replacing the disk

-   <http://nilesh-joshi.blogspot.com/2012/10/weird-issue-with-veritas-after.html>

When performing a disk replacement in VxVM 4.1 and VxVM 5.0, the disk
being replaced does not show up in the output of a \"vxdisk list\"
command. Instead a \"NONAMEs2\" entry is seen\...

### Announcement: DTrace for Oracle Linux General Availability

-   <https://blogs.oracle.com/linux/entry/announcement_dtrace_for_oracle_linux>

Today we are announcing the general availability of DTrace for Oracle
Linux. It is available to download from ULN for Oracle Linux Support
customers.

### \"getent hosts\" on IPv4/IPv6. Linux vs. Solaris

-   <https://blogs.oracle.com/LetTheSunShineIn/entry/getent_hosts_on_ipv4_ipv6>

\"getent hosts (server name)\" is typically run when one wants to check
whether /etc/nsswitch.conf is correctly set up. I recently found
differences between Linux and Solaris when it tries to find IPv4/IPv6
addresses.

### What\'s up with LDoms: Part 6 - Sizing the IO Domain

-   <https://blogs.oracle.com/cmt/entry/what_s_up_with_ldoms6>

By now, we\'ve seen how to create the basic setup, create a simple
domain and configure networking and disk IO. We know that for typical
virtual IO, we use vswitches and virtual disk services to provide
virtual network and disk services to the guests. The question to address
here is: How much CPU and memory is required in the Control and
IO-domain (or in any additional IO domain) to provide these services
without being a bottleneck?

### zfsday: ZFS Performance Analysis and Tools

-   <http://dtrace.org/blogs/brendan/2012/12/29/zfsday-zfs-performance-analysis-and-tools/>

At zfsday 2012, I gave a talk on ZFS performance analysis and tools,
discussing the role of old and new observability tools for investigating
ZFS, including many based on DTrace. This was a fun talk -- probably my
best so far -- spanning performance analysis from the application level
down through the kernel and to the storage device level.

My background with ZFS includes leading various performance work for the
world's first ZFS-based storage appliance at Sun Microsystems and later
Oracle, and now further analysis and tuning as Joyent's lead performance
engineer where we run a public cloud on ZFS. Given the risk of other
tenants (noisy neighbors) interfering with your performance, I can't
imagine running a cloud on anything else. This talk includes the tools
and tuning we use to make sure ZFS runs smoothly.
