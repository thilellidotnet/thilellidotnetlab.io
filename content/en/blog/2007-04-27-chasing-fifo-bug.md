---
date: "2007-04-27T19:48:56Z"
tags:
- Bug
- Crash
title: Chasing FIFO Bug
---

When testing RELENG\_6 (for weeks now), i encountered a strange bug
running an UP and a SMP kernel under *heavy* parallel load, as when
building the world using the `-j` flag. The symptom was simply a panic,
no more no less.

After posting about it on
[current@](http://lists.freebsd.org/pipermail/freebsd-current/2005-September/055436.html "The freebsd-current Archives"),
i received good help from Robert N M Watson himself asking for crash
dump and testing. After some work on his side, he came with a lot of
work on the FreeBSD\'s FIFO implementation, now
[committed](http://www.freebsd.org/cgi/cvsweb.cgi/src/sys/fs/fifofs/fifo_vnops.c "CVS log for src/sys/fs/fifofs/fifo_vnops.c")
to the source tree.

As a side note, it be mentioned that these modifications seems to solve
the problem even on the UP machine, but Robert said that this one may
had another origin and may not be totally solved. Here is an excerpt of
one of his reply:

> This is actually interesting \-- Kris Kenneway reported the same thing
> \-- that is, that with the most recent spate of FIFO bug fixes, the
> problem has gone away. The only problem is that I don\'t think I fixed
> a bug with the symptoms that you have experienced, which suggests
> instead that I\'ve changed the timing of the bug so that it occurs
> less rarely. I sent Kris a set of assertion patches to run with, and
> he generates some nice parallel make load, and I have a regression
> test I\'ve been running. I think we\'re set for now and I\'ll continue
> to work on reproducing it after my recent fifo cleanup. It is possible
> I fixed a race condition without meaning to, of course\... Please let
> me know if you have any further FIFO panics! Thanks again, Robert N M
> Watson

I just can say that panic doesn\'t occur since then. Thanks to him.
