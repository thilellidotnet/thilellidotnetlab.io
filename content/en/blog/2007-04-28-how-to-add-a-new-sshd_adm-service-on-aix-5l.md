---
date: "2007-04-28T09:56:29Z"
tags:
- SSH
- Service
title: How to Add a New "sshd_adm" Service on AIX 5L
---
--- /etc/ssh/sshd_config_adm    Tue Jul  5 13:36:44 2005
***************
*** 11,16 ****
--- 11,17 ----
--- 35,45 ----
***************
*** 77,82 ****
--- 76,82 ----

Create the sshd\_adm service:

    # mkssys -p /usr/sbin/sshd \   /* Absolute path to the subsystem executable
                                      program. */
             -s sshd_adm \         /* Name that uniquely identifies the subsys. */
             -u 0 \                /* User id for the subsystem. */
             -a "-D -f /etc/ssh/sshd_config_adm" \   /* Arguments that must be
                                                        passed to the command. */
             -e /dev/console \     /* Where the subsystem standard error data is
                                      placed. */
             -i /dev/console \     /* Where the subsys. standard input is routed. */
             -o /dev/console \     /* Where the subsys. standard output is placed. */
             -R \                  /* Subsystem is restarted if the subsystem stops
                                      abnormally. */
             -Q \                  /* Multiple instances of the subsystem are not
                                      allowed to run at the same time. */
             -S \                  /* Subsystem uses the signals communication
                                      method. */
             -f 9 \                /* Signal sent to the subsystem when a forced
                                      stop of the subsystem is requested. */
             -n 15 \               /* Signal sent to the subsystem when a normal
                                      stop of the subsystem is requested. */
             -E 20 \               /* Execution priority of the subsystem. */
             -G ssh \              /* Subsystem belongs to the group specified. */
             -d \                  /* Inactive subsystems are displayed when the
                                      lssrc -a command request is made. */
             -w 20                 /* Time, in seconds, allowed to elapse between a
                                      stop cancel (SIGTERM) signal and a subsequent
                                      SIGKILL signal. */

Check the service\'s configuration:

    # lssrc -S -s sshd_adm                   
    #subsysname:synonym:cmdargs:path:uid:auditid:standin:standout:standerr:action:multi:contact:svrkey:svrmtype:\
     priority:signorm:sigforce:display:waittime:grpname:
    sshd_adm::-D -f /etc/ssh/sshd_config_adm:/usr/sbin/sshd:0:0:/dev/console:/dev/console:/dev/console:-R:-Q:-S:0:0:\
    20:15:9:-d:20:ssh:

Or directly querying the ODM internal database:

    # odmget -q subsysname=sshd_adm SRCsubsys

    SRCsubsys:
            subsysname = "sshd_adm"
            synonym = ""
            cmdargs = "-D -f /etc/ssh/sshd_config_adm"
            path = "/usr/sbin/sshd"
            uid = 0
            auditid = 0
            standin = "/dev/console"
            standout = "/dev/console"
            standerr = "/dev/console"
            action = 1
            multi = 0
            contact = 2
            svrkey = 0
            svrmtype = 0
            priority = 20
            signorm = 15
            sigforce = 9
            display = 1
            waittime = 20
            grpname = "ssh"

Create (copy) the sshd\_adm configuration file:

    # cp /etc/ssh/sshd_config /etc/ssh/sshd_config_adm

Edit and modify the sshd **and** sshd\_adm configuration files according
to:

    # diff -c /etc/ssh/sshd_config /etc/ssh/sshd_config_adm
    *** /etc/ssh/sshd_config        Tue Jul  5 13:36:44 2005
      # default value.
      
      #Port 22
    + Port 77
      #Protocol 2,1
      #ListenAddress 0.0.0.0
      #ListenAddress ::
    ***************
    *** 34,46 ****
      
      #LoginGraceTime 2m
      #PermitRootLogin yes
    - PermitRootLogin no
      #StrictModes yes
      
      #RSAAuthentication yes
      #PubkeyAuthentication yes
      #AuthorizedKeysFile   .ssh/authorized_keys
    - AuthorizedKeysFile    /var/.ssh/%u/authorized_keys
      
      # For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
      #RhostsRSAAuthentication no
      #AllowTcpForwarding yes
      #GatewayPorts no
      #X11Forwarding no
    + X11Forwarding yes
      #X11DisplayOffset 10
      #X11UseLocalhost yes
      #PrintMotd yes

Start the new service and restart the old one:

    # stopsrc -g ssh
    # startsrc -g ssh

**Here, the same `sshd` binary is used for the two services sshd and
sshd\_adm. There is no need to dissociate them (as on GNU/Linux or
Solaris for `ps(1)` listings) since the `lssrc` command can do that more
easily.**

Because `sshd` and `sshd_adm` are in the same group of service, there is
no need to add anything else to be able to start `sshd_adm` at run-level
\#2:

    # ls -l /etc/rc.d/rc?.d/*ssh*
    -r-xr-xr-x   1 root     system          307 Apr 29 17:46 /etc/rc.d/rc2.d/Ksshd
    -r-xr-xr-x   1 root     system          308 Apr 29 17:46 /etc/rc.d/rc2.d/Ssshd
