---
date: "2007-04-27T20:46:15Z"
tags:
- X11
- Service
title: Enabling the X Virtual Frame Buffer on AIX
---

1.  *corleone* is the hostname of the AIX server on which to install the
    XFVB server
2.  *beastie* is the hostname of the display (with a screen) test system

Because it is not always possible to use an X11 server, for example when
there is no hardware graphics adapter (as in a p550), it is possible to
replace it with a XVFB: which is intended to be used in a *rendering
server* environment and allow an application to render into the **main
memory** of the computer instead of the graphics card.

The XVFB for AIX 5.X is installed from the product CDs, as listed below:

    # lslpp -al | grep vfb | sort -u
      OpenGL.OpenGL_X.dev.vfb    5.3.0.0  COMMITTED  OpenGL Virtual Frame Buffer
      X11.vfb                    5.3.0.0  COMMITTED  Virtual Frame Buffer Software

As we don\'t want to use it directly, we are not launching it via the
`xinit` program which starts both the X server and the window manager.
Here is one of its simple invocation, put into the `inittab` to be able
to *respawn* it easily:

    # mkitab "Xvfb:2:respawn:/usr/bin/X11/X -force -vfb -x abx -x dbe -x GLX :0 2>&1 | alog -tboot > /dev/console"

To reload a dead X server or force `init` to reread `/etc/inittab`, just
use the following command:

    # init q   /* Not needed if /etc/inittab was modified using `mkitab' */
    # ps auwx | grep -i vfb
    root   725164  0.0  0.0 9128 8868   - A   14:49:30  0:00 /usr/bin/X11/X -force -vfb -x abx -x dbe -x GLX :0

### Verifying that XVFB is being used

To verify that an X server is running with the XVFB, use the following
command on corleone:

    # /usr/lpp/X11/Xamples/bin/xprop -display :0 -root | grep VFB
    XVFB_SCREEN(STRING) = "TRUE"

### Verifying that XVFB is working

*Your system must be on a network and you need access to another system
(with a screen) to view the contents of the XVFB.*

On the XVFB system, run the `xclock` client program:

    # xclock -display :0

On the other system, make sure X is running and that clients can
connect:

    # xhost +corleone

On the XVFB system, find the window ID for the `xclock` client:

    # xwininfo -root -tree -display :0 | grep xclock

On the XVFB system, use `xwd`/`xwud` to display the client window of the
XVFB system on the other system:

    # xwd -id 0x800009 -display :0 | xwud -display beastie:0

An image of the `xclock` you started on the XVFB system displays on the
other system. That\'s it.

Please refer to the IBM
[website](http://publib.boulder.ibm.com/infocenter/pseries/v5r3/index.jsp?topic=/com.ibm.aix.aixwnpgd/doc/aixwnpgd/xvfb.htm "The X Virtual Frame Buffer")
for more information on the X Virtual Frame Buffer.
