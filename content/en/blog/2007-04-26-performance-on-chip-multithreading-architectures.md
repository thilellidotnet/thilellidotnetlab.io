---
date: "2007-04-26T20:48:43Z"
tags:
- CMT
title: Performance on Chip Multithreading Architectures
---

Here is a very nice entry from the [Sun Developer
Network](http://developers.sun.com/prodtech/solaris/reference/techart/ "Technical Articles and Tips")
on throughput computing, focusing on the Solaris operating system, and
the new CoolThreads Technology build at the heart of the very new and
innovative `UltraSPARC` processor
[architecture](http://www.sun.com/processors/UltraSPARC-T1/ "32 threads in 2 sq. inches").

The content is enhanced with many examples and well explained
differences between terms commonly used these days, such as CMT, SMP,
LWP or `core`.

Read on: [Maximizing Application Performance on CMT
Architectures](http://developers.sun.com/solaris/articles/app_perf_cmt.html "Maximizing Application Performance on Chip Multithreading (CMT) Architectures").
