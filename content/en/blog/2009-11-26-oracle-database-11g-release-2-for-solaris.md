---
date: "2009-11-26T13:41:50Z"
tags:
- Oracle
title: Oracle Database 11g Release 2 For Solaris
---

As a follow-up to [the preceding
entry](/post/2009/10/07/Upcoming-Oracle-RDBMS-And-Solaris-News) on the
upcoming availability on the Oracle Database 11g Release 2 on both the
Solaris SPARC and x86 releases, we can see that this is a reality as of
today. Both architectures are [readily available for
download](http://www.oracle.com/technology/software/products/database/index.html).

As a system administrator I think this very interesting and encouraging,
not only because of the availability of one of the more robust RDBMS
system on Solaris platforms, but because this is some *actions* taken
after *words* from Oracle which seems to fit together. And so, the
interest in Solaris as an OS of choice is more reinforced now.
