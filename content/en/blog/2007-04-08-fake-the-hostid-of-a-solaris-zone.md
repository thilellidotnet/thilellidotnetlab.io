---
date: "2007-04-08T19:55:58Z"
tags:
- DTrace
- Zone
- Hostid
title: Fake the hostid of a Solaris Zone
---

#### Using the zhostid script from Brendan Gregg

`zhostid` is a DTrace daemon to change the hostid to different values
for each zone. You can [get
it](http://www.brendangregg.com/DTrace/zhostid "Changing hostids for Solaris Zones")
right now and lots of other useful DTrace utilities from Brendan
Gregg\'s DTrace Tools
[dedicated](http://www.brendangregg.com/dtrace.html "DTraceToolkit") web
page.

Here are a little illustration of its usage:

    # /usr/bin/hostid
    837d47dd
    #
    # zlogin ngzone
    [Connected to zone 'ngzone' pts/3]
    Last login: Mon May 22 15:16:12 on pts/2
    Sun Microsystems Inc.   SunOS 5.10      Generic January 2005
    # /usr/bin/hostid
    837d47dd
    # exit

    [Connection to zone 'ngzone' pts/3 closed]
    #
    # cd /tmp && chmod 740 zhostid
    # grep ngzone zhostid   /* Configure the desired hostid per zone. */
                    ngzone         20a82f32
    # ./zhostid &
    [1] 7330
    # /usr/bin/hostid
    837d47dd
    #
    # zlogin ngzone
    [Connected to zone 'ngzone' pts/3]
    Last login: Tue May 23 10:12:40 on pts/3
    Sun Microsystems Inc.   SunOS 5.10      Generic January 2005
    # /usr/bin/hostid
    20a82f32
    # exit

    [Connection to zone 'ngzone' pts/3 closed]
    #
    # jobs
    [1]+  Running                 ./zhostid &
    # kill -TERM %1
    [1]+  Killed                  ./zhostid
    #
    # zlogin ngzone
    [Connected to zone 'ngzone' pts/3]
    Last login: Tue May 23 10:13:07 on pts/3
    Sun Microsystems Inc.   SunOS 5.10      Generic January 2005
    # /usr/bin/hostid
    837d47dd
    # exit

    [Connection to zone 'ngzone' pts/3 closed]

#### Using the dynamic library interposition

To use library interposition, you need to create a special shared
library and set the \${LD\_PRELOAD} environment variable. When
\${LD\_PRELOAD} is set, the dynamic linker will use the specified
library before any other when it searches for shared libraries. The
\${LD\_PRELOAD} variable allows you to interpose any calls (including
system calls) resolved through shared objects without replacing the
dynamic libraries. The symbols can be redefined to collect the calling
and the timing data for the function calls without recompiling or
relinking the application. This can be particularly useful when all the
code necessary for relinking is not available.

For the purpose of this post, we will use the `gcc` (and `libgcc`) GNU
compiler environment tools. Let\'s go:

    # /usr/bin/hostid
    835768ed
    #
    # PATH=/usr/sfw/bin:${PATH}
    # LD_LIBRARY_PATH=/usr/sfw/lib:${LD_LIBRARY_PATH}
    # export PATH LD_LIBRARY_PATH
    # cd /tmp
    # cat << EOF > newhostid.c
    long gethostid(void) {
     return 0xabcdef;
    }
    EOF
    # gcc -fPIC -c newhostid.c
    # gcc -shared -o newhostid.so newhostid.o
    #
    # LD_PRELOAD=/tmp/newhostid.so   /* Now, let's instrument the dynamic loader to
                                        use our freshly build object library. */
    # export LD_PRELOAD
    #
    # /usr/bin/hostid
    00abcdef

Enjoy!
