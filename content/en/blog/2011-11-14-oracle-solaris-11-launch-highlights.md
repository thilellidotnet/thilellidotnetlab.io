---
date: "2011-11-14T09:49:54Z"
tags:
- Oracle
- Press
title: Oracle Solaris 11 Launch Highlights
---

Here is a little press review around the launch of Oracle Solaris 11:

### Oracle Solaris 11 How to Articles

-   <http://www.oracle.com/technetwork/server-storage/solaris11/documentation/how-to-517481.html>

1.  Installation
2.  System Configuration
3.  Network Management
4.  Software Management
5.  Virtualization
6.  Development

### Oracle Solaris : 11 New Things You Need To Know (Flash Video)

-   <http://download.oracle.com/otndocs/tech/OTN_Demos/11_Things/S11-Featurette/S11-Featurette-play.html>

### Recent Benchmarks Using Oracle Solaris 11

-   <https://blogs.oracle.com/BestPerf/entry/20111109_solaris_11_benchmark_list>

The following is a list of links to recent benchmarks which used Oracle
Solaris 11.

1.  SPARC T4-2 Delivers World Record SPECjvm2008 Result with Oracle
    Solaris 11
2.  SPARC T4-4 Produces World Record Oracle OLAP Capacity
3.  SPARC T4-2 Server Beats Intel (Westmere AES-NI) on ZFS Encryption
    Tests
4.  SPARC T4 Processor Beats Intel (Westmere AES-NI) on AES Encryption
    Tests
5.  SPARC T4 Processor Outperforms IBM POWER7 and Intel (Westmere
    AES-NI) on OpenSSL AES Encryption Test
6.  SPARC T4-1 Server Outperforms Intel (Westmere AES-NI) on IPsec
    Encryption Tests
7.  SPARC T4-2 Server Beats Intel (Westmere AES-NI) on SSL Network Tests
8.  SPARC T4-2 Server Beats Intel (Westmere AES-NI) on Oracle Database
    Tablespace Encryption Queries

### UNIX 03 Product Standard Conformance

-   <http://www.opengroup.org/openbrand/register/>
-   <http://www.opengroup.org/openbrand/register/brand3585.htm>

Oracle Solaris 11 is certified on SPARC and X86-based platforms as
conforming to the UNIX 03 product standard, effective November 8, 2011,
per The Open Group.

### Oracle dubs Solaris 11 world\'s \'first cloud OS\'

*\'Absolutely spanking\' IBM and HP*

-   <http://www.theregister.co.uk/2011/11/09/oracle_solaris_11_unix_os/>

More importantly, the delay is the result of Oracle\'s desire to fully
leverage the Sparc processor, the Solaris operating system, and the
Oracle stack of database, middleware, and application software as a
highly tuned system with a better system for testing and patching
software in the entire stack as it changes and thereby allowing Oracle
to command a premium for Sparc-based systems because they are easier to
operate and support.

This is, of course, the old AS/400 value proposition that IBM has been
selling its midrange customers for more than two decades. The difference
now is that Oracle actually believes it, and IBM, which makes a lot more
money selling services to integrate piece parts and support them than
selling its Power Systems running the integrated IBM i software stack,
can\'t afford to.

Fowler opened up his technical review of Solaris 11 by reminding
everyone that Solaris had more deployments than HP-UX and AIX combined,
and added that \"operating systems are something that only improve over
time.\"

Fowler said that Solaris 10, which was launched in January 2005, would
be getting its own updates soon and would, in fact, be supported on
future Sparc T5 and M4 systems due next year.

On roadmaps for the past year and a half, Fowler has shown that
Oracle\'s long-term goal is to deliver in late 2014 or early 2015 a
machine that has 64 sockets with a total of 16,384 threads and
supporting 64TB of main memory.

### Oracle Solaris 11: The First Cloud OS

-   <https://blogs.oracle.com/solaris/entry/oracle_solaris_11_the_first>

Oracle Solaris 11 is the first operating system engineered with cloud
computing in mind. We believe you should expect more from your OS \--
especially as you start considering public, private and hybrid clouds
for enterprise-class workloads.

And what\'s most important: all of this is integrated, engineered and
optimized to work together: the whole is greater than the sum of its
parts. It\'s the power of this that makes Oracle Solaris unique.

### S11 X11: ye olde window system in today\'s new operating system

-   <https://blogs.oracle.com/alanc/entry/s11_x11>

Today\'s the big release for Oracle Solaris 11, after 7 years of
development. For me, the Solaris 11 release comes a little more than 11
years after I joined the X11 engineering team at what was then Sun, and
finishes off some projects that were started all the way back then.

In total, we recorded 1512 change request id\'s during Solaris 11
development, from the time we forked the "Nevada" gate from the Solaris
10 release until the final code freeze for todays release - some were
one line bug fixes, some were man page updates, some were minor RFE\'s
and some were major projects, but in the end, the result is both very
different (and hopefully much better) than what we started with, and
yet, still contains the core X11 code base with 24 years of backwards
compatibility in the core protocols and APIs.

### The most inviting Solaris 11 features - Part I, Boot Environments

-   <https://blogs.oracle.com/orasysat/entry/the_most_inviting_solaris_111>

\"Having been involved in various projects around the upcoming Solaris
11 release, we had the possibility to compile a list of features we
assume UNIX Engineers will find to be cornerstones of Solaris 11 based
platforms. It wasn\'t easy to keep the list short, due to the sheer
amount of innovation and the tight integration of the new- or updated
technologies in this Solaris release.

We have planned a series of blog posts with a short preview of each
Solaris 11 technologies in the list, in a Question and Answer form.\"

This is the first post, featuring Boot Environments.

### Solaris 11 Released As Cloud Virtualisation OS

-   <http://www.eweekeurope.co.uk/news/solaris-11-released-as-cloud-virtualisation-os-45461>

The delayed release has been much-anticipated because Solaris still
matches the combined user base for HP-UX and AIX, Oracle executive vice
president of systems John Fowler claimed. However the transition from
Sun to Oracle has hit the server division quite hard and Oracle is no
doubt hoping that the new OS release will help turn the tide.

### Oracle Solaris Goes to 11

-   <http://www.pcworld.com/businesscenter/article/243537/oracle_solaris_goes_to_11.html>

Solaris, a Unix implementation, was originally developed by Sun
Microsystems, which Oracle acquired last year. While not as widely known
for its cloud software, Oracle has been marketing Solaris as a
cloud-friendly OS. In Oracle\'s architecture, users can set up different
partitions, called Zones, inside a Solaris implementation, which would
allow different workloads to run simultaneously, each within their own
environment, on a single machine.

### First impressions of Solaris 11 11/11

-   <http://sparcv9.blogspot.com/2011/11/first-impressions-of-solaris-11-1111.html>

I have had a few hours to try the final Solaris 11 release, overall I
think it is far more stable and polished than the previous \"Early
Adaptors\" release. Besides the fact that I am unable to use semi-old
SPARC gear to test the release since only the latest generations of
hardware are supported I have found few real problem so far.

### Watch the Oracle Solaris 11 Launch

-   <http://www.oracle.com/us/corporate/events/solaris11launch/>

Watch as Oracle executives Mark Hurd and John Fowler announce the launch
of Oracle Solaris 11, which brings proven enterprise capabilities to
private, public, and hybrid clouds. Oracle Solaris 11 is the industry\'s
best UNIX operating system with unique features including advanced file
system technologies, advanced security, and built-in virtualization in
every dimension.

Learn how Oracle Solaris 11 has been engineered, tested, and supported
to get the most of SPARC and x86 systems including Oracle Exalogic
Elastic Cloud, Oracle Exadata Database Machine, and SPARC SuperCluster
engineered systems.

### Solaris 11: Oracle Launches Cloud OS

-   <http://netmgt.blogspot.com/2011/11/solaris-11-oracle-launches-cloud-os.html>

Full highlights of the launch of Oracle Solaris 11.
