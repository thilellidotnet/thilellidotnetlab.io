---
date: "2007-05-23T14:42:34Z"
tags:
- POWER
- Comparison
title: Upcoming IBM AIX 6 features vs. Sun Solaris 10 and OpenSolaris
---

I can say I am impressed by both the upcoming
[POWER6](http://www.eweek.com/article2/0,1895,2134418,00.asp)
[processor](http://arstechnica.com/news.ars/post/20070521-ibms-power6-flies-the-coop-at-4-7ghz.html)
and the new AIX 6 [release
preview](http://www-03.ibm.com/systems/p/os/aix/v61/preview.html) . They
both are innovative, and come with a large set of new or updated
features. For the purpose of this entry, I will concentrate on a little
side-by-side comparison between the features announced for AIX, and
those available in Solaris.

1.  The Workload Partitions (WPAR) on AIX seems to be very similar to
    the Containers, or Zones, which is an [Virtualization
    feature](http://www.sun.com/software/solaris/virtualization.jsp)
    found on Solaris.
2.  Live Application Mobility on AIX, which will make possible to move
    WPAR without end-user disruption, is not available as-is in Solaris.
    You currently can `attach` and `detach` a halted zone, not a running
    zone. This is due to the fact that Containers is primarily intended
    to be a Utilization feature, not a virtualization approach. It can
    be mentioned that an upcoming Solaris equivalent may be the
    [hypervisor Xen](http://www.opensolaris.org/os/community/xen/)
    support, although not an exact match to WPAR.
3.  Role Based Access Control, Trusted AIX, Security Expert LDAP
    integration, and Secure by Default on AIX are all already present
    via the [Security
    features](http://www.sun.com/software/solaris/security.jsp) and [Sun
    Java Enterprise System
    Components](http://www.sun.com/software/javaenterprisesystem/index.jsp)
    found on Solaris.
4.  The Encrypting file system on AIX (using JFS2) does not have an
    equivalent, yet. Watch carefully the work currently being done on
    the [Cryptographic
    Framework](http://www.opensolaris.org/os/project/crypto/) from
    OpenSolaris, particularly the [ZFS on disk encryption
    support](http://opensolaris.org/os/project/zfs-crypto/) project.
5.  The Graphical Installation on AIX already has a counterpart on
    Solaris. More, there exists an active OpenSolaris project code-name
    [Caiman, Solaris Install
    Revisited](http://www.opensolaris.org/os/project/caiman/) which is a
    completely new installation infrastructure for Solaris with a
    simplified web graphical and text user interfaces, and Live CD/DVD
    integration.
6.  The general Continuous availability features on AIX are interesting,
    and based on IBM longstanding experience with mainframe technology.
    Solaris currently has a pretty great list of [Availability
    features](http://www.sun.com/software/solaris/availability.jsp). Not
    to mention the impressive RAS features provided by the recently
    announced [SPARC Enterprise
    Servers](http://www.sun.com/servers/sparcenterprise/) build on top
    of the SPARC64 VI processor. The Dynamic tracing capability
    advertised by IBM (probevue) seems very interesting. We will see how
    much this enhanced feature will differ from the [Observability
    feature](http://www.sun.com/software/solaris/observability.jsp)
    known as DTrace on Solaris. Please refer to the ongoing discussion
    about [DTrace on
    AIX](http://www.opensolaris.org/jive/thread.jspa?threadID=31286) on
    the `dtrace-discuss` forum on opensolaris.org.
7.  POWER6 processor exploitation using AIX 6. It may be noted that
    processor specific features\--or technology\--exploitation are
    currently available using Solaris, with a wide range of SPARC-based
    and x64/x86-based systems, up to the upcoming [Niagara 2
    Processor](http://www.sun.com/processors/niagara/), and the very
    awaited [ROCK
    successor](http://blogs.sun.com/jonathan/entry/rock_arrived).
8.  Last, the [Binary
    compatibility](http://www-03.ibm.com/systems/p/os/aix/compatibility/index.html)
    program for AIX. Solaris has a similar and rather complete
    compatibility program known as the [Solaris Binary Application
    Guarantee
    Program](http://www.sun.com/software/solaris/guarantee.jsp), which
    covers both binary and source code compatibility.

Although this comparison is more an entry point for comparing the two
biggest players in the UNIX world these days than a complete description
of each side, don\'t forget to take into account the following remaining
two points:

-   All the features are currently free of charge on Sun Solaris. Even
    the upcoming features which will be backported or incorporated in
    future Solaris Updates or new releases. This is the [Support and
    Services](http://www.sun.com/software/solaris/support.jsp) which are
    charged, according to your needs. On AIX, nothing is freely
    available, and some advanced features will be available only
    *through a separately offered licensed program product*.
-   Although some specific features are not yet available in Solaris,
    most of the aforementioned ones are currently provided by Solaris 10
    from the GA release, in January 2005. Please consult the [What's
    New](http://www.sun.com/software/solaris/whats_new.jsp) page for
    detailed features availability on the Solaris OS. AIX 6, along with
    the POWER6 processor specific features, will be available in
    late 2007.

Last, all of these news from IBM are very interesting, and features such
as WPAR, Live Application Mobility, and probevue are particularly
exciting. So, wait and see!

*Update \#1 (2007-05-24):* Please consult [this interesting blog
entry](http://blogs.sun.com/jimlaurent/entry/imitation_is_the_sincerest_form)
from Jim Laurent on the same subject.

*Update \#2 (2008-10-16):* I encourage you to read Joerg Moellenkamp
entries on [UNIX virtualization
technologies](http://www.c0t0d0s0.org/archives/4931-Analysing-a-so-called-Comparison-about-Virtualisation-at-IBM-Developerworks.html#extended),
and [IBM´s Workload
Partition](http://www.c0t0d0s0.org/archives/3421-Some-thoughts-about-IBMs-Workload-Partition..html).
