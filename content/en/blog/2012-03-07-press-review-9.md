---
date: "2012-03-07T15:42:33Z"
tags:
- Press
title: 'Press Review #9'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### Are SSD-based arrays a bad idea?

-   <http://storagemojo.com/2012/03/05/are-ssd-based-arrays-a-bad-idea/>

Think: if NAND flash storage arrays were being developed today, what is
the chance that we'd put the flash into little bricks and then plug a
bunch of them into a backplane? So why do it now?

### Shall I use Zones or LDOMs?

-   <https://blogs.oracle.com/orasysat/entry/shall_i_use_zones_or>

Of course one can\'t answer this question without talking about the
platform requirements and the reasons to pick the right technologies,
but before we\'d go into details, let me get the most important
statement straight: Zones and LDOMs are not rivalling, but complementary
technologies. If you need kernelspace separation, use ldoms. But run
your applications in zones within those ldoms anyway!

### Zones? Clusters? Clustering zones? Zoneclusters?

-   <https://blogs.oracle.com/orasysat/entry/zones_clusters_clustering_zones_zoneclusters>

Everyone values zones, Solaris\' builtin OS-virtualization. They are
near-footprintless. Their administration is delegable. They have their
own bootenvironments. Easily cloneable with ZFS snapshots, etc. They are
also cleanly integratable with Solaris Cluster in different ways - this
post should shed some light on the different options, and provide an
example of zoneclusters.

### POWER: Loss of Sony Playstation Platform

-   <http://netmgt.blogspot.com/2012/03/power-loss-of-sony-playstation-platform.html>

Apple abandoned PowerPC for Intel in 2006, leaving IBM POWER without a
desktop partner. Sony is rumored to discontinue use of IBM POWER for
their gaming consoles in the PlayStation 4, starting the decline of
POWER in the gaming market. POWER7+ from IBM is now nearly a half-year
late and IBM has still not delivered as of March 2012.

### Cheatsheet for configuring\...

-   <http://www.c0t0d0s0.org/archives/7440-Cheatsheet-for-configuring-a-new-nodename-in-Solaris-11.html>
-   <http://www.c0t0d0s0.org/archives/7439-Cheatsheet-for-configuring-the-networking-in-Solaris-11.html>

There are quite a number of changes in the procedures to configure some
of the networking parameters. Many things have changed, that were just
editing of a file in the past, have now command-line based tools in
order to change their parameters. Before you ask: The reason for this
steps are quite simple.

### Solaris Fingerprint Database - How it\'s done in Solaris 11

-   <https://blogs.oracle.com/cmt/entry/solaris_fingerprint_database_how_it>

Many remember the Solaris Fingerprint Database. It was a great tool to
verify the integrity of a solaris binary. Unfortunately, it went away
with the rest of sunsolve, and was not revived in the replacement, \"My
Oracle Support\". Here\'s the good news: It\'s back for Solaris 11, and
it\'s better than ever!

### Sun ZFS Storage Appliance : can do blocks, can do files too!

-   <https://blogs.oracle.com/roch/entry/sun_zfs_storage_appliance_can>

As a benchmark SPC-1\'s profile is close to what a fixed block size DB
would actually be doing. See Fast Safe Cheap : Pick 3 for more details
on that result. Here, for an encore, we\'re showing today how the ZFS
Storage appliance can perform in a totally different environment :
generic NFS file serving.

### The USE Method\...

-   <http://dtrace.org/blogs/brendan/2012/02/29/the-use-method/>
-   <http://dtrace.org/blogs/brendan/2012/03/01/the-use-method-solaris-performance-checklist/>
-   <http://dtrace.org/blogs/brendan/2012/03/07/the-use-method-linux-performance-checklist/>

A serious performance issue arises, and you suspect it's caused by the
server. What do you check first? Back when I was teaching operating
system performance, I wanted a methodology my students could follow to
find common issues quickly, without overlooking important areas. Like an
emergency checklist in a flight manual, it would be something simple,
straightforward, complete and fast. I eventually came up with the "USE"
method (short for "Utilization Saturation and Errors"), which I've used
many times successfully in enterprise environments, and more recently in
cloud computing environments.

### Getting Started with Oracle Unbreakable Enterprise Kernel Release 2

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/uek-rel2-getting-started-1555632.html>

This article describes how you can update your Oracle Linux systems to
the latest version of the Unbreakable Enterprise Kernel. By switching to
the latest Unbreakable Enterprise Kernel, you can get the latest
innovations in mainline Linux. Switching is easy---applications and the
operating system remain unchanged. There is no need to perform a full
re-install; only the relevant RPM packages are replaced. You can obtain
future updates easily from the Unbreakable Linux Network to keep your
systems fully patched and secured.

### Oracle Solaris 11 Cheatsheet

-   <http://www.c0t0d0s0.org/archives/7442-Oracle-Solaris-11-Cheatsheet.html>

In the last few days i created a cheat sheet for Solaris 11 \... while
it\'s still a work in progress (it will be surely longer in the future).

### Performance impact of the new mtmalloc memory allocator

-   <http://www.c0t0d0s0.org/archives/7443-Performance-impact-of-the-new-mtmalloc-memory-allocator.html>

I didn\'t wrote about this as it was in my phase of silence but there
was some change in the allocator area, Solaris 10 got a revamped
mtmalloc allocator in version Solaris 10 08/11 (as described in
\"libmtmalloc Improvements\"). The new memory allocator was introduced
to Solaris development by the PSARC case 2010/212.

### Linux Kernel Performance: Flame Graphs

-   http://dtrace.org/blogs/brendan/2012/03/17/linux-kernel-performance-flame-graphs/

To get the most out of your systems, you want detailed insight into what
the operating system kernel is doing. A typical approach is to sample
stack traces; however, the data collected can be time consuming to read
or navigate. Flame Graphs are a new way to visualize sampled stack
traces, and can be applied to the Linux kernel for some useful (and
stunning!) visualizations.

### \'Cheap\' Oracle box bashes NetApp benchmark

*Save one MILLION dollars, get 32% more speed*

-   <http://www.theregister.co.uk/2012/03/15/oracle_zfs_spec_result/>

The Sun ZFS 7320 scored 134,140 SPECsfs2008 IOPS with an overall
response time of 1.51msecs and cost \$179,602.

Oracle quotes a price of \$1,215,290 for NetApp\'s FAS3270 which scored
101,183 IOPS with a 1.66msec response time.

The Oracle-NetApp pricing difference is huge and, on the face of it,
paying \$1,035,698 more for 32 per cent less performance is not an
attractive idea for a basic NFS file-serving box.

### Red Hat Enterprise Linux to Oracle Solaris 11 Evaluation

-   <http://www.oracle.com/technetwork/server-storage/solaris11/overview/redhat-mapping-guide-1555721.html>

The following guide gives an overview of some of the technologies
included in Oracle Solaris 11 and the direct benefit you can get by
using some of these features. This guide also provides a similar
technology mapping, where possible, between Red Hat Enteprise Linux and
Oracle Solaris 11, so that administrators with knowledge in the former
can kick start their learning experience if planning deploy the latter.

### How To install Solaris 11 automated install server

-   <https://blogs.oracle.com/unixman/entry/how_to_install_solaris_11>

This a quick blog entry designed to outline the commands that can aid in
the process of setting up a Solaris 11 Automated Install server. More
details and an overview of what\'s changed, are of course available at
the Simplified Installation section of the Oracle Solaris 11 Spotlight
pages.

### Great Solaris 10 features paving the way to Solaris 11

-   <https://blogs.oracle.com/orasysat/entry/great_solaris_10_features>

Again: the main message is: Go for Solaris 11 if you can. If you need to
run Solaris 10, we recommend deploying the mentioned technologies, they
can and will improve your daily system engineering business and prepare
your platform for the move to Solaris 11.

### How to Find Out What\'s in an Oracle Solaris Binary File

-   <http://www.oracle.com/technetwork/articles/servers-storage-dev/o11-160-binary-1451031.html>

How to determine the contents of Oracle Solaris binaries and what tools
you can use to read, extract, and delete sections. Plus, the effect of
compiler flags on binary file size and how to reduce the size of the
executable.

### SPARC: Life in the Fast Lane - 10 Months Later

-   <http://netmgt.blogspot.fr/2012/03/sparc-life-in-fast-lane-10-months-later.html>

Both Oracle and Fujitsu are independently pursuing SPARC in disjoint,
non-overlapping, markets. They are not the only vendors creating new
production quality SPARC processors (as noted by the former \#1 HPC
system from China.) SPARC appears to have a long road ahead, being
implemented by multiple vendors, and each implementation performing best
in it\'s class.
