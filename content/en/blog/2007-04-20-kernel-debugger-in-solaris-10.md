---
date: "2007-04-20T20:42:28Z"
tags:
- Kernel
- Debug
title: Kernel Debugger in Solaris 10
---

Here is a little note to point out those two blog entries on how to
debug kernel problem at boot time, from [Dan
Mick](http://blogs.sun.com/dmick/entry/diagnosing_kernel_hangs_panics_with "Diagnosing kernel hangs...")
and [Eric
Lowe](http://blogs.sun.com/elowe/entry/debugging_early_in_boot "Core Dumps of a Kernel Hacker's Brain"),
respectively.

Too bad i didn\'t have a serial console on my Sun Ultra 20 workstation.
I will try these steps when U20 will be near my hands, though.
