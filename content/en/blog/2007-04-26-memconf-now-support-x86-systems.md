---
date: "2007-04-26T20:26:17Z"
tags:
- memory
- x86
title: memconf Now Support x86 Systems
---
------- ------ --- ------------------- --------------------
unknown in use 0   A0                  Bank0/1
unknown in use 0   A1                  Bank2/3
unknown in use 0   A2                  Bank4/5
unknown in use 0   A3                  Bank6/7
total memory = 2047MB (1.9990234375GB)
</pre>

After months of waiting, the well known `memconf` utility now support
x86 systems. Try
[it](http://myweb.cableone.net/4schmidts/memconf.html "UNIX memconf utility")
yourself!

Here is the output on my personal [Sun Ultra
20](http://sunsolve.sun.com/handbook_pub/Systems/Ultra20/Ultra20.html "Sun Ultra[tm] 20 Workstation")
workstation (an AMD Opteron machine) running build 51 of
[OpenSolaris](http://www.opensolaris.org/ "OpenSolaris Project"):

    # memconf
    hostname: unic.thilelli.net
    Sun Ultra 20 Workstation (Solaris x86 machine)
    Type    Status Set Device Locator      Bank Locator
