---
date: "2008-05-02T13:43:39Z"
tags:
- PHP
- Bug
title: PHP APC Extension Bug With Optimized Open Source Software Stack
---

To easily manage LDAP accounts (and general LDAP entries in fact), we
have created a [Solaris
Zone](http://dlc.sun.com/osol/docs/content/SYSADRM/zones.intro-1.html)
and installed the excellent [Cool Stack
bundle](http://cooltools.sunsource.net/coolstack/) to host the LAM
([LDAP Account Manager](http://lam.sourceforge.net/)) management web
tool. But after upgrading the Cool Stack to version 1.2 we encountered a
very annoying problem mostly with freezing web pages, and generally
ending up in restarting the Apache web server provided by the Cool
Stack. After some troubleshooting, we discover that this behavior was
[introduced by a
bug](http://forums.sun.com/thread.jspa?threadID=5254435) in the
`APC-3.0.14` module bundled with the updated `php-5.2.4` scripting
software in this version of the Cool Stack.

Luckily, the bug was already fixed and a new version of the APC
extension of PHP is available for download (in fact, just replace to
original `apc.so` module by the new one). All the Cool Stack related
problems, associated fixes and instructions are listed on the [Cool
Stack 1.2
Patches](http://cooltools.sunsource.net/coolstack/csk12patches.html)
page: be sure to keep in sync\' if you are a Cool Stack consumer.
