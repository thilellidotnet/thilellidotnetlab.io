---
date: "2007-09-13T07:55:37Z"
tags:
- Bug
- Keyboard
title: European Type6 USB Keyboard Circumflex Key Issue... Solved
---

I recently worked with Dermot Malone, the Responsible Engineer for [this
bug report](http://bugs.opensolaris.org/view_bug.do?bug_id=6515644).

The fact is the described problem persist as explained in the
Description field of the bug report, running snv\_70. But I must say I
found *something* recently: as of now, I used the default C locale. But
after reading [this
documentation](http://docs.sun.com/app/docs/doc/817-2521/utf8-21349?a=view)
(go to *Unicode Locale: en\_US.UTF-8 Support*), I decided to try the
en\_US.UTF-8 locale, directly chosen from the `dtlogin` screen\... and
found that I can now have a similar English environment as before (using
C locale), while supporting accent and circumflex characters from
applications which support this locale (for example the GNOME Terminal,
or Mozilla Firefox). Hum, I just find a little big curious that most of
accent characters (say \'é\') works properly using the C locale, but
that I need to set an other locale to be able to use the circumflex
characters (say \'ô\').

As a matter of interest, Dermot ask me why I didn\'t use the
fr\_FR.UTF-8 locale. Here is my answer:

> Well, I am a French guy, but systematically install English operating
> systems, and use the default C locale. In IT, English is \_the\_
> standard, and all things (manual pages, messages, format strings,
> etc.) are all homogeneous this way. As a side note, I am sure not to
> encounter the problems found on RedHat Linux systems when the default
> locale is not properly supported by the OS sub-systems themselves (the
> RC scripts generally sets the LANG=C (or something like that) for
> \'grep\', \'sed\' and \'awk\' for example), or some third party
> products (such as the IBM TSM Backup Archive client).
>
> Now, using the en\_US.UTF-8 locale on Solaris and Solaris Express, I
> can have best of both worlds: a fully functional (and supported)
> English environment, and be able to use extended characters specific
> to my language.
