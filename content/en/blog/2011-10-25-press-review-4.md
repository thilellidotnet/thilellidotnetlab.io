---
date: "2011-10-25T13:37:21Z"
tags:
- Press
title: 'Press Review #4'
---

Here is a little press review around Oracle technologies, and Solaris in
particular:

### Oracle Solaris 11 Launch Webcast

*Build Your Next-Generation Datacenter in the Cloud*

-   <http://oracle.com/goto/solaris11event>

Watch Oracle executives Mark Hurd and John Fowler live from the Oracle
Solaris 11 launch event in New York, and learn how you can build your
next-generation datacenter in the cloud.

1.  Accelerate internal, public, and hybrid cloud applications
2.  Optimize application deployment with built-in virtualization
3.  Achieve top performance and cost advantages with Oracle Solaris
    11--based engineered systems

### Extra, Extra! Show Daily Newspaper Archives Now Available

-   <http://blogs.oracle.com/oracleopenworld/entry/extra_extra_show_daily_newspaper>

From opening day through It\'s a Wrap!, the show daily newspaper covered
all the highlights of Oracle OpenWorld 2011. Now, in addition to
watching keynote and session replays on YouTube.com/Oracle, and
downloading presentations from Content Catalog, you can also download
free .pdfs of each issue.

### T4 arrives!

-   <http://blogs.oracle.com/jsavit/entry/t4_arrives>

A natural question for SPARC and Solaris customers would be \"should I
use a T4, a T3, or an M-series product?\" Now that T-series has a
broader range of applicability, there\'s more choice in platform
selection: a T4 can be used in cases where M-series would have been the
only answer. There\'s more overlap.

![sparc.t4.1.png](/blog/sparc.t4.1.jpg "sparc.t4.1.png, Oct 2011")

In general, the M-series will still have the advantage for vertically
scaling workloads that need massive CPU, memory, and I/O capacity, that
need the higher redundancy and reliability features, and depend on the
ability to add capacity to a running system by inserting CPU boards when
needed. The T3 product will still find use in pure throughput computing
applications because it has the higher core density and lower software
license core factor (0.25 instead of 0.5).

![sparc.t4.2.png](/blog/sparc.t4.2.jpg "sparc.t4.2.png, Oct 2011")

The T4 processor and the servers based on it mark a new level of
performance for SPARC processors. With record performance it changes the
game (and turns over stale assumptions) about SPARC performance. It also
illustrates the commitment Oracle has to SPARC and Solaris, and our
increased ability to execute on delivering faster system products. By
adding single CPU performance to T-series, it extends the ability to
leverage Oracle VM Server for SPARC (LDoms) for a broader range of
applications. Big news indeed - and Oracle Open World is just starting
up, so watch Oracle.com and blogs.oracle.com closely the next few days.

![sparc.t4.3.png](/blog/sparc.t4.3.jpg "sparc.t4.3.png, Oct 2011")

### Oracle Takes The Midrange Fight To IBM

-   <http://www.itjungle.com/tfh/tfh100311-story01.html>

Just like IBM lowered the boom on Sun back in the early 2000s with its
AIX on Power ramp\--made possible because IBM charged AS/400 shops
exorbitant prices for hardware and software so it could discount AIX
boxes insanely to win over Sun shops\--Oracle is going to lower the boom
on IBM and Hewlett-Packard in the portion of the server racket that is
devoted to running Oracle, PeopleSoft, Siebel, JDE, and other
applications. This time around, Oracle will be making deals on its
database, middleware, and application software to help it push its iron.
Mark my words. Larry Ellison is not joking around here; he was just
waiting for the chip engineers to get the right processor out the door.

Very roughly speaking, I would guess that this Sparc cluster should have
roughly the same performance as a Power 770 running database workloads,
but it could be higher because of the compression and SQL pre-chewing of
the Exadata storage arrays and the flash integrated into the Sparc T4-4
nodes when used in the SuperCluster.

### Oracle previews Solaris 11, due in November

-   <http://www.theregister.co.uk/2011/10/04/oracle_solaris_preview/>

Fowler did not give the precise number of cores, threads, or memory
capacity that Solaris 11 would span, but said Oracle took the time to
rework the Solaris kernel with a new scheduler and a new I/O handler
that would allow it to span tens of thousands of CPUs, hundreds of
terabytes of main memory, exabytes of storage, and hundreds of gigabits
of networking bandwidth. (In past presentations of the Sparc/Solaris
roadmap, Fowler showed the design goal of a future Sparc system due in
late 2014 or early 2015 as spanning 64 sockets in a single system image
with a total of 16,384 threads and supporting 64TB of main memory.)

Solaris 11 has support for the dynamic threading implemented in the new
Sparc T4 processors, launched last week, and also sports a latency-aware
kernel memory allocator, an optimized shared memory stack, a parallel
network stack, adaptable thread and memory placement, and enhancements
in NUMA I/O (which will be important in future Sparc T series machines,
presumably). The scheduler is aware of the different possible topologies
in both x86 and Sparc systems and has NUMA-aware kernel memory fan out.

![solaris11.rdbms.png](/blog/solaris11.rdbms.jpg "solaris11.rdbms.png, Oct 2011")

Fowler also said that 600 customers had Solaris 11 running in production
already. Presumably he meant actual Solaris 11, not Solaris 11 Express.
And he reminded OpenWorld attendees of Oracle\'s compatibility guarantee
for Solaris applications: \"Your applications will run on 11 or it is my
problem.\"

### Oracle Has Built A Modern, Cloudy AS/400

-   <http://www.itjungle.com/tfh/tfh101011-story05.html>

I\'ve been thinking a lot lately about the juxtaposition of Oracle\'s
\"engineered systems\" and IBM\'s \"workload optimized systems,\" not
just because these things are grabbing headlines, but because all the
major system makers are trying to figure out a new way to sell a very
old idea: machines designed to do very specific work rather than being
general purpose systems. Only this time around, more than a few of them
are trying to make these engineered systems out of commodity processors,
memory, disk, flash, and networking components. The secret sauce\--and
the profit\--in each one of them is not the hardware, but how a
collection of hardware supports a very specific stack of application
software.

If I were Larry Ellison, I would start pitching the cloud against on
premise entry servers or build some baby clusters\--probably both.
Either could mean big trouble for Big Blue and its Power Systems
business.

### Solaris 10 8/11 (Update 10) Patchset now available

-   <http://blogs.oracle.com/patch/entry/solaris_10_8_11_update>

As you may know by now, these patchsets will bring all pre-existing
packages up to the same software level as the corresponding Solaris
Update. For example, all ZFS and Zones functionality is entirely
contained in pre-existing packages, so applying the patchset will
provide all the ZFS and Zones functionality and bug fixes contained in
the corresponding Solaris Update.

When we release the Solaris Update patchset, we try to fix any serious
late breaking issues found with the corresponding Solaris Update
patchset. A list of additional patches added and the Caveats they
address is contained in the patchset README.

Applying the patchset is not the same as upgrading to the Solaris Update
release, as the patchset will not include any new packages introduced in
the Solaris Update or any obsolete packages deleted in the Update.

### Solaris 9 transitioning to Extended Support

-   <http://blogs.oracle.com/patch/entry/solaris_9_transitioning_to_extended>

Just a quick heads-up that Solaris 9 will transition to Vintage support
(old sun terminology) / Extended support (Oracle terminology) at the end
of this month.

Solaris 9 patches released from November 1, 2011, will have
Vintage/Extended access entitlement by default, which means that only
customers with an Extended Support contract for Solaris will be able to
access them.

### Oracle Solaris 11 Highlights from Oracle OpenWorld 2011!

-   <http://blogs.oracle.com/solaris/entry/oracle_solaris_11_latest_content>

Oracle Solaris 11 had some exciting news at Oracle OpenWorld 2011 this
year! If you missed the John Fowler keynote or the Oracle Solaris 11
sessions - you can still catch all the highlights from links below.

### What\'s New in Oracle Solaris 11

-   <http://blogs.oracle.com/JeffV/entry/what_s_new_in_oracle>

Oracle Solaris 11 adds new features to the \#1 Enterprise OS, Solaris
10. Some of these features were in \"preview form\" in Solaris 11
Express. The feature sets introduced there have been greatly expanded in
order to make Solaris 11 ready for your data center. Also, new features
have been added that were not in Solaris 11 Express in any form.

The list of features below is not exhaustive. Complete documentation
about changes to Solaris will be made available. To learn more, register
for the Solaris 11 launch. You can attend in person, in New York City,
or via webcast.

### That Perplexing Power7+ Processor

-   <http://www.itjungle.com/tfh/tfh101711-story03.html>

What IBM is telling business partners is that the old Power7 machines
and the new Power7 machines are essentially the same except for the
memory and I/O capacity differences\--including essentially the same
price. They have the same software editions riding on top of them and
for most customers, according to Big Blue, either machine will work
fine. Those who have high-bandwidth networking and storage needs will
want the newer machines, or those that are doing lots of virtualization
or other memory-chewing workloads. At around \$200 per GB, that extra
memory is not cheap, so not everyone will want to go there anyway. In
2012, IBM told business partners, it will start pushing the fatter
Power7 machines and later in the year it will withdraw the older boxes.
The key thing, IBM told resellers was DO NOT DISRUPT 4Q11 SALES.

As I point out elsewhere in this issue, if you are buying one of the
skinnier Power Systems from last year\'s catalog, you should demand some
kind of compensation. There\'s no way the older machines are of the same
value on the street with smaller potential memory and slower I/O. It
probably isn\'t much of a difference, but there\'s no way it can be
zero.

This newsletter advocates for Power Systems customers and it wants IBM
to do more than worry about fourth quarter sales. It wants IBM to start
taking a more aggressive technical fight to Intel so all of us in the
IBM i ecosystem can do better.

### Using SystemTap

-   <http://dtrace.org/blogs/brendan/2011/10/15/using-systemtap/>

While using SystemTap, I've been keeping notes of what I've been doing,
including what worked and what didn't, and how I fixed problems. It's
proven a handy reference.

In some recent threads about DTrace, people have asked how it compares
to SystemTap -- with some commenting that they haven't had time to study
either. I've been encouraged to post about my experiences, which is easy
to do from my notes. I could (and probably should) get some of these
into the SystemTap bug database, too.

What I'm sharing are various experiences of using SystemTap, including
times where I made mistakes because I didn't know what I was doing. I'll
begin with a narrative about tracing disk I/O, which connects together
various experiences. After that it gets a little discontiguous, clipped
from various notes. I'll try to keep this technical, positive, and
constructive. Perhaps this will help the SystemTap project itself, to
see what a user (with a strong Dynamic Tracing and kernel engineering
background) struggles with.

### Using Oracle Ksplice to Update Oracle Linux Systems Without Rebooting

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/ksplice-linux-518455.html>

Oracle Ksplice is an exciting new addition to the Oracle Linux Premier
Support subscription. Oracle Ksplice technology allows you to update
systems with new kernel security errata (CVE) without the need to
reboot, which enables you to remain current with OS vulnerability
patches while minimizing downtime. Oracle Ksplice actively applies
updates to the running kernel image, instead of making on-disk changes
that would take effect only after a subsequent reboot.

Another requirement for getting Oracle Ksplice updates is the use of the
Unbreakable Enterprise Kernel from Oracle. The lowest Oracle Linux
kernel version at the time of this writing is 2.6.32-100.28.9. This
kernel (and newer) can be installed on both Oracle Linux 5 and Oracle
Linux 6 distribution versions. Customers with Red Hat Enterprise Linux
(RHEL) 5 and 6 can do the simple migration to Oracle Linux and apply the
packages on their existing installation of RHEL. Oracle does not offer
Oracle Ksplice for Red Hat compatible kernels.

### How to Create a Customized Oracle Solaris 11 Image Using the Distribution Constructor

-   <http://www.oracle.com/technetwork/articles/servers-storage-admin/o11-087-sol11-dist-const-496819.html>

This article describes how to create customized Oracle Solaris 11 images
that contain customized software.

A brief overview of the reasons for creating a customized Oracle Solaris
11 image is provided. Relevant concepts are introduced, followed by a
real example of using the Distribution Constructor to create custom
(\"golden\") images. The example concludes with an illustration of
taking the created image and making it available for consumption by
systems as part of the provisioning process.
