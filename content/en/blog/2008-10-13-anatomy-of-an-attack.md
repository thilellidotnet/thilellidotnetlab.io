---
date: "2008-10-13T12:38:28Z"
tags: []
title: Anatomy Of An Attack
---

Well, although I didn\'t generally give credibility by speaking about
public FUD, I will just let you know about [really
great](http://www.cuddletech.com/blog/pivot/entry.php?id=972), and
[point by point explanations](http://blogs.zdnet.com/Murphy/?p=1268) on
the recent InfoWorld (and New York Times) publication from Paul Krill
[Is Sun Solaris on its
deathbed?](http://www.infoworld.com/d/networking/sun-solaris-its-deathbed-837)

As [Jim Grisanzio](http://www.opensolaris.org//viewProfile.jspa?id=863)
stated recently on the `advocacy-discuss` mailing list:

> \[\...\] More importantly, though, is that the original article not
> only fell flat but it was actually aggressively rejected by many in
> the open source community. That\'s an interesting shift out there. And
> a good one, too.

Well, I can\'t agree more with Jim on [his
points](http://blogs.sun.com/jimgris/entry/deathbed).

*Update \#1 (2008-10-28):* Don\'t forget to read the [interesting
inputs](http://www.c0d0s0.org/archives/4943-Is-the-Linux-community-afraid-of-Opensolaris.html)
from Joerg Moellenkamp.
