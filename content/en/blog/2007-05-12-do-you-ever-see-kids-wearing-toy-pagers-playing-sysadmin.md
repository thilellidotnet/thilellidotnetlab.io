---
date: "2007-05-12T14:38:51Z"
tags: []
title: Do You Ever See Kids Wearing Toy Pagers Playing Sysadmin?
---

Well, this little question was in fact extracted from a [recent blog
entry](http://valleywag.com/tech/silicon-valley-users-guide/how-do-i-get-my-sysadmin-to-do-anything-230469.php)
by Paul Boutin (which defines itself as former systems administrator),
and was the starting point for one of most interesting explanation from
Ben Rockwood (still him :)), entitled [Systems Administration as a
Career: A
Response](http://www.cuddletech.com/blog/pivot/entry.php?id=817). But if
I personally find its words so interesting to me it is just because\...
they can be mine!

Basically, I can have say the same thing. In the past, I was not *really
good* at school. Just enough to be able to go further. I got a personal
computer relatively late at home. And I found really, really late that I
was particularly interested by "Systems". In fact, it was late at High
School when I discovered both UNIX, and the feeling I clearly want to be
a System Administrator. When other students get development projects, I
asked for sysadmin oriented tasks, even if that was not planned in the
school\'s program. And I got one (building a NIS environment based on
Solaris servers deserving heterogeneous clients for the upcoming
centralized UNIX infrastructure at school\--using Solaris, HP-UX, and
RedHat Linux).

Since then, I am a really passionate SA, and OSS believer. Today, I have
more than six year playing this role, sometimes differently depending on
customers needs and always evolving technologies. I can honestly say
that I have permanently something new to learn, to try, to manage, or to
build. More, I found myself to be in a great place to work with a lots
of very nice\--and sometimes totally nuts\--guys, coming from very
different backgrounds such as database and network engineers, managers,
end-users, and pretty nice Internet IT-communities. So no, really, I
didn\'t want to change my "role" anytime soon.

Last, if you are a staff manager for a system administrator right now,
and think that its career *evolution* will obligatory go through project
or team management, I urge you to read (or think more closely about)
Ben\'s [points](http://www.cuddletech.com/blog/pivot/entry.php?id=817)
one more time. Really. You will see that there are a lot of things which
can be done in this area, as defined by `benr`.
