---
date: "2013-04-02T13:40:06Z"
tags:
- Press
title: Oracle have announced new systems based on the new SPARC T5 and M5 processors
---

Oracle have announced new systems based on the new T5 and M5 processors
(thank you Henkis, from [It\'s a UNIX
system!](http://sparcv9.blogspot.com/), for the following short resume):

1.  T5 16 cores @ 3.6GHz 8MB L3\$ 1-8 socket systems PCIe 3.0
2.  M5 6 cores @ 3.6GHz 48MB L3\$ 32 socket system PCIe 3.0

The T5 has doubled the number of S3 cores from the T4 and increased the
clock frequency to 3.6GHz.

The M5 processor is also based on the S3 core (rebranded M4) clocked at
3.6Ghz but is has 6 cores and 48MB L3\$. The M systems supports up to 32
M5 processor so a fully configured systems will have 192 cores and 1536
strands (hardware threads). The M5-32 have 32TB of memory in a single
system.

Example of a SPARC T5 configuration:

-   8 CPU x 16 cores x 8 compute threads = 1024 vCPU
-   4TB of RAM
-   8 x 600GB = 4.8TB (10KRPM SAS)
-   4 x 10GbE
-   8 RU (Rack Unit)
-   Support Oracle Solaris 10 and Solaris 11
-   Logical Domains (up to 128 by chassis), and Zones (8192 per OS
    instance)
-   Public list price: 626,000\$

Example of a SPARC M5 configuration:

![](http://www.oracle.com/ocom/groups/public/@ocom/documents/digitalasset/1920473.png)

-   32 CPU x 6 cores x 8 compute threads = 1536 vCPU
-   32TB of RAM
-   Support Oracle Solaris 10 and Solaris 11
-   Dynamic Domains (up to 4 by chassis), Logical Domains (up to 512 by
    chassis), and Zones (8192 per OS instance)
-   Public list price: *nothing public ;)*

All new systems both T5 and M5 supports LDOM (Oracle VM for SPARC), plus
the old Dynamic Domain technology for the M5. The M5 gets an ILOM as
Service Processor (as for T5 and the x86 server lines), exit the legacy
XSCF.

### Oracle Unveils SPARC Servers with the World's Fastest Microprocessor

-   <http://www.oracle.com/us/corporate/press/1923343>

New SPARC Servers Redefine the Economics of Enterprise Computing:
Deliver Extreme Performance and Value for Database and Enterprise
Applications, Trump the Competition on Multiple Business-Critical
Workloads.

### Oracle\'s new T5 Sparcs boost scalability in chip and chassis

-   <http://www.theregister.co.uk/2013/03/26/oracle_sparc_t5_m5_server/>

![](http://regmedia.co.uk/2013/03/25/sparc_t5_chip.jpg)

Oracle is launching its much-awaited Sparc T5 processors for entry and
midrange servers, along with Sparc M5 processors to effectively replace
the iron it currently resells from server and chip partner Fujitsu.

The T5-8 maxxes out at 128 cores, 1,024 threads, and 4TB of memory
\[\...\]

At the moment, Oracle is shipping only one box based on the Sparc M5,
with 32 sockets, called the Sparc M5-32 (obviously). Fully configured,
this big-iron box weighs in at 192 cores, 1,536 threads, and 32TB of
main memory. No one has as much memory in a single image today -- not
IBM, not Silicon Graphics, not HP, and not Fujitsu.

The Sparc M5-32 box puts Oracle/Sun back into big iron.

### The SPARC T5 Servers have landed

-   <https://blogs.oracle.com/orasysat/entry/t5>

Having announced the T5 servers doesn\'t make the T4s go away. It is not
a platform replacing technology, but a platform extending one! Oracle is
going to offer SPARC T4 and T5 servers side-by-side!

![](https://blogs.oracle.com/orasysat/resource/SPARCLaunch/SPARC_Portfolio-40.png)

### Oracle claims performance crown with SPARC processor

-   <http://www.itnews.com.au/News/337901,oracle-claims-performance-crown-with-sparc-processor.aspx>

Oracle this morning launched the SPARC T5 processor and servers based on
it which the company says are the fastest machines currently in the
world.

Ellison aims his first Oracle \'mainframe\' at Big Blue.

-   <http://www.theregister.co.uk/2013/03/26/oracle_ellison_m5_t5_launch/>

\"These machines deliver better integer performance than the IBM Power
series,\" proclaimed Ellison. \"The T5 microprocessor itself delivers
better integer performance than IBM\'s PowerPC chip. Now that is really
extraordinary, because IBM has had that lead for a very, very long time
for integer rate performance, but that lead now moves over from IBM
Power to Sparc T5.

\"A lot of people are surprised by this,\" continued Ellison. \"When
Oracle bought Sun, a lot of people thought the Sparc microprocessor was
a real laggard. There were a lot of people who believed that we would
never catch up. Well, we have done better than catch up. We caught up,
and then we passed the competition. We passed x86 and we passed IBM
Power.\"

### Announcing New SPARC Servers with the World\'s Fastest Microprocessor

-   <http://www.oracle.com/us/corporate/events/sparc2013/index.html>

Announcing New SPARC Servers: *Webcast replay*.

### Oracle Processor Core Factor Table

-   <http://www.oracle.com/us/corporate/contracts/processor-core-factor-table-070634.pdf>

The \"Oracle Processor Core Factor Table\" has been updated in order to
include SPARC T5 and M5: It\'s 0.5 for both.

### Oracle\'s SPARC T5-2, SPARC T5-4, SPARC T5-8, and SPARC T5-1B Server Architecture White Paper

-   <http://www.oracle.com/technetwork/server-storage/sun-sparc-enterprise/documentation/o13-024-sparc-t5-architecture-1920540.pdf>

### SPARC M5-32 Server Architecture Whitepaper

-   <http://www.oracle.com/technetwork/server-storage/sun-sparc-enterprise/documentation/o13-024-m5-32-architecture-1920556.pdf>

### Driving Up Price/Performance and Driving Out Cost with Oracle 's SPARC T5 Servers

-   <http://www.oracle.com/us/products/servers-storage/servers/sparc/oracle-sparc/sparc-t5-biz-wp-1924220.pdf>

### Performance & Best Practices

-   <https://blogs.oracle.com/BestPerf/>

1.  SPARC T5-8 Produces TPC-C Benchmark Single-System World Record
    Performance
2.  SPARC T5-8 Delivers SPECjEnterprise2010 Benchmark World Record
    Performance
3.  SPARC T5-2 Achieves SPECjbb2013 Benchmark World Record Result
4.  SPARC T5-8 Realizes SAP SD Two-Tier Benchmark World Record for 8
    Chip Systems
5.  SPARC T5 Systems Deliver SPEC CPU2006 Rate Benchmark Multiple World
    Records
6.  SPARC T5-2 Achieves JD Edwards EnterpriseOne Benchmark World Records
7.  SPARC T5-2 Scores Siebel CRM Benchmark World Record
8.  SPARC T5 Systems Produce Oracle TimesTen Benchmark World Record
9.  SPARC T5-8 Delivers Oracle OLAP World Record Performance
10. SPARC T5-2 Achieves ZFS File System Encryption Benchmark World
    Record
11. SPARC T5-2 Obtains Oracle Internet Directory Benchmark World Record
    Performance
12. SPARC T5-2 Scores Oracle FLEXCUBE Universal Banking Benchmark World
    Record Performance
13. SPARC T5-2 Performance Running Oracle Fusion Middleware SOA
14. SPARC T5-1B Performance Running Oracle Communications ASAP
15. SPARC M5-32 Produces SAP SD Two-Tier Benchmark World Record for SAP
    Enhancement Package 5 for SAP ERP 6.0

### Boom!

-   <https://blogs.oracle.com/solaris/entry/boom>

So that\'s quite a \"boom.\" With the launch of the new SPARC T5 and M5
series of servers, we\'ve set over a dozen new performance records, and
shown that back in 2010 Oracle did indeed establish a SPARC roadmap that
it could execute on.

![](https://blogs.oracle.com/solaris/resource/2013/sparc-t5-specs.jpg)

### Hot Chips 24: SPARC T5 Overview

-   <http://netmgt.blogspot.fr/2013/03/hot-chips-24-sparc-t5-overview.html>

Every year, the best of engineering talent comes together in academia
for Hot Chips conference, to present the best system designs. During Hot
Chips 24, Session 9 - the SPARC T5 was presented by Sebastian Turullols
and Ram Sivaramakrishnan from Oracle on Wednesday, August 29, 2012. This
processor was released 6 months later, by Oracle with their T5 systems
on Tuesday March 26, 2013.

### SPARC T5 System Performance for Encryption Microbenchmark

-   <https://blogs.oracle.com/BestPerf/entry/20130326_sparc_t5_ucrypto>

The cryptography benchmark suite was internally developed by Oracle to
measure the maximum throughput of in-memory, on-chip encryption
operations that a system can perform. Multiple threads are used to
achieve the maximum throughput. Systems powered by Oracle\'s SPARC T5
processor show outstanding performance on the tested encryption
operations, beating Intel processor based systems.
