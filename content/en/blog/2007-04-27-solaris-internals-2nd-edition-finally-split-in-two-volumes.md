---
date: "2007-04-27T17:56:43Z"
tags: []
title: Solaris Internals 2nd Edition, Finally Split in Two Volumes
---

One of my most *very awaited* book these days certainly is the
forthcoming [Solaris
Internals](http://www.solarisinternals.com/ "Solaris Internals book web site"),
2nd Edition \-- note: [The Complete
FreeBSD](http://www.lemis.com/~grog/Documentation/CFBSD/ "CFBSD from Greg Lehey's web page")
is one from my previous hot list ;). The content cover an incredible
list of various topics, especially updated with the new Solaris 10 and
OpenSolaris features, such as:

-   Virtual Memory
-   File systems, including ZFS
-   Zones
-   Resource Management
-   Process Rights Management
-   DTrace
-   MDB
-   Solaris Performance tools

Interestingly, this one will finally be available on two separated but
complementary books.

From this point, please follow these interesting blog entries from two
of the original book writers:

1.  Brendan Gregg: [Solaris Internals 2nd
    Edition](http://bdgregg.blogspot.com/2006/04/solaris-internals-2nd-edition-this.html "Solaris Internals 2nd Edition")
2.  Richard McDougall: [Performance, Observability, DTrace and
    MDB](http://blogs.sun.com/roller/page/rmc?entry=performance_observability_dtrace_and_mdb "Performance, Observability, DTrace and MDB")

Can\'t **wait** for these to be available, really!
