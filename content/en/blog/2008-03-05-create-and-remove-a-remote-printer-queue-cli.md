---
date: "2008-03-05T14:03:51Z"
tags:
- Utility
title: Create And Remove A Remote Printer Queue (CLI)
---
----------------------------------------------------------------------------
hostname: locname1: ready and waiting
no entries
</pre>
<p>That's it!</p>

You can easily create and remove a remote printer queue using the BSD
type spooler. You just have to fill the configuration file
`/tmp/lp.list` properly, i.e. provide the local printer name, the remote
LPD server, and the remote printer queue:

    # cat << EOF > /tmp/lp.list
    locname1 lpdserv1 remname1
    locname2 lpdserv2 remname2
    EOF

Then, just run the appropriate script depending of the desired behavior.
Follow, an example when removing the two queues:

    # cat << EOF > /tmp/lp.remove
    #!/usr/bin/env sh

    for lplocal in `awk '{print $1}' /tmp/lp.list`; do
      /usr/sbin/lpshut
      /usr/bin/cancel ${lplocal} -e 2> /dev/null
      /usr/sbin/lpadmin -x${lplocal}
      /usr/sbin/lpsched -v
      sleep 1
    done

    exit 0
    EOF
    # sh /tmp/lp.remove
    scheduler stopped
    scheduler is running
    scheduler stopped
    scheduler is running
    # lpstat -olocname1
    no system default destination
    lpstat: "locname1" not a request id or a destination

And now, the creation:

    # cat << EOF > /tmp/lp.create
    #!/usr/bin/env sh

    while read lp; do
      eval set -- `IFS=" "; printf '"%s" ' ${lp}`
      lplocal="$1"
      lpserver="$2"
      lpremote="$3"

      /usr/sbin/lpshut
      /usr/sbin/lpadmin -p${lplocal} -orm${lpserver} -orp${lpremote} \
       -mrmodel -v/dev/null -orc -ob3 -ocmrcmodel -osmrsmodel
      /usr/sbin/accept ${lplocal}
      /usr/bin/enable ${lplocal}
      /usr/sbin/lpsched -v
      sleep 1
    done < /tmp/lp.list

    exit 0
    EOF
    # sh /tmp/lp.create
    scheduler stopped
    destination "locname1" now accepting requests
    printer "locname1" now enabled
    scheduler is running
    scheduler stopped
    destination "locname2" now accepting requests
    printer "locname2" now enabled
    scheduler is running
    # lpstat -olocname1
    no system default destination
    printer queue for locname1
                             Windows LPD Server
                       Printer \\lpdserv1
    emname1
    Owner       Status         Jobname          Job-Id    Size   Pages  Priority
