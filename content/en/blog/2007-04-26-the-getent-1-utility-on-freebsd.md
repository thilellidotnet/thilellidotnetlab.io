---
date: "2007-04-26T21:00:10Z"
tags:
- Utility
title: The getent(1) Utility on FreeBSD!
---

Yes, you got it by now: the `getent(1)` utility finally reached the
current tree some weeks ago and has just been
[MFC\'ed](http://www.freebsd.org/cgi/cvsweb.cgi/src/usr.bin/getent/ "src/usr.bin/getent/")!
What a good news to have this very nice tool.

I was happy to be mentioned in the commit log as one of the original
demander, since `getent(1)` can already be found on other OSes such as
[Sun
Solaris](http://www.sun.com/software/solaris/ "Solaris Enterprise System")/[OpenSolaris](http://www.opensolaris.org/ "OpenSolaris"),
[NetBSD](http://www.netbsd.org/ "Of course it runs NetBSD") or
[GNU/Linux](http://kernel.org/ "The Linux Kernel Archives") and is
generally useful.
