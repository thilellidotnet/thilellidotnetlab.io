---
date: "2007-08-22T20:34:43Z"
tags:
- x86
title: SunSpectrum Enterprise Service Plan Supports Solaris 10 Across Non-Sun x64/x86
  Servers
---

You are a big corporate Sun customer? You are particularly interested in
running the Solaris operating system on various and heterogeneous
x64/x86 platforms including non-Sun servers (more and more hardware
providers are certifying their servers to run the Solaris 10 OS [these
days](http://www.sun.com/aboutsun/pr/2007-08/sunflash.20070816.1.xml))?

You are already aware that H-P can be a SPOC (Single Point Of Contact)
for both their hardware and the support for the Red Hat Enterprise Linux
system, and will enjoy the very same mechanism for the Solaris platform?

Well. It is now possible\... and a little more in fact: this quiet the
*opposite*. I mean, you can ask Sun to be the SPOC for running Solaris
on Sun and non-Sun systems. I feel strongly having Sun to be the very
first entry point is a *must*, in particular since they know really well
(today) what can be done on Solaris on x64/x86 platforms, seeing the
rise of work which is currently achieve through the [OpenSolaris
project](http://www.opensolaris.org/). Not only Solaris (and its open
source counterparts distributions) is becoming more and more relevant in
the x64/x86 AMD/Intel world, but they are leading innovations on these
platforms.

So, enjoy the [not-so-recent
announcement](http://www.sun.com/aboutsun/pr/2007-06/sunflash.20070619.2.xml)
about the new SunSpectrum Enterprise Service Plan for enterprise
customers.
