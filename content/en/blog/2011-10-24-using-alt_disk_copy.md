---
date: "2011-10-24T12:41:25Z"
tags:
- Patch
- Upgrade
title: Using alt_disk_copy
---
----------------------------------------------------------------------------
Path: /usr/lib/objrepos bos.rte.libc 5.3.0.43 COMMITTED libc Library
xlC.aix50.rte 6.0.0.13 COMMITTED C Set ++ Runtime for AIX 5.0 lslpp: Fileset
bos.adt.prof not installed. # oslevel -s 5300-04-03 # bosboot -a bosboot: Boot
image is 25199 512 byte blocks. # bootlist -m normal -o hdisk0 blv=hd5 hdisk0
blv=hd5 hdisk1 blv=hd5 hdisk1 blv=hd5 # lspv hdisk0 00ce4b7ac3e70e6e rootvg
active hdisk1 00ce4b7ac3e71355 rootvg active # lsvg -p rootvg rootvg: PV_NAME
PV STATE TOTAL PPs FREE PPs FREE DISTRIBUTION hdisk0 active 511 38
00..00..00..00..38 hdisk1 active 511 38 00..00..00..00..38 # lsvg -l rootvg
rootvg: LV NAME TYPE LPs PPs PVs LV STATE MOUNT POINT hd5 boot 1 2 2
closed/syncd N/A hd6 paging 64 128 2 open/syncd N/A hd8 jfs2log 1 2 2
open/syncd N/A hd4 jfs2 1 2 2 open/syncd / hd2 jfs2 20 40 2 open/syncd /usr
hd9var jfs2 12 24 2 open/syncd /var hd3 jfs2 16 32 2 open/syncd /tmp hd1 jfs2
16 32 2 open/syncd /home hd10opt jfs2 16 32 2 open/syncd /opt cdc jfs2 2 4 2
open/syncd /tools/list/cdc tivoli jfs2 2 4 2 open/syncd /tools/list/tivoli
lvcontrolm jfs2 80 160 2 open/syncd /apps/controlm lvsysload jfs2 8 16 2
open/syncd /tools/list/sysload sinagioslv jfs2 2 4 2 open/syncd
/tools/list/sinagios ctmem640lv jfs2 128 256 2 open/syncd
/apps/controlm/ctmem640 ctmsv640lv jfs2 96 192 2 open/syncd
/apps/controlm/ctmsv640 samlv jfs2 8 16 2 open/syncd /tools/list/sam #
unmirrorvg rootvg hdisk1 0516-1246 rmlvcopy: If hd5 is the boot logical volume,
please run 'chpv -c ' as root user to clear the boot record and avoid a
potential boot off an old boot image that may reside on the disk from which
this logical volume is moved/removed. 0516-1132 unmirrorvg: Quorum requirement
turned on, reboot system for this to take effect for rootvg. 0516-1144
unmirrorvg: rootvg successfully unmirrored, user should perform bosboot of
system to reinitialize boot records. Then, user must modify bootlist to just
include: hdisk0. # chpv -c hdisk1 # bootlist -m normal -o hdisk0 hdisk0 blv=hd5
hdisk0 blv=hd5 # reducevg rootvg hdisk1 # lspv hdisk0 00ce4b7ac3e70e6e rootvg
active hdisk1 00ce4b7ac3e71355 None # mount
surville:/applis/list/nim/LPP/AIX530/lpp_aix530TL07SP01 /mnt # alt_disk_copy -d
hdisk1 -b update_all -l /mnt/installp/ppc Calling mkszfile to create new
/image.data file. Checking disk sizes. Creating cloned rootvg volume group and
associated logical volumes. Creating logical volume alt_hd5. Creating logical
volume alt_hd6. Creating logical volume alt_hd8. Creating logical volume
alt_hd4. Creating logical volume alt_hd2. Creating logical volume alt_hd9var.
Creating logical volume alt_hd3. Creating logical volume alt_hd1. Creating
logical volume alt_hd10opt. Creating logical volume alt_cdc. Creating logical
volume alt_tivoli. Creating logical volume alt_lvcontrolm. Creating logical
volume alt_lvsysload. Creating logical volume alt_sinagioslv. Creating logical
volume alt_ctmem640lv. Creating logical volume alt_ctmsv640lv. Creating logical
volume alt_samlv. Creating /alt_inst/ file system. Creating
/alt_inst/apps/controlm file system. Creating /alt_inst/apps/controlm/ctmem640
file system. Creating /alt_inst/apps/controlm/ctmsv640 file system. Creating
/alt_inst/home file system. Creating /alt_inst/opt file system. Creating
/alt_inst/tmp file system. Creating /alt_inst/tools/list/cdc file system.
Creating /alt_inst/tools/list/sam file system. Creating
/alt_inst/tools/list/sinagios file system. Creating
/alt_inst/tools/list/sysload file system. Creating /alt_inst/tools/list/tivoli
file system. Creating /alt_inst/usr file system. Creating /alt_inst/var file
system. Generating a list of files for backup and restore into the alternate
file system... Backing-up the rootvg files and restoring them to the alternate
file system... cannot access ./apps/controlm/ctmsrv/ctm/tmp/O23aUO: No such
file or directory 0505-148 alt_disk_install: WARNING: an error occurred during
backup. Installing optional filesets or updates into altinst_rootvg...
install_all_updates: Initializing system parameters. install_all_updates: Log
file is /var/adm/ras/install_all_updates.log install_all_updates: Checking for
updated install utilities on media. install_all_updates: Updating install
utilities to latest level on media.
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Verifying selections...done Verifying requisites...done Results... [...]
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Installation Summary -------------------- Name Level Part Event Result
-------------------------------------------------------------------------------
xlsmp.aix52.rte 1.7.0.0 USR APPLY SUCCESS [...] bos.sysmgt.trace 5.3.7.0 ROOT
APPLY SUCCESS installp: * * * A T T E N T I O N ! ! ! Software changes
processed during this session require any diskless/dataless clients to which
this SPOT is currently allocated to be rebooted. install_all_updates: Log file
is /var/adm/ras/install_all_updates.log install_all_updates: Result = SUCCESS
Modifying ODM on cloned disk. Building boot image on cloned disk. forced
unmount of /alt_inst/var forced unmount of /alt_inst/usr forced unmount of
/alt_inst/tools/list/tivoli forced unmount of /alt_inst/tools/list/sysload
forced unmount of /alt_inst/tools/list/sinagios forced unmount of
/alt_inst/tools/list/sam forced unmount of /alt_inst/tools/list/cdc forced
unmount of /alt_inst/tmp forced unmount of /alt_inst/opt forced unmount of
/alt_inst/home forced unmount of /alt_inst/apps/controlm/ctmsv640 forced
unmount of /alt_inst/apps/controlm/ctmem640 forced unmount of
/alt_inst/apps/controlm forced unmount of /alt_inst forced unmount of /alt_inst
Changing logical volume names in volume group descriptor area. Fixing LV
control blocks... Fixing file system superblocks... Bootlist is set to the boot
disk: hdisk1 # lspv hdisk0 00ce4b7ac3e70e6e rootvg active hdisk1
00ce4b7ac3e71355 altinst_rootvg # bootlist -m normal -o hdisk1 blv=hd5 hdisk1
blv=hd5 # alt_rootvg_op -W -d hdisk1 Waking up altinst_rootvg volume group ...
# lspv hdisk0 00ce4b7ac3e70e6e rootvg active hdisk1 00ce4b7ac3e71355
altinst_rootvg active # lsvg -l altinst_rootvg altinst_rootvg: LV NAME TYPE LPs
PPs PVs LV STATE MOUNT POINT alt_hd5 boot 1 1 1 closed/syncd N/A alt_hd6 paging
64 64 1 closed/syncd N/A alt_hd8 jfs2log 1 1 1 open/syncd N/A alt_hd4 jfs2 1 1
1 open/syncd /alt_inst alt_hd2 jfs2 32 32 1 open/syncd /alt_inst/usr alt_hd9var
jfs2 12 12 1 open/syncd /alt_inst/var alt_hd3 jfs2 16 16 1 open/syncd
/alt_inst/tmp alt_hd1 jfs2 16 16 1 open/syncd /alt_inst/home alt_hd10opt jfs2
16 16 1 open/syncd /alt_inst/opt alt_cdc jfs2 2 2 1 open/syncd
/alt_inst/tools/list/cdc alt_tivoli jfs2 2 2 1 open/syncd
/alt_inst/tools/list/tivoli alt_lvcontrolm jfs2 80 80 1 open/syncd
/alt_inst/apps/controlm alt_lvsysload jfs2 8 8 1 open/syncd
/alt_inst/tools/list/sysload alt_sinagioslv jfs2 2 2 1 open/syncd
/alt_inst/tools/list/sinagios alt_ctmem640lv jfs2 128 128 1 open/syncd
/alt_inst/apps/controlm/ctmem640 alt_ctmsv640lv jfs2 96 96 1 open/syncd
/alt_inst/apps/controlm/ctmsv640 alt_samlv jfs2 8 8 1 open/syncd
/alt_inst/tools/list/sam # nawk ' \ $1 ~ /^xlC.aix50.rte$/ &amp;&amp; $NF ~
/SUCCESS/ || \ $1 ~ /^bos.rte.libc$/ &amp;&amp; $NF ~ /SUCCESS/ {print $0}' \
/alt_inst/var/adm/ras/install_all_updates.log xlC.aix50.rte 9.0.0.1 USR APPLY
SUCCESS bos.rte.libc 5.3.7.1 USR APPLY SUCCESS # alt_rootvg_op -C -l
/mnt/installp/ppc -w bos.adt.prof Installing optional filesets or updates into
altinst_rootvg...
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Verifying selections...done Verifying requisites...Verifying requisites...done
Results... SUCCESSES --------- Filesets listed in this section passed
----------------- bos.adt.prof 5.3.0.0 # Base Profiling Support bos.adt.prof
5.3.7.1 # Base Profiling Support &lt;&lt; End of Success Section &gt;&gt;
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Verifying build dates...done FILESET STATISTICS ------------------ 2 Selected
to be installed, of which: 2 Passed pre-installation verification ---- 2 Total
to be installed
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
installp: APPLYING software for: bos.adt.prof 5.3.0.0 [...]
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Installation Summary -------------------- Name Level Part Event Result
-------------------------------------------------------------------------------
bos.adt.prof 5.3.0.0 USR APPLY SUCCESS bos.adt.prof 5.3.7.1 USR APPLY SUCCESS
[...] # umount /mnt # mount surville:/applis/list/nim/DISTRIB /mnt #
alt_rootvg_op -C -l /mnt/vacpp.90.aix52-53TL5.jan2010 -w xlC.aix50.rte
Installing optional filesets or updates into altinst_rootvg...
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Verifying selections...done Verifying requisites...done Results... [...]
+-----------------------------------------------------------------------------+
+-----------------------------------------------------------------------------+
Pre-installation Failure/Warning Summary
---------------------------------------- Name Level Pre-installation
-------------------------------------------------------------------------------
xlC.aix50.rte 9.0.0.0 Already superseded by 9.0.0.1 Installation Summary
-------------------- Name Level Part Event Result
-------------------------------------------------------------------------------
xlC.rte 9.0.0.10 USR APPLY SUCCESS xlC.aix50.rte 9.0.0.10 USR APPLY SUCCESS
xlC.aix50.rte 9.0.0.10 USR COMMIT SUCCESS xlC.rte 9.0.0.10 USR COMMIT SUCCESS #
umount /mnt # alt_rootvg_op -S -t hdisk1 Putting volume group altinst_rootvg to
sleep ... Building boot image on cloned disk. forced unmount of /alt_inst/var
forced unmount of /alt_inst/usr forced unmount of /alt_inst/tools/list/tivoli
forced unmount of /alt_inst/tools/list/sysload forced unmount of
/alt_inst/tools/list/sinagios forced unmount of /alt_inst/tools/list/sam forced
unmount of /alt_inst/tools/list/cdc forced unmount of /alt_inst/tmp forced
unmount of /alt_inst/opt forced unmount of /alt_inst/home forced unmount of
/alt_inst/apps/controlm/ctmsv640 forced unmount of
/alt_inst/apps/controlm/ctmem640 forced unmount of /alt_inst/apps/controlm
forced unmount of /alt_inst forced unmount of /alt_inst Fixing LV control
blocks... Fixing file system superblocks... # shutdown -Fr # oslevel -s
5300-07-01-0748 # lslpp -l xlC.aix50.rte bos.adt.prof bos.rte.libc Fileset
Level State Description
----------------------------------------------------------------------------
2007 XL C++ Runtime for AIX IZ02378 Symptom Text: ----------------------------
Fileset xlC.aix50.rte:9.0.0.1 is applied on the system. Fileset
xlC.msg.Ja_JP.rte is not applied on the system. Fileset xlC.msg.en_US.rte is
not applied on the system. Fileset xlC.msg.ja_JP.rte is not applied on the
system. Fileset xlC.rte:9.0.0.1 is applied on the system. All filesets for
IZ02378 were found. # lspv hdisk0 00ce4b7ac3e70e6e old_rootvg hdisk1
00ce4b7ac3e71355 rootvg active
####################################################################################
####################################################################################
####################################################################################
# alt_rootvg_op -X old_rootvg # lspv hdisk0 00ce4b7ac3e70e6e None hdisk1
00ce4b7ac3e71355 rootvg active # extendvg -f rootvg hdisk0 # mirrorvg -m rootvg
hdisk0 0516-1804 chvg: The quorum change takes effect immediately. 0516-1126
mirrorvg: rootvg successfully mirrored, user should perform bosboot of system
to initialize boot records. Then, user must modify bootlist to include: hdisk0
hdisk1. # bosboot -a bosboot: Boot image is 43349 512 byte blocks. # bootlist
-m normal -o hdisk0 hdisk1 hdisk0 blv=hd5 hdisk0 blv=hd5 hdisk1 blv=hd5 hdisk1
blv=hd5 # lspv hdisk0 00ce4b7ac3e70e6e rootvg active hdisk1 00ce4b7ac3e71355
rootvg active # lsvg -p rootvg rootvg: PV_NAME PV STATE TOTAL PPs FREE PPs FREE
DISTRIBUTION hdisk1 active 511 26 22..04..00..00..00 hdisk0 active 511 26
22..04..00..00..00 # lsvg -l rootvg rootvg: LV NAME TYPE LPs PPs PVs LV STATE
MOUNT POINT hd5 boot 1 2 2 closed/syncd N/A hd6 paging 64 128 2 open/syncd N/A
hd8 jfs2log 1 2 2 open/syncd N/A hd4 jfs2 1 2 2 open/syncd / hd2 jfs2 32 64 2
open/syncd /usr hd9var jfs2 12 24 2 open/syncd /var hd3 jfs2 16 32 2 open/syncd
/tmp hd1 jfs2 16 32 2 open/syncd /home hd10opt jfs2 16 32 2 open/syncd /opt cdc
jfs2 2 4 2 open/syncd /tools/list/cdc tivoli jfs2 2 4 2 open/syncd
/tools/list/tivoli lvcontrolm jfs2 80 160 2 open/syncd /apps/controlm lvsysload
jfs2 8 16 2 open/syncd /tools/list/sysload sinagioslv jfs2 2 4 2 open/syncd
/tools/list/sinagios ctmem640lv jfs2 128 256 2 open/syncd
/apps/controlm/ctmem640 ctmsv640lv jfs2 96 192 2 open/syncd
/apps/controlm/ctmsv640 samlv jfs2 8 16 2 open/syncd /tools/list/sam

PI, voici le mode opératoire qui a été utilisé pour cette montée de TL
et installation des packages pré-requis entièrement réalisé sur
l\'alternate rootvg afin de ne pas rencontrer le souci de \"l\'œuf et de
la poule\", et de garder un retour arrière simplifié. Dans ce modop, on
peut soit utiliser un troisième disque disponible, soit casser
temporairement le miroir système en place si jamais\--ce qui a été fait
ici. Peut-être peut-on tenter une manipulation similaire afin de monter
la version d\'HDLM qui nous embête tant en ce moment ?\... A tester :)
Note : Une autre possibilité est le recours au multibos plutôt qu\'au
alt\_disk\_copy (non-détaillé ici, mais je peux produire un modop
\"similaire\" si besoin). /\* \*
\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*
\* Prereqs for Contol-M Server and EM 6.4.0: \* xlC.aix50.rte of version
\>= 9.0.0.3 \* bos.adt.prof of version \> 5.3.0.62 \* bos.rte.libc of
version \> 5.3.0.63 \*
\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*
\*/ \# mount surville:/applis/list/nim/MKSYSB /mnt \# mksysb -i
/mnt/\`uname -n\`/\`uname -n\`.\`oslevel -s\`.mksysb Creating
information file (/image.data) for rootvg. Creating list of files to
back up .. Backing up 78532
files\...\...\...\...\...\...\...\...\...\... 23817 of 78532 files
backed up (30%)\...\...\...\...\...\...\...\..... 78532 of 78532 files
backed up (100%) 0512-038 mksysb: Backup Completed Successfully. \#
umount /mnt \# lslpp -cl \*.alt\_disk\_install.\* \| nawk -F\\: \'\$2 \~
/alt\_disk\_install/ {print \$2}\' bos.alt\_disk\_install.boot\_images
bos.alt\_disk\_install.rte \# lslpp -l xlC.aix50.rte bos.adt.prof
bos.rte.libc Fileset Level State Description Pre-installation
Verification\... Summaries: Pre-installation Verification\...
pre-installation verification and will be installed. Selected Filesets
BUILDDATE Verification \... Installing Software\... Summaries:
Pre-installation Verification\... Summaries: Failure/Warning Path:
/usr/lib/objrepos bos.adt.prof 5.3.7.1 COMMITTED Base Profiling Support
bos.rte.libc 5.3.7.1 COMMITTED libc Library xlC.aix50.rte 9.0.0.10
COMMITTED XL C/C++ Runtime for AIX 5.2 \# instfix -ik IZ02378 -va
IZ02378 Abstract: August
