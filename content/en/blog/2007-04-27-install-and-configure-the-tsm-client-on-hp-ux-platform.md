---
date: "2007-04-27T21:22:26Z"
tags:
- TSM
title: Install and Configure the TSM Client on HP-UX Platform
---

1.  *beastie* is the hostname of the local system
2.  *rafal* is the hostname of the repository system hosting the
    necessary packages in the depot
3.  *moliere* is the hostname of the TSM backup server

Be sure to obtain the `sam(1m)` display:

    # DISPLAY=beastie:0
    # export DISPLAY

Check if the os level and product version agreed on the same
specifications, and that there is sufficient disk space to install the
TSM client components:

    # uname -a
    # model
    # bdf /opt
    # vgdisplay vg00
    # swlist | grep OnLineJFS

Launch the System Administration Manager (see the `sam(1)` manual page
for more information) and follow these steps:

    # sam&
    /*
     * Software Management
     *  Install Software to Local Host
     *   Source Depot Type: Network Directory/CDROM
     *   Source Host Name... rafal
     *   Source Depot Path... /var/spool/sw/1100
     *    select "TIVsm   5.2.2   Tivoli Storage Manager Client"
     */

Configure the new tool:

    # cd /opt/tivoli/tsm/client/ba/bin
    # cp dsm.sys.smp dsm.sys
    # cp dsm.opt.smp dsm.opt
    # nslookup moliere   /* Check that the backup server is available. */
    # vi dsm.opt
    # vi dsm.sys         /* Modify the `nodename' accordingly. */
    # vi inclexcl.excl   /* Adapt the content following the recommendation from
                            the storage/backup team. */
    # dsmc q ses         /* Tune the TSM Server Connection Information following
                            the recommendation from the storage/backup team. */

Launch the TSM scheduler at run-level \#3:

    # cat << EOF > /sbin/init.d/tsmsched
    #!/bin/sh 

    killproc() {            # kill the named process(es)
            pid=`/usr/bin/ps -ef|
                 /usr/bin/grep '[d]smc sched' |
                 awk '{ print $2 }'`
            if [ "$pid" != "" ];
            then
                    kill $pid
                    sleep 5
                    kill -0 $pid
                    [ $? -ne 0 ] && kill -9 $pid
            fi
    }

    startproc() {
            if [ -f /usr/bin/dsmc ]; then
                    (/usr/bin/nohup /usr/bin/dsmc sched &) >/dev/null 2>&1
                    echo  "dsmc
    " 
            fi
    }

    case $1 in
    'start_msg')
            echo  "starting TSM scheduler:" 
            ;;
    'start')
            startproc
            ;;
    'stop_msg')
            echo  "stopping TSM scheduler:" 
            ;;
    'stop')
            killproc
            ;;
    'restart')
            echo  "restarting TSM scheduler:" 
            killproc
            startproc
            ;;
    *)
            echo "usage: `basename $0` {start|stop}"
            ;;
    esac
    EOF
    # chmod 744 /sbin/init.d/tsmsched
    # ln -s /sbin/init.d/tsmsched /sbin/rc3.d/S300tsmsched
    # /sbin/rc3.d/S300tsmsched start
