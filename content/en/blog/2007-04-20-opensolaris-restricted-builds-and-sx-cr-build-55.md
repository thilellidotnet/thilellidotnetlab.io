---
date: "2007-04-20T20:41:35Z"
tags: []
title: OpenSolaris Restricted Builds, and SX:CR build 55
---

As very, very well explained by [Stephen
Hahn](http://blogs.sun.com/sch/entry/opensolaris_restricted_builds_through_the "Restricted builds through the ages")
recently, restricting operating system\'s development specific builds
isn\'t new. In fact, it is more than desirable and as he said on his
blog this can serve all of us (community *users* and community
*makers*):

> Certainly current distributions (or emerging ones) might consider
> using the stabilized build 55\--however we got here\--as an opportune
> version to base their next release upon.

Can\'t wait for the SX:CR build 55 to update my Solaris 11/06 systems!
