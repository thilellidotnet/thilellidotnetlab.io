---
date: "2007-04-28T09:55:01Z"
tags:
- TSM
- Service
- Scheduler
title: How to Add a New "tsmsched" Service on AIX 5L
---

Create the tsmsched service:

    # mkssys -p /usr/bin/dsmc \   /* Absolute path to the subsystem executable
                                     program. */
             -s tsmsched \        /* Name that uniquely identifies the subsystem. */
             -u 0 \               /* User id for the subsystem. */
             -a sched \           /* Arguments to be passed to the command. */
             -e /dev/console \    /* Where the subsystem standard error data is
                                     placed. */
             -i /dev/console \    /* Where the subsys. standard input is routed. */
             -o /dev/console \    /* Where the subsys. standard output is placed. */
             -R \                 /* Subsystem is restarted if the subsystem stops
                                     abnormally. */
             -Q \                 /* Multiple instances of the subsystem are not
                                     allowed to run at the same time. */
             -S \                 /* Subsystem uses the signals communication
                                     method. */
             -f 9 \               /* Signal sent to the subsystem when a forced
                                     stop of the subsystem is requested. */
             -n 15 \              /* Signal sent to the subsystem when a normal
                                     stop of the subsystem is requested. */
             -E 20 \              /* Execution priority of the subsystem. */
             -G tsm \             /* Subsystem belongs to the group specified. */
             -d \                 /* Inactive subsystems are displayed when the
                                     lssrc -a command request is made. */
             -w 20                /* Time, in seconds, allowed to elapse between a
                                     stop cancel (SIGTERM) signal and a subsequent
                                     SIGKILL signal. */

Check the service\'s configuration:

    # lssrc -S -s tsmsched
    #subsysname:synonym:cmdargs:path:uid:auditid:standin:standout:standerr:action:multi:contact:svrkey:svrmtype:\
     priority:signorm:sigforce:display:waittime:grpname:
    tsmsched::sched:/usr/bin/dsmc:0:0:/dev/console:/dev/console:/dev/console:-R:-Q:-S:0:0:\
     20:15:9:-d:20:tsm:

Or directly querying the ODM internal database:

    # odmget -q subsysname=tsmsched SRCsubsys

    SRCsubsys:
            subsysname = "tsmsched"
            synonym = ""
            cmdargs = "sched"
            path = "/usr/bin/dsmc"
            uid = 0
            auditid = 0
            standin = "/dev/console"
            standout = "/dev/console"
            standerr = "/dev/console"
            action = 1
            multi = 0
            contact = 2
            svrkey = 0
            svrmtype = 0
            priority = 20
            signorm = 15
            sigforce = 9
            display = 1
            waittime = 20
            grpname = "tsm"

Start the new service:

    # startsrc -s tsmsched

Launch the TSM scheduler at run-level \#2:

    # cat << EOF > /etc/rc.d/rc2.d/Stsmsched
    #!/usr/bin/env ksh
    ###############################################################
    # name: {K|S}tsmsched
    # purpose: script that will start or stop the tsmsched service.
    ###############################################################

    case "$1" in
    start)
      startsrc -g tsm
      ;;
    stop)
      stopsrc -g tsm
      ;;
    *)
      echo "Usage: $0 {start|stop}"
      exit 1
    esac

    exit 0
    EOF
    # ln /etc/rc.d/rc2.d/Stsmsched /etc/rc.d/rc2.d/Ktsmsched
    # chmod 744 /etc/rc.d/rc2.d/?tsmsched
