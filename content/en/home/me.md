---
title: "Me"
image: "justme_low.jpg"
---

### Bio

Multi-platform UNIX systems administrator and consultant in mutualized
and virtualized environments.  Architecture and expertise building
strong UNIX/Linux experience in large enterprises such as banking and
financial services, IT services, Telecommunication and multimedia
companies.  [Oracle ACE] Alumnus, Oracle Certified Associate ([OCA]),
and Professional ([OCP]) in [Solaris] technology realm.

I currently work for [CA-GIP] in Paris, France.  My resume is available
as [ODT], and [PDF] formats--in French.

### Articles

#### Interview published in the Oracle Magazine

I had the great opportunity to be profiled for the *Peer-to-Peer* colum
of the [Oracle Magazine], which is an international publication
published by Oracle on a monthly basis.

Although the interview is now available [online], it has also been
truncated because of some space concerns, and other things related to
the format of the publication.  For those who are interested, here is
the [complete interview].

#### GLMF #106

Participation in the writing of an article on OpenSolaris published in
the GNU/Linux Magazine France, with [Yves Mettier]: brief history,
major distributions, use with a concrete case, and evolution to come.
Access to the magazine is [available] for reference purpose.

#### The Oracle ACE Program newsletter

Some blog posts were advertised in the Oracle ACE Watch section of the
following newsletters:

- [September 2011] issue
- [October 2011] issue
- [Mars 2012] issue

### Random FreeBSD stuff (old)

- FreeBSD [Port] I was the maintainer
- FreeBSD [PR] I've sent


[Oracle ACE]: http://www.oracle.com/technetwork/community/oracle-ace-faq-100746.html
[OCA]: /home/OCA_OSolaris11SysAdmin_eCertificate.pdf "Oracle Certified Associate, Oracle Solaris 11 System Administrator"
[OCP]: /home/OCP_OSolaris11SysAdmin_eCertificate.pdf "Oracle Certified Professional, Oracle Solaris 11 System Administrator"
[Solaris]: https://www.oracle.com/solaris/
[CA-GIP]: https://www.credit-agricole.com/marques-et-metiers/toutes-nos-marques/credit-agricole-group-infrastructure-platform "Crédit Agricole Group Infrastructure Platform"
[ODT]: /home/cv.odt "CV: Open Document Text"
[PDF]: /home/cv.pdf "CV: Portable Document Format"
[Yves Mettier]: http://ymettier.free.fr/
[available]: https://boutique.ed-diamond.com/numeros-deja-parus/97-lm106.html
[Oracle Magazine]: https://blogs.oracle.com/oraclemagazine/
[online]: https://blogs.oracle.com/oraclemagazine/in-with-the-new
[complete interview]: /blog/2011-08-15-interview-published-in-the-oracle-magazine/
[September 2011]: http://www.oracle.com/technetwork/community/oracle-ace/ace-newsletter-sept2011-522154.pdf
[October 2011]: http://www.oracle.com/technetwork/community/oracle-ace/ace-newsletter-oct2011-522155.pdf
[Mars 2012]: http://www.oracle.com/technetwork/community/oracle-ace/ace-newsletter-mar2012-1569461.pdf
[Port]: https://www.freshports.org/www/nanoblogger/ "Source package system"
[PR]: https://bugs.freebsd.org/bugzilla/buglist.cgi?content=thilelli.net "Problem Reports"

