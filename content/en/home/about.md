---
title: "About"
image: "website_transparent.png"
weight: 8
---

### Why choose this domain name?

As for a lot of things in this world, there is no real reason behind it.
It must be unique, quickly identifiable and... have a hidden meaning
(like all Good Names(TM)).

So, after some time trying to figure out which may be a good name for
this place, my choice went for a very uncommon one: **thilelli**.  This
name, also sometimes spelled tilelli, is in fact a (female) first name
with Berber's origin, which means Freedom.  What a hidden meaning for an
open source based project, isn't it?

### Who own it?

The short answer is basically... me.  A more longer answer is that it
was registered at [gandi.net].  More information  related can be found
at whatever [WhoIs] you prefer.

### Why is this site in English?

Because English is so common in Information Technology (the main purpose
of this blog), I thought it may be necessary to write in this language
in order to be more widely accessible through a lot of readers.
However, since I am a French guy and not a native English speaker, any
spelling/grammar mistakes are not to be excluded.  Feel free to
proofread my pages and email me, if any.  Thank you!

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License].


[gandi.net]: https://www.gandi.net/
[WhoIs]: https://whois.gandi.net/en/results?search=thilelli.net
[Creative Commons Attribution-ShareAlike 4.0 International License]: http://creativecommons.org/licenses/by-sa/4.0/

