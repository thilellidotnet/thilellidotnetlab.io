---
title: "Contact"
---

If you care, the **best way** to contact me is certainly by email.
Although this an asynchronous and informal method, I really tend to
prefer this way since it best corresponds to my needs (at least for a
first contact).  Generally speaking, I am very busy.  I am busy with
real life, and I am busy working as an IT system engineer.  I rarely
answer GSM mobile phone calls directly, especially with an unknown
caller phone number.

Take into account that I am attentive to the format of email I receive,
too.  Please consider reading [Using Internet mail], written by Greg
Lehey, to have an idea of what I mean here (although a little bit
outdated, it is mostly always applicable).  If you are always interested
to contact me, you can [email me] right now.


[Using Internet mail]: http://www.lemis.com/grog/email/email.php
[email me]: mailto:julien.gabel@thilelli.net

